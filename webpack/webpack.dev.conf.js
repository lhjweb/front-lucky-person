const path = require('path')
const merge = require('webpack-merge')
const baseWebpackConfig = require('./webpack.base.conf.js')
const CaseSensitivePathsPlugin = require('case-sensitive-paths-webpack-plugin')
// const SpeedMeasurePlugin = require('speed-measure-webpack-plugin')
// const smp = new SpeedMeasurePlugin()
const webpack = require('webpack')
const PORT = 7111
function resolve(dir) {
  return path.join(__dirname, '..', dir)
}

const finalConfig = merge(baseWebpackConfig, {
  mode: 'development',
  // devtool: 'eval-source-map',
  devtool: 'cheap-module-eval-source-map',
  plugins: [
    new CaseSensitivePathsPlugin(),
    new webpack.HotModuleReplacementPlugin()
  ],
  devServer: { // https://webpack.docschina.org/configuration/dev-server/#devserver
    port: PORT,
    contentBase: path.join(__dirname, '../src'),
    compress: true,
    // clientLogLevel: 'none',
    // disableHostCheck: true, // 此选项绕过主机检查。不建议这样做，因为不检查主机的应用程序容易受到 DNS 重新连接攻击
    overlay: { // 浏览器中显示警告和错误：
      errors: true
      // warnings: true
    },
    historyApiFallback: true, // 任意的 404 响应都可能需要被替代为 index.html
    hot: true,
    // hotOnly: true, // 在没有
    inline: true,  // 默认情况下，应用程序启用内联模式(inline mode), 构建消息将会出现在浏览器控制台。
    noInfo: true, // dev-server 隐藏 webpack bundle 信息之类的消息
    open: false,
    proxy: {}
  }
})

// module.exports = smp.wrap(finalConfig)
module.exports = finalConfig

console.log(`启动地址: localhost:${PORT}`)
