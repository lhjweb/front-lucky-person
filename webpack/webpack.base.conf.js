const path = require('path')
const webpack = require('webpack')
const HtmlWebpackPlugin = require('html-webpack-plugin')
const MiniCssExtractPlugin = require('mini-css-extract-plugin')
const ProgressBarPlugin = require('progress-bar-webpack-plugin')
// const FirendlyErrorePlugin = require('friendly-errors-webpack-plugin')
const HappyPack = require('happypack')
const os = require('os')
const happyThreadPool = HappyPack.ThreadPool({ size: os.cpus().length })

const argv = require('yargs-parser')(process.argv.slice(2))
const mode = argv.mode || 'development'
const devMode = mode === 'development'

const APP_PATH = path.resolve(__dirname, '../src')
const DIST_PATH = path.resolve(__dirname, '../dist')

function resolve(dir) {
  return path.join(__dirname, '..', dir)
}

// Define the class
class FilterPlugin {
  constructor(options) {
    this.options = options
  }

  apply(compiler) {
    compiler.hooks.afterEmit.tap(
      'FilterPlugin',
      (compilation) => {
        compilation.warnings = (compilation.warnings).filter(
          warning => !this.options.filter.test(warning.message)
        )
      }
    )
  }
}

module.exports = {
  entry: [
    '@babel/polyfill',
    `${APP_PATH}/index.js`
  ],
  output: {
    filename: 'js/[name].[hash].js',
    path: DIST_PATH,
    publicPath: '/'
  },
  stats: {
    entrypoints: false,
    children: false
    // warningsFilter: /Entrypoint mini-css-extract-plugin = */
  },
  module: {
    rules: [{
      test: /\.js[x]?$/,
      loaders: ['happypack/loader?id=babel'],
      include: resolve('src'),
      exclude: /node_modules/
    },
    {
      test: /\.less$/,
      use: [{
        loader: devMode ? 'style-loader' : MiniCssExtractPlugin.loader
      },
      {
        loader: 'css-loader'
      },
      {
        loader: 'postcss-loader',
        options: {
          plugins: [
            require('autoprefixer')
          ]
        }
      }, {
        loader: 'less-loader',
        options: {
          // modifyVars: {
          //   'primary-color': '#739153',
          //   'link-color': '#739153',
          //   'border-radius-base': '3px',
          //   'font-size-base': '12px'
          // },
          javascriptEnabled: true
        }
      }
      ]
    },
    {
      test: /\.scss$/,
      use: [{
        loader: devMode ? 'style-loader' : MiniCssExtractPlugin.loader
      },
      {
        loader: 'css-loader',
        options: {
          modules: true,
          localIdentName: '[path][name]_[local]--[hash:base64:5]'
        }
      },
      {
        loader: 'postcss-loader',
        options: {
          plugins: [
            require('autoprefixer')
          ]
        }
      }, {
        loader: 'sass-loader'
      }
      ]
    },
    {
      test: /\.css$/,
      use: [{
        loader: devMode ? 'style-loader' : MiniCssExtractPlugin.loader
      },
      {
        loader: 'css-loader'
      }]
    },
    {
      test: /\.(png|jpg|gif|woff|svg|eot|woff2|tff)$/,
      use: [{
        loader: 'url-loader',
        options: {
          outputPath: 'images/', // devMode ? 'images/' : 'images/', // 输出**文件夹
          // publicPath: '/',
          name: '/[name].[ext]',
          limit: 500 // 是把小于500B的文件打成Base64的格式，写入JS
        }
      }],
      exclude: /node_modules/
    }
    ]
  },
  plugins: [
    // slove the issue:  https://github.com/webpack-contrib/mini-css-extract-plugin/issues/250
    new FilterPlugin({ filter: /\[mini-css-extract-plugin]\nConflicting order between:/ }),
    new HappyPack({
      id: 'babel',
      loaders: ['babel-loader'],
      threadPool: happyThreadPool,
      // 允许 HappyPack 输出日志
      verbose: true
      // options: {
      //   babelrc: true,
      //   cacheDirectory: true // 启用缓存
      // }
    }),
    new ProgressBarPlugin(),
    // new FirendlyErrorePlugin(),
    new MiniCssExtractPlugin({
      // Options similar to the same options in webpackOptions.output
      // both options are optional
      filename: devMode ? 'styles/[name].[hash:4].css' : 'styles/[name].[contenthash].css',
      chunkFilename: devMode ? 'styles/[name].[hash:4].css' : 'styles/[name].[contenthash].css'
    }),
    new HtmlWebpackPlugin({
      template: './public/index.html',
      favicon: './src/assets/images/favicon.ico', // favicon编译
      inject: 'body',
      minify: {
        removeComments: true,
        collapseWhitespace: true,
        removeAttributeQuotes: true
      }
    }),
    new webpack.DefinePlugin({
      'process.env': {
        NODE_ENV: JSON.stringify(process.env.ENV)
      }
    })
  ],
  resolve: {
    modules: ['node_modules', path.join(__dirname, './node_modules')],
    extensions: ['index.js', '.jsx', '.js', '.json', '.less'],
    alias: {
      // HOST, // 各个环境变量中引入
      '@': resolve('src'),
      '@http': resolve('src/utils/http'),
      '@api': resolve('src/apis/index'),
      '@img': resolve('src/assets/images'),
      '@comp': resolve('src/components'),
      '@reg': resolve('src/utils/reg'),
      '@utils': resolve('src/utils/utils'),
      '@yhUtil': resolve('src/utils/yhUtil'),
      '@variant': resolve('src/utils/variant'),
      '@host': resolve('src/utils/host'),
      '@mix': resolve('src/assets/styles/common-mixins.less')
    }
  }
}
