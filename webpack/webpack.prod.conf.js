const baseWebpackConfig = require('./webpack.base.conf')
const OptimizeCSSAssetsPlugin = require('optimize-css-assets-webpack-plugin')
// const UglifyJSPlugin = require('uglifyjs-webpack-plugin')
// const ParallelUglifyPlugin = require('webpack-parallel-uglify-plugin')
const TerserPlugin = require('terser-webpack-plugin')
// const webpack = require('webpack')
const merge = require('webpack-merge')
const path = require('path')

const devMode = process.env.ENV === 'development' || process.env.ENV === 'test'
function resolve(dir) {
  return path.join(__dirname, '..', dir)
}

// baseWebpackConfig.plugins.push(
//   new ParallelUglifyPlugin({ // 多进程压缩
//     cacheDir: '.cache/',
//     uglifyJS: {
//       output: {
//         comments: false, // 删除所有的注释
//         beautify: false
//       },
//       compress: {
//         warnings: false, // 在UglifyJs删除没有用到的代码时不输出警告
//         drop_console: true, // 删除所有的 `console` 语句，可以兼容ie浏览器
//         collapse_vars: true, // 内嵌定义了但是只用到一次的变量
//         reduce_vars: true // 提取出出现多次但是没有定义成变量去引用的静态值
//       }
//     }
//   })
// )

module.exports = merge(baseWebpackConfig, {
  devtool: devMode ? 'eval-source-map' : 'false',
  mode: 'production',
  optimization: {
    splitChunks: {
      chunks: 'async', // 共有三个值可选：initial(初始模块)、async(按需加载模块)和all(全部模块)
      cacheGroups: { // cacheGroups对象，定义了需要被抽离的模块
      }
    },
    minimizer: [
      new TerserPlugin({
        cache: true,
        parallel: true
      }),
      new OptimizeCSSAssetsPlugin({
        assetNameRegExp: /\.css$/g,
        cssProcessor: require('cssnano'),
        cssProcessorOptions: {
          safe: true,
          discardComments: {
            removeAll: true
          }
        }
      })
    ]
  }
})
