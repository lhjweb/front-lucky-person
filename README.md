# 基本信息

# 脚本与命令
- `npm start`/`npm run dev`，运行开发项目
- `npm run build:test`，以 prod 环境打包测试环境项目
- `npm run build`，以 prod 环境打包项目
- `npm run lint`，eslint 检查
- `npm run test`, 运行测试用例

# 单元测试学习
- 学习[karam](https://karma-runner.github.io/3.0/index.html)
- 学习[mocha](https://mochajs.org/#getting-started)或者阮一峰的[mocha入门](http://www.ruanyifeng.com/blog/2015/12/a-mocha-tutorial-of-examples.html)
- 学习断言库[chai](https://www.chaijs.com/)

### karam.conf.js配置示例

```js
files: [
	'./src/utils/http.test.js', // 要测试代码的位置
	'./_test_/*.js' // 测试用例地址
],
preprocessors: {
	'/src/utils/http.test.js': ['babel', 'coverage'], // es6编译测试代码的位置
	'./_test_/*.js': ['babel'] // es6编译测试用例地址
},
```

# locales配置规则
```
	pageName.desc
```

# 代码规范约定

> 参考 [Airbnb React/JSX Style Guide](https://github.com/airbnb/javascript/tree/master/react)

***开始开发前必须安装并开启 eslint 检查***

vscode 参考[这里](https://marketplace.visualstudio.com/items?itemName=dbaeumer.vscode-eslint)

### 本版本react为 **16.8+**

- 通用逻辑抽取,可以尝试使用[react hooks](https://reactjs.org/docs/hooks-intro.html)

### 函数定义

- 事件处理函数以 `handle` 为前缀，并采用箭头函数定义
- 数据请求函数建议以 `request` 单独封装，并仅接受 api 请求需要的参数，不要在函数中进行参数 formatter 等操作

### JSX

- 若一行（120 列，含缩进）不能放下所有组件属性，需分行放置，且属性、组件闭合标签单独成行
- 无 children 的组件直接使用 `/>` 闭合
- 每个文件只定义一个组件，且文件名和组件名一致
- 组件文件名（组件名）采用大驼峰方式，如 `OrderList`

### 组件通信

- 非父子组件通信使用 redux
- 数据类 props 命名使用名词、形容词
- 回调函数、时间处理类 props 命名使用 `CB` \ `on` 修饰

### 样式

- 公共组件根 class 格式为 `comp__<COMPNAME>`，e.g. `comp__input-number`
- 组件根 class 使用当前组件名，以 `-` 分割，全部小写，e.g. `sign-in`
- less/css/scss 文件中的样式使用根 class 包裹
- sass 中，对于 dom 结构上不在本组件中的节点（如 modal、popover 等），使用时需定义其 class，且在 less/css/scss 文件中使用 `:local()` 包裹 class，e.g.
	```scss
	:local(.create-account-modal) {
		// your styles
	}
	```
- 推荐：非公用组件在定义样式时都使用 sass 文件 `:local()` 包裹根 class 以避免样式覆盖


# [公共组件说明](./src/components/README.md)

# 公共样式

### 头部面包屑

添加组件
```
// 默认自动菜单匹配
// 支持 list 形式
<Bread {...this.props} list={['生产中心', '工单管理']}/>
```

### 头部表单

class: common-header-form

表单项宽 200，间距 10


### 多态 api result

成功文本样式：`success`，失败文本：`fail`

api 返回的富文本用 `result-detail` 包裹

```js
Modal.info({
  className: 'modal-api-result',
  title: '导入结果',
  centered: true,
  content: (
    <React.Fragment>
      <p>
        本次导入成功 <span className="success">{ 5 }</span> 条，失败 <span className="fail">{ 3 }</span> 条
      </p>
      <div className="result-detail" dangerouslySetInnerHTML={{__html: '未知的错误</br>未知的错误</br>未知的错误</br>未知的错误</br>未知的错误</br>未知的错误</br>未知的错误</br>未知的错误</br>未知的错误</br>未知的错误</br>未知的错误</br>未知的错误</br>'}} />
    </React.Fragment>
  ),
  iconType: '--'
})
```

# webpack alias 路径
| alias | 路径 |
| ----- | --- |
| @ | ./src |
| @http | ./src/utils/http.js |
| @api | ./src/apis//api.js |
| @img | ./src/assets/images |
| @comp | ./src/components |
| @reg | './src/utils/reg.js|
| @utils | ./src/utils/utils.js|
| @variant | './src/utils/variant.js|
| @host | './src/host.js'|
| @mix | ./src/assets/styles/common-mixins.less |

> less 中引入 mixin 时注意加 `~`，如：`@import "~@mix"`


# 项目目录结构
> 参考 [How to better organize your React applications](https://medium.com/@alexmngn/how-to-better-organize-your-react-applications-2fd3ea1920f1)

```
├─ public	# 公用资源，index.html
├─ src	    # 项目源码
	├─ api  # api mapper，每个模块单独成文件
  ├─ assets	# 静态资源
    ├─ iconfont	# icon
    ├─ images	# 图片
    ├─ styles
      ├─ antd-theme.less	# antd 定制主题变量
      ├─ normalize.css
  ├─ components  # 公共组件
  ├─ api  # 枚举值、mapper
    ├─ api  # api mapper，每个模块单独成文件
  ├─ pages	# 页面组件模块
  ├─ components	# 公共组件
  ├─ utils	# 工具模块
    ├─ http.js	   # axios 封装
		├─ reg.js	    # 常用正则表达式
 ├─ webpack	# webpack 等配置文件
```

# ant design 主题定制
参考 ant design 官网 [Ant Design 的样式变量](https://ant-design.gitee.io/docs/react/customize-theme-cn#Ant-Design-%E7%9A%84%E6%A0%B7%E5%BC%8F%E5%8F%98%E9%87%8F)
一节，在 [antd-theme.less](./src/assets/styles/antd-theme.less) 定义相关 less 变量

# 其他

### api 请求
api 配置在 src/apis/api.js

host 定义在 src/utils/host.js

http请求返回code为200进入Promise.resolve, 否则进入Promise.reject

```javascript
	http.get(API_ORDER_LIST).then(data => {
		// 返回code为200进入此处
	}, err => {
		// code非200进入此处
	})
```

如果不希望预处理返回的数据（主要是弹窗展示错误信息），需设置 `config.disablePreProcessResp` 为 `true`

示例：
```javascript
// api.js
{
  // ...
  API_ORDER_LIST: [ 'fresh', '/fresh/order/list' ]
}

// your-component.jsx
import http from '@http'
import { API_ORDER_LIST } from '@api'

function requestXXX() {
  return http.get(API_ORDER_LIST)
}

function requestXXXX() {
  return http.post(API_ORDER_LIST, { k: 12 }, { params: { id: 1 } })
}

// 不预处理返回的数据
async function requestXXXXX() {
  return await http.get(API_ORDER_LIST, {}, { disablePreProcessResp: true })
}

// 不提示报错弹框
async function requestXXXXX() {
  return await http.get(API_ORDER_LIST, {}, { noWarn: true })
}
```

### iconfont 使用
为使用 UI 设计 icon 时的原色，项目采用 symbol 引用的方法引入 iconfont 图标。

使用方法为：

```html
<svg class="icon" aria-hidden="true">
  <use xlink:href="YOUR-ICON-SYMBOL" />
</svg>
```

> 注意 `<svg>` 标签必须拥有 icon class

欲使用 symbol 为 `#icon-menu-price` 的图标，react 代码为：

```javascript
import React from 'react'

const MyComp = ({ icon }) => (
  <p>
    icon with original color:
    <svg className="icon" aria-hidden="true">
      <use xlinkHref={`#icon-${icon}`} />
    </svg>
  </p>
)

render(
  <MyComp icon="menu-price" />,
  document.getElementById('app')
)
```

> 注意 `<use>` 标签的 `xlinkHref` 属性


[项目中用法示例](src/layout/Components/SideMenu.js)


# 捡坑/填坑

### ant table 表格嵌套表格

父表格 `dataSource` 最好不要使用 `children` 字段承载子表格数据，否则会被渲染成树状结果。
关于这点，即使设置了父表格的 `childrenColumnName` 属性，在使用 `getCheckboxProps` 属性设置父表格行勾选状态时，record 参数依然拿不到本行的子表格数据

### 浮点数运算精度问题

请使用 `number-precision` 做浮点数运算

示例：
```js
import NP from 'number-precision'

NP.plus(0.1, 0.2);             // = 0.3, not 0.30000000000000004
```

> 注意，plus 要求至少两个参数，否则会报错，常出现在如下情况：

```js
import NP from 'number-precision'

NP.plus(...lst.map(ele => ele.count));  // 如果 lst.length < 2 就会报错

// 保险写法：
NP.plus(0, 0, ...lst.map(ele => ele.count));
```

## 分支命名规则
- feature-430--xxx(姓名/功能)
- bugfix-530-xxx(姓名/功能)
#### 例子：
 - feature-0101-jimmy
 - feature-630-productCenter
 - bugfix-930-jimmy
 - bugfix-1230-changeHost
