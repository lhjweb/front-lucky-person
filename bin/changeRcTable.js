// antd rc-Table   'nbsp;'
// 执行命令 ：node bin/initRcTable

const fs = require('fs')
const path = require('path')

let filePath = path.join(__dirname, '..', '/node_modules')
try {
  const antdSrc = 'rc-table/es/ExpandableTable.js'
  fs.readFile(`${filePath}/${antdSrc}`, 'utf8', (err, data) => {
    // 将rc-table 中的 'nbsp;' 删除
    let result = data.replace(/\&nbsp\;/g, '')
    fs.writeFile(`${filePath}/${antdSrc}`, result, 'utf8', (err) => {
      if (err) {
        return console.log(err)
      } else {
        console.log('tips: remove rc-table "nbsp;" success')
      }
    })
  })
} catch (e) {
  console.log('error: remove rc-table "nbsp;" failed')
  console.log(e)
}
