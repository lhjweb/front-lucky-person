import React from 'react'
import Loadable from 'react-loadable'
import { Redirect, Route, Router, Switch } from 'react-router-dom'
import { history } from '@utils'
import { CompLoading as loading } from '@comp'
import routesConfig from './routesConfig'

// 在页面渲染前设置 title
export const wrapTitle = title => (loaded, props) => {
  const Comp = loaded.default
  document.title = title || '采购中台'

  return <Comp {...props} />
}

const routes = routesConfig.map(({ title, component, ...props }, index) =>
  <Route {...props} key={index} component={Loadable({ loader: component, loading, render: wrapTitle(title) })}/>)

const RedirectTo404 = () => <Redirect to="/404" />
const Routes = () => (
  <Router history={history}>
    <Switch>
      {routes}
      <Route component={RedirectTo404} />
    </Switch>
  </Router>
)

export default Routes
