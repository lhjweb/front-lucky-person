import React from 'react'
import Loadable from 'react-loadable'
import { Switch, Route, Router, Redirect } from 'react-router-dom'
import { CompLoading as loading } from '@comp'
import { history } from '@utils'
import Layout from '@/layout'
import { wrapTitle } from './routesInMenuLayout'
import { index } from './routesConfig'

const routes = index.map(({ title, component, ...props }, index) =>
  <Route {...props} key={index} component={Loadable({ loader: component, loading, render: wrapTitle(title) })}/>)

const Home = () => <Redirect to="/login" />

const Routes = () => (
  <Router history={history}>
    <Switch>
      <Route exact path="/" component={Home} />
      { routes }
      <Route component={Layout} />
    </Switch>
  </Router>
)

export default Routes
