export const index = [
  {
    exact: true,
    path: '/login',
    component: () => import('../pages/Login')
  }, {
    exact: true,
    path: '/404',
    component: () => import('../pages/NotFound')
  }, {
    exact: true,
    title: '活动成本统计',
    path: '/picking/print',
    component: () => import('../pages/ActivityCostStatistics/prizeCostPrint')
  }, {
    exact: true,
    title: '活动核销',
    path: '/picking/writeOffPrint',
    component: () => import('../pages/ActivitiesWriteOff/WriteOffPrint')
  }
]

// 活动管理
const activityManage = [
  {
    title: '活动配置',
    path: '/activityManage/activityResets',
    component: () => import('../pages/ActivityResets/list')
  }, {
    title: '活动配置详情编辑',
    path: '/activityManage/activityEditor',
    component: () => import('../pages/ActivityResets/detail')
  }, {
    title: '活动素材',
    path: '/activityManage/activityMaterial',
    component: () => import('../pages/ActivityMaterial')
  }, {
    title: '奖项调整',
    path: '/activityManage/activityAwardAdjustment',
    component: () => import('../pages/ActivityAwardAdjustment')
  }
]

// 活动统计
const activityStatistics = [
  {
    title: '活动参与统计',
    path: '/activityStatistics/activityStatistics',
    component: () => import('../pages/ActivityStatistics')
  }, {
    title: '出奖明细查询',
    path: '/activityStatistics/activityAwardSearch',
    component: () => import('../pages/ActivityAwardSearch')
  }, {
    title: '活动数据分析',
    path: '/activityStatistics/activityDataAnalysis',
    component: () => import('../pages/ActivityDataAnalysis')
  }
]

// 活动核销
const activitiesWriteOff = [
  {
    title: '活动核销',
    path: '/activitiesWriteOff/writeOff',
    component: () => import('../pages/ActivitiesWriteOff/WriteOff')
  }, {
    title: '活动核销 - 查看',
    path: '/activitiesWriteOff/writeOffDetial',
    component: () => import('../pages/ActivitiesWriteOff/WriteOffDetail')
  }

]

// 活动成本
const activityCost = [
  {
    title: '活动成本统计',
    path: '/activityCost/activityCostStatistics',
    component: () => import('../pages/ActivityCostStatistics/main')
  }, {
    title: '活动成本统计',
    path: '/activityCost/activityCostStatisticsDetail',
    component: () => import('../pages/ActivityCostStatistics/detail')
  }, {
    title: '奖品单价设置',
    path: '/activityCost/activityAwardPriceSetting',
    component: () => import('../pages/ActivityAwardPriceSetting')
  }
]

export default [
  ...activityManage,
  ...activityStatistics,
  ...activitiesWriteOff,
  ...activityCost
]
