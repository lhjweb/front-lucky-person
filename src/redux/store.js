import thunk from 'redux-thunk' // redux-thunk解决异步回调
import { createLogger } from 'redux-logger'
import { createStore, applyMiddleware, compose } from 'redux'
import reducers from './reducer.js'

const middleware = [thunk]
if (process.env.NODE_ENV === 'development') { // dev环境显示打印log
  middleware.push(createLogger())
}

const enhancer = compose(
  applyMiddleware(...middleware), // 中间件
  window.__REDUX_DEVTOOLS_EXTENSION__
    ? window.__REDUX_DEVTOOLS_EXTENSION__()
    : f => f
)
const stores = createStore(reducers, enhancer) // 创建 store

if (module.hot) {
  module.hot.accept('./reducer', () => {
    const reducers = require('./reducer').default

    stores.replaceReducer(reducers(stores.asyncReducers))
  })
}

export default stores
