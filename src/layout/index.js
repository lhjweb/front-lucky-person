/**
* Created by Jimmy on 2018-04-22
*/
import React from 'react'
import PropTypes from 'prop-types'
import { Layout, BackTop, Spin } from 'antd'
import SideMenu from './Components/SideMenu'
import AccountBox from './Components/AccountBox'
import Routes from '@/router/routesInMenuLayout'
import './style/index.less'
import { event } from '@utils'

const { Header, Sider, Content } = Layout

class App extends React.PureComponent {
  constructor(props) {
    super(props)

    this.contentRef = React.createRef()

    this.state = {
      loading: false,
      isCollapsed: false // 当前收起状态
    }

    const _this = this
    event.on('showLoading', _this.showLoading)
    event.on('hideLoading', _this.hideLoading)
  }

  showLoading = () => {
    this.setState({
      loading: true
    })
  }

  hideLoading = () => {
    this.setState({
      loading: false
    })
  }

  render() {
    const { history: { location } } = this.props
    const { isCollapsed, loading } = this.state

    return (
      <Layout className="layout--menu-layout">
        <Header className="layout--header">
          <div className="logo-box">
            <div className="logo" />
          </div>
          <AccountBox/>
        </Header>
        <Layout>
          <Sider
            className="layout--sider"
            width={196}
            collapsible
            collapsed={isCollapsed}
            trigger={null}
          >
            <div className="menu-box">
              <SideMenu location={location} collapsed={isCollapsed} />
            </div>
          </Sider>
          <Content className="layout--content-wrapper">
            <div id="global-layout" className="layout--content" ref={this.contentRef}>
              <Spin spinning={loading} delay={1000}>
                <Routes />
              </Spin>
            </div>
          </Content>
        </Layout>

        <BackTop target={() => this.contentRef.current}/>
      </Layout>
    )
  }
}

App.propTypes = {
  history: PropTypes.object
}

export default App
