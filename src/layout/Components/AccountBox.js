/**
* Created by Jimmy on 2019-05-07
*/
import React from 'react'
import { Avatar, Modal } from 'antd'
import { withRouter } from 'react-router'
import {
  API_LOGOUT
} from '@api'
import http from '@http'
import { getVariant, removeVariant, LOGIN_TOKEN } from '@variant'
import '../style/AccountBox.less'

class AccountBox extends React.Component {
  constructor(props) {
    super(props)

    const username = getVariant('username')
    this.state = {
      username
    }
  }

  /**
   * 退出按钮
   * @param e
   */
  handleLogoutClk = e => {
    e && e.preventDefault()
    Modal.confirm({
      title: '确认退出登录？',
      centered: true,
      onOk: () => new Promise((resolve, reject) => {
        http.put(API_LOGOUT).then(() => {
          // 清除缓存
          window.localStorage.clear()
          window.sessionStorage.clear()
          removeVariant(LOGIN_TOKEN)

          this.props.history.push('/login')
        }).then(resolve).catch(reject)
      })
    })
  }

  render() {
    const {
      username
    } = this.state
    return (
      <div className="account-box">
        <Avatar className="avatar" icon="user" size={34} src={require('@img/user.png')} />
        <span>{username}</span>
        <span className="slash" />
        <span className="as-btn" onClick={this.handleLogoutClk}>退出</span>
      </div>
    )
  }
}

AccountBox.propTypes = {
}

export default withRouter(AccountBox)
