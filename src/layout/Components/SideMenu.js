/**
* Created by Jimmy on 2018-04-22
*/
import React from 'react'
import PropTypes from 'prop-types'
import { Menu, Icon } from 'antd'
import { NavLink } from 'react-router-dom'
import '../style/SideMenu.less'
import { getVariant, setVariant } from '@variant'
import http from '@/utils/http'
import { API_GET_OPERATION_OBJECTIVES } from '@/apis'
const { SubMenu } = Menu

const target = process.env.NODE_ENV === 'development' ? null : '_blank'

/**
 * 格式化菜单数据
 * @param data
 * @param parentPath
 * @returns {Array}
 */
const formatterData = function(data, parentPath = '/') {
  const list = []
  data.sort((a, b) => a.rank - b.rank).forEach((item, index) => {
    if (item.operationObjectives) {
      list.push({
        index,
        id: item.id,
        icon: item.code,
        name: item.name,
        path: `${parentPath}${item.displayUrl}`,
        children: formatterData(item.operationObjectives, `${parentPath}${item.displayUrl}/`)
      })
    } else {
      list.push({
        index,
        id: item.id,
        name: item.name,
        path: `${parentPath}${item.displayUrl}`
      })
    }
  })
  return list
}

class SideMenu extends React.PureComponent {
  constructor(props) {
    super(props)

    /* 获取菜单 */
    const menus = getVariant('luckyMenu') || []
    const openKeys = this.getDefaultCollapsedSubMenus(props, menus)
    this.state = {
      menus, // 一级菜单
      openKeys,
      props
    }
  }

  async componentDidMount() { // 获取最新菜单
    const resp = await http.get(API_GET_OPERATION_OBJECTIVES, { businessType: 7 })
    const menus = formatterData(resp)

    const openKeys = this.getDefaultCollapsedSubMenus(this.props, menus)
    this.setState({
      menus,
      openKeys
    })
    setVariant('luckyMenu', menus)
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.location.pathname !== this.props.location.pathname) {
      const openKeys = this.getDefaultCollapsedSubMenus(nextProps, this.state.menus)
      this.setState({
        openKeys
      })
    }
  }

  /**
   * 获取默认展开一级菜单
   * @param props
   * @param menus
   * @returns {string[]|Array}
   */
  getDefaultCollapsedSubMenus = (props, menus) => {
    const { location: { pathname } } = props
    const snippets = pathname.split('/').slice(1, -1)
    const currentPathSnippets = snippets.map((item, index) =>
      // const arr = snippets.filter((_, i) => i <= index)
      // return arr.join('/')
      `/${item}`
    )
    let currentMenuSelectedKeys = []
    currentPathSnippets.forEach((item) => {
      currentMenuSelectedKeys = currentMenuSelectedKeys.concat(this.getSelectedMenuKeys(item, menus))
    })
    if (currentMenuSelectedKeys.length === 0) return ['']
    return currentMenuSelectedKeys
  }

  /**
   * 获取一位菜单项 key 数组
   * @param menus
   * @returns {Array}
   */
  getFlatMenuKeys = (menus) => {
    let keys = []
    menus.forEach((item) => {
      if (item.children) {
        keys.push(item.path)
        keys = keys.concat(this.getFlatMenuKeys(item.children))
      } else {
        keys.push(item.path)
      }
    })
    return keys
  }

  /**
   * 获取当前选中的菜单项 key 数组
   * @param path
   * @param menus
   * @returns {*[]}
   */
  getSelectedMenuKeys = (path, menus) => {
    const flatMenuKeys = this.getFlatMenuKeys(menus)
    const result = flatMenuKeys.filter((item) => {
      const itemRegExpStr = `^${item.replace(/:[\w-]+/g, '[\\w-]+')}$`
      const itemRegExp = new RegExp(itemRegExpStr)
      return itemRegExp.test(path.replace(/\/$/, ''))
    })
    // console.error(result)
    return result
  }

  handleOpenChange = openKeys => {
    const lastOpenKey = openKeys[openKeys.length - 1]
    const isMainMenu = this.state.menus.some(
      item => lastOpenKey && (item.key === lastOpenKey || item.path === lastOpenKey)
    )
    this.setState({
      openKeys: isMainMenu ? [lastOpenKey] : [...openKeys]
    })
  }

  /**
   * 获取新子菜单
   * @param menusData
   * @param parentData
   * @returns {Array|*}
   */
  getNavMenuItems = (menusData, parentData) => {
    if (!menusData) return []
    return menusData.map((item, index) => {
      if (!item.name) return null
      let itemPath
      if (item.path && item.path.indexOf('http') === 0) {
        itemPath = item.path
      } else {
        itemPath = `/${item.path || ''}`.replace(/\/+/g, '/')
      }
      // 在没配置二级情况也显示一级菜单（!parentData）
      if (!parentData || (item.children && item.children.some(child => child.name))) {
        return item.hideInMenu ? null :
          (
            <SubMenu
              title={
                item.icon ? (
                  <span>
                    <Icon type={item.icon} className="self-icon"/>
                    <span>{item.name}</span>
                  </span>
                ) : item.name
              }
              key={item.key || item.path || index}
            >
              {this.getNavMenuItems(item.children, item)}
            </SubMenu>
          )
      }
      const icon = item.icon && <Icon type={item.icon}/>
      return item.hideInMenu ? null :
        (
          <Menu.Item
            key={item.key || item.path || index}
          >
            {
              <NavLink
                to={itemPath}
                target={target}
                replace={itemPath === this.props.location.pathname}
              >
                {icon}<span>{item.name}</span>
              </NavLink>
            }
          </Menu.Item>
        )
    })
  }

  render() {
    const { collapsed, location: { pathname } } = this.props
    const { openKeys, menus } = this.state

    let selectedKeys = this.getSelectedMenuKeys(pathname, menus)
    if (!selectedKeys.length) {
      selectedKeys = [openKeys[openKeys.length - 1]]
    }

    return (
      <Menu
        mode="inline"
        className="comp_side-menu"
        onOpenChange={this.handleOpenChange}
        selectedKeys={selectedKeys}
        openKeys={collapsed ? [] : openKeys}
      >
        {this.getNavMenuItems(menus)}
      </Menu>
    )
  }
}

SideMenu.propTypes = {
  collapsed: PropTypes.bool,
  location: PropTypes.object
}

export default SideMenu
