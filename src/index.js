/**
 * Created by Jimmy on 2018-04-22  test
 */
import React from 'react'
import { render } from 'react-dom'
import { Provider } from 'react-redux'
import stores from './redux/store'
import { ConfigProvider, message } from 'antd'
import moment from 'moment'
import Router from './router'
import { history } from '@utils'
import 'moment/locale/zh-cn'
// import './assets/icons/iconfont'  // symbol 引入的 iconfont
import './index.less'
import '@/assets/polyfill.js'
import zhCN from 'antd/es/locale/zh_CN'
// 开发环境性能监控
// import Perf from 'react-addons-perf'
// window.Perf = Perf

// moment 日期格式设置
moment.locale('zh-cn')

// message 位置设置
message.config({
  top: 300
})

// 埋点
try {
  if (window.yh_trace) {
    window.yh_trace.initReact(history, 'yh-supply-platform') // 参数为路由对象、项目标识
  }
} catch (e) {
  console.error('init trace failed!')
}

render(
  <ConfigProvider locale={zhCN}>
    <Provider store={stores} >
      <Router />
    </Provider>
  </ConfigProvider>,
  document.getElementById('app')
)

if (module.hot) {
  module.hot.accept()
}
