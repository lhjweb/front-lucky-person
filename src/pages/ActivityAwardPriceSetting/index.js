/*
 * @Author: lihongjie
 * @Date: 2019-09-20 11:07:51
 * @Last Modified by: lihongjie
 * @Last Modified time: 2020-01-17 17:28:12
 */

import React from 'react'
import { message } from 'antd'
import { Bread } from '@comp'
import { PageForm, PageTable, AddNewEditModal } from './Components'
import http from '@http'
import moment from 'moment'
import {
  API_POST_QUERY_PRIZE,
  API_GET_QUERY_PRIZE_PRICE_HISTORY
} from '@api'

export default class App extends React.Component {
  state = {
    searchResult: [], // 获取查询结果
    searchParams: {}, // 搜索传值
    total: 0, // 查询总数
    page: 1, // 当前页码
    size: 100, // 当前分页数
    tableLoading: false, // 列表loading
    isShowEditModal: false, // 编辑弹框是否显示
    editData: null,
    dialogResult: [], // 编辑页面历史记录
    dialogLoading: false,
    dialogTotal: 0
  }

  componentDidMount() {
    // this._fetchSearchResult() // 需求优化，打开页面没有数据，查询才有数据
  }

  /* 获取查询结果 */
  _fetchSearchResult = () => {
    const { searchParams, page, size } = this.state

    const params = Object.assign({}, searchParams, { page, size })

    this.setState({
      tableLoading: true,
      total: 0,
      searchResult: []
    })
    http.post(API_POST_QUERY_PRIZE, params).then(res => {
      const { totalNum } = res
      this.setState({
        total: totalNum || 0,
        searchResult: res.result || []
      })
    }, err => console.error(err)).finally(this.resetSelectedRowKeys)
  }
  handleRefesh = () => {
    this.handleSearchResult()
  }

  /* 搜索查询结果 */
  handleSearchResult = (params = this.state.searchParams) => {
    if (!params) return false
    for (let idx in params) {
      if (!params[idx] || params[idx] === '') delete params[idx]
    }
    this.setState({
      page: 1,
      searchParams: params
    }, this._fetchSearchResult)
  }
  /* 编辑 */
  handleEdit =(record) => {
    this.setState({
      isShowEditModal: true,
      editData: record,
      dialogLoading: true
    }, () => {
      const { actCode, shopCode, prizeItemNo } = record
      this.handleDialogResult(actCode, shopCode, prizeItemNo)
    })
  }
  handleDialogResult =(actCode, shopCode, prizeItemNo) => {
    http.get(API_GET_QUERY_PRIZE_PRICE_HISTORY, { actCode, shopCode, prizeItemNo }).then(res => {
      const { totalNum, queryPrizePriceHistories } = res
      this.setState({ dialogResult: queryPrizePriceHistories || [], dialogTotal: totalNum, dialogLoading: false })
    })
  }
  /* 关闭弹框 */
  handleCancel = () => this.setState({ isShowEditModal: false, editData: null })

  /* ---------------- 表格操作 ----------------- */
  /* 页码和每页显示数量改变 */
  pageSizeChange = (current, pageSize) => {
    if (!current || !pageSize) return false
    this.setState({
      page: current,
      size: pageSize
    }, this._fetchSearchResult)
  }

  /* 更新查询结果 - 刷新表格数据 */
  reloadTableData = () => this.setState({ page: 1 }, this._fetchSearchResult)

  /* 重置选中的key */
  resetSelectedRowKeys = () => {
    this.setState({
      tableLoading: false
    })
  }
  /* ---------------- 表格操作 end ----------------- */
  render() {
    const {
      searchResult,
      tableLoading,
      total,
      page,
      size,
      isShowEditModal,
      editData,
      dialogResult,
      dialogLoading,
      dialogTotal
    } = this.state

    return (
      <section className="stay-introduction-of-new-product-list">
        <Bread {...this.props}/>
        <PageForm
          handleSearchResult={this.handleSearchResult}
        />
        <PageTable
          loading={tableLoading}
          total={total}
          page={page}
          size={size}
          searchResult={searchResult}
          pageSizeChange={this.pageSizeChange}
          handleEdit={this.handleEdit}
        />
        {isShowEditModal && <AddNewEditModal
          visible={isShowEditModal}
          handleCancel={this.handleCancel}
          handleRefesh={this.handleRefesh}
          dialogLoading={dialogLoading}
          dialogResult={dialogResult}
          editData={editData}
          dialogTotal={dialogTotal}
        />}
      </section>
    )
  }
}
