/*
 * @Author: lihongjie
 * @Date: 2019-09-19 15:44:36
 * @Last Modified by: lihongjie
 * @Last Modified time: 2020-01-17 14:33:27
 */

import React from 'react'
import { Table } from 'antd'
import { getPagination } from '@yhUtil'
import { LongText } from '@comp'
import _ from 'lodash'

class PageTable extends React.PureComponent {
  columns = [
    { width: 50, title: '序号', dataIndex: 'index', render: (text, record, index) => index + 1 },
    { width: 150, title: '活动编码', dataIndex: 'actCode', render: (text) => <LongText title={text} width={150} >{text}</LongText> },
    { width: 150, title: '活动名称', dataIndex: 'actName' },
    { width: 80, title: '活动奖项', dataIndex: 'prizeItemName' },
    { width: 50, title: '是否登记', dataIndex: 'isNeedRegisted', render: (text) => text === 1 ? <span>是</span> : <span>否</span> },
    { width: 80, title: '奖品名称', dataIndex: 'prizeName' },
    { width: 80, title: '奖品单价', dataIndex: 'unitPrice' },
    { width: 100, title: '门店编码', dataIndex: 'shopCode' },
    { width: 80, title: '操作', dataIndex: 'option', render: (text, record) => (
      <span>
        <a href="javascript:void(0)" onClick={() => this.props.handleEdit(record)}>编辑</a>
      </span>
    )
    }
  ]
  render() {
    const { searchResult = [], loading, total, page, size, pageSizeChange } = this.props

    // 分页
    const _pagination = getPagination({
      total,
      current: page,
      pageSize: size,
      onChange: pageSizeChange
    })

    // 列表
    let columns = [...this.columns]

    return (
      <section className="page-table">
        <Table
          scroll={{ x: _.sumBy(columns, 'width') }}
          rowKey={'id'}
          columns={columns}
          loading={loading}
          dataSource={searchResult}
          pagination={false}
        />
      </section>
    )
  }
}

export default PageTable
