/*
 * @Author: lihongjie
 * @Date: 2019-09-20 15:19:40
 * @Last Modified by: lihongjie
 * @Last Modified time: 2019-10-10 11:15:08
 */

import React from 'react'
import { Form } from 'antd'
import {
  CollapsibleFormBox,
  CommonInput,
  ActivityName
} from '@comp'

/*
* @Form.create()
* 利用装饰器简化代码，将表单验证操作统一到一个装饰器中进行
* 原：Form.create()(App)
* */
@Form.create()
class PageForm extends React.PureComponent {
  /* 查询 */
  handleSearch = e => {
    e.preventDefault()
    const { form: { validateFields }, handleSearchResult } = this.props
    validateFields((error, values) => {
      if (!error) {
        let { actName } = values
        // 活动名称
        actName = actName ? actName.split(' ')[1] : ''
        // 整合参数
        const queryParams = { ...values, actName }
        // 去除空项
        Object.keys(queryParams).forEach(item => {
          !queryParams[item] && delete queryParams[item]
        })
        handleSearchResult({ ...queryParams })
      }
    })
  }

  /* 重置 */
  handleReset = () => this.props.form.resetFields()

  render() {
    const { form: { getFieldDecorator } } = this.props
    return (
      <Form className="page-form">
        <CollapsibleFormBox
          onSearch={this.handleSearch}
          onReset={this.handleReset}
        >
          <div>
            <CommonInput
              getFieldDecorator={getFieldDecorator}
              decorator="shopName"
              label="门店名称"
              placeholder="请输入门店名称"
            />
            <ActivityName
              getFieldDecorator={getFieldDecorator}
              // rules={[{ required: true, message: '活动名称必填' }]}
            />
            <CommonInput
              getFieldDecorator={getFieldDecorator}
              decorator="actCode"
              label="活动编码"
              placeholder="请输入活动编码"
            />
          </div>
        </CollapsibleFormBox>
      </Form>
    )
  }
}

export default PageForm
