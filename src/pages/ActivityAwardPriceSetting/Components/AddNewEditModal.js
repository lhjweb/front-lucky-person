/*
 * @Author: lihongjie
 * @Date: 2019-09-23 11:19:34
 * @Last Modified by: lihongjie
 * @Last Modified time: 2020-01-17 17:28:32
 */

import React, { Component } from 'react'
import { Form, message, Collapse, Icon, Table } from 'antd'
const { Panel } = Collapse
import { CommonSelect, YHModal, CommonInput } from '@comp'
import {
  API_POST_UPDATE_PRIZE_COST
} from '@api'
import http from '@http'
import '../style/edit.less'
import _ from 'lodash'

const customPanelStyle = {
  borderRadius: 4,
  marginBottom: 10,
  overflow: 'hidden'
}
@Form.create()
class AddNewEditModal extends Component {
  constructor(props) {
    super(props)

    this.state = {
      isDisable: false, // 是否编辑弹框
      id: ''
    }
  }
  componentDidMount() {
    this.initData()
  }
  columns = [
    { width: 100, title: '奖品名称', dataIndex: 'prizeName' },
    { width: 100, title: '奖品单价', dataIndex: 'unitPrice' },
    { width: 120, title: '奖品税费', dataIndex: 'taxFee' },
    { width: 150, title: '编辑时间', dataIndex: 'updateTime' },
    { width: 100, title: '编辑人', dataIndex: 'updater' }
  ]

  initData = () => {
    console.log(this.props.editData, '2222')
    const { editData = {}, form: { setFieldsValue } } = this.props
    setTimeout(() => {
      setFieldsValue({
        prizeItemName: editData.prizeItemName, // 奖项名称
        isNeedRegisted: editData.isNeedRegisted,  // 是否登记
        prizeName: editData.prizeName,  // 奖品名称
        unitPrice: editData.unitPrice // 奖品单价
      })
    }, 100)
  }
  // 编辑弹框提交操作
  handleOk = () => new Promise((resolve, reject) => {
    const { form: { validateFields }, handleCancel, editData = {} } = this.props
    validateFields((error, values) => {
      if (error) return reject()
      let { id } = editData
      let data = { ...values, id }
      http.post(API_POST_UPDATE_PRIZE_COST, data).then(res => {
        message.success('修改成功')
        this.props.form.resetFields()
        this.props.handleRefesh()
      }).then(resolve).catch(reject)
      handleCancel()
    })
  })

  render() {
    // 列表
    let columns = [...this.columns]
    const { visible, handleCancel, form: { getFieldDecorator }, dialogResult, dialogLoading, dialogTotal } = this.props
    return (
      <YHModal
        visible={visible}
        title={'编辑奖品单价'}
        maskClosable={false}
        width="750px"
        okText="提交"
        onOk={this.handleOk}
        onCancel={handleCancel}
      >
        <Form>
          <div className='editBetween'>
            <CommonInput
              getFieldDecorator={getFieldDecorator}
              disabled
              label="奖项名称"
              decorator="prizeItemName"
              rules={[{ required: true, message: '奖项名称必填!' }]}
            />
            <CommonSelect
              getFieldDecorator={getFieldDecorator}
              label="是否登记"
              allowClear={false}
              decorator="isNeedRegisted"
              disabled
              rules={[{ required: true, message: '是否登记必填!' }]}
              list={[
                { value: '否', code: 0 },
                { value: '是', code: 1 }
              ]}
              placeholder="请选择是否登记"
            />
          </div>
          <div className='editBetween'>
            <CommonInput
              getFieldDecorator={getFieldDecorator}
              label="奖品名称"
              rules={[{ required: true, message: '奖品名称必填!' }]}
              decorator="prizeName"
            />
            <CommonInput
              getFieldDecorator={getFieldDecorator}
              label="奖品单价"
              rules={[{ required: true, message: '奖品单价必填!' }, {
                pattern: /^(?!00)\d+(\.\d{1,2})?$/,
                message: '奖品单价必须大于等于0.01'
              }]}
              decorator="unitPrice"
            />
          </div>
          <div className="editBetween">
            <CommonSelect
              getFieldDecorator={getFieldDecorator}
              label="奖品税费"
              allowClear={false}
              decorator="isPayTax"
              list={[
                { value: '否', code: 0 },
                { value: '是', code: 1 }
              ]}
              placeholder="请选择奖品税费"
            />
            <CommonInput
              getFieldDecorator={getFieldDecorator}
              label="编辑人"
              disabled
              decorator="updater"
            />
          </div>

        </Form>
        <Collapse bordered={true} defaultActiveKey={dialogResult.length > 0 ? '1' : 0} expandIcon={({ isActive }) => <Icon type="caret-right" rotate={isActive ? 90 : 0} />}>
          <Panel header={`收起历史记录 ${dialogTotal}条`} key="1" style={customPanelStyle}>
            <Table
              scroll={{ x: _.sumBy(columns, 'width'), y: 200 }}
              rowKey={'id'}
              columns={columns}
              loading={dialogLoading}
              dataSource={dialogResult}
              pagination={false}
            />
          </Panel>
        </Collapse>,
      </YHModal>
    )
  }
}

export default AddNewEditModal
