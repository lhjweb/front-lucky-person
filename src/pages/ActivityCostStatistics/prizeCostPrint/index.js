/*
 * @Description: 活动成本统计 - 打印
 * @company: YH
 * @Author: chengcheng
 * @Date: 2019-09-20 17:37:08
 * @LastEditors: chengcheng
 * @LastEditTime: 2019-09-22 15:55:27
 */

import React from 'react'
import {
  message,
  Button
} from 'antd'
import { Table, Header } from './components'
import { getQueryString } from '@utils'
import {
  API_GET_SETTING_GET_PRIZE_SHOP_COST_DETAIL
} from '@api'
import http from '@http'
import moment from 'moment'
import './style/index.less'

export default class PrizeCostPrint extends React.Component {
  constructor() {
    super()
    this.state = {
      actCode: getQueryString('actCode'), // 活动编码
      shopCode: getQueryString('shopCode'), // 门店编码
      currentTime: moment().format('YYYY-MM-DD HH:mm:ss'),
      actName: '', // 活动名称
      activeStateFlag: 0, // 活动状态
      fromToDate: '', // 活动日期
      openDay: '', // 活动天数
      shopName: '', // 门店名称
      dataSource: [], // 表格中的数据
      loading: false // 列表loading
    }
  }

  componentWillMount() {
    try {
      this._fetchOrderNo()
    } catch (err) {
      console.error(err)
    }
  }

  /* 打印 */
  handlePrint = () => {
    if (window.print) {
      window.print()
    } else {
      document.execCommand('print')
    }
  }

  /* 关闭窗口 */
  _closeWindow = () => window.setTimeout(() => window.close(), 1000)

  /* 获取订单详情 */
  _fetchOrderNo = () => {
    const { actCode, shopCode } = this.state
    if (!actCode || !shopCode) return (0, this._closeWindow)(message.error('活动编号/门店编号错误'))
    this.setState({ loading: true })

    let api = API_GET_SETTING_GET_PRIZE_SHOP_COST_DETAIL

    // 发送请求
    http.get(api, { actCode, shopCode }).then(res => {
      this.setState({
        actName: res.actName,
        activeStateFlag: res.activeStateFlag,
        fromToDate: res.fromToDate,
        openDay: res.openDay,
        shopCode: res.shopCode,
        shopName: res.shopName,
        dataSource: res.prizes,
        loading: false
      })
    })
  }

  render() {
    const { actName, currentTime } = this.state
    return (
      <section className={'outbound-order'}>
        <Button className={'noprint'} type={'primary'} onClick={this.handlePrint}>打印</Button>
        <div className={'title'}>{actName} 奖品成本统计</div>
        <div className={'orderNo'}>打印时间：{currentTime}</div>
        <Header {...this.state} />
        <Table {...this.state}/>
      </section>
    )
  }
}
