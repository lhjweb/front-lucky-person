/*
 * @Description: 活动成本统计 - 打印(合计行)
 * @company: YH
 * @Author: chengcheng
 * @Date: 2019-09-20 17:37:08
 * @LastEditors: chengcheng
 * @LastEditTime: 2019-09-24 10:58:07
 */
import React, { PureComponent } from 'react'
// import { Row, Col } from 'antd'
import '../style/index.less'

export default class FooterComponent extends PureComponent {
  render() {
    const { sumTotal } = this.props

    return (
      <section className='footer'>
        {/* <Row className={'group'}>
          <Col span={24} className={'item'}>合计：{sumTotal || 0}</Col>
        </Row> */}
        <table>
          <tbody>
            <tr>
              <th>合计：</th>
              <th></th>
              <th></th>
              <th style={{ paddingLeft: '7px' }}>{sumTotal}</th>
            </tr>
          </tbody>
        </table>
      </section>
    )
  }
}
