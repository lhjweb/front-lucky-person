/*
 * @Description: 活动成本统计 - 打印( 表格 )
 * @company: YH
 * @Author: chengcheng
 * @Date: 2019-09-20 17:37:08
 * @LastEditors: chengcheng
 * @LastEditTime: 2019-09-24 10:50:26
 */
import React, { PureComponent } from 'react'
import { Table } from 'antd'
import Footer from './Footer'
import np from 'number-precision'

export default class TableComponent extends PureComponent {
  constructor(props) {
    super(props)

    this.state = {
      dataSources: [] // 原数据
    }

    this.columns = [
      { title: '奖项名称', dataIndex: 'prizeItemName' },
      { title: '兑奖数量', dataIndex: 'takeCount' },
      { title: '奖品单价 (均价)', dataIndex: 'unitPrice', render: text => (text || 0) },
      { title: '奖品成本', dataIndex: 'cost', render: (text, record) => {
        const count = record.takeCount || 0
        const unit = record.unitPrice || 0
        const cost = np.times(count, unit)
        return <span>{cost}</span>
      } }
    ]
  }

  render() {
    const { dataSource, loading } = this.props

    // 计算成本合计
    const sumTotal = dataSource.reduce((prev, cur) => {
      const count = cur.takeCount || 0
      const unit = cur.unitPrice || 0
      const rowCost = np.times(count, unit)
      prev = np.plus(prev, rowCost)
      return prev
    }, 0)

    return (
      <Table
        style={{ marginTop: '10px' }}
        loading={loading}
        columns={this.columns}
        dataSource={dataSource}
        pagination={false}
        bordered
        size="middle"
        rowKey={record => record.id || Math.random()}
        footer={() => (<Footer sumTotal={sumTotal || 0}/>)}
      />
    )
  }
}
