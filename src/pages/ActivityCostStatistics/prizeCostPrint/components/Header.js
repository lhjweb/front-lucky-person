/*
 * @Description: 活动成本统计 - 打印(头部)
 * @company: YH
 * @Author: chengcheng
 * @Date: 2019-09-20 17:37:08
 * @LastEditors: chengcheng
 * @LastEditTime: 2019-09-22 15:56:40
 */
import React from 'react'
import { Row, Col } from 'antd'
import '../style/index.less'

// 格式化活动状态
const actStatus = {
  0: '未提交',
  1: '未开始',
  2: '正在进行',
  3: '已结束',
  4: '已废弃'
}

export default class HeaderComponent extends React.Component {
  render() {
    const {
      actName,
      activeStateFlag,
      fromToDate,
      openDay,
      shopCode,
      shopName
    } = this.props

    return (
      <section className={'header'}>
        <Row className={'group'}>
          <Col span={8} className={'item'}>门店名称：{shopName || ''}</Col>
          <Col span={8} className={'item'}>门店编号：{shopCode || ''}</Col>
          <Col span={8} className={'item'}>活动名称：{actName || ''}</Col>
          <Col span={8} className={'item'}>活动日期：{fromToDate || ''}</Col>
          <Col span={8} className={'item'}>活动天数：{openDay || ''}</Col>
          <Col span={8} className={'item'}>活动状态：{actStatus[activeStateFlag] || ''}</Col>
        </Row>
      </section>
    )
  }
}
