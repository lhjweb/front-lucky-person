/*
 * @Description: 活动成本统计 - 头部展示
 * @company: YH
 * @Author: chengcheng
 * @Date: 2019-09-20 10:59:26
 * @LastEditors: chengcheng
 * @LastEditTime: 2019-09-20 11:24:34
 */
import React from 'react'
import { Descriptions } from 'antd'
import '../style/DetailHeader.less'

// 格式化活动状态
const actStatus = {
  0: '未提交',
  1: '未开始',
  2: '正在进行',
  3: '已结束',
  4: '已废弃'
}

export default class DetailHeader extends React.Component {
  state = {}

  render() {
    const {
      actName,
      shopName,
      shopCode,
      fromToDate,
      openDay,
      activeStateFlag
    } = this.props

    return (
      <div className="detail-header">
        <Descriptions>
          <Descriptions.Item label="活动名称">{actName || ''}</Descriptions.Item>
          <Descriptions.Item label="门店名称">{shopName || ''}</Descriptions.Item>
          <Descriptions.Item label="门店编号">{shopCode || ''}</Descriptions.Item>
          <Descriptions.Item label="活动日期">{fromToDate || ''}</Descriptions.Item>
          <Descriptions.Item label="活动天数">{openDay || ''}</Descriptions.Item>
          <Descriptions.Item label="活动状态">{actStatus[activeStateFlag] || ''}</Descriptions.Item>
        </Descriptions>
      </div>
    )
  }
}
