/*
 * @Description: 活动成本统计 - 查看明细 - 表格
 * @company: YH
 * @Author: chengcheng
 * @Date: 2019-09-20 10:16:10
 * @LastEditors: chengcheng
 * @LastEditTime: 2019-09-24 10:39:14
 */
import React from 'react'
import { Table } from 'antd'
import np from 'number-precision'
import '../style/PageTable.less'

export default class PageTable extends React.Component {
  state = {}

  columns = [
    { title: '奖项名称', dataIndex: 'prizeItemName' },
    { title: '兑奖数量', dataIndex: 'takeCount' },
    { title: '奖品单价 (均价)', dataIndex: 'unitPrice', render: text => (text || 0) },
    { title: '奖品税费', dataIndex: 'taxFee', render: text => (text || 0) },
    { title: '奖品成本', dataIndex: 'cost', render: (text, record) => {
      const count = record.takeCount || 0
      const unit = record.unitPrice || 0
      const taxFee = record.taxFee || 0
      const cost = np.times(count, unit)
      const costTwo = np.times(count, taxFee)
      const total = np.plus(cost, costTwo)
      return <span>{total}</span>
    } }
  ]

  render() {
    const { dataSource } = this.props

    // 计算成本合计
    const sumTotal = dataSource.reduce((prev, cur) => {
      const count = cur.takeCount || 0
      const unit = cur.unitPrice || 0
      const rowCost = np.times(count, unit)
      prev = np.plus(prev, rowCost)
      return prev
    }, 0)

    // 成本合计
    // const footer = <section className="footer-sum">合计: <span className="footer-num">{sumTotal}</span></section>
    const footer = (
      <table>
        <tbody>
          <tr>
            <th>合计：</th>
            <th></th>
            <th></th>
            <th style={{ paddingLeft: '7px' }}>{sumTotal}</th>
          </tr>
        </tbody>
      </table>
    )

    return (
      <div className="page-table">
        <Table
          columns={this.columns}
          dataSource={dataSource}
          rowKey={record => record.id || Math.random()}
          title={() => '活动奖品成本明细'}
          footer={() => footer}
        />
      </div>
    )
  }
}
