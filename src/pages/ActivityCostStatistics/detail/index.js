/*
 * @Description: 活动成本统计 - 查看明细页面
 * @company: YH
 * @Author: chengcheng
 * @Date: 2019-09-19 14:19:15
 * @LastEditors: chengcheng
 * @LastEditTime: 2019-09-20 11:31:29
 */
import React from 'react'
import { Button } from 'antd'
import { Bread } from '@comp'
import { DetailHeader, PageTable } from './Components'
import {
  API_GET_SETTING_GET_PRIZE_SHOP_COST_DETAIL
} from '@api'
import http from '@http'

export default class ActivityCostStatisticsDetail extends React.Component {
  state = {
    actName: '', // 活动名称
    activeStateFlag: '', // 活动状态
    fromToDate: '', // 活动日期
    openDay: '', // 活动天数
    shopCode: '', // 门店编码
    shopName: '', // 门店名称
    prizes: [] // 活动奖品成本明细
  }

  componentDidMount() {
    const { actCode, shopCode } = this.props.location.state
    this.setState({ actCode, shopCode }, () => this.fetchData(actCode, shopCode))
  }

  /* 查询明细页面数据 */
  fetchData = (actCode, shopCode) => {
    http.get(API_GET_SETTING_GET_PRIZE_SHOP_COST_DETAIL, { actCode, shopCode }).then(res => {
      this.setState({
        actName: res.actName,
        activeStateFlag: res.activeStateFlag,
        fromToDate: res.fromToDate,
        openDay: res.openDay,
        shopCode: res.shopCode,
        shopName: res.shopName,
        prizes: res.prizes
      })
    })
  }

  render() {
    const {
      actName,
      activeStateFlag,
      fromToDate,
      openDay,
      shopCode,
      shopName,
      prizes
    } = this.state
    return (
      <div className="activity-cost-statistics-detail">
        <Bread
          list={['活动成本', '活动成本统计', '查看明细']}
          right={[<Button onClick={() => window.history.back(-1)}>返回</Button>]}
        />
        <DetailHeader
          actName={actName}
          activeStateFlag={activeStateFlag}
          fromToDate={fromToDate}
          openDay={openDay}
          shopCode={shopCode}
          shopName={shopName}
        />
        <PageTable
          dataSource={prizes}
        />
      </div>
    )
  }
}
