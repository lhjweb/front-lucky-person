/*
 * @Description: 活动成本统计 - 表格
 * @company: YH
 * @Author: chengcheng
 * @Date: 2019-09-18 14:40:41
 * @LastEditors: chengcheng
 * @LastEditTime: 2019-09-24 11:00:14
 */
import React from 'react'
import { Table } from 'antd'
import { LongText } from '@comp'
import { getPagination } from '@yhUtil'
import _ from 'lodash'
// import np from 'number-precision'
import '../style/index.less'

export default class PageTable extends React.Component {
  state = {}

  columns = [
    { width: 50, title: '序号', dataIndex: 'index', render: (text, record, index) => index + 1 },
    { width: 150, title: '活动编码', dataIndex: 'actCode', render: (text) => <LongText title={text} width={150} >{text}</LongText> },
    { width: 150, title: '活动名称', dataIndex: 'actName', render: (text) => <LongText title={text} width={150} >{text}</LongText> },
    { width: 60, title: '门店编号', dataIndex: 'shopCode' },
    { width: 150, title: '门店名称', dataIndex: 'shopName', render: (text) => <LongText title={text} width={150} >{text}</LongText> },
    { width: 180, title: '活动日期', dataIndex: 'createdTime', render: (text, record) => `${record.startTime} ~ ${record.endTime}` },
    { width: 80, title: '活动天数', dataIndex: 'openDay' },
    { width: 100, title: '奖品成本总计', dataIndex: 'unitPrice' },
    { width: 100, title: '操作', dataIndex: 'operation', className: 'hoverText', render: (text, record) => <div>
      <span className="mr10">
        <a onClick={() => this.props.handleCheckDetail(record)}>查看明细</a>
      </span>
      <span>
        <a onClick={() => this.props.handlePrint(record)}>打印</a>
      </span>
    </div>
    }
  ]

  render() {
    const { loading, dataSource, sumTotal, totalNum, page, size, pageSizeChange } = this.props

    // 控制分页
    const _pagination = getPagination({
      // total: totalNum,
      current: page,
      pageSize: size,
      onChange: pageSizeChange
    })

    // 计算成本总和
    // const totalNum = dataSource.reduce((prev, cur) => {
    //   prev = np.plus(prev, cur.unitPrice)
    //   return prev
    // }, 0)

    // 合计栏
    const footer = (
      <table>
        <tbody>
          <tr>
            <th>合计：</th>
            <th></th>
            <th></th>
            <th></th>
            <th></th>
            <th></th>
            <th style={{ paddingLeft: '30px' }}>{sumTotal}</th>
            <th></th>
          </tr>
        </tbody>
      </table>
    )

    return (
      <section className="page-table">
        <div className="result-num common__tool">共{totalNum || 0}条查询结果</div>
        <Table
          scroll={{ x: _.sumBy(this.columns, 'width') }}
          rowKey={() => Math.random()}
          columns={this.columns}
          loading={loading}
          dataSource={dataSource}
          pagination={_pagination}
          footer={() => footer}
        />
      </section>
    )
  }
}
