/*
 * @Description: 活动成本统计 - 表单
 * @company: YH
 * @Author: chengcheng
 * @Date: 2019-09-18 11:11:34
 * @LastEditors: chengcheng
 * @LastEditTime: 2019-09-23 10:06:39
 */
import React from 'react'
import { Form } from 'antd'
import { CollapsibleFormBox, CommonInput, RangePicker, ActivityName } from '@comp'
import moment from 'moment'

@Form.create()
export default class PageForm extends React.Component {
  state = {}

  /* 查询 */
  handleSearch = () => {
    const { form: { validateFields } } = this.props
    validateFields((error, { time, actName, ...values }) => {
      if (!error) {
        // 格式化开始和结束时间
        const startDate = (time || []).length ? moment(time[0]).format('YYYY-MM-DD') : ''
        const endDate = (time || []).length ? moment(time[1]).format('YYYY-MM-DD') : ''
        // 活动名称
        actName = actName ? actName.split(' ')[1] : ''
        // 整合参数
        const queryParams = { startDate, endDate, actName, ...values }
        // 去除空项
        Object.keys(queryParams).forEach(item => {
          !queryParams[item] && delete queryParams[item]
        })
        this.props.handleFormSearch({ ...queryParams })
      }
    })
  }

  /* 重置 */
  handleReset = () => this.props.form.resetFields()

  render() {
    const { getFieldDecorator, setFieldsValue, getFieldValue } = this.props.form
    return (
      <Form className="page-form">
        <CollapsibleFormBox
          onSearch={this.handleSearch}
          onReset={this.handleReset}
        >
          <div>
            <CommonInput
              getFieldDecorator={getFieldDecorator}
              decorator="actCode"
              label="活动编码"
              placeholder="活动编码"
            />
            <ActivityName
              getFieldDecorator={getFieldDecorator}
            />
            <CommonInput
              getFieldDecorator={getFieldDecorator}
              decorator="shopCode"
              label="门店号"
              placeholder="请输入门店号"
            />
            <RangePicker
              getFieldDecorator={getFieldDecorator}
              setFieldsValue={setFieldsValue}
              getFieldValue={getFieldValue}
              label="查询日期"
              decorator="time"
              placeholder={['开始时间', '结束时间']}
            />
          </div>
        </CollapsibleFormBox>
      </Form>
    )
  }
}
