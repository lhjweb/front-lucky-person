/*
 * @Description: 活动成本统计
 * @company: YH
 * @Author: chengcheng
 * @Date: 2019-09-18 10:35:18
 * @LastEditors: chengcheng
 * @LastEditTime: 2019-09-24 10:02:10
 */
import React from 'react'
import { Bread } from '@comp'
import { PageForm, PageTable } from './Components'
import {
  API_POST_REPORT_QUERY_PRIZE_COST
} from '@api'
import http from '@http'

export default class ActivityCostStatistics extends React.Component {
  state = {
    sumTotal: 0, // 成本合计
    totalNum: 0, // 查询总数
    page: 1, // 当前页码
    size: 10, // 当前分页数
    tableLoading: false,
    dataSource: [], // 表格数据
    searchParams: {} // 表单查询数据
  }

  componentDidMount() {
    this._fetchSearchResult()
  }

  /* 获取查询结果 */
  _fetchSearchResult = () => {
    const { searchParams, page, size } = this.state
    const params = Object.assign({}, searchParams, { page, size })
    this.setState({
      tableLoading: true,
      totalNum: 0,
      searchResult: []
    })
    http.post(API_POST_REPORT_QUERY_PRIZE_COST, params).then(res => {
      this.setState({
        sumTotal: res.sumTotal,
        dataSource: res.pageList.result,
        totalNum: res.pageList.totalNum,
        tableLoading: false
      })
    })
  }

  /* 处理表单查询 */
  handleFormSearch = searchParams => {
    this.setState({ searchParams, page: 1 }, () => this._fetchSearchResult())
  }

  /* 打印 */
  handlePrint = ({ actCode, shopCode }) => {
    window.open(`/picking/print?actCode=${actCode}&shopCode=${shopCode}`, 'newWindow', 'height=500, width=1300, top=0, left=0, toolbar=no, menubar=no, scrollbars=no, resizable=no, location=no, status=no')
  }

  /* 查看明细 */
  handleCheckDetail = ({ actCode, shopCode }) => {
    this.props.history.push({
      pathname: '/activityCost/activityCostStatisticsDetail',
      state: {
        actCode,
        shopCode
      }
    })
  }

  /* 页码和每页显示数量改变 */
  pageSizeChange = (current, pageSize) => {
    if (!current || !pageSize) return false
    this.setState({
      page: current,
      size: pageSize
    }, this._fetchSearchResult)
  }

  render() {
    const { tableLoading, dataSource, totalNum, page, size, sumTotal } = this.state
    return (
      <section className="activity-cost-statistics">
        <Bread {...this.props} />
        <PageForm
          handleFormSearch={this.handleFormSearch}
        />
        <PageTable
          tableLoading={tableLoading}
          dataSource={dataSource}
          totalNum={totalNum}
          sumTotal={sumTotal}
          page={page}
          size={size}
          handlePrint={this.handlePrint}
          pageSizeChange={this.pageSizeChange}
          handleCheckDetail={this.handleCheckDetail}
        />
      </section>
    )
  }
}
