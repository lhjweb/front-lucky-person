/*
 * @Author: lihongjie
 * @Date: 2019-09-19 15:28:32
 * @Last Modified by: lihongjie
 * @Last Modified time: 2019-09-26 14:49:53
 */

import React from 'react'
import { message } from 'antd'
import { Bread, YHButton } from '@comp'
import { PageForm, PageTable } from './Components'
import {
  API_POST_QUERY_SHOP_RULE_DETAIL,
  API_POST_SHOP_RULE_APPLY
} from '@api'
import http from '@http'
import moment from 'moment'

export default class App extends React.Component {
  state = {
    searchParams: {}, // 查询条件
    dataConfig: null, // 表格查询参数
    tableLoading: false, // 列表loading
    editDatas: [], // 修改数据
    actCode: '' // 活动编码
  }

  /* 搜索查询结果 */
  handleSearchResult = (params) => {
    if (!params) return false
    for (let idx in params) {
      if (!params[idx] || params[idx] === '') delete params[idx]
    }

    if (params.actName) {
      // 活动名称
      params.actName = params.actName.split(' ')[1]
    }

    this.setState({
      actCode: '', // 重置值
      editDatas: [], // 重置值
      searchParams: params, // 存查询条件
      dataConfig: {
        method: 'POST',
        urlConfig: API_POST_QUERY_SHOP_RULE_DETAIL,
        params
      }
    })
  }

  /**
   * 应用 - 提交修改数据
   */
  handleSubmit = () => new Promise((resolve, reject) => {
    const { editDatas, actCode, searchParams } = this.state
    console.log(editDatas, actCode)
    if (!editDatas.length) {
      message.error('无修改数据信息')
      return false
    }
    const params = {
      actCode,
      shopRules: editDatas
    }
    http.post(API_POST_SHOP_RULE_APPLY, params).then(res => {
      message.success('应用成功')
      this.handleSearchResult(searchParams)
    }).then(resolve).catch(reject)
  })

  /* ---------------- 表格操作 ----------------- */
  /* 表格改变 */
  handleAfterChange = (changes, source, selectDataSource, selectData, dataSource, data, isSelectAll, setData) => {
    // 修改操作
    if (changes && changes[0]) {
      this._tableEditChange(changes, source, selectDataSource, selectData, dataSource, data, isSelectAll, setData)
    }
  }

  /* 表格修改操作 */
  _tableEditChange = (changes, source, selectDataSource, selectData, dataSource, data, isSelectAll, setData) => {
    let row = changes[0][0]

    let enName = changes[0][1]
    let oldData = changes[0][2]
    let newData = changes[0][3]
    let item = dataSource[row] // 当前行数据
    // 去除重复选择
    if (oldData === newData || (!oldData && newData === '')) return false

    if (!/^[0-9]+$/.test(newData)) {
      message.warning('请输入正整数')
      return false
    }

    const { editDatas } = this.state
    let newDatas = [...editDatas]
    let findData = newDatas.find(o => o.id === item.id)
    this.setState({
      actCode: item.actCode
    })
    if (findData) { // 已存在修改数据中
      findData[enName] = Number(newData)
    } else {
      findData = {
        accessCountLimit: item.accessCountLimit,
        activeDate: moment(item.activeDate).format('YYYY-MM-DD'),
        appendCount: item.appendCount,
        checkedRate: item.checkedRate,
        prizeItemNo: item.prizeItemNo,
        shopCode: item.shopCode,
        id: item.id
      }
      findData[enName] = Number(newData)
      newDatas.push(findData)
    }

    this.setState({
      editDatas: newDatas
    })
  }

  /* ---------------- 表格操作 end ----------------- */
  render() {
    const {
      dataConfig,
      tableLoading,
      editDatas
    } = this.state
    return (
      <section className="stay-introduction-of-new-product-list">
        <Bread {...this.props}/>
        <PageForm
          handleSearchResult={this.handleSearchResult}
        />
        <section className="page-tool common__tool">
          <div className="fl"></div>
          <div className="fr">
            <YHButton type="primary" onClick={this.handleSubmit} disabled={!editDatas.length}>应用</YHButton>
          </div>
        </section>
        <PageTable
          loading={tableLoading}
          dataConfig={dataConfig}
          handleAfterChange={this.handleAfterChange}
        />
      </section>
    )
  }
}
