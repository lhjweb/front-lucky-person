/*
 * @Author: lihongjie
 * @Date: 2019-09-19 15:44:36
 * @Last Modified by: lihongjie
 * @Last Modified time: 2019-10-10 14:47:36
 */

import React from 'react'
import { TableMul } from '@comp'
import moment from 'moment'

class PageTable extends React.PureComponent {
  columns = [
    // { width: 50, title: '序号', dataIndex: 'index', render: (text, record, index) => index + 1 },
    { width: 50, chName: '门店号', enName: 'shopCode' },
    { width: 80, chName: '出奖奖项', enName: 'prizeItemNo' },
    { width: 120, chName: '出奖时间', enName: 'activeDate', render: (instance, TD, row, col, prop, value, cellProperties) => <span>{moment(value).format('YYYY-MM-DD')}</span> },
    { width: 80, chName: '日平均出奖数', enName: 'prizeCount' },
    { width: 80, chName: '已出奖', enName: 'saleCount' },
    { width: 50, chName: '已兑奖', enName: 'takeCount' },
    { width: 120, chName: '中奖概率', enName: 'checkedRate', editor: true },
    { width: 120, chName: '增加出奖数', enName: 'appendCount', editor: true },
    { width: 120, chName: '客流值阈值', enName: 'accessCountLimit', editor: true }
  ]
  render() {
    const { dataConfig, loading, handleAfterChange } = this.props

    return (
      <section className="page-table">
        <TableMul
          columns={this.columns}
          loading={loading}
          dataConfig={dataConfig}
          afterChange={handleAfterChange}
        />
      </section>
    )
  }
}

export default PageTable
