/*
 * @Author: lihongjie
 * @Date: 2019-09-24 14:03:34
 * @Last Modified by: lihongjie
 * @Last Modified time: 2019-10-14 14:23:56
 */

import React from 'react'
import { Form } from 'antd'
import {
  CollapsibleFormBox,
  ActivityName,
  CommonInput,
  CommonSelect
} from '@comp'
import {
  API_GET_QUERY_ACTNAME_CODES
} from '@api'
import moment from 'moment'

// 活动状态 数据
const currentStatusList = [
  { code: '0', value: '未提交' },
  { code: '1', value: '未开始' },
  { code: '2', value: '正在进行' },
  { code: '3', value: '已结束' },
  { code: '4', value: '已废除' }
]
/*
* @Form.create()
* 利用装饰器简化代码，将表单验证操作统一到一个装饰器中进行
* 原：Form.create()(App)
* */
@Form.create()
class PageForm extends React.PureComponent {
  state = {
    listConfig: {
      method: 'get',
      urlConfig: API_GET_QUERY_ACTNAME_CODES
    }
  }
  /* 查询 */
  handleSearch = () => {
    const { form: { validateFields }, handleSearchResult } = this.props
    validateFields((error, values) => {
      if (error) return false
      handleSearchResult(values)
    })
  }
   /* 活动名称选择 */
   handleSelect = (value) => {
     const { form: { setFieldsValue } } = this.props
     let actCode
     if (value) {
       actCode = value.split(' ')[0]
     }
     setFieldsValue({
       actCode
     })
   }

  /* 重置 */
  handleReset = () => this.props.form.resetFields()

  render() {
    const { form: { getFieldDecorator } } = this.props
    return (
      <Form className="page-form">
        <CollapsibleFormBox
          onSearch={this.handleSearch}
          onReset={this.handleReset}
          isApplication={this.handleApplication}
        >
          <div>
            <ActivityName
              // rules={[{ required: true, message: '活动名称必填' }]}
              onChange={this.handleSelect}
              getFieldDecorator={getFieldDecorator}/>
            <CommonInput
              getFieldDecorator={getFieldDecorator}
              decorator="actCode"
              label="活动编码"
              placeholder="请输入活动编码"
            />
            <CommonInput
              getFieldDecorator={getFieldDecorator}
              decorator="shopCode"
              label="门店号"
              placeholder="请输入门店号"
            />
            <CommonSelect
              getFieldDecorator={getFieldDecorator}
              label="活动状态"
              decorator="activeStateFlag"
              placeholder="请选择"
              list={currentStatusList}
            />
          </div>
        </CollapsibleFormBox>
      </Form>
    )
  }
}

export default PageForm
