/*
 * @Description: 活动核销 - 打印 - 奖品明细
 * @company: YH
 * @Author: chengcheng
 * @Date: 2019-09-25 18:05:44
 * @LastEditors: chengcheng
 * @LastEditTime: 2019-09-26 14:59:36
 */
import React from 'react'
import { Table } from 'antd'
import '../style/index.less'

export default class TableComponent extends React.Component {
  state = {}

  columns = [
    // { title: '序列号', dataIndex: 'index' },
    { title: '奖项名称', dataIndex: 'prizeItemName' },
    { title: '奖品名称', dataIndex: 'prizeName' },
    { title: '税费', dataIndex: 'taxFee' }
  ]

  render() {
    // const { prizeItemName, prizeName, taxFee } = this.props
    const { dataSource: { prizeItemName, prizeName, taxFee }, loading } = this.props
    const tableData = [{ prizeItemName, prizeName, taxFee }]
    // console.log(tableData, prizeItemName, prizeName, taxFee)
    return (
      <Table
        style={{ marginTop: '10px' }}
        loading={loading}
        columns={this.columns}
        dataSource={tableData}
        pagination={false}
        bordered
        // size="middle"
        rowKey={record => record.id || Math.random()}
      />
    )
  }
}
