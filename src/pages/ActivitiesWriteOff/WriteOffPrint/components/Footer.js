/*
 * @Description: 活动成本统计 - 打印(合计行)
 * @company: YH
 * @Author: chengcheng
 * @Date: 2019-09-20 17:37:08
 * @LastEditors: chengcheng
 * @LastEditTime: 2019-09-26 15:07:56
 */
import React, { PureComponent } from 'react'
import { Row, Col } from 'antd'
import '../style/index.less'

export default class FooterComponent extends PureComponent {
  render() {
    const { customerName, settlementUser } = this.props
    return (
      <section className={'footer'}>
        <Row className={'group'}>
          <Col span={6} className={'item'}>中奖人：{customerName || ''}</Col>
          <Col span={6} className={'item'}>核销人：{settlementUser || ''}</Col>
          <Col span={6} className={'item'}>店长：</Col>
          <Col span={6} className={'item'}>财务：</Col>
        </Row>
      </section>
    )
  }
}
