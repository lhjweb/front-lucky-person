/*
 * @Description: 活动成本统计 - 打印(头部)
 * @company: YH
 * @Author: chengcheng
 * @Date: 2019-09-20 17:37:08
 * @LastEditors: chengcheng
 * @LastEditTime: 2019-09-25 18:14:37
 */
import React from 'react'
// import { Row, Col } from 'antd'
import { Descriptions } from 'antd'
import '../style/index.less'

export default class HeaderComponent extends React.Component {
  render() {
    const { shopName, machineNo, tradeNo, tradeMoney, submitTime, actName, customerName, customerPhone, remark } = this.props

    return (
      <section className={'header'}>
        <Descriptions>
          <Descriptions.Item className={'item'} label="门店名称">{shopName || ''}</Descriptions.Item>
          <Descriptions.Item className={'item'} label="机号">{machineNo || ''}</Descriptions.Item>
          <Descriptions.Item className={'item'} label="支付流水号">{tradeNo || ''}</Descriptions.Item>
          <Descriptions.Item className={'item'} label="消费金额">{tradeMoney || ''}</Descriptions.Item>
          <Descriptions.Item className={'item'} label="抽奖时间">{submitTime || ''}</Descriptions.Item>
          <Descriptions.Item className={'item'} label="参与活动">{actName || ''}</Descriptions.Item>
          <Descriptions.Item className={'item'} label="中奖人">{customerName || ''}</Descriptions.Item>
          <Descriptions.Item className={'item'} label="联系方式">{customerPhone || ''}</Descriptions.Item>
          <Descriptions.Item className={'item'} label="备注">{remark || ''}</Descriptions.Item>
        </Descriptions>
      </section>
    )
  }
}
