/*
 * @Description: 活动成本统计 - 打印
 * @company: YH
 * @Author: chengcheng
 * @Date: 2019-09-20 17:37:08
 * @LastEditors: chengcheng
 * @LastEditTime: 2019-09-26 15:20:42
 */

import React from 'react'
import {
  message,
  Button
} from 'antd'
import { Header, PageTable, Footer } from './components'
import { getQueryString } from '@utils'
import {
  API_GET_QUERY_REPORT_GET_GOD_RECORD_DETAIL
} from '@api'
import http from '@http'
import moment from 'moment'
import './style/index.less'

export default class PrizeCostPrint extends React.Component {
  constructor() {
    super()
    this.state = {
      id: getQueryString('id'), // id
      currentTime: moment().format('YYYY-MM-DD HH:mm:ss'),
      dataSource: [], // 表格中的数据
      loading: false // 列表loading
    }
  }

  componentWillMount() {
    try {
      this._fetchOrderNo()
    } catch (err) {
      console.error(err)
    }
  }

  /* 打印 */
  handlePrint = () => {
    if (window.print) {
      window.print()
    } else {
      document.execCommand('print')
    }
  }

  /* 关闭窗口 */
  _closeWindow = () => window.setTimeout(() => window.close(), 1000)

  /* 获取订单详情 */
  _fetchOrderNo = () => {
    const { id } = this.state
    if (!id) return (0, this._closeWindow)(message.error('用户id错误'))
    this.setState({ loading: true })

    let api = API_GET_QUERY_REPORT_GET_GOD_RECORD_DETAIL

    // 发送请求
    http.get(api, null, { urlParams: [id] }).then(res => {
      this.setState({ dataSource: { ...res }, loading: false })
    })
  }

  render() {
    const { dataSource: { actName, shopCode }, currentTime } = this.state
    return (
      <section className={'outbound-order'}>
        <Button className={'noprint'} type={'primary'} onClick={this.handlePrint}>打印</Button>
        <div className={'title'}>{actName} 领奖核销单</div>
        <div className={'printInfo'}>
          <div>打印时间：{currentTime}</div>
          <div>门店ID: {shopCode}</div>
        </div>
        <Header {...this.state.dataSource} />
        <PageTable {...this.state} />
        <Footer {...this.state.dataSource} />
      </section>
    )
  }
}
