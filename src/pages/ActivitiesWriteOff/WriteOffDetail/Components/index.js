/*
 * @company: YH
 * @Author: chengcheng
 * @Date: 2019-09-25 15:12:10
 * @LastEditors: chengcheng
 * @LastEditTime: 2019-09-25 16:25:49
 */
export { default as DetailHeader } from './DetailHeader'
export { default as DetailBody } from './DetailBody'
