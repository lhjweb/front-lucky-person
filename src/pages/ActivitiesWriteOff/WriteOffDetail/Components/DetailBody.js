/*
 * @Description: 活动核销 - 查看 - 中奖内容
 * @company: YH
 * @Author: chengcheng
 * @Date: 2019-09-25 16:21:52
 * @LastEditors: chengcheng
 * @LastEditTime: 2019-09-25 16:40:59
 */
import React from 'react'
import { Descriptions } from 'antd'

export default class DetilBody extends React.Component {
  state = {}

  render() {
    const { prizeItemName, prizeName, taxFee } = this.props.headData
    return (
      <div className="detail-body">
        <Descriptions title="获奖奖品清单" layout="vertical" bordered size="small">
          <Descriptions.Item label="奖项名称">{prizeItemName || ''}</Descriptions.Item>
          <Descriptions.Item label="奖品">{prizeName || ''}</Descriptions.Item>
          <Descriptions.Item label="税费">{taxFee || ''}</Descriptions.Item>
        </Descriptions>
      </div>
    )
  }
}
