/*
 * @Description: 活动核销 - 查看（头部）
 * @company: YH
 * @Author: chengcheng
 * @Date: 2019-09-25 15:46:06
 * @LastEditors: chengcheng
 * @LastEditTime: 2019-09-25 16:06:17
 */
import React from 'react'
import { Descriptions } from 'antd'
import '../style/index.less'

export default class DetailHeader extends React.Component {
  state = {}

  render() {
    const { shopName, machineNo, tradeNo, tradeMoney, submitTime, actName, customerName, customerPhone, remark } = this.props.headData
    return (
      <div className="detail-header">
        <Descriptions>
          <Descriptions.Item label="门店名称">{shopName || ''}</Descriptions.Item>
          <Descriptions.Item label="机号">{machineNo || ''}</Descriptions.Item>
          <Descriptions.Item label="支付流水号">{tradeNo || ''}</Descriptions.Item>
          <Descriptions.Item label="消费金额">{tradeMoney || ''}</Descriptions.Item>
          <Descriptions.Item label="抽奖时间">{submitTime || ''}</Descriptions.Item>
          <Descriptions.Item label="参与活动">{actName || ''}</Descriptions.Item>
          <Descriptions.Item label="中奖人">{customerName || ''}</Descriptions.Item>
          <Descriptions.Item label="联系方式">{customerPhone || ''}</Descriptions.Item>
          <Descriptions.Item label="备注">{remark || ''}</Descriptions.Item>
        </Descriptions>
      </div>
    )
  }
}
