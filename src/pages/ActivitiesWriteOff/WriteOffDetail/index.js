/*
 * @Description: 活动核销 - 查看
 * @company: YH
 * @Author: chengcheng
 * @Date: 2019-09-25 15:10:20
 * @LastEditors: chengcheng
 * @LastEditTime: 2019-09-25 16:26:14
 */
import React from 'react'
import { Button } from 'antd'
import { Bread } from '@comp'
import {
  API_GET_QUERY_REPORT_GET_GOD_RECORD_DETAIL
} from '@api'
import http from '@http'
import { DetailHeader, DetailBody } from './Components'

export default class WriteOffDetail extends React.Component {
  state = {
    headData: {} // 头部表单中的数据
  }

  componentDidMount() {
    const { id } = this.props.location.state
    this.setState({ id }, () => this._fetchData(id))
  }

  /* 请求页面数据 */
  _fetchData = id => {
    http.get(API_GET_QUERY_REPORT_GET_GOD_RECORD_DETAIL, null, { urlParams: [id] }).then(res => {
      this.setState({
        headData: { ...res }
      })
    }).catch(err => console.log(err))
  }

  render() {
    const { headData } = this.state
    return (
      <div className="write-off-detail">
        <Bread
          list={['活动核销', '查看']}
          right={[<Button onClick={() => window.history.back(-1)}>返回</Button>]}
        />
        <DetailHeader
          headData={headData}
        />
        <DetailBody
          headData={headData}
        />
      </div>
    )
  }
}
