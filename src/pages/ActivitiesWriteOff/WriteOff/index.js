/*
 * @Author: caoyukang
 * @Date: 2019-09-19 11:45:18
 * @Last Modified by: lihongjie
 * @Last Modified time: 2020-01-17 10:49:19
 */
import React from 'react'
import { Bread } from '@comp'
import { Modal } from 'antd'
import http from '@http'
import {
  PageForm,
  PageTable,
  PageTool
} from './Componments'
import {
  queryGodRecord
} from './api/ajax'
import { API_POST_EXPORT_LIST_GOD_RECORD } from '@api'
import { defaultTableCfg } from './api/config'

const { page, size, total } = defaultTableCfg
class Main extends React.Component {
    state={
      searchResult: [], // 表格数据
      page, // 当前页码
      size, // 数量多少
      total, // 总数据
      viewPopIsVisible: false, // 查看pop
      lookResult: {}, // 查看接口返回
      searchParams: {} // 查询条件
    }

    componentDidMount() {
      this._refreshTableData()
    }

    // 刷新table数据
    _refreshTableData = (values = {}) => {
      const { page, size } = this.state
      const params = { ...values, page, size }
      queryGodRecord(params).then(({ result: searchResult = [], totalNum }) => {
        this.setState({ searchResult, total: totalNum })
      })
    }

    // 点击下一页或者上一页
    pageSizeChange = (page, size) => {
      this.setState({
        page,
        size
      }, this._refreshTableData)
    }

    // 查询table数据
    handleFormSearch = values => {
      this.setState({
        page: 1,
        size: 10,
        searchParams: values
      }, () => this._refreshTableData(values))
    }

    handleLook = ({ id }) => { // 查看
      this.props.history.push({
        pathname: '/activitiesWriteOff/writeOffDetial',
        state: { id }
      })
    }

    /* 导出按钮 */
  handleExport = () => {
    Modal.confirm({
      title: '提示',
      content: <span>您确认导出 <b className="red">所有数据</b> ?</span>,
      onOk: () => {
        const { page, size, searchParams } = this.state
        const params = { ...searchParams, page, size }
        http.post(API_POST_EXPORT_LIST_GOD_RECORD, params).then(res => this._refreshTableData())
      }
    })
  }

    // 打印按钮
    handlePrint = ({ id }) => {
      window.open(`/picking/writeOffPrint?id=${id}`, 'newWindow', 'height=500, width=1300, top=0, left=0, toolbar=no, menubar=no, scrollbars=no, resizable=no, location=no, status=no')
    }

    render() {
      const {
        searchResult,
        page,
        size,
        total
      } = this.state
      return <>
         <Bread {...this.props} />
         <PageForm
           handleFormSearch={this.handleFormSearch}
         />
         <PageTool
           total={total}
           handleExport={this.handleExport}
         />
         <PageTable
           page={page}
           size={size}
           total={total}
           searchResult={searchResult}
           pageSizeChange={this.pageSizeChange}
           handlePrint={this.handlePrint}
           handleLook={this.handleLook}
         />
    </>
    }
}
export default Main
