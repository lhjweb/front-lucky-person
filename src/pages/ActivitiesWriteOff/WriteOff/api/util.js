/*
 * @Author: caoyukang
 * @Date: 2019-09-24 17:19:11
 * @Last Modified by: caoyukang
 * @Last Modified time: 2019-09-24 17:23:55
 */
export const setUrl = (apiList, id) => { // 深拷贝设置值
  let list = JSON.parse(JSON.stringify(apiList))
  let path = list[1]
  list[1] = `${path}/${id}`
  return list
}
