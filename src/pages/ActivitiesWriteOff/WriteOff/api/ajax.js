/*
 * @Author: caoyukang
 * @Date: 2019-09-19 11:58:27
 * @Last Modified by: caoyukang
 * @Last Modified time: 2019-09-25 10:30:35
 */
import http from '@http'
import { setUrl } from './util'

import {
  API_POST_QUERY_REPORT_QUERY_GOD_RECORD,
  API_GET_QUERY_REPORT_GET_GOD_RECORD_DETAIL,
  API_POST_QUERY_REPORT_SETTLEMENT,
  AP_POST_QUERY_UN_SUBMIT_PRIZE
} from '@api'

export const queryGodRecord = (params = {}) =>  // 活动核销 - 查询
  http.post(API_POST_QUERY_REPORT_QUERY_GOD_RECORD, params)

export const getGodRecordDetail = (id = 0) =>  // 活动核销 - 详情
  http.get(setUrl(API_GET_QUERY_REPORT_GET_GOD_RECORD_DETAIL, id), { })

export const settlement = (params = {}, id = 0) => { // 活动核销 - 核销
  http.post(setUrl(API_POST_QUERY_REPORT_SETTLEMENT, id), params)
}

export const submitPrize = (params = {}) => { // 奖项设置 - 查询
  return http.post(AP_POST_QUERY_UN_SUBMIT_PRIZE, params)
}
