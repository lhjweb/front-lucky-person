/*
 * @Author: caoyukang
 * @Date: 2019-09-19 11:45:24
 * @Last Modified by: lihongjie
 * @Last Modified time: 2020-01-17 10:11:46
 */
import React from 'react'
export const tableColumns = [ // 表头
  {
    title: '序号',
    width: 50,
    dataIndex: 'index',
    render: (text, record, index) => index + 1
  }, {
    title: '活动编码',
    width: 180,
    dataIndex: 'actCode'
  }, {
    title: '活动名称',
    width: 180,
    dataIndex: 'actName'
  }, {
    title: '门店编号',
    width: 180,
    dataIndex: 'shopCode'
  }, {
    title: '流水号',
    width: 180,
    dataIndex: 'checkedNo'
  }, {
    title: '中奖人',
    width: 100,
    dataIndex: 'customerName'
  }, {
    title: '联系方式',
    width: 150,
    dataIndex: 'customerPhone'
  }, {
    title: '奖项',
    width: 60,
    dataIndex: 'prizeItemName'
  }, {
    title: '抽奖时间',
    width: 150,
    dataIndex: 'submitTime'
  }, {
    title: '销核时间',
    width: 150,
    dataIndex: 'settlementTime'
  }, {
    title: '销核人',
    dataIndex: 'settlementUserName',
    width: 150
  }, {
    title: '备注',
    width: 180,
    dataIndex: 'remark',
    render(text) {
      return <span className="ellipsis">{text}</span>
    }
  }
]

export const defaultTableCfg = {
  page: 1, // 初始化页码
  size: 10, // 数量多少
  total: 0 // 总数据
}
