/*
 * @Author: caoyukang
 * @Date: 2019-09-19 11:45:09
 * @Last Modified by: caoyukang
 * @Last Modified time: 2019-09-30 11:33:12
 */
import React from 'react'
import { Table } from 'antd'
import _ from 'lodash'
import { getPagination } from '@yhUtil'

import { tableColumns } from '../api/config'

export default ({
  searchResult,
  page: current,
  size: pageSize,
  total,

  handleLook,
  handlePrint,
  pageSizeChange: onChange
}) => <section className="page-table">
  <Table
    columns={[
      ...tableColumns,
      {
        title: '操作',
        fixed: 'right',
        width: 80,
        indexData: 'option',
        render(selectedRowList) {
          return <>
            <a className="mr10" onClick={() => handleLook(selectedRowList)} >查看</a>
            <a onClick={() => handlePrint(selectedRowList)}>打印</a>
          </>
        }
      }
    ]}
    scroll={{ x: _.sumBy(tableColumns, 'width') }}
    rowKey={record => record.id}
    loading={false}
    dataSource={searchResult}
    pagination={getPagination({
      total,
      current,
      pageSize,
      onChange
    })}
    rowKey={record => record.id || Math.random()}
  />
</section>
