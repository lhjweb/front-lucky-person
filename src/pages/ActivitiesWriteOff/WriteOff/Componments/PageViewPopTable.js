/*
 * @Author: caoyukang
 * @Date: 2019-09-19 11:44:57
 * @Last Modified by: caoyukang
 * @Last Modified time: 2019-09-30 11:26:53
 */
import React from 'react'
import { Table } from 'antd'
// import { getPagination } from '@yhUtil'

import { popTableColumns } from '../api/config'

export default ({
  pageSizeChange
}) => <section className="page-table">
  <Table
    columns={popTableColumns}
    loading={false}
    rowKey={record => record.id}
    dataSource={[]}
  />
</section>
