/*
 * @Author: caoyukang
 * @Date: 2019-09-19 11:45:12
 * @Last Modified by: lihongjie
 * @Last Modified time: 2020-01-17 13:48:36
 */
import React from 'react'
import { Form, message } from 'antd'
import {
  CollapsibleFormBox,
  CommonSelect,
  CommonInput,
  ActivityName,
  DatePicker
} from '@comp'
import moment from 'moment'
import {
  AP_POST_QUERY_UN_SUBMIT_PRIZE
} from '@api'
import http from '@http'
@Form.create()
class PageForm extends React.Component {
  state = {
    prizeItemNameList: [] // 奖项名称列表
  }

  handleSearch = () => { // 查询table数据
    const { form: { validateFields }, handleFormSearch } = this.props
    validateFields((err, { actName, actCode, shopCode, customerPhone, checkedNo, settlementTime, ...values }) => {
      if (err) return message.error(err)
      actCode = actCode && actCode.replace(/(^\s*)|(\s*$)/g, '')
      shopCode = shopCode && shopCode.replace(/(^\s*)|(\s*$)/g, '')
      customerPhone = customerPhone && customerPhone.replace(/(^\s*)|(\s*$)/g, '')
      checkedNo = checkedNo && checkedNo.replace(/(^\s*)|(\s*$)/g, '')
      settlementTime = settlementTime ? moment(settlementTime).format('YYYY-MM-DD') : ''
      // 活动名称
      actName = actName ? actName.split(' ')[1] : ''
      // 整合参数
      const queryParams = { ...values, actCode, shopCode, customerPhone, checkedNo, settlementTime }
      // 去除空项
      Object.keys(queryParams).forEach(item => {
        !queryParams[item] && delete queryParams[item]
      })
      handleFormSearch({ actName, ...queryParams })
    })
  }

  /* 活动编码变化时搜索奖项列表 */
  handleActCodeBlur = e => {
    const value = e.target.value || ''
    // _.debounce(this.reqPrizeItemNameList(value), 500) // 在此处添加防抖函数
    this.reqPrizeItemNameList(value)
  }

  /* 请求奖项列表数据 */
  reqPrizeItemNameList = value => {
    http.post(AP_POST_QUERY_UN_SUBMIT_PRIZE, { actCode: value }).then(res => {
      // 如果有返回结果，将结果组成奖项名称列表
      if ((res || []).length) {
        this.setState({ prizeItemNameList: res.map(item => ({ code: item.prizeItemNo, value: item.prizeItemName })) })
      }
    })
  }

  // 重置
  handleReset = () => {
    this.props.form.resetFields()
    this.setState({
      prizeItemNameList: null
    })
  }

  render() {
    const { form: { getFieldDecorator, setFieldsValue, getFieldValue } } = this.props
    return <Form className="page-form">
      <CollapsibleFormBox
        onSearch={this.handleSearch}
        onReset={this.handleReset}
      >
        <div>
          <CommonInput
            getFieldDecorator={getFieldDecorator}
            label="活动编码"
            decorator="actCode"
            placeholder="请输入活动编码"
            allowClear={true}
            onBlur={this.handleActCodeBlur}
          />
          <ActivityName
            getFieldDecorator={getFieldDecorator}
          />
          <CommonInput
            getFieldDecorator={getFieldDecorator}
            decorator="shopCode"
            label="门店编号"
            allowClear={true}
            placeholder="请输入门店编号"
          />
          <DatePicker
            getFieldDecorator={getFieldDecorator}
            setFieldsValue={setFieldsValue}
            getFieldValue={getFieldValue}
            label="核销时间"
            placeholder="核销时间"
            decorator="settlementTime"
          />
          <CommonInput
            getFieldDecorator={getFieldDecorator}
            label="联系方式"
            decorator="customerPhone"
            placeholder="请输入联系方式"
            allowClear={true}
          />
          <CommonSelect
            getFieldDecorator={getFieldDecorator}
            label="奖项"
            list={this.state.prizeItemNameList}
            decorator="prizeItemNo"
            placeholder="请选择奖项"
            onChange={() => {}}
          />
          <CommonInput
            getFieldDecorator={getFieldDecorator}
            decorator="checkedNo"
            label="流水号"
            allowClear={true}
            placeholder="请输入流水号"
          />
        </div>
      </CollapsibleFormBox>
    </Form>
  }
}

export default PageForm
