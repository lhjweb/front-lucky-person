/*
 * @Author: lihongjie
 * @Date: 2019-12-31 10:06:19
 * @Last Modified by: lihongjie
 * @Last Modified time: 2020-01-17 10:27:07
 */

import React from 'react'
import { Button, Icon } from 'antd'
import {
  API_POST_TAX_CATEGORY_EXPORT_ERROR
} from '@api'
import http from '@http'

export default class App extends React.PureComponent {
  /* 报错导出 确定按钮 */
  handleExprotError = () => http.post(API_POST_TAX_CATEGORY_EXPORT_ERROR).finally(this.props.reloadTableData)

  render() {
    const { total, handleExport } = this.props

    return (
      <section className="page-tool common__tool">
        <div className="fl">
          <span>共{total ? total : 0}条查询结果</span>
        </div>
        <div className="fr">
          <Button className="ml20" onClick={handleExport} disabled={!total}><Icon type="download" /> 导出</Button>
        </div>
      </section>
    )
  }
}
