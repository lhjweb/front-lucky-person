/*
 * @Author: lihongjie
 * @Date: 2019-09-19 15:44:36
 * @Last Modified by: lihongjie
 * @Last Modified time: 2019-10-16 19:27:02
 */

import React from 'react'
import { Table } from 'antd'
import { getPagination } from '@yhUtil'
import { LongText } from '@comp'
import _ from 'lodash'
import NP from 'number-precision'

class PageTable extends React.PureComponent {
  state = {
    isShowEditNewProducyModal: false
  }
  total=(x, y) => {
    let num = x && y && y !== 0 ? NP.divide(x, y) : ''
    let num2 = NP.round(num, 2)
    let result = NP.times(num2, 100)
    return result
  }
  columns = [
    { width: 50, title: '序号', dataIndex: 'index', render: (text, record, index) => index + 1 },
    { width: 150, title: '活动名称', dataIndex: 'actName', render: (text) => <LongText title={text} width={150} >{text}</LongText> },
    { width: 100, title: '门店号', dataIndex: 'shopCode', render: (text) => <LongText title={text} width={100} >{text}</LongText> },
    { width: 100, title: '达标人数', dataIndex: 'passCount', render: (text) => <LongText title={text} width={100} >{text}</LongText> },
    { width: 100, title: '参与人数', dataIndex: 'accessCount', render: (text) => <LongText title={text} width={100} >{text}</LongText> },
    { width: 100, title: '参与率', dataIndex: 'key1', render: (text, record) =>
      <LongText width={100}>{ this.total(record.accessCount, record.passCount)}%</LongText>
    },
    { width: 120, title: '中奖人数', dataIndex: 'checkedCount', render: (text) => <LongText title={text} width={100}>{text}</LongText> },
    { width: 100, title: '中奖率', dataIndex: 'key2', render: (text, record) =>
      <LongText width={100}>{this.total(record.checkedCount, record.accessCount)}%</LongText>
    },
    { width: 80, title: '兑奖人数', dataIndex: 'takeCount', render: (text) => <LongText title={text} width={100}>{text}</LongText> },
    { width: 80, title: '兑奖率', dataIndex: 'key3', render: (text, record) =>
      <LongText width={100}>{this.total(record.takeCount, record.checkedCount)}%</LongText>
    }
  ]
  render() {
    const { searchResult = [], loading, total, page, size, pageSizeChange } = this.props

    // 分页
    const _pagination = getPagination({
      total,
      current: page,
      pageSize: size,
      onChange: pageSizeChange
    })

    // 列表
    let columns = [...this.columns]

    return (
      <section className="page-table">
        <Table
          scroll={{ x: _.sumBy(columns, 'width') }}
          rowKey={'id'}
          columns={columns}
          loading={loading}
          dataSource={searchResult}
          pagination={_pagination}
        />
      </section>
    )
  }
}

export default PageTable
