/*
 * @Author: lihongjie
 * @Date: 2019-09-23 11:20:12
 * @Last Modified by: lihongjie
 * @Last Modified time: 2019-09-23 11:36:39
 */

import React from 'react'
import { Form } from 'antd'
import {
  CollapsibleFormBox,
  ActivityName,
  CommonInput,
  StartAndEndDatePicker
} from '@comp'
import moment from 'moment'

/*
* @Form.create()
* 利用装饰器简化代码，将表单验证操作统一到一个装饰器中进行
* 原：Form.create()(App)
* */
@Form.create()
class PageForm extends React.PureComponent {
  /* 查询 */
  handleSearch = e => {
    e.preventDefault()
    const { form: { validateFields }, handleSearchResult } = this.props
    validateFields((error, values) => {
      if (!error) {
        let { actName } = values
        // 活动名称
        actName = actName ? actName.split(' ')[1] : ''
        // 整合参数
        const queryParams = { ...values, actName }
        // 去除空项
        Object.keys(queryParams).forEach(item => {
          !queryParams[item] && delete queryParams[item]
        })
        handleSearchResult({ ...queryParams })
      }
    })
  }

  /* 重置 */
  handleReset = () => this.props.form.resetFields()
  render() {
    const { form: { getFieldDecorator, setFieldsValue, getFieldValue } } = this.props
    return (
      <Form className="page-form">
        <CollapsibleFormBox
          onSearch={this.handleSearch}
          onReset={this.handleReset}
        >
          <div>
            <ActivityName
              getFieldDecorator={getFieldDecorator}/>
          </div>
        </CollapsibleFormBox>
      </Form>
    )
  }
}

export default PageForm
