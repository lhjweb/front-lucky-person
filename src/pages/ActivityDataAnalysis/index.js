/*
 * @Author: lihongjie
 * @Date: 2019-09-19 15:28:32
 * @Last Modified by: lihongjie
 * @Last Modified time: 2019-09-29 16:35:48
 */

import React from 'react'
import { message } from 'antd'
import { Bread } from '@comp'
import { PageForm, PageTable } from './Components'
import http from '@http'
import moment from 'moment'
import {
  API_POST_QUERY_ACCSS_ANALYSIS
} from '@api'

export default class App extends React.Component {
  state = {
    searchResult: [], // 获取查询结果
    searchParams: {}, // 搜索传值
    total: 0, // 查询总数
    page: 1, // 当前页码
    size: 10, // 当前分页数
    tableLoading: false // 列表loading
  }

  componentDidMount() {
    this._fetchSearchResult()
  }

  /* 获取查询结果 */
  _fetchSearchResult = () => {
    const { searchParams, page, size } = this.state

    const params = Object.assign({}, searchParams, { page, size })

    this.setState({
      tableLoading: true,
      total: 0,
      searchResult: []
    })
    http.post(API_POST_QUERY_ACCSS_ANALYSIS, params).then(res => {
      const { result, totalNum } = res
      this.setState({
        total: totalNum || 0,
        searchResult: result || []
      })
    }, err => console.error(err)).finally(this.resetSelectedRowKeys)
  }
  handleRefesh = () => {
    this.handleSearchResult()
  }

  /* 搜索查询结果 */
  handleSearchResult = (params = this.state.searchParams) => {
    console.log(params)
    if (!params) return false
    for (let idx in params) {
      if (!params[idx] || params[idx] === '') delete params[idx]
    }
    // 创建时间修改传递格式
    if (params.submitTime) {
      params.submitTime = moment(params.submitTime).format('YYYY-MM-DD')
    }

    this.setState({
      page: 1,
      searchParams: params
    }, this._fetchSearchResult)
  }

  /* ---------------- 表格操作 ----------------- */
  /* 页码和每页显示数量改变 */
  pageSizeChange = (current, pageSize) => {
    if (!current || !pageSize) return false
    this.setState({
      page: current,
      size: pageSize
    }, this._fetchSearchResult)
  }

  /* 更新查询结果 - 刷新表格数据 */
  reloadTableData = () => this.setState({ page: 1 }, this._fetchSearchResult)

  /* 重置选中的key */
  resetSelectedRowKeys = () => {
    this.setState({
      tableLoading: false
    })
  }
  /* ---------------- 表格操作 end ----------------- */
  render() {
    const {
      searchResult,
      tableLoading,
      total,
      page,
      size
    } = this.state

    return (
      <section className="stay-introduction-of-new-product-list">
        <Bread {...this.props}/>
        <PageForm
          handleSearchResult={this.handleSearchResult}
        />
        <PageTable
          loading={tableLoading}
          total={total}
          page={page}
          size={size}
          searchResult={searchResult}
          pageSizeChange={this.pageSizeChange}

        />
      </section>
    )
  }
}
