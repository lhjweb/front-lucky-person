/*
 * @Author: lihongjie
 * @Date: 2019-09-12 18:14:45
 * @Last Modified by: lihongjie
 * @Last Modified time: 2019-10-17 13:50:20
 */

import React from 'react'
import { Button, Modal, message } from 'antd'
import { Bread, PageTabs } from '@comp'
import { PageTable, AddNewOneModal, AddNewTwoModal, AddNewThrModal, AddNewFourModal, AddNewFiveModal } from './Components'
import http from '@http'
import {
  API_POST_QUERY_ACTIVITY_INFO,
  API_POST_QUERY_SHOP,
  AP_POST_QUERY_UN_SUBMIT_PRIZE,
  API_POST_QUERY_PRIZE_TIME,
  API_POST_QUERY_SHOP_RULE,
  API_GET_DELETE_ACTIVITY_INFO,
  API_GET_DELETE_SHOP,
  API_GET_DELETE_PRIZE,
  API_GET_DELETE_PRIZE_TIME,
  API_GET_DELETE_SHOP_RULE,
  API_POST_CONFIG_SUBMIT
} from '@api'
import { getQueryString } from '@utils'
import { resolve, reject } from 'q'

const userId = getQueryString('userId')
const isEdit = Boolean(getQueryString('isEdit'))// 按钮是否可编辑
const actCode = getQueryString('actCode')
export default class App extends React.Component {
  state = {
    searchResult: [], // 获取查询结果
    total: 0, // 查询总数
    page: 1, // 当前页码
    size: 100, // 当前分页数
    tableLoading: false, // 列表loading
    // currentUrl: '', // 当前的api
    tabType: getQueryString('pageTab') || '1', // tabs：1-活动信息 2-活动门店 3-奖项设置  4-开奖时间 5-门店奖项
    isShowAddOneModal: false, // 1-是否显示活动信息弹框
    isShowAddTwoModal: false, // 2-是否显示活动门店弹框
    isShowAddThrModal: false, // 3-是否显示奖项设置弹框
    isShowAddFourModal: false, // 4-是否显示开奖时间弹框
    isShowAddFiveModal: false, // 5-是否显示门店奖项
    editData: null, // 编辑的数据
    isEditble: false// 判断新增，编辑所调接口不同

  }

  componentDidMount() {
    this.handleRefesh()
  }
  /* 活动信息查询*/
  handleQuestActivity = () => {
    this.setState({
      tableLoading: true,
      total: 0,
      searchResult: []
    })
    http.post(API_POST_QUERY_ACTIVITY_INFO, { userId, actCode }).then(res => {
      this.setState({
        tableLoading: false,
        searchResult: res || []
      })
    }), err => this.setState({ TableLoading: false })
  }
  /* 根据不同的tabType调取不同的接口*/
  fetchSelectTab = () => {
    const { tabType } = this.state
    const urlMap = {
      1: API_POST_QUERY_ACTIVITY_INFO,
      2: API_POST_QUERY_SHOP,
      3: AP_POST_QUERY_UN_SUBMIT_PRIZE,
      4: API_POST_QUERY_PRIZE_TIME,
      5: API_POST_QUERY_SHOP_RULE
    }
    const currentUrl = urlMap[tabType] || urlMap[1]
    this.setState({
      tableLoading: true
    })
    http.post(currentUrl, { userId, actCode }).then(res => {
      this.setState({
        tableLoading: false,
        searchResult: res || []
      })
    })
  }
  handleRefesh = () => {
    if (this.state.tabType === '1') {
      this.handleQuestActivity()
    }
  }
  /* tabs 切换 */
  handleTabsChange = (key = this.state.tabType) => {
    this.setState({
      tabType: key
    }, () => {
      this.fetchSelectTab()
    })
  }
  /* 新增按钮 */
  handleCreated = () => {
    const { tabType } = this.state
    let url = API_POST_QUERY_ACTIVITY_INFO
    if (tabType === 1) {
      this.setState({
        isShowAddOneModal: false
      })
    } else if (tabType === '2') {
      http.post(url, { userId, actCode }).then(res => {
        this.setState({
          isEditble: false,
          isShowAddTwoModal: true,
          editData: res[0]
        })
      })
    } else if (tabType === '3') {
      http.post(url, { userId, actCode }).then(res => {
        this.setState({
          isEditble: false,
          isShowAddThrModal: true,
          editData: res[0]
        })
      })
    } else if (tabType === '4') {
      http.post(url, { userId, actCode }).then(res => {
        this.setState({
          isEditble: false,
          isShowAddFourModal: true,
          editData: res[0]
        })
      })
    } else if (tabType === '5') {
      http.post(url, { userId, actCode }).then(res => {
        this.setState({
          isEditble: false,
          isShowAddFiveModal: true,
          editData: res[0]
        })
      })
    }
  }
  /** 提交 */
  handleSubmit =() => {
    Modal.confirm({
      title: '提示',
      content: '提交之后将跳转到活动配置页?',
      okText: '确定',
      cancelText: '取消',
      onOk: () => new Promise((resolve, reject) => {
        http.post(API_POST_CONFIG_SUBMIT, {}).then(res => {
          message.success('提交成功')
          this.props.history.push('/activityManage/activityResets')
        }).then(resolve).catch(reject)
      })
    })
  }
  /* 1-编辑 */
  handleEditOne = (record) => {
    this.setState({
      isShowAddOneModal: true,
      editData: record
    })
  }
  // /* 1-删除 */
  // handleDeleteOne =(id) => {
  //   Modal.confirm({
  //     title: '操作确认',
  //     content: '确定删除吗？',
  //     okText: '确定',
  //     cancelText: '取消',
  //     onOk: () => {
  //       http.get(API_GET_DELETE_ACTIVITY_INFO, {}, { urlParams: [id] }).then(res => {
  //         this.handleQuestActivity()
  //       }, err => console.error(err))
  //     }
  //   })
  // }
  /* 2-编辑 */
  handleEditTwo = (record) => {
    this.setState({
      isShowAddTwoModal: true,
      isEditble: true,
      editData: record
    })
  }
  /* 2-删除 */
  handleDeleteTwo =(id) => {
    Modal.confirm({
      title: '操作确认',
      content: '确定删除吗？',
      okText: '确定',
      cancelText: '取消',
      onOk: () => new Promise((resolve, reject) => {
        http.get(API_GET_DELETE_SHOP, {}, { urlParams: [id] }).then(res => {
          this.fetchSelectTab()
        }, err => console.error(err)).then(resolve).catch(reject)
      })
    })
  }
    /* 3-编辑 */
    handleEditThr = (record) => {
      this.setState({
        isShowAddThrModal: true,
        isEditble: true,
        editData: record
      })
    }
    /* 3-删除 */
    handleDeleteThr =(id) => {
      Modal.confirm({
        title: '操作确认',
        content: '确定删除吗？',
        okText: '确定',
        cancelText: '取消',
        onOk: () => new Promise((resolve, reject) => {
          http.get(API_GET_DELETE_PRIZE, {}, { urlParams: [id] }).then(res => {
            this.fetchSelectTab()
          }, err => console.error(err)).then(resolve).catch(reject)
        })
      })
    }
    /* 4-编辑 */
    handleEditFour = (record) => {
      this.setState({
        isShowAddFourModal: true,
        isEditble: true,
        editData: record
      })
    }
    /* 4-删除 */
    handleDeleteFour =(id) => {
      Modal.confirm({
        title: '操作确认',
        content: '确定删除吗？',
        okText: '确定',
        cancelText: '取消',
        onOk: () => new Promise((resolve, reject) => {
          http.get(API_GET_DELETE_PRIZE_TIME, {}, { urlParams: [id] }).then(res => {
            this.fetchSelectTab()
          }, err => console.error(err)).then(resolve).catch(reject)
        })
      })
    }
    /* 5-编辑 */
    handleEditFive = (record) => {
      this.setState({
        isShowAddFiveModal: true,
        isEditble: true,
        editData: record
      })
    }
    /* 5-删除 */
    handleDeleteFive =(id) => {
      Modal.confirm({
        title: '操作确认',
        content: '确定删除吗？',
        okText: '确定',
        cancelText: '取消',
        onOk: () => new Promise((resolve, reject) => {
          http.get(API_GET_DELETE_SHOP_RULE, {}, { urlParams: [id] }).then(res => {
            this.fetchSelectTab()
          }, err => console.error(err)).then(resolve).catch(reject)
        })
      })
    }
  /* 关闭弹框 */
  handleCancel = () => this.setState({ isShowAddOneModal: false, isShowAddTwoModal: false, isShowAddThrModal: false, isShowAddFourModal: false, isShowAddFiveModal: false, editData: null })
  /* ---------------- 表格操作 ----------------- */

  /* ---------------- 表格操作 end ----------------- */

  render() {
    const {
      tableLoading,
      total,
      page,
      size,
      tabType,
      searchResult,
      isShowAddOneModal,
      isShowAddTwoModal,
      isShowAddThrModal,
      isShowAddFourModal,
      isShowAddFiveModal,
      editData,
      isEditble
    } = this.state
    // tab分类
    const tabPanes = [{ key: '1', tab: '活动信息' }, { key: '2', tab: '活动门店' }, { key: '3', tab: '奖项设置' }, { key: '4', tab: '开奖时间' }, { key: '5', tab: '门店奖项' }]
    const operations = <div>
      {isEdit ? <Button onClick={this.handleCreated} style={{ width: '80px', marginRight: '10px' }} type="primary" disabled={tabType === '1'}> 新增 </Button> : <Button disabled type="primary" style={{ width: '80px' }}>新增</Button>}
      {isEdit ? <Button onClick={this.handleSubmit}style={{ width: '80px' }} type="primary" disabled={!searchResult.length}> 提交 </Button> : <Button disabled style={{ width: '80px' }}>提交</Button>}
    </div>

    return (
      <section className="stay-introduction-of-new-product-list">
        <Bread list={['活动管理', '活动配置详情']}/>
        <PageTabs
          tabPanes={tabPanes}
          disabled={tableLoading}
          activeKey={tabType}
          type="card"
          onChange={this.handleTabsChange}
          tabBarExtraContent={operations}
        />
        <PageTable
          loading={tableLoading}
          total={total}
          page={page}
          size={size}
          searchResult={searchResult}
          tabType={tabType}
          isEdit={isEdit}
          handleEditOne={this.handleEditOne}
          handleDeleteOne={this.handleDeleteOne}
          handleEditTwo={this.handleEditTwo}
          handleDeleteTwo={this.handleDeleteTwo}
          handleEditThr={this.handleEditThr}
          handleDeleteThr={this.handleDeleteThr}
          handleEditFour={this.handleEditFour}
          handleDeleteFour={this.handleDeleteFour}
          handleEditFive={this.handleEditFive}
          handleDeleteFive={this.handleDeleteFive}
        />
        {
          isShowAddOneModal && <AddNewOneModal
            visible={isShowAddOneModal}
            handleCancel={this.handleCancel}
            handleRefesh={this.handleRefesh}
            editData={editData}
            tabType={tabType}
          />
        }
        {
          isShowAddTwoModal && <AddNewTwoModal
            visible={isShowAddTwoModal}
            handleCancel={this.handleCancel}
            fetchSelectTab={this.fetchSelectTab}
            editData={editData}
            tabType={tabType}
            isEditble={isEditble}
          />
        }
        {
          isShowAddThrModal && <AddNewThrModal
            visible={isShowAddThrModal}
            handleCancel={this.handleCancel}
            fetchSelectTab={this.fetchSelectTab}
            editData={editData}
            tabType={tabType}
            isEditble={isEditble}
          />
        }
        {
          isShowAddFourModal && <AddNewFourModal
            visible={isShowAddFourModal}
            handleCancel={this.handleCancel}
            fetchSelectTab={this.fetchSelectTab}
            editData={editData}
            tabType={tabType}
            isEditble={isEditble}
          />
        }
        {
          isShowAddFiveModal && <AddNewFiveModal
            visible={isShowAddFiveModal}
            handleCancel={this.handleCancel}
            fetchSelectTab={this.fetchSelectTab}
            editData={editData}
            tabType={tabType}
            isEditble={isEditble}
          />
        }
      </section>
    )
  }
}
