/*
* Created by lihongjie on 2019/09/10
* */

import React, { Component } from 'react'
import { Form, message } from 'antd'
import { CommonSelect, YHModal, CommonInput, StartAndEndDatePicker } from '@comp'
import {
  API_POST_UPDATE_ACTIVITY_INFO
} from '@api'
import http from '@http'
import moment from 'moment'

const dateFormat = 'YYYY/MM/DD'
@Form.create()
class AddNewOneModal extends Component {
  constructor(props) {
    super(props)

    this.state = {
      isDisable: false, // 是否编辑弹框
      id: ''
    }
  }
  componentDidMount() {
    if (this.props.editData) {
      this.setState({
        isDisable: true
      }, this.initData)
    }
  }

  initData = () => {
    const { editData = {}, form: { setFieldsValue } } = this.props
    setFieldsValue({
      id: editData.id, // 活动ID
      actName: editData.actName,  // 活动名称
      startDate: moment(editData.startDate, dateFormat), // 开始时间
      endDate: moment(editData.endDate, dateFormat), // 结束时间
      minMoney: editData.minMoney,  // 门槛金额
      isDouble: editData.isDouble, // 是否翻倍
      maxTimes: editData.maxTimes// 最大抽奖次数
    })
  }
  // 新增/编辑弹框提交操作
  handleOk = () => new Promise((resolve, reject) => {
    const { form: { validateFields }, handleCancel } = this.props
    validateFields((error, values) => {
      if (error) return reject()
      let { startDate, endDate } = values
      startDate = moment(startDate).format('YYYY-MM-DD HH:mm:ss')
      endDate = moment(endDate).format('YYYY-MM-DD HH:mm:ss')
      let data = { ...values, startDate, endDate }
      http.post(API_POST_UPDATE_ACTIVITY_INFO, data).then(res => {
        message.success('修改成功')
        this.props.form.resetFields()
        this.props.handleRefesh()
      }).then(resolve).catch(reject)
      handleCancel()
    })
  })

  render() {
    const { isDisable } = this.state
    const disabledDate = (current) => current < moment().startOf('day')
    const { visible, handleCancel, form: { getFieldDecorator, setFieldsValue, getFieldValue } } = this.props
    return (
      <YHModal
        visible={visible}
        title={isDisable ? '编辑' : '新增'}
        maskClosable={false}
        width="400px"
        okText="提交"
        onOk={this.handleOk}
        onCancel={handleCancel}
      >
        <Form>
          <CommonInput
            getFieldDecorator={getFieldDecorator}
            disabled
            label="活动ID"
            decorator="id"
            rules={[{ required: true, message: '活动ID必填!' }]}
          />
          <CommonInput
            getFieldDecorator={getFieldDecorator}
            label="活动名称"
            rules={[{ required: true, message: '活动名称必填!' }]}
            decorator="actName"
          />
          <StartAndEndDatePicker
            getFieldDecorator={getFieldDecorator}
            setFieldsValue={setFieldsValue}
            getFieldValue={getFieldValue}
            format={dateFormat}
            disabledDate={disabledDate}
            rules={[{ required: true, message: '活动时间必填!' }]}
            startDecorator='startDate'
            endDecorator='endDate'
            label="活动时间"

          />
          <CommonInput
            getFieldDecorator={getFieldDecorator}
            label="门槛金额"
            rules={[{ required: true, message: '门槛金额必填!' }]}
            decorator="minMoney"
          />
          <CommonSelect
            getFieldDecorator={getFieldDecorator}
            label="是否翻倍"
            allowClear={false}
            decorator="isDouble"
            rules={[{ required: true, message: '是否翻倍必填!' }]}
            list={[
              { value: '否', code: 0 },
              { value: '是', code: 1 }
            ]}
            placeholder="请选择是否翻倍"
          />
          <CommonInput
            getFieldDecorator={getFieldDecorator}
            label="最大抽奖数"
            rules={[{ required: true, message: '最大抽奖数必填!' }]}
            decorator="maxTimes"
          />
        </Form>
      </YHModal>
    )
  }
}

export default AddNewOneModal
