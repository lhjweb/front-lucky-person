/*
 * @Author: lihongjie
 * @Date: 2019-09-18 16:17:14
 * @Last Modified by: lihongjie
 * @Last Modified time: 2019-10-09 17:36:53
 */

import React, { Component } from 'react'
import { Form, message } from 'antd'
import { YHModal, CommonInput, TimePicker } from '@comp'
import {
  API_POST_ADD_PRIZE_TIME, // 新增
  API_POST_UPDATE_PRIZE_TIME// 修改

} from '@api'
import http from '@http'
import moment from 'moment'

const TimeFormat = 'HH:mm'
@Form.create()
class AddNewFourModal extends Component {
  constructor(props) {
    super(props)

    this.state = {
      // isDisable: false, // 是否编辑弹框
      id: ''
    }
  }
  componentDidMount() {
    this.initData()
  }

  initData = () => {
    const { editData = {}, form: { setFieldsValue } } = this.props
    setTimeout(() => {
      setFieldsValue({
        id: editData.id, // 活动id
        actName: editData.actName, // 活动名称
        prizeItemNo: editData.prizeItemNo,  // 奖项编码
        prizeItemName: editData.prizeItemName, // 奖项名称
        startTime: editData.startTime ? moment(editData.startTime, TimeFormat) : null, // 开始时间
        endTime: editData.endTime ? moment(editData.endTime, TimeFormat) : null // 结束时间
      })
    }, 100)
  }
  // 新增/编辑 弹框提交操作
  handleOk = () => new Promise((resolve, reject) => {
    const { form: { validateFields }, handleCancel, editData } = this.props
    let url = ''
    if (this.props.isEditble) {
      // 编辑接口
      url = API_POST_UPDATE_PRIZE_TIME
    } else {
      // 新增接口
      url = API_POST_ADD_PRIZE_TIME
    }
    validateFields((error, values) => {
      if (error) return reject()
      let { startTime, endTime } = values
      startTime = startTime.format('HH:mm')
      endTime = endTime.format('HH:mm')
      let { actCode } = editData
      let data = { actCode, ...values, startTime, endTime }
      http.post(url, data).then(res => {
        message.success('修改成功')
        this.props.form.resetFields()
        this.props.fetchSelectTab()
      }).then(resolve).catch(reject)
      handleCancel()
    })
  })

  render() {
    const { visible, handleCancel, form: { getFieldDecorator, getFieldValue, setFieldsValue }, isEditble } = this.props
    return (
      <YHModal
        visible={visible}
        title={isEditble ? '编辑' : '新增'}
        maskClosable={false}
        width="400px"
        okText="提交"
        onOk={this.handleOk}
        onCancel={handleCancel}
      >
        <Form>
          <CommonInput
            getFieldDecorator={getFieldDecorator}
            label="活动ID"
            rules={[{ required: true, message: '活动ID必填!' }]}
            decorator="id"
            disabled
          />
          <CommonInput
            getFieldDecorator={getFieldDecorator}
            label="活动名称"
            disabled
            rules={[{ required: true, message: '活动名称必填!' }]}
            decorator="actName"
          />
          <CommonInput
            getFieldDecorator={getFieldDecorator}
            label="奖项编码"
            disabled={isEditble ? true : false}
            rules={[{ required: true, message: '奖项编码必填!' }]}
            decorator="prizeItemNo"
          />
          <CommonInput
            getFieldDecorator={getFieldDecorator}
            label="奖项名称"
            disabled={isEditble ? true : false}
            rules={[{ required: true, message: '奖项名称必填!' }]}
            decorator="prizeItemName"
          />
          <TimePicker
            getFieldDecorator={getFieldDecorator}
            getFieldValue={getFieldValue}
            setFieldsValue={setFieldsValue}
            format={TimeFormat}
            decorator='startTime'
            label="出奖开始时间"
          />
          <TimePicker
            getFieldDecorator={getFieldDecorator}
            getFieldValue={getFieldValue}
            setFieldsValue={setFieldsValue}
            format={TimeFormat}
            decorator='endTime'
            label="出奖结束时间"
          />
        </Form>
      </YHModal>
    )
  }
}

export default AddNewFourModal
