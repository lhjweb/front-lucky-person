/*
 * @Author: lihongjie
 * @Date: 2019-09-18 16:17:06
 * @Last Modified by: lihongjie
 * @Last Modified time: 2019-09-24 11:32:44
 */

import React, { Component } from 'react'
import { Form, message } from 'antd'
import { YHModal, CommonInput } from '@comp'
import {
  API_POST_ADD_SHOP, // 新增
  API_POST_UPDATE_SHOP// 修改

} from '@api'
import http from '@http'

@Form.create()
class AddNewTwoModal extends Component {
  constructor(props) {
    super(props)

    this.state = {
      // isDisable: false, // 是否编辑弹框
      id: ''
    }
  }
  componentDidMount() {
    this.initData()
  }

  initData = () => {
    const { editData = {}, form: { setFieldsValue } } = this.props
    setTimeout(() => {
      setFieldsValue({
        id: editData.id, // 活动id
        actName: editData.actName,  // 活动名称
        shopCode: editData.shopCode  // 门店号
      })
    }, 100)
  }
  // 新增/编辑 弹框提交操作
  handleOk = () => new Promise((resolve, reject) => {
    const { form: { validateFields }, handleCancel, editData } = this.props
    let url = ''
    if (this.props.isEditble) {
      url = API_POST_UPDATE_SHOP
    } else {
      url = API_POST_ADD_SHOP
    }
    validateFields((error, values) => {
      if (error) return reject()
      let { actCode } = editData
      let data = { ...values, actCode }
      http.post(url, data).then(res => {
        message.success('修改成功')
        this.props.form.resetFields()
        this.props.fetchSelectTab()
      }).then(resolve).catch(reject)
      handleCancel()
    })
  })

  render() {
    const { visible, handleCancel, form: { getFieldDecorator }, isEditble } = this.props
    return (
      <YHModal
        visible={visible}
        title={isEditble ? '编辑' : '新增'}
        maskClosable={false}
        width="400px"
        okText="提交"
        onOk={this.handleOk}
        onCancel={handleCancel}
      >
        <Form>
          <CommonInput
            getFieldDecorator={getFieldDecorator}
            label="活动ID"
            rules={[{ required: true, message: '活动ID必填!' }]}
            decorator="id"
            disabled
          />
          <CommonInput
            getFieldDecorator={getFieldDecorator}
            label="活动名称"
            disabled
            rules={[{ required: true, message: '活动名称必填!' }]}
            decorator="actName"
          />
          <CommonInput
            getFieldDecorator={getFieldDecorator}
            label="门店号"
            rules={[{ required: true, message: '门店号必填!' }]}
            decorator="shopCode"
          />
        </Form>
      </YHModal>
    )
  }
}

export default AddNewTwoModal
