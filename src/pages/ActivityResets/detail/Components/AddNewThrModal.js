/*
 * @Author: lihongjie
 * @Date: 2019-09-18 16:17:14
 * @Last Modified by: lihongjie
 * @Last Modified time: 2019-10-14 14:30:15
 */

import React, { Component } from 'react'
import { Form, message } from 'antd'
import { YHModal, CommonInput, CommonSelect } from '@comp'
import {
  API_POST_ADD_SUBMIT_PRIZE, // 新增
  API_POST_UPDATE_SUBMIT_PRIZE// 修改

} from '@api'
import http from '@http'

@Form.create()
class AddNewThrModal extends Component {
  constructor(props) {
    super(props)

    this.state = {
      // isDisable: false, // 是否编辑弹框
      id: ''
    }
  }
  componentDidMount() {
    this.initData()
  }

  initData = () => {
    const { editData = {}, form: { setFieldsValue } } = this.props
    setTimeout(() => {
      setFieldsValue({
        id: editData.id, // 活动id
        actName: editData.actName,  // 活动名称
        prizeItemNo: editData.prizeItemNo,  // 奖项编码
        prizeItemName: editData.prizeItemName, // 奖项名称
        prizeCount: editData.prizeCount, // 奖项数量
        isNeedRegisted: editData.isNeedRegisted // 是否需要登记
      })
    }, 100)
  }
  // 新增/编辑 弹框提交操作
  handleOk = () => new Promise((resolve, reject) => {
    const { form: { validateFields }, handleCancel, editData } = this.props
    let url = ''
    if (this.props.isEditble) {
      // 编辑接口
      url = API_POST_UPDATE_SUBMIT_PRIZE
    } else {
      // 新增接口
      url = API_POST_ADD_SUBMIT_PRIZE
    }
    validateFields((error, values) => {
      if (error) return reject()
      let { prizeItemNo, prizeCount } = values
      prizeItemNo = parseFloat(prizeItemNo)
      prizeCount = parseFloat(prizeCount)
      let data = { ...editData, ...values, prizeItemNo, prizeCount }
      http.post(url, data).then(res => {
        message.success('修改成功')
        this.props.form.resetFields()
        this.props.fetchSelectTab()
      }).then(resolve).catch(reject)
      handleCancel()
    })
  })

  render() {
    console.log(this.props.isEditble, '3')
    const { visible, handleCancel, form: { getFieldDecorator }, isEditble } = this.props
    return (
      <YHModal
        visible={visible}
        title={isEditble ? '编辑' : '新增'}
        maskClosable={false}
        width="400px"
        okText="提交"
        onOk={this.handleOk}
        onCancel={handleCancel}
      >
        <Form>
          <CommonInput
            getFieldDecorator={getFieldDecorator}
            label="活动ID"
            rules={[{ required: true, message: '活动ID必填!' }]}
            decorator="id"
            disabled
          />
          <CommonInput
            getFieldDecorator={getFieldDecorator}
            label="活动名称"
            disabled
            rules={[{ required: true, message: '活动名称必填!' }]}
            decorator="actName"
          />
          <CommonInput
            getFieldDecorator={getFieldDecorator}
            label="奖项编码"
            rules={[{ required: true, message: '奖项编码必填!' }]}
            decorator="prizeItemNo"
          />
          <CommonInput
            getFieldDecorator={getFieldDecorator}
            label="奖项名称"
            rules={[{ required: true, message: '奖项名称必填!' }]}
            decorator="prizeItemName"
          />
          <CommonInput
            getFieldDecorator={getFieldDecorator}
            label="奖项数量"
            rules={[{ required: true, message: '奖项数量必填!' }]}
            decorator="prizeCount"
          />
          <CommonSelect
            getFieldDecorator={getFieldDecorator}
            label="是否需要登记"
            allowClear={false}
            decorator="isNeedRegisted"
            rules={[{ required: true, message: '是否需要登记必填!' }]}
            list={[
              { value: '是', code: 1 },
              { value: '否', code: 0 }
            ]}
            placeholder="请选择是否需要登记"
          />
        </Form>
      </YHModal>
    )
  }
}

export default AddNewThrModal
