/**
 * Created by Zero on 2019/06/05
 */

import React from 'react'
import { Table } from 'antd'
import { LongText } from '@comp'
import _ from 'lodash'
import moment from 'moment'
import { getQueryString } from '@utils'

const styleGray = {
  color: 'gray'
}

const isEdit = Boolean(getQueryString('isEdit')) // 按钮是否可编辑
class PageTable extends React.PureComponent {
  constructor(props) {
    super(props)
    this.state = {
      columns: this.column1
    }
  }
  // 活动信息
  column1 = [
    { width: 50, title: '序号', dataIndex: 'index', render: (text, record, index) => index + 1 },
    { width: 50, title: '活动ID', dataIndex: 'id' },
    { width: 150, title: '活动名称', dataIndex: 'actName', render: (text) => <LongText title={text} width={150} >{text}</LongText> },
    { width: 180, title: '开始日期', dataIndex: 'startDate', render: (text) => <span>{moment(text).format('YYYY-MM-DD')}</span> },
    { width: 180, title: '结束日期', dataIndex: 'endDate', render: (text) => <span>{moment(text).format('YYYY-MM-DD')}</span> },
    { width: 100, title: '门槛金额', dataIndex: 'minMoney' },
    { width: 100, title: '是否翻倍', dataIndex: 'isDouble', render: (text) => text === 1 ? <span>是</span> : <span>否</span> },
    { width: 80, title: '最大抽奖数', dataIndex: 'maxTimes' },
    { width: 100, title: '操作', dataIndex: 'operation', className: 'hoverText', render: (text, record) => (
      <div>
        {isEdit ? <span>
          <a href="javascript:void(0)" onClick={() => this.props.handleEditOne(record)}>修改 </a>
        </span> : <span style={styleGray}> 修改 </span>}
        {/* {isEdit ? <span>
          <a href="javascript:void(0)" onClick={() => this.props.handleDeleteOne(record.id)}> 删除</a>
        </span> : <span style={styleGray}> 删除 </span>} */}
      </div>
    ) }
  ]
  // 活动门店
  column2 = [
    { width: 50, title: '序号', dataIndex: 'index', render: (text, record, index) => index + 1 },
    { width: 50, title: '活动ID', dataIndex: 'id' },
    { width: 150, title: '活动名称', dataIndex: 'actName', render: (text) => <LongText title={text} width={150} >{text}</LongText> },
    { width: 120, title: '门店号', dataIndex: 'shopCode' },
    { width: 100, title: '操作', dataIndex: 'operation', className: 'hoverText', render: (text, record) => (
      <div>
        {isEdit ? <span>
          <a href="javascript:void(0)" onClick={() => this.props.handleEditTwo(record)}>修改 </a>
        </span> : <span style={styleGray}> 修改 </span>}
        {isEdit ? <span>
          <a href="javascript:void(0)" onClick={() => this.props.handleDeleteTwo(record.id)}> 删除</a>
        </span> : <span style={styleGray}> 删除 </span>}
      </div>
    ) }
  ]
  // 奖项设置
  column3 = [
    { width: 50, title: '序号', dataIndex: 'index', render: (text, record, index) => index + 1 },
    { width: 80, title: '活动ID', dataIndex: 'id' },
    { width: 150, title: '活动名称', dataIndex: 'actName', render: (text) => <LongText title={text} width={150} >{text}</LongText> },
    { width: 100, title: '奖项', dataIndex: 'prizeItemNo' },
    { width: 180, title: '奖项名称', dataIndex: 'prizeItemName' },
    { width: 120, title: '奖项数量', dataIndex: 'prizeCount' },
    { width: 150, title: '是否需要登记', dataIndex: 'isNeedRegisted', render: (text) => text === 0 ? <span>否</span> : <span>是</span> },
    { width: 100, title: '操作', dataIndex: 'operation', className: 'hoverText', render: (text, record) => (
      <div>
        {isEdit ? <span>
          <a href="javascript:void(0)" onClick={() => this.props.handleEditThr(record)}>修改 </a>
        </span> : <span style={styleGray}> 修改 </span>}
        {isEdit ? <span>
          <a href="javascript:void(0)" onClick={() => this.props.handleDeleteThr(record.id)}> 删除</a>
        </span> : <span style={styleGray}> 删除 </span>}
      </div>
    ) }
  ]
  // 开奖时间
  column4 = [
    { width: 50, title: '序号', dataIndex: 'index', render: (text, record, index) => index + 1 },
    { width: 80, title: '活动ID', dataIndex: 'id' },
    { width: 150, title: '活动名称', dataIndex: 'actName', render: (text) => <LongText title={text} width={120} >{text}</LongText> },
    { width: 150, title: '奖项', dataIndex: 'prizeItemNo' },
    { width: 150, title: '奖项名称', dataIndex: 'prizeItemName' },
    { width: 180, title: '出奖开始日期', dataIndex: 'startTime' },
    { width: 180, title: '出奖结束日期', dataIndex: 'endTime' },
    { width: 100, title: '操作', dataIndex: 'operation', className: 'hoverText', render: (text, record) => (
      <div>
        {isEdit ? <span>
          <a href="javascript:void(0)" onClick={() => this.props.handleEditFour(record)}>修改 </a>
        </span> : <span style={styleGray}> 修改 </span>}
        {isEdit ? <span>
          <a href="javascript:void(0)" onClick={() => this.props.handleDeleteFour(record.id)}> 删除</a>
        </span> : <span style={styleGray}> 删除 </span>}
      </div>
    ) }
  ]
  // 门店奖项
  column5 = [
    { width: 50, title: '序号', dataIndex: 'index', render: (text, record, index) => index + 1 },
    { width: 80, title: '活动ID', dataIndex: 'id' },
    { width: 150, title: '活动名称', dataIndex: 'actName', render: (text) => <LongText title={text} width={150} >{text}</LongText> },
    { width: 100, title: '参与门店', dataIndex: 'shopCode' },
    { width: 80, title: '关联奖项', dataIndex: 'prizeItemNo' },
    { width: 100, title: '奖项名称', dataIndex: 'prizeItemName' },
    { width: 150, title: '开奖开始日期', dataIndex: 'startDate', render: (text) => <span>{moment(text).format('YYYY-MM-DD')}</span> },
    { width: 150, title: '开奖结束日期', dataIndex: 'endDate', render: (text) => <span>{moment(text).format('YYYY-MM-DD')}</span> },
    { width: 80, title: '出奖数量', dataIndex: 'prizeCount' },
    { width: 80, title: '中奖概率', dataIndex: 'checkedRate' },
    { width: 100, title: '开奖客流阈值', dataIndex: 'accessCountLimit' },
    { width: 150, title: '操作', dataIndex: 'operation', className: 'hoverText', render: (text, record) => (
      <div>
        {isEdit ? <span>
          <a href="javascript:void(0)" onClick={() => this.props.handleEditFive(record)}>修改 </a>
        </span> : <span style={styleGray}> 修改 </span>}
        {isEdit ? <span>
          <a href="javascript:void(0)" onClick={() => this.props.handleDeleteFive(record.id)}> 删除</a>
        </span> : <span style={styleGray}> 删除 </span>}
      </div>
    ) }
  ]

  componentWillReceiveProps(nextProps) {
    const { tabType } = nextProps
    const column = {
      1: this.column1,
      2: this.column2,
      3: this.column3,
      4: this.column4,
      5: this.column5
    }
    this.setState({
      columns: column[tabType]
    })
  }
  render() {
    const { loading, searchResult = [] } = this.props
    const { columns } = this.state
    return (
      <section className="page-table">
        <Table
          scroll={{ x: _.sumBy(columns, 'width') }}
          rowKey={'id'}
          columns={columns}
          loading={loading}
          pagination={false}
          dataSource={searchResult}
        />
      </section>
    )
  }
}

export default PageTable
