/*
 * @Author: lihongjie
 * @Date: 2019-09-18 16:17:14
 * @Last Modified by: lihongjie
 * @Last Modified time: 2019-09-25 10:49:42
 */

import React, { Component } from 'react'
import { Form, message } from 'antd'
import { YHModal, CommonInput, StartAndEndDatePicker } from '@comp'
import {
  API_POST_ADD_SHOP_RULE, // 新增
  API_POST_UPDATE_SHOP_RULE// 修改

} from '@api'
import http from '@http'
import moment from 'moment'

const dateFormat = 'YYYY/MM/DD'
@Form.create()
class AddNewFiveModal extends Component {
  constructor(props) {
    super(props)

    this.state = {
      // isDisable: false, // 是否编辑弹框
      id: ''
    }
  }
  componentDidMount() {
    this.initData()
  }

  initData = () => {
    const { editData = {}, form: { setFieldsValue } } = this.props
    setTimeout(() => {
      setFieldsValue({
        id: editData.id, // 活动id
        actName: editData.actName,  // 活动名称
        shopCode: editData.shopCode, // 参与门店
        prizeItemNo: editData.prizeItemNo, // 关联奖项
        prizeItemName: editData.prizeItemName, // 奖项名称
        startDate: moment(editData.startDate, dateFormat), // 开始时间
        endDate: moment(editData.endDate, dateFormat), // 结束时间
        prizeCount: editData.prizeCount, // 出奖数量
        checkedRate: editData.checkedRate, // 中奖概率
        accessCountLimit: editData.accessCountLimit // 开奖客流阈值

      })
    }, 100)
  }
  // 新增/编辑 弹框提交操作
  handleOk = () => new Promise((resolve, reject) => {
    const { form: { validateFields }, handleCancel, editData } = this.props
    let url = ''
    if (this.props.isEditble) {
      // 编辑接口
      url = API_POST_UPDATE_SHOP_RULE
    } else {
      // 新增接口
      url = API_POST_ADD_SHOP_RULE
    }
    validateFields((error, values) => {
      if (error) return reject()
      let { startDate, endDate, accessCountLimit, checkedRate, prizeCount, prizeItemNo } = values
      startDate = moment(startDate).format('YYYY-MM-DD HH:mm:ss')
      endDate = moment(endDate).format('YYYY-MM-DD HH:mm:ss')
      accessCountLimit = parseFloat(accessCountLimit)
      checkedRate = parseFloat(checkedRate)
      prizeCount = parseFloat(prizeCount)
      prizeItemNo = parseFloat(prizeItemNo)
      let { actCode, cellStyleMap, openDay } = editData
      let data = { ...values, startDate, endDate, accessCountLimit, checkedRate, prizeCount, prizeItemNo, actCode, cellStyleMap, openDay }
      http.post(url, data).then(res => {
        message.success('修改成功')
        this.props.form.resetFields()
        this.props.fetchSelectTab()
      }).then(resolve).catch(reject)
      handleCancel()
    })
  })

  render() {
    console.log(this.props.isEditble, '3')
    const { visible, handleCancel, form: { getFieldDecorator, setFieldsValue, getFieldValue }, isEditble } = this.props
    return (
      <YHModal
        visible={visible}
        title={isEditble ? '编辑' : '新增'}
        maskClosable={false}
        width="400px"
        okText="提交"
        onOk={this.handleOk}
        onCancel={handleCancel}
      >
        <Form>
          <CommonInput
            getFieldDecorator={getFieldDecorator}
            label="活动ID"
            rules={[{ required: true, message: '活动ID必填!' }]}
            decorator="id"
            disabled
          />
          <CommonInput
            getFieldDecorator={getFieldDecorator}
            label="活动名称"
            disabled
            rules={[{ required: true, message: '活动名称必填!' }]}
            decorator="actName"
          />
          <CommonInput
            getFieldDecorator={getFieldDecorator}
            label="参与门店"
            rules={[{ required: true, message: '参与门店必填!' }]}
            decorator="shopCode"
          />
          <CommonInput
            getFieldDecorator={getFieldDecorator}
            label="关联奖项"
            rules={[{ required: true, message: '关联奖项必填!' }]}
            decorator="prizeItemNo"
          />
          <CommonInput
            getFieldDecorator={getFieldDecorator}
            label="奖项名称"
            rules={[{ required: true, message: '奖项名称必填!' }]}
            decorator="prizeItemName"
          />
          <StartAndEndDatePicker
            getFieldDecorator={getFieldDecorator}
            setFieldsValue={setFieldsValue}
            getFieldValue={getFieldValue}
            format={dateFormat}
            rules= {
              [{ required: true, message: 'Please select time!' }]
            }
            startDecorator='startDate'
            endDecorator='endDate'
            label="活动时间"
          />
          <CommonInput
            getFieldDecorator={getFieldDecorator}
            label="出奖数量"
            rules={[{ required: true, message: '出奖数量必填!' }]}
            decorator="prizeCount"
          />
          <CommonInput
            getFieldDecorator={getFieldDecorator}
            label="中奖概率"
            rules={[{ required: true, message: '中奖概率必填!' }]}
            decorator="checkedRate"
          />
          <CommonInput
            getFieldDecorator={getFieldDecorator}
            label="开奖流阈值"
            rules={[{ required: true, message: '开奖流阈值必填!' }]}
            decorator="accessCountLimit"
          />

        </Form>
      </YHModal>
    )
  }
}

export default AddNewFiveModal
