/*
 * @Author: lihongjie
 * @Date: 2019-09-12 18:15:08
 * @Last Modified by: lihongjie
 * @Last Modified time: 2019-10-17 15:02:20
 */
/**
 * Created by lihongjie on 2019/09/10
 */

import React from 'react'
import { Table } from 'antd'
import { getPagination } from '@yhUtil'
import { LongText } from '@comp'
import _ from 'lodash'
import moment from 'moment'
const styleGray = {
  color: 'gray'
}
class PageTable extends React.PureComponent {
  state = {
    isShowEditNewProducyModal: false
  }

  columns = [
    { width: 50, title: '序号', dataIndex: 'index', render: (text, record, index) => index + 1 },
    { width: 150, title: '活动编码', dataIndex: 'actCode', render: (text) => <LongText title={text} width={150} >{text}</LongText> },
    { width: 180, title: '活动名称', dataIndex: 'actName' },
    { width: 180, title: '活动日期', dataIndex: 'startDate', render: (text, record) => {
      const startDate = moment(text).format('YYYY-MM-DD')
      const endDate = moment(record.endDate).format('YYYY-MM-DD')
      return (
        <span>{`${startDate}-${endDate}`}</span>
      )
    } },
    { width: 80, title: '门槛金额', dataIndex: 'minMoney' },
    { width: 80, title: '最大抽奖数', dataIndex: 'maxTimes' },
    { width: 100, title: '创建人', dataIndex: 'creater', render: (text) => <LongText title={text} width={100} >{text}</LongText> },
    { width: 150, title: '创建时间', dataIndex: 'createTime', render: (text) => <LongText title={text} width={150} >{text}</LongText> },
    { width: 80, title: '审核状态', dataIndex: 'isAudit', render: (text, record, index) => text === 0 ? <span>未审核</span> : <span>已审核</span> },
    { width: 80, title: '活动状态', dataIndex: 'activeStateFlag', render: (text, record, index) => {
      if (text === 0) {
        return <span>未提交</span>
      } else if (text === 1) {
        return <span>未开始</span>
      } else if (text === 2) {
        return <span>正在进行</span>
      } else if (text === 3) {
        return <span>已结束</span>
      } else if (text === 4) {
        return <span>已废除</span>
      } else {
        return <span></span>
      }
    } },

    { width: 120, title: '操作', dataIndex: 'operation', fixed: 'right', className: 'hoverText', render: (text, record) => <div>
      {
        record.activeStateFlag === 4 ?
          <span style={styleGray}>查看</span> : <span>
            <a href="javascript:void(0)" onClick={() => this.props.handleHref(record)}> 查看 </a>
          </span>
      }
      {
        record.isAudit == 1 || record.activeStateFlag === 4 ? <span style={styleGray}>编辑</span> : <span>
          <a href="javascript:void(0)" onClick={() => this.props.handleEdit(record)}> 编辑</a>
        </span>
      }

      {
        record.isAudit === 1 || record.activeStateFlag === 4 ? <span style={styleGray}> 审核 </span> : <span>
          <a href="javascript:void(0)" onClick={() => this.props.handleAudit(record, '审核后不可修改，确认通过审核？', '1')}> 审核 </a>
        </span>
      }
      {
        record.isAudit === 1 || record.activeStateFlag === 4 ? <span style={styleGray}>废弃</span> : <span>
          <a href="javascript:void(0)" onClick={() => this.props.handleAudit(record, '确认废弃该活动吗？', '4')}> 废弃 </a>
        </span>
      }

    </div>
    }
  ]
  render() {
    const { searchResult = [], loading, total, page, size, pageSizeChange } = this.props

    // 分页
    const _pagination = getPagination({
      total,
      current: page,
      pageSize: size,
      onChange: pageSizeChange
    })

    // 列表
    let columns = [...this.columns]

    return (
      <section className="page-table">
        <Table
          scroll={{ x: _.sumBy(columns, 'width') }}
          rowKey={'id'}
          columns={columns}
          loading={loading}
          dataSource={searchResult}
          pagination={_pagination}
        />
      </section>
    )
  }
}

export default PageTable
