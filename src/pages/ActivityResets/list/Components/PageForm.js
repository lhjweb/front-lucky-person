/*
 * @Author: lihongjie
 * @Date: 2019-09-12 18:14:58
 * @Last Modified by: lihongjie
 * @Last Modified time: 2019-10-08 11:13:28
 */
/**
 * Created by lihongjie on 2019/09/10
 */

import React from 'react'
import { Form } from 'antd'
import {
  CollapsibleFormBox,
  CommonSelect,
  CommonInput,
  ActivityName,
  DatePicker
} from '@comp'
import moment from 'moment'

/*
* @Form.create()
* 利用装饰器简化代码，将表单验证操作统一到一个装饰器中进行
* 原：Form.create()(App)
* */
@Form.create()
class PageForm extends React.PureComponent {
  /* 查询 */
  handleSearch = e => {
    e.preventDefault()
    const { form: { validateFields }, handleSearchResult } = this.props
    validateFields((error, values) => {
      if (!error) {
        let { actName, startDate } = values
        startDate = startDate ? moment(startDate).format('YYYY-MM-DD') : ''
        // 活动名称
        actName = actName ? actName.split(' ')[1] : ''
        // 整合参数
        const queryParams = { ...values, actName, startDate }
        // 去除空项
        Object.keys(queryParams).forEach(item => {
          !queryParams[item] && delete queryParams[item]
        })
        handleSearchResult({ ...queryParams })
      }
    })
  }

  /* 重置 */
  handleReset = () => this.props.form.resetFields()

  render() {
    const { form: { getFieldDecorator, setFieldsValue, getFieldValue } } = this.props
    const currentStatusList = [
      { code: '0', value: '未提交' },
      { code: '1', value: '未开始' },
      { code: '2', value: '正在进行' },
      { code: '3', value: '已结束' },
      { code: '4', value: '已废除' }
    ]
    const auditStatusList = [
      { code: '0', value: '未审核' },
      { code: '1', value: '已审核' }
    ]
    return (
      <Form className="page-form">
        <CollapsibleFormBox
          onSearch={this.handleSearch}
          onReset={this.handleReset}
        >
          <div>
            <ActivityName
              getFieldDecorator={getFieldDecorator}/>
            <DatePicker
              getFieldDecorator={getFieldDecorator}
              setFieldsValue={setFieldsValue}
              getFieldValue={getFieldValue}
              label="活动时间"
              placeholder="活动时间"
              decorator="startDate"
            />
            <CommonSelect
              getFieldDecorator={getFieldDecorator}
              label="活动状态"
              decorator="activeStateFlag"
              placeholder="请选择"
              autoComplete="off"
              list={currentStatusList}
            />
            <CommonInput
              getFieldDecorator={getFieldDecorator}
              decorator="actCode"
              label="活动编码"
              placeholder="请输入供应商编码"
            />
            <CommonSelect
              getFieldDecorator={getFieldDecorator}
              label="审核状态"
              decorator="isAudit"
              placeholder="请选择"
              autoComplete="off"
              list={auditStatusList}
            />
          </div>
        </CollapsibleFormBox>
      </Form>
    )
  }
}

export default PageForm
