/*
 * @Author: lihongjie
 * @Date: 2019-09-12 18:15:37
 * @Last Modified by: lihongjie
 * @Last Modified time: 2019-10-17 14:18:36
 */
/**
 * Created by lihongjie on 2019/09/10
 */

import React from 'react'
import { Button, Modal } from 'antd'
import { Import } from '@comp'
import {
  API_POST_SETTING_EXCEL,
  API_POST_SETTING_SUBMIT
} from '@api'
import http from '@http'

const userId = window.localStorage.getItem('id')
class PageTool extends React.PureComponent {
  constructor(props) {
    super(props)
  }
  // 获取搜索参数
  getData = () => {
    this.props.getImportData()
  }
  /**
   * 导入成功
   */
  handleImportSuccess = () => {
    Modal.confirm({
      title: '提示',
      content: '导入成功将跳转到编辑页面?',
      okText: '确定',
      cancelText: '取消',
      onOk: () => {
        let isEdit = true
        let url = `/activityManage/activityEditor?isEdit=${isEdit}&userId=${userId}`
        window.open(url)
      }
    })
    // 刷新配置总列表
    const { handleRefresh } = this.props
    handleRefresh()
  }
  /* 活动删除*/
  handleDeleteActivity = () => {
    http.post(API_POST_SETTING_SUBMIT, { userId }).then(res => {}).catch((e) => {
      // console.error(e)
    }).finally(() => {})
  }
  render() {
    const { handleDownTemplate, handleRefresh } = this.props

    return (
      <section className="page-tool common__tool">
        <div className="fl">
          <Button className="mr10" onClick={handleDownTemplate} key='0'>模板下载</Button>
          <Import
            className="mr10"
            actionConfig={{ urlConfig: API_POST_SETTING_EXCEL }}
            onOk={() => {
              this.handleImportSuccess()
            }}
            onClick={() => {
              this.handleDeleteActivity()
            }}
            onExprotErrorCancel = {() => {
              handleRefresh()
            }}
            openFileDialogOnClick={true}
          />
        </div>
        <div className="fr">
        </div>
      </section>
    )
  }
}

export default PageTool
