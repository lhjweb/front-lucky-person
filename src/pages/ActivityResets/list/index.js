/*
 * @Author: lihongjie
 * @Date: 2019-09-06 10:27:27
 * @Last Modified by: lihongjie
 * @Last Modified time: 2019-10-17 13:48:48
 */

import React from 'react'
import { message, Modal } from 'antd'
import { Bread } from '@comp'
import { PageForm, PageTool, PageTable } from './Components'
import http from '@http'
import moment from 'moment'
import {
  API_POST_GET_SETTING_QUERY_ACTIVITY, // 查询列表
  API_GET_AUDIT_ACTIVITY  // 审核
} from '@api'

export default class App extends React.Component {
  state = {
    searchResult: [], // 获取查询结果
    searchParams: {}, // 搜索传值
    total: 0, // 查询总数
    page: 1, // 当前页码
    size: 10, // 当前分页数
    tableLoading: false // 列表loading

  }

  componentDidMount() {
    this._fetchSearchResult()
  }

  /* 获取查询结果 */
  _fetchSearchResult = () => {
    const { searchParams, page, size } = this.state

    const params = Object.assign({}, searchParams, { page, size })

    this.setState({
      tableLoading: true,
      total: 0,
      searchResult: []
    })
    http.post(API_POST_GET_SETTING_QUERY_ACTIVITY, params).then(res => {
      const { result, totalNum } = res
      this.setState({
        total: totalNum || 0,
        searchResult: result || []
      })
    }, err => console.error(err)).finally(this.resetSelectedRowKeys)
  }
  handleRefresh = () => {
    this._fetchSearchResult()
  }

  /* 搜索查询结果 */
  handleSearchResult = (params = this.state.searchParams) => {
    if (!params) return false
    for (let idx in params) {
      if (!params[idx] || params[idx] === '') delete params[idx]
    }
    // 创建时间修改传递格式
    if (params.startDate) {
      params.startDate = moment(params.startDate).format('YYYY-MM-DD')
    }
    if (params.endDate) {
      params.endDate = moment(params.endDate).format('YYYY-MM-DD')
    }

    this.setState({
      page: 1,
      searchParams: params
    }, this._fetchSearchResult)
  }
  /**
   * 模板下载
   */
  handleDownTemplate = () => {
    window.open('http://10.251.64.121:8000/pc-mid-p-dev/luckyDay/import-template.xlsx')
  }
  /* 操作-查看*/
  handleHref = (record) => {
    let isEdit = ''
    let userId = ''
    let actCode = record.actCode
    let url = `/activityManage/activityEditor?isEdit=${isEdit}&userId=${userId}&actCode=${actCode}`
    window.open(url)
  }

  /* 编辑*/
  handleEdit = (record) => {
    let isEdit = true
    let userId = ''
    let actCode = record.actCode
    let url = `/activityManage/activityEditor?isEdit=${isEdit}&userId=${userId}&actCode=${actCode}`
    window.open(url)
  }

  /* 审核/作废 */
  handleAudit =(params, content, activeState) => {
    Modal.confirm({
      title: '操作确认',
      content,
      okText: '确定',
      cancelText: '取消',
      onOk: () => new Promise((resolve, reject) => {
        http.get(API_GET_AUDIT_ACTIVITY, { activeState }, { urlParams: [params.id] }).then(res => {
          this.handleRefresh()
        }, err => console.error(err)).then(resolve).catch(reject)
      })
    })
  }
  /* ---------------- 表格操作 ----------------- */
  /* 页码和每页显示数量改变 */
  pageSizeChange = (current, pageSize) => {
    if (!current || !pageSize) return false
    this.setState({
      page: current,
      size: pageSize
    }, this._fetchSearchResult)
  }

  /* 更新查询结果 - 刷新表格数据 */
  reloadTableData = () => this.setState({ page: 1 }, this._fetchSearchResult)

  /* 重置选中的key */
  resetSelectedRowKeys = () => {
    this.setState({
      tableLoading: false
    })
  }
  /* ---------------- 表格操作 end ----------------- */
  render() {
    const {
      searchResult,
      tableLoading,
      total,
      page,
      size
    } = this.state

    return (
      <section className="stay-introduction-of-new-product-list">
        <Bread {...this.props}/>
        <PageForm
          handleSearchResult={this.handleSearchResult}
        />
        <PageTool
          searchResult={searchResult}
          handleDownTemplate={this.handleDownTemplate}
          handleRefresh={this.handleRefresh}
          handleHref={this.handleHref}
        />
        <PageTable
          loading={tableLoading}
          total={total}
          page={page}
          size={size}
          handleHref={this.handleHref}
          searchResult={searchResult}
          pageSizeChange={this.pageSizeChange}
          handleEdit={this.handleEdit}
          handleAudit={this.handleAudit}
        />
      </section>
    )
  }
}
