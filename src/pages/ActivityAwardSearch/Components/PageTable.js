/*
 * @Author: lihongjie
 * @Date: 2019-09-19 15:44:36
 * @Last Modified by: lihongjie
 * @Last Modified time: 2019-10-10 16:20:17
 */

import React from 'react'
import { Table } from 'antd'
import { getPagination } from '@yhUtil'
import { LongText } from '@comp'
import _ from 'lodash'

class PageTable extends React.PureComponent {
  state = {
    isShowEditNewProducyModal: false
  }

  columns = [
    { width: 50, title: '序号', dataIndex: 'index', render: (text, record, index) => index + 1 },
    { width: 150, title: '活动名称', dataIndex: 'actName', render: (text) => <LongText title={text} width={150} >{text}</LongText> },
    { width: 100, title: '门店号', dataIndex: 'shopCode' },
    { width: 100, title: '奖项名称', dataIndex: 'prizeItemName' },
    { width: 100, title: '奖项总数', dataIndex: 'prizeCount' },
    { width: 100, title: '预计出奖', dataIndex: 'futureCount' },
    { width: 100, title: '实际出奖', dataIndex: 'actualCount' }
  ]
  render() {
    const { searchResult = [], loading, total, page, size, pageSizeChange } = this.props

    // 分页
    const _pagination = getPagination({
      total,
      current: page,
      pageSize: size,
      onChange: pageSizeChange
    })

    // 列表
    let columns = [...this.columns]

    return (
      <section className="page-table">
        <Table
          scroll={{ x: _.sumBy(columns, 'width') }}
          rowKey={'id'}
          columns={columns}
          loading={loading}
          dataSource={searchResult}
          pagination={_pagination}
        />
      </section>
    )
  }
}

export default PageTable
