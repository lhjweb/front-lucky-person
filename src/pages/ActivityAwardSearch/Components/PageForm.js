/**
 * Created by lihongjie on 2019/09/10
 */

import React from 'react'
import { Form } from 'antd'
import {
  CollapsibleFormBox,
  ActivityName,
  CommonInput,
  StartAndEndDatePicker
} from '@comp'
import {
  API_GET_QUERY_ACTNAME_CODES
} from '@api'
import moment from 'moment'

/*
* @Form.create()
* 利用装饰器简化代码，将表单验证操作统一到一个装饰器中进行
* 原：Form.create()(App)
* */
@Form.create()
class PageForm extends React.PureComponent {
  /* 查询 */
  handleSearch = e => {
    e.preventDefault()
    const { form: { validateFields }, handleSearchResult } = this.props
    validateFields((error, values) => {
      if (!error) {
        let { actName, startDate, endDate } = values
        startDate = startDate ? moment(startDate).format('YYYY-MM-DD') : ''
        endDate = endDate ? moment(endDate).format('YYYY-MM-DD') : ''
        // 活动名称
        actName = actName ? actName.split(' ')[1] : ''
        // 整合参数
        const queryParams = { ...values, actName, startDate, endDate }
        // 去除空项
        Object.keys(queryParams).forEach(item => {
          !queryParams[item] && delete queryParams[item]
        })
        handleSearchResult({ ...queryParams })
      }
    })
  }

  /* 重置 */
  handleReset = () => this.props.form.resetFields()
  render() {
    const { form: { getFieldDecorator, setFieldsValue, getFieldValue } } = this.props
    return (
      <Form className="page-form">
        <CollapsibleFormBox
          onSearch={this.handleSearch}
          onReset={this.handleReset}
        >
          <div>
            <ActivityName
              getFieldDecorator={getFieldDecorator}/>
            <CommonInput
              getFieldDecorator={getFieldDecorator}
              decorator="shopCode"
              label="门店号"
              placeholder="请输入门店号"
            />
            <StartAndEndDatePicker
              getFieldDecorator={getFieldDecorator}
              setFieldsValue={setFieldsValue}
              getFieldValue={getFieldValue}
              startDecorator='startDate'
              endDecorator='endDate'
              label="出奖日期"
            />
          </div>
        </CollapsibleFormBox>
      </Form>
    )
  }
}

export default PageForm
