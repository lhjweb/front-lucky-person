import React from 'react'
import '../style/ShinningInput.less'

class ShinningInput extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      isFocused: false,
      value: ''
    }
    this.inputRef = null
  }

  componentWillReceiveProps(nextProps) {
    if (this.props.loginWay !== nextProps.loginWay) {
      this.setState({ isFocused: false, value: '' })
    }
  }

  componentDidMount() {
    this.setState({ value: this.inputRef.value })
  }
  handleCompClick = e => {
    this.inputRef.focus()
  }
  handleInputFocus = e => {
    this.setState({ isFocused: true, value: e.target.value })
    this.props.onFocus && this.props.onFocus(e)
  }

  handleInputBlur = e => {
    this.setState({ isFocused: false, value: e.target.value })
    this.props.onBlur && this.props.onBlur(e)
  }
  handleInputChange = e => {
    this.setState({ value: e.target.value })
    this.props.onChange && this.props.onChange(e)
  }
  render() {
    const { isFocused, value } = this.state
    const { label, placeholder, cover, loginWay, ...passThroughProps } = this.props
    const hasVal = value.toString().length > 0
    const className = [
      'shinningInput',
      isFocused || hasVal ? 'focused' : 'blured'
    ].join(' ')
    // console.log('render-value', value)
    return (
      <div className={className} onClick={this.handleCompClick}>
        <label>{ label }</label>
        <input
          className={loginWay}
          {...passThroughProps}
          placeholder={isFocused ? '' : placeholder}
          ref={ref => {
            this.inputRef = ref
          }}
          onChange={this.handleInputChange}
          onFocus={this.handleInputFocus}
          onBlur={this.handleInputBlur}
        />
        {
          cover && <div className="cover"></div>
        }
      </div>
    )
  }
}

export default ShinningInput
