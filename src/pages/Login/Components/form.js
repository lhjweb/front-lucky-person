import React, { Component } from 'react'
import { message, Button } from 'antd'
import ShinningInput from './ShinningInput'
import '../style/form.less'
function phoneValidator(phone) {
  return /^((13[0-9])|(14[5,7])|(15[0-3,5-9])|(17[0,3,5-8])|(18[0-9])|166|198|199|(147))\d{8}$/.test(phone)
}
function passwordValidator(password) {
  return /^[a-z|A-Z|0-9]{6,12}$/.test(password)
}
class Comps extends Component {
  constructor(props) {
    super(props)
    this.state = {
      username: '',
      password: '',
      tel: '',
      code: '',
      newPassword: '',
      comfirmPassword: '',
      step: 0,
      timerCount: 0,
      timer: null,
      codeError: '',
      codeDisabled: true,
      resetError: '',
      resetDisabled: true,

      isFocused: false,
      loginWay: 'acc'
    }
  }
  // 表单输入框值变化
  _handleChange = name => e => {
    this.setState({ [name]: e.target.value })
    if (name === 'tel' && phoneValidator(e.target.value)) {
      this.setState({
        codeError: '',
        codeDisabled: false
      })
    }
    if (name === 'comfirmPassword') {
      if (!passwordValidator(e.target.value)) {
        this.setState({ resetError: '6-12位，数字字母均可', resetDisabled: true })
      } else {
        const resetDisabled = this.state.newPassword !== e.target.value
        this.setState({ resetDisabled, resetError: resetDisabled ? '两次密码不一致' : '' })
      }
    }
  }
  // 表单下一步流程
  nextStep() {
    const { step } = this.state
    if (step < 2) {
      this.setState({ step: step + 1 })
    } else {
      console.log('submit')
    }
  }
  // 登录事件
  submitLogin() {
    const {
      username,
      password,
      newPassword,
      comfirmPassword,
      step,
      loginWay
    } = this.state
    if (step === 0) {
      this.props.onLogin && this.props.onLogin(username, password, loginWay)
    }
    if (step === 2) {
      if (newPassword === comfirmPassword) {
        this._resetPwd()
      } else {
        this.setState({
          resetError: '两次密码不一致',
          resetDisabled: true
        })
      }
    }
  }
  // 上一步／撤回
  prevStep() {
    const { step } = this.state
    this.setState({ step: step - 1 }, this.clearTelAndCode)
  }
  // 清空tel code
  clearTelAndCode=() => {
    const { step } = this.state
    if (step === 0) {
      this.setState({
        tel: '',
        code: ''
      })
    }
  }
  // 获取验证码
  _getCode() {
    const { tel, timerCount } = this.state

    if (timerCount > 0) {
      return false
    }
    this._setTimer()
    this.props.onGetCode && this.props.onGetCode(tel)
  }
  // 验证码倒计时
  _setTimer() {
    this._clearTimer()
    const timer = setInterval(() => {
      const count = this.state.timerCount - 1
      this.setState({ timerCount: count })
      if (count === 0) {
        this._clearTimer()
      }
    }, 1000)
    this.setState({ timerCount: 59, timer })
  }
  // 清楚验证码倒计时
  _clearTimer() {
    const { timer } = this.state
    timer && clearInterval(timer)
  }
  // 重置密码
  _resetPwd(param) {
    const {
      username,
      newPassword,
      tel,
      code
    } = this.state
    this.setState({ resetDisabled: true })
    this.props.onReset
      &&
      this.props.onReset({ username, newPassword, tel, code }).then(data => {
        message.success('密码修改成功，请重新登陆。')
        this.setState({ resetDisabled: false, step: 0 }, this.clearTelAndCode)
        // this.props.onLogin && this.props.onLogin(tel, newPassword)
      }, err => {
        this.setState({ resetError: '密码修改失败', resetDisabled: false })
      })
  }
  // 验证手机号
  _handleCheckTel(e) {
    if (!phoneValidator(e.target.value)) {
      this.setState({ codeError: '请输入有效的手机号', codeDisabled: true })
    } else {
      this.setState({ codeError: '', codeDisabled: false })
    }
  }
  // 验证验证码是否正确
  _handleCheckCode(e) {
    this.nextStep()
    // const { tel, code } = this.state
    // if (this.props.onCheckCode) {
    //   this.nextStep()
    //   this.props.onCheckCode({ tel, code }).then(data => {
    //     this.nextStep()
    //   })
    // } else {
    //   this.nextStep()
    // }
    // return false
  }
  // 验证密码格式
  _handleCheckPwd(e) {
    if (!passwordValidator(e.target.value)) {
      this.setState({ resetError: '6-12位，数字字母均可', resetDisabled: true })
    } else {
      this.setState({ resetError: '', resetDisabled: false })
    }
  }
  // 错误提示
  dangerRender(err) {
    return err ?
      (<span>
        <img className="error-icon" src={require('../../../assets/images/danger.png')} />
        {err}
      </span>)
      :
      (<span></span>)
  }
  // 表单内回车键登录
  _handleKeyup = (e) => {
    if (e.keyCode === 13) {
      const { username, password } = this.state
      const { canLogin } = this.props
      const canSubmit = username && password && canLogin
      canSubmit && this.submitLogin()
    }
    e.preventDefault()
  }
  // 登录方式切换
  check = (way, e) => {
    e.preventDefault()
    const { loginWay } = this.state
    if (loginWay !== way) {
      this.props.clearErrorMsg()
      this.setState({
        loginWay: way,
        username: '',
        password: ''
      })
    }
  }
  // 登陆渲染
  loginRender() {
    const { username, password, loginWay } = this.state
    const { loginError = '', canLogin } = this.props
    const canSubmit = username && password && canLogin
    return (
      <div className="flex-column">
        <div className="logo"><img className="logo-img" src={require('@img/logo-header-icon-new.png')} alt=""/></div>
        <div>
          <a
            onClick={this.check.bind(this, 'acc')}
            className={this.state.loginWay === 'acc' ? 'title' : 'subtitle' }>
            账户登录
          </a>
          <a
            onClick={this.check.bind(this, 'oa')}
            className={this.state.loginWay === 'oa' ? 'title oa' : 'subtitle oa' }>
            OA登录
          </a>
        </div>
        <div className="login-inps">
          <ShinningInput
            label="用户名"
            value={username}
            loginWay={loginWay}
            cover
            onChange={this._handleChange('username').bind(this)}
            onKeyUp={this._handleKeyup}
          />
          <ShinningInput
            value={password}
            label="密码"
            loginWay={loginWay}
            cover
            type="password"
            onChange={this._handleChange('password').bind(this)}
            onKeyUp={this._handleKeyup}
          />
          <div className="login-tip">
            {this.dangerRender(loginError)}
            <a onClick={this.nextStep.bind(this)}>忘记密码？</a>
          </div>
        </div>
        <div className="login-footer">
          <div className="auto-login"></div>
          <Button
            className="submit"
            type="primary"
            loading={!canLogin}
            disabled={!canSubmit}
            onClick={this.submitLogin.bind(this)}
          >
            { canLogin ? '登录' : '登录中'}
          </Button>
        </div>
      </div>
    )
  }
  //
  checkRender() {
    const {
      timerCount,
      tel,
      code,
      codeError,
      codeDisabled
    } = this.state
    const disabled = !tel || !code || codeDisabled
    return (
      <div className="flex-column">
        <div className="logo"><img className="logo-img" src={require('@img/logo-header-icon-new.png')} alt=""/></div>
        <div className="flex-between">
          <div className="back" onClick={this.prevStep.bind(this)}>
            <img className="back-icon" src={require('../../../assets/images/back.png')} />
            返回
          </div>
          <div className="title">手机验证</div>
        </div>
        <div className="login-inps">
          <ShinningInput
            label="手机号"
            value={tel}
            onChange={this._handleChange('tel').bind(this)}
            onBlur={e => this._handleCheckTel(e)}
          />
          <ShinningInput
            label="验证码"
            value={code}
            onChange={this._handleChange('code').bind(this)}
            maxLength="10"
          />
          <a
            className="get-code"
            onClick={this._getCode.bind(this)}
          >
            {timerCount > 0 ? `${timerCount}s` : '获取验证码'}
          </a>
          <div className="login-tip">
            {this.dangerRender(codeError)}
          </div>
        </div>
        <div className="login-footer">
          <Button
            className="submit"
            type="primary"
            onClick={this._handleCheckCode.bind(this)}
            disabled={disabled}
          >下一步</Button>
        </div>
      </div>
    )
  }
  //
  resetRender() {
    const {
      newPassword,
      comfirmPassword,
      resetDisabled,
      resetError
    } = this.state
    const { canLogin } = this.props
    const disabled = !newPassword || !comfirmPassword || resetDisabled || !canLogin
    return (
      <div className="flex-column">
        <div className="logo"><img className="logo-img" src={require('@img/logo-header-icon-new.png')} alt=""/></div>
        <div className="flex-between">
          <div className="back" onClick={this.prevStep.bind(this)}>
            <img className="back-icon" src={require('../../../assets/images/back.png')} />
            返回
          </div>
          <div className="title">设置密码</div>
        </div>
        <div className="login-inps">
          <ShinningInput
            value={newPassword}
            type="password"
            label="新密码"
            onChange={this._handleChange('newPassword').bind(this)}
            onBlur={e => this._handleCheckPwd(e)}
          />
          <ShinningInput
            value={comfirmPassword}
            type="password"
            label="确认密码" onChange={this._handleChange('comfirmPassword').bind(this)}
          />
          <div className="login-tip">
            {this.dangerRender(resetError)}
          </div>
        </div>
        <div className="login-footer">
          <Button
            className="submit"
            type="primary"
            onClick={this.submitLogin.bind(this)}
            disabled={disabled}
          >提交</Button>
        </div>
      </div>
    )
  }
  render() {
    const { step } = this.state
    return (
      <form className="login-form">
        {step === 0 && this.loginRender()}
        {step === 1 && this.checkRender()}
        {step === 2 && this.resetRender()}
      </form>
    )
  }
}

export default Comps
