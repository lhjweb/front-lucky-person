/**
 * @Description: 登陆页
 * @Author: zero
 * @Date: 2019-54-17 19:54:53
 * @LastEditors: zero
 * @LastEditTime: 2019-54-17 19:54:53
*/

import React from 'react'
import Form from './Components/form'
import {
  API_GET_LOGIN,
  API_POST_USER_CENTER_SMS_CODE,
  API_POST_USER_CENTER_GET_BACK,
  API_POST_USER_CENTER_CHECK_CODE,
  API_GET_MENU_BUTTON_PERMISSSION_INFO
} from '@api'
import './style/index.less'
import md5 from 'md5'
import http from '@http'
import { setVariant, LOGIN_TOKEN } from '@variant'
import PropTypes from 'prop-types'

class Login extends React.Component {
  static contextTypes = {
    router: PropTypes.object.isRequired
  }
  constructor(props) {
    super(props)
    this.state = {
      loginError: '',
      canLogin: true
    }
  }

  // 登录
  handleLogin = (loginName, password, loginWay) => {
    const { canLogin } = this.state
    if (!canLogin) return
    this.setState({ canLogin: false })
    let loginType = loginWay === 'acc' ? 0 : 1
    const loginPassword = loginWay === 'acc' ? md5(password) : password
    http.post(API_GET_LOGIN, { loginName, loginPassword, loginType }, { withCredentials: false }).then(data => {
      this.initUserInfo(data)
      this.getButtonPermission()
    }, err => {
      this.setState({ loginError: err.message, canLogin: true })
    })
  }

  // 处理用户信息
  initUserInfo = (data = {}) => {
    const userInfo = {
      email: data.email,
      id: data.id,
      telephone: data.telephone,
      userNo: data.userNumber,
      username: data.name,
      [LOGIN_TOKEN]: data.loginToken
    }
    let key
    for (key in userInfo) {
      setVariant(key, userInfo[key])
    }
    setVariant('userInfos', userInfo)
    try {
      window.yh_trace.setUserId(userInfo.id)
      window.yh_trace.sendEvent('login_success', userInfo.username)
    } catch (e) {
      console.error(e)
    }
  }

  // 获取按钮级别权限
  getButtonPermission = () => {
    http.get(API_GET_MENU_BUTTON_PERMISSSION_INFO, { businessType: 7 }).then(data => {
      setVariant('supplyButtonPermission', data)
      this.setState({ loginError: '', canLogin: true })
      this.context.router.history.push('/activityManage/activityResets') // 登陆成功跳转页面
    })
  }

  // 获取验证码
  requestCode = telephone => {
    http.post(API_POST_USER_CENTER_SMS_CODE, { telephone }, { withCredentials: false, params: { telephone } }).then(data => {
      console.log(data)
    })
  }

  // 重置密码
  resetPwd = ({ tel, code, newPassword }) => http.post(API_POST_USER_CENTER_GET_BACK, { telephone: tel, smsCode: code, password: newPassword }, { withCredentials: false })
  // 校验验证码
  handleCheckCode = opt => http.get(API_POST_USER_CENTER_CHECK_CODE, { mobile: opt.tel, smsCode: opt.code }, { withCredentials: false })

  /* 将错误信息置空 */
  clearErrorMsg = () => {
    this.setState({ loginError: '' })
  }

  render() {
    const { loginError, canLogin } = this.state
    return (
      <div className="page__login">
        <div className="login-content">
          <Form
            canLogin={canLogin}
            loginError={loginError}
            clearErrorMsg={this.clearErrorMsg}
            onLogin={this.handleLogin}
            onGetCode={this.requestCode}
            onReset={this.resetPwd}
            onCheckCode={this.handleCheckCode}
          />
        </div>
      </div>
    )
  }
}

export default Login
