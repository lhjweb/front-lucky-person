import React, { PureComponent } from 'react'
import { TableMul } from '@comp'
import { Button } from 'antd'
// import { HotTable } from '@handsontable/react'
import http from '@http'
import { API_POST_BRANDAPPLYORDER_BASEPAGESEARCH } from '@api'

export default class App extends PureComponent {
  constructor(props) {
    super(props)
    this.state = {
      dataSource: []
    }
  }

  componentDidMount() {
    http.post(API_POST_BRANDAPPLYORDER_BASEPAGESEARCH, { page: 1, size: 10 }).then(res => {
      this.setState({
        dataSource: res.result
      })
    })
  }

  columns = [
    {
      chName: '拼搏编码',
      enName: 'brandCode',
      render: (instance, TD, row, col, prop, value, cellProperties) => value,
      editor: true,
      width: 200
    }, {
      chName: '申请单号',
      enName: 'brandName',
      render: (instance, TD, row, col, prop, value, cellProperties) => value,
    }, {
      chName: '测试',
      enName: 'remarks',
      render: (instance, TD, row, col, prop, value, cellProperties) => {
        const record = instance.getSourceDataAtRow(row)
        console.log(1)
        return value
      }
    }, {
      chName: '创建人',
      enName: 'createdBy'
    }, {
      chName: '备注',
      enName: 'remarks',
      editor: true
    }
  ]

  handleAfterChange = (changes, type) => {
    let [rowNumber, prop, oldValue, newValue ] = changes[0]
    console.log(rowNumber)
    console.log(prop)
    console.log(oldValue)
    console.log(newValue)
  }

  hanldleAdd = () => {
    this.setState({
      dataSource: this.state.dataSource.concat({})
    })
  }

  render() {
    const { dataSource } = this.state
    console.log(dataSource)
    return (
      <div>
        <div><Button onClick={this.hanldleAdd}>新增</Button></div>
        <TableMul
          ref={dom => this.TableMul = dom}
          // columnHeaderHeight={29}
          // rowHeights={30}
          checkAllBox={true}
          columns={this.columns}
          data={dataSource}
          selectRows={this.selectedRowKeys}
          afterChange={this.handleAfterChange}
        />
      </div>
    )
  }
}
