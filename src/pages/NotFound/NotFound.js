import React from 'react'
// import PropTypes from 'prop-types'

import './NotFound.less'

function NotFound(props) {
  return (
    <div className="page__not-found">
      <div className="img-box" />
    </div>
  )
}

NotFound.propTypes = {
}

export default NotFound
