/*
 * @Author: lihongjie
 * @Date: 2019-09-19 15:44:36
 * @Last Modified by: lihongjie
 * @Last Modified time: 2020-01-03 15:27:41
 */

import React from 'react'
import { Table } from 'antd'
import { getPagination } from '@yhUtil'
import { LongText } from '@comp'
import _ from 'lodash'

class PageTable extends React.PureComponent {
  state = {
    isShowEditNewProducyModal: false
  }

  columns = [
    { width: 50, title: '序号', dataIndex: 'index', render: (text, record, index) => index + 1 },
    { width: 150, title: '活动名称', dataIndex: 'actName', render: (text) => <LongText title={text} width={150} >{text}</LongText> },
    { width: 80, title: '门店号', dataIndex: 'shopCode' },
    { width: 150, title: '参与时间', dataIndex: 'submitTime', render: (text) => <LongText title={text} width={150} >{text}</LongText> },
    { width: 120, title: '流水号', dataIndex: 'tradeNo', render: (text) => <LongText title={text} width={120} >{text}</LongText> },
    { width: 180, title: '机号', dataIndex: 'posNo', render: (text) => <LongText title={text} width={180} >{text}</LongText> },
    { width: 50, title: '奖项', dataIndex: 'prizeItemName' },
    { width: 100, title: '客户姓名', dataIndex: 'customerName', render: (text) => <a title={text} title={text} href="javascript:void(0)" >{text}</a> },
    { width: 120, title: '联系方式', dataIndex: 'customerPhone', render: (text) => <LongText title={text} width={120} >{text}</LongText> },
    { width: 80, title: '是否兑奖', dataIndex: 'isTake', render: (text) => text === 1 ? <span>是</span> : <span>否</span> },
    { width: 100, title: '备注', dataIndex: 'remark', render: (text) => <LongText title={text} width={100} >{text}</LongText> }
  ]
  render() {
    const { searchResult = [], loading, total, page, size, pageSizeChange } = this.props

    // 分页
    const _pagination = getPagination({
      total,
      current: page,
      pageSize: size,
      onChange: pageSizeChange
    })

    // 列表
    let columns = [...this.columns]

    return (
      <section className="page-table">
        <Table
          scroll={{ x: _.sumBy(columns, 'width') }}
          rowKey={'id'}
          columns={columns}
          loading={loading}
          dataSource={searchResult}
          pagination={_pagination}
        />
      </section>
    )
  }
}

export default PageTable
