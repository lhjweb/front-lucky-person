/**
 * Created by lihongjie on 2019/09/10
 */

import React from 'react'
import { Form } from 'antd'
import {
  CollapsibleFormBox,
  ActivityName,
  CommonInput,
  DatePicker,
  CommonSelect
} from '@comp'
import {
  API_GET_QUERY_ACTNAME_CODES
} from '@api'
import moment from 'moment'

/*
* @Form.create()
* 利用装饰器简化代码，将表单验证操作统一到一个装饰器中进行
* 原：Form.create()(App)
* */
@Form.create()
class PageForm extends React.PureComponent {
  state = {
    listConfig: {
      method: 'get',
      urlConfig: API_GET_QUERY_ACTNAME_CODES
    }
  }
  /* 查询 */
  handleSearch = e => {
    e.preventDefault()
    const { form: { validateFields }, handleSearchResult } = this.props
    validateFields((error, values) => {
      if (!error) {
        let { actName, submitTime } = values
        submitTime = submitTime ? moment(submitTime).format('YYYY-MM-DD') : ''
        // 活动名称
        actName = actName ? actName.split(' ')[1] : ''
        // 整合参数
        const queryParams = { ...values, actName, submitTime }
        console.log(queryParams, 'queryParams')
        // 去除空项
        // Object.keys(queryParams).forEach(item => {
        //   !queryParams[item] && delete queryParams[item]
        // })
        handleSearchResult({ ...queryParams })
      }
    })
  }

  /* 重置 */
  handleReset = () => this.props.form.resetFields()
  render() {
    const { form: { getFieldDecorator } } = this.props
    return (
      <Form className="page-form">
        <CollapsibleFormBox
          onSearch={this.handleSearch}
          onReset={this.handleReset}
        >
          <div>
            <ActivityName
              getFieldDecorator={getFieldDecorator}/>
            <CommonInput
              getFieldDecorator={getFieldDecorator}
              decorator="actCode"
              label="活动编码"
              placeholder="请输入活动编码"
            />
            <CommonInput
              getFieldDecorator={getFieldDecorator}
              decorator="shopCode"
              label="门店号"
              placeholder="请输入门店号"
            />
            <CommonSelect
              getFieldDecorator={getFieldDecorator}
              label="是否兑奖"
              allowClear={false}
              decorator="isTake"
              list={[
                { value: '否', code: 0 },
                { value: '是', code: 1 }
              ]}
              placeholder="请选择是否兑奖"
            />
            <DatePicker
              getFieldDecorator={getFieldDecorator}
              label="参与日期"
              placeholder="参与日期"
              decorator="submitTime"
            />
            <CommonInput
              getFieldDecorator={getFieldDecorator}
              decorator="tradeNo"
              label="流水号"
              placeholder="请输入流水号"
            />
            <CommonInput
              getFieldDecorator={getFieldDecorator}
              decorator="customerPhone"
              label="联系方式"
              placeholder="请输入联系方式"
            />
            <CommonInput
              getFieldDecorator={getFieldDecorator}
              decorator="prizeItemNo"
              label="奖项编号"
              placeholder="请输入奖项编号"
            />
          </div>
        </CollapsibleFormBox>
      </Form>
    )
  }
}

export default PageForm
