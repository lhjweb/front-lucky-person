/**
 * @Description: 活动素材
 * @Author: zero
 * @Date: 2019-09-19 15:32:11
 * @LastEditors: zero
 * @LastEditTime: 2019-09-19 15:32:11
*/

import React from 'react'
import { Bread } from '@comp'
import { message, Modal } from 'antd'
import { PageForm, PageContent } from './Components'
import http from '@http'
import {
  API_POST_SETTING_QUERY_PRIZE_BY_CODE,
  API_GET_SETTING_QUERY_ACTIVITY_IMG,
  API_POST_SETTING_IMAGE_CANCEL_SUBMIT
} from '@api'
import host from '@host'

class App extends React.Component {
  state = {
    actCode: '', // 活动编码
    searchResult: null, // 获取查询结果 - 活动信息
    searchParams: {}, // 搜索传值
    tableLoading: false, // 列表loading
    activityUrl: '', // 活动预览地址
    dataSource: [], // 图片数据
    bgPath: '', // 背景图片
    actRemark: '', // 活动规则
    activityType: '0' // 活动选项 0 - 九宫格 、1 - 大转盘 、 2- 砸金蛋
  }

  /**
   * 获取查询结果
   * @returns {Q.Promise<T> | Promise<any> | Promise<T> | *}
   * @private
   */
  _fetchSearchResult = () => {
    const { searchParams } = this.state
    const params = { ...searchParams }

    this.setState({
      tableLoading: true,
      searchResult: null
    })
    return http.post(API_POST_SETTING_QUERY_PRIZE_BY_CODE, params).then(res => {
      if (res[0]) {
        const searchResult = res[0]
        const actCode = searchResult.actCode

        if (!actCode) return message.error('数据有误，缺少活动编码')

        this.setState({
          actCode,
          searchResult
        })

        this.fetchActivityImg(actCode)
      } else {
        message.info('查询无结果')
      }
    })
  }

  /**
   * 查询活动图片
   */
  fetchActivityImg = (actCode = this.state.actCode) => {
    http.get(API_GET_SETTING_QUERY_ACTIVITY_IMG, { actCode }).then(res => {
      // 遍历出背景图片
      let bgPath = ''
      let actRemark = '' // 活动规则
      let activityType = this.state.activityType // 活动选项
      if (res && res.length) {
        actRemark = res[0].actRemark
        activityType = res[0].gameModeFlag.toString()
        res.find(item => {
          if (/^bg\./.test(item.imgName)) {
            bgPath = item.imgPath
          }
        })
      }

      this.setState({
        bgPath,
        actRemark,
        activityType,
        dataSource: res || []
      })
      this.handleReLoad() // 刷新
    }).finally(this.resetSelectedRowKeys)
  }

  /**
   * 搜索查询结果
   * @param params
   * @returns {boolean|void}
   */
  hacdleSearchResult = (params = this.state.searchParams) => {
    if (!params) return false
    for (let idx in params) {
      if (!params[idx] || params[idx] === '') delete params[idx]
    }

    // 活动名称
    if (params.actName) params.actName = params.actName.split(' ')[1]

    return this.setState({
      searchParams: params
    }, this._fetchSearchResult)
  }

  /**
   * 重置选中的key
   */
  resetSelectedRowKeys = () => this.setState({
    tableLoading: false
  })

  /**
   * 刷新 iframe 页面
   */
  handleReLoad = () => {
    const { searchResult } = this.state
    const { actCode, shopCode } = searchResult || {}
    this.setState({
      activityUrl: ''
    }, () => {
      this.setState({
        activityUrl: `${host.prize}lucky/drawingRoom?actCode=${actCode}&shopCode=${shopCode}&history=${Math.random()}`
      })
    })
  }

  /**
   * 活动规则改变
   * @param value
   */
  handleActRemarkChange = e => this.setState({
    actRemark: e.target.value
  })

  /**
   * 切换活动选项改变
   * @param e
   */
  handleActiveitTypeChange = e => {
    const { actCode } = this.state
    Modal.confirm({
      title: '提示',
      content: '确认修改活动选项?',
      onOk: () => new Promise((resolve, reject) => {
        http.post(API_POST_SETTING_IMAGE_CANCEL_SUBMIT, { actCode }).then(() => {
          this.setState({
            activityType: e.target.value
          })
          this.fetchActivityImg(actCode) // 查询活动图片
        }).then(resolve).catch(reject)
      })
    })
  }

  render() {
    const { actCode, dataSource, bgPath, actRemark, searchResult, tableLoading, activityUrl, activityType } = this.state

    return (
      <section className="activity-material">
        <Bread {...this.props}></Bread>
        <PageForm
          hacdleSearchResult={this.hacdleSearchResult}
        />
        {searchResult && <PageContent
          actCode={actCode}
          tableLoading={tableLoading}
          activityUrl={activityUrl}
          dataSource={dataSource}
          bgPath={bgPath}
          actRemark={actRemark}
          activityType={activityType}
          handleShowModal={this.handleShowModal}
          handleReLoad={this.handleReLoad}
          fetchActivityImg={this.fetchActivityImg}
          handleActRemarkChange={this.handleActRemarkChange}
          handleActiveitTypeChange={this.handleActiveitTypeChange}
        />}
      </section>
    )
  }
}

export default App
