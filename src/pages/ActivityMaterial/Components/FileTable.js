/**
 * @Description: 上传文件表格展示
 * @Author: zero
 * @Date: 2019-09-20 17:57:58
 * @LastEditors: zero
 * @LastEditTime: 2019-09-20 17:57:58
*/

import '../style/FileTable.less'
import React from 'react'
import { Table, message } from 'antd'
import { Import } from '@comp'
import { API_POST_SETTING_UPLOAD_IMG } from '@/apis'

class FileTable extends React.Component {
  columns = [
    { width: 100, title: '图片名称', dataIndex: 'imgName' },
    { width: 40, title: '缩略图', dataIndex: 'imgPath', render: (text, record, index) => (
      <span className="thumbnail" style={{ backgroundImage: `url(${text})` }}></span>
    )
    },
    { width: 80, title: '对应数据', dataIndex: 'prizeItemName' },
    { width: 80, title: '执行结果', dataIndex: 'status', render: (text, record) => record.message ? <span className="red">失败</span> : '成功' },
    { width: 120, title: '结果原因', dataIndex: 'message', render: (text, record) => record.message ? <span className="red">{text}</span> : '成功' },
    { width: 80, title: '操作', dataIndex: 'resultValue', render: (text, record, index) => {
      const uploadFileData = {
        actCode: record.actCode, // 活动编码
        gameMode: record.gameModeFlag, // 抽奖方式,0:九宫格,1:大转盘,2:砸金蛋 ,
        imgCategory: record.imgCategoryFlag // 素材类别,0:抽奖图片,1:中奖图片,2:背景图片(bg.jpg) 3:未中奖图片(null.jpg)
      }
      return (
        <Import
          fileType={['jpeg', 'jpg', 'png', 'gif']}
          actionConfig={{ urlConfig: API_POST_SETTING_UPLOAD_IMG, data: uploadFileData }}
          onOk={this.handleImportSuccess}
          beforeImport={this.beforeImport.bind(this, record.imgName || '')}
        >
          <span className="upload-again">重新上传</span>
        </Import>
      )
    }
    }
  ]

  /**
   * 上传文件前处理
   * @param file
   * @returns {boolean}
   */
  beforeImport = (imgName, file) => {
    const regexp = new RegExp(imgName, 'i')
    if (file && file.name && !regexp.test(file.name)) {
      message.error(`上传文件名必须为${imgName}`)
      return false
    }
  }

  /**
   * 上传成功
   */
  handleImportSuccess = () => {
    message.success('上传成功')
    this.props.fetchActivityImg() // 查询活动图片
  }

  render() {
    const { dataSource = [], loading } = this.props

    return (
      <Table
        bordered={true}
        className="page-table"
        scroll={{ x: _.sumBy(this.columns, 'width'), y: 400 }}
        rowKey={record => record.id}
        columns={this.columns}
        loading={loading}
        dataSource={dataSource}
        pagination={false}
      />
    )
  }
}

export default FileTable
