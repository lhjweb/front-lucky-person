/**
 * @Description:
 * @Author: zero
 * @Date: 2019-09-19 16:45:53
 * @LastEditors: zero
 * @LastEditTime: 2019-09-19 16:45:53
*/

import '../style/ConfigView.less'
import React from 'react'
import { Button, Radio, Descriptions, Input } from 'antd'

// 效果图
const examples = {
  0: require('@img/sudoku.png'), // 九宫格
  1: require('@img/slyderAdventures.png'), // 大转盘
  2: require('@img/eggFrenzy.png') // 砸金蛋
}

class ConfigView extends React.Component {
  render() {
    const { activityType, actRemark, handleShowModal, handleDeleteAll, handleSubmit, handleActRemarkChange, handleActiveitTypeChange, handleReLoad } = this.props

    const maxSize = 300 // 最多字数
    const numberWords = Number(maxSize - actRemark.length)

    return (
      <div className="config-view">
        <Button type="danger" className="cancel fr" onClick={handleDeleteAll}>全部作废</Button>
        <Button type="primary" className="submit fr mr10" onClick={handleSubmit}>应用全部</Button>
        <div className="config-container">
          <Descriptions>
            <Descriptions.Item label="活动选项">
              <Radio.Group value={activityType} onChange={handleActiveitTypeChange}>
                <Radio value={'0'}>九宫格</Radio>
                <Radio value={'1'}>大转盘</Radio>
                <Radio value={'2'}>砸金蛋</Radio>
              </Radio.Group>
            </Descriptions.Item>
          </Descriptions>
          <div className="activity-type-box">
            <img className="example-img" src={examples[activityType]} alt="效果图"/>
          </div>
          <h3>效果图</h3>
          <div>
            <Button className="btn-w" type="primary" ghost onClick={() => handleShowModal('isShowUploadImageFileModal')}>上传图片压缩包</Button>
          </div>
          <div>
            <Button className="btn-w" type="primary" ghost onClick={handleReLoad} >预览图片效果</Button>
          </div>
          <div>
            <Input.TextArea onChange={handleActRemarkChange} placeholder="请输入活动规则" className="actRemark" value={actRemark} rows={4} maxLength={300} />
            <div className="number-words">可输入 {numberWords} 字数</div>
          </div>
        </div>
      </div>
    )
  }
}

export default ConfigView
