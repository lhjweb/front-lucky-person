/**
 * @Description:
 * @Author: zero
 * @Date: 2019-09-19 16:05:59
 * @LastEditors: zero
 * @LastEditTime: 2019-09-19 16:05:59
*/

import React from 'react'
import { Form } from 'antd'
import { CollapsibleFormBox, CommonSelect, CommonInput, ActivityName } from '@comp'

// 活动状态
const activityStatu = [
  { code: '1', value: '未开始(已审核)' },
  { code: '2', value: '正在进行' }
]

@Form.create()
class PageForm extends React.Component {
  /* 查询 */
  handleSearch = e => {
    e.preventDefault()
    const { form: { validateFields }, hacdleSearchResult } = this.props
    validateFields((error, values) => {
      if (!error) hacdleSearchResult(values)
    })
  }

  /* 重置 */
  handleReset = () => this.props.form.resetFields()

  /* 活动名称选择 */
  handleSelect = (value) => {
    const { form: { setFieldsValue } } = this.props
    let actCode
    if (value) {
      actCode = value.split(' ')[0]
    }
    setFieldsValue({
      actCode
    })
  }

  render() {
    const { form: { getFieldDecorator } } = this.props
    return (
      <Form className="page-form">
        <CollapsibleFormBox
          onSearch={this.handleSearch}
          onReset={this.handleReset}
        >
          <div>
            <ActivityName
              getFieldDecorator={getFieldDecorator}
              onChange={this.handleSelect}
            />
            <CommonInput
              getFieldDecorator={getFieldDecorator}
              label="活动编码"
              decorator="actCode"
              allowClear={true}
              rules={[{ required: true, message: '活动编码必填' }]}
              placeholder="请输入活动编码"
              // initialValue="HD000001"
            />
            <CommonSelect
              getFieldDecorator={getFieldDecorator}
              label="活动状态"
              decorator="activeStateFlag"
              allowClear={true}
              list={activityStatu}
              placeholder="请选择"
            />
          </div>
        </CollapsibleFormBox>
      </Form>
    )
  }
}

export default PageForm
