/**
 * @Description:
 * @Author: zero
 * @Date: 2019-09-19 16:17:28
 * @LastEditors: zero
 * @LastEditTime: 2019-09-19 16:17:28
*/

import '../style/PageContent.less'
import React from 'react'
import { Modal, message } from 'antd'
import { ResultView, ConfigView, UploadImageFileModal } from './index'
import {
  API_POST_SETTING_IMAGE_CANCEL_SUBMIT,
  API_POST_SETTING_IMAGE_CONFIRM_SUBMIT
} from '@api'
import http from '@http'

class PageContent extends React.Component {
  state = {
    isShowUploadImageFileModal: false // 是否显示上传图片文件弹框
  }

  /**
   * 显示弹框
   * @param type
   */
  handleShowModal = type => this.setState({
    [type]: true
  })

  /**
   * 关闭弹框
   * @param hsaUploadValue
   */
  handleCancel = () => this.setState({
    isShowUploadImageFileModal: false
  })

  /**
   *  全部作废
   */
  handleDeleteAll = () => Modal.confirm({
    title: '提示',
    content: '确认全部作废?',
    onOk: () => new Promise((resolve, reject) => {
      http.post(API_POST_SETTING_IMAGE_CANCEL_SUBMIT, { actCode: this.props.actCode }).then(() => {
        message.success('全部作废成功')
        this.props.fetchActivityImg() // 查询活动图片
      }).then(resolve).catch(reject)
    })
  })

  /**
   * 上传确定按钮
   */
  handleUploadOk = () => this.handleCancel()

  /**
   * 应用全部按钮
   */
  handleSubmit = () => Modal.confirm({
    title: '温馨提示',
    content: '确认应用全部?',
    onOk: () => new Promise((resolve, reject) => {
      http.post(API_POST_SETTING_IMAGE_CONFIRM_SUBMIT, {
        actCode: this.props.actCode,
        actRemark: this.props.actRemark || ''
      }).then(() => {
        message.success('应用全部成功')
        this.props.fetchActivityImg() // 查询活动图片
      }).then(resolve).catch(reject)
    })
  })

  render() {
    const { isShowUploadImageFileModal } = this.state
    const { actCode, tableLoading, bgPath, actRemark, activityUrl, dataSource, activityType, handleReLoad, handleActRemarkChange, fetchActivityImg, handleActiveitTypeChange } = this.props

    return (
      <div className="page-content">
        <ResultView
          actCode={actCode}
          activityType={activityType}
          activityUrl={activityUrl}
          bgPath={bgPath}
          fetchActivityImg={fetchActivityImg}
        />
        <ConfigView
          actCode={actCode}
          activityType={activityType}
          actRemark={actRemark}
          handleReLoad={handleReLoad}
          handleActRemarkChange={handleActRemarkChange}
          handleShowModal={this.handleShowModal}
          handleDeleteAll={this.handleDeleteAll}
          handleSubmit={this.handleSubmit}
          handleActiveitTypeChange={handleActiveitTypeChange}
        />
        {isShowUploadImageFileModal && <UploadImageFileModal
          actCode={actCode}
          dataSource={dataSource}
          activityType={activityType}
          tableLoading={tableLoading}
          visible={isShowUploadImageFileModal}
          onOk={this.handleUploadOk}
          onCancel={this.handleCancel}
          fetchActivityImg={fetchActivityImg}
        />}
      </div>
    )
  }
}

export default PageContent
