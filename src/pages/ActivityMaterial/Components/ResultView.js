/**
 * @Description:
 * @Author: zero
 * @Date: 2019-09-19 16:45:02
 * @LastEditors: zero
 * @LastEditTime: 2019-09-19 16:45:02
*/

import '../style/ResultView.less'
import React from 'react'
import { Button, message, Spin } from 'antd'
import { Import } from '@comp'
import { API_POST_SETTING_UPLOAD_IMG } from '@api'

class ResultView extends React.Component {
  state = {
    page: 1, // 分页
    isComplete: true // loading
  }

  componentWillReceiveProps(nextProps, nextContext) {
    if (this.props.activityUrl !== nextProps.activityUrl) {
      this.setState({
        isComplete: true
      })
    }
  }

  // 结果内容
  resultContent = () => {
    const { actCode, activityUrl, activityType } = this.props
    const { page, isComplete } = this.state

    if (page === 2) {
      return (
        <div className="success">
          <div className="tips-h1">对不起</div>
          <div className="tips-h3">您的抽奖次数已用完！</div>
          <div className="prize">
            <ul className="prize-group">
              <li className="prize-box">
                <div className="prize-img"><img src={require('@img/prize-1.png')} alt="特等奖"/></div>
                <div className="prize-name">特等奖</div>
                <div className="prize-info">格力高端空调扇一台</div>
              </li>
              <li className="prize-box">
                <div className="prize-img"><img src={require('@img/prize-2.png')} alt="幸运奖"/></div>
                <div className="prize-name">幸运奖</div>
                <div className="prize-info">金龙鱼大豆食用油一瓶</div>
              </li>
            </ul>
            <div className="tax-cost">
              <span>税费：</span><span>¥5</span>
            </div>
          </div>
          <div className="note">备注：根据《中华人民共和国个人所得税法》规定，对于偶然性质的所得应向所得人征收奖品价值20%偶然所得税款。</div>
          <div className="btn-submit">去兑奖</div>
        </div>
      )
    } else if (actCode) {
      return (
        <Spin className="show-window" spinning={isComplete} tip="Loading...">
          {
            activityUrl ? <iframe id="activityWindow" name="activityWindow" width={375} height={667} src={activityUrl} onLoad={this.iframeLoad} frameBorder="0" style={{ maxWidth: '100%' }}></iframe> : null
          }
        </Spin>
      )
    }

    /* if (activityType === '0') { // 九宫格
      return (
        <div className="sudoku-box">
          <ul className="box">
            <li className="box-item">1</li>
            <li className="box-item">2</li>
            <li className="box-item">3</li>
            <li className="box-item">4</li>
            <li className="box-item">5</li>
            <li className="box-item">6</li>
            <li className="box-item">7</li>
            <li className="box-item">8</li>
            <li className="box-item">9</li>
          </ul>
        </div>
      )
    } else if (activityType === '1') { // 大转盘
      return (
        <div>slyderAdventures</div>
      )
    } else if (activityType === '2') { // 砸金蛋
      return (
        <div>eggFrenzy</div>
      )
    }*/
  }

  /*
   * load 状态
   * */
  iframeLoad = () => {
    this.setState({ isComplete: false })
  }

  /**
   * 上传成功
   */
  handleImportSuccess = () => {
    message.success('上传成功')
    this.props.fetchActivityImg() // 查询活动图片
  }

  /**
   * 上传文件前处理
   * @param file
   * @returns {boolean}
   */
  beforeImport = file => {
    if (file && file.name && !/^bg/i.test(file.name)) {
      message.error('上传文件名必须为bg')
      return false
    }
  }

  /**
   * 改变 page
   * @param page
   */
  handleChangePage = page => this.setState({ page })

  render() {
    const { actCode, bgPath, activityType } = this.props
    const { page } = this.state

    // 图片上传传参
    const uploadFileData = {
      actCode, // 活动编码
      gameMode: activityType, // 抽奖方式,0:九宫格,1:大转盘,2:砸金蛋 ,
      imgCategory: 2 // 素材类别,0:抽奖图片,1:中奖图片,2:背景图片(bg.jpg) 3:未中奖图片(null.jpg)
    }

    return (
      <div className="result-view">
        <h1 className="location">{ page === 2 ? '2/中奖结果页' : '1/抽奖页'}</h1>
        <div className="result-content">
          <Button className="btn-l" type="link" icon="double-left" size="large" disabled={page === 1} onClick={() => this.handleChangePage(1)} ></Button>
          <Button className="btn-r" type="link" icon="double-right" size="large" disabled={page === 2} onClick={() => this.handleChangePage(2)} ></Button>
          <div className="activity-box">
            <div className="result-bg" style={{ backgroundImage: `url(${bgPath})` }}></div>
            <div className="result-box">
              {this.resultContent()}
            </div>
          </div>
        </div>
        <Import
          ghost
          type="primary"
          className="update-bg"
          title="更换底图"
          disabled={!actCode}
          fileType={['jpeg', 'jpg', 'png', 'gif']}
          actionConfig={{ urlConfig: API_POST_SETTING_UPLOAD_IMG, data: uploadFileData }}
          beforeImport={this.beforeImport}
          onOk={this.handleImportSuccess}
        />
      </div>
    )
  }
}
// 上传文件名必须为bg，例如：bg.png,bg.jpeg,bg.gif
export default ResultView
