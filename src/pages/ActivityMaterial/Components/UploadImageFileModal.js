/**
 * @Description: 上传图片文件弹框
 * @Author: zero
 * @Date: 2019-09-19 21:17:38
 * @LastEditors: zero
 * @LastEditTime: 2019-09-19 21:17:38
*/

import '../style/UploadImageFileModal.less'
import React from 'react'
import { message } from 'antd'
import { FileTable } from './index'
import { YHModal, UploadFile } from '@comp'
import {
  API_POST_SETTING_UPLOAD_ZIP_IMG
} from '@api'

class UploadImageFileModal extends React.Component {
  state = {
    lotteryDrawList: [], // 抽奖图片
    hasDrawnList: [] // 中奖图片
  }

  /**
   * 上传成功
   * @param fileList
   */
  handleUploadSuccess = (type, fileList) => {
    message.success('导入成功')
    this.setState({
      [type]: fileList.pop()
    }, this.props.fetchActivityImg)
  }

  render() {
    const { lotteryDrawList, hasDrawnList } = this.state
    const { actCode, dataSource, tableLoading, activityType, fetchActivityImg, ...props } = this.props

    const uploadFileData = {
      actCode, // 活动编码
      gameMode: activityType // 抽奖方式,0:九宫格,1:大转盘,2:砸金蛋 ,
    }

    return (
      <YHModal
        title="上传图片文件"
        width={800}
        {...props}
      >
        <div className="upload-image-file-modal">
          <div className="header">
            <h3>抽奖图片</h3>
            <UploadFile
              key="lotteryDraw"
              title={'只能上传zip格式文件'}
              fileType={['zip']}
              actionConfig={{ urlConfig: API_POST_SETTING_UPLOAD_ZIP_IMG, data: { ...uploadFileData,
                imgCategory: 0 // 素材类别,0:抽奖图片,1:中奖图片,2:背景图片(bg.jpg) 3:未中奖图片(null.jpg)
              } }}
              fileList={lotteryDrawList}
              onOk={this.handleUploadSuccess.bind(this, 'lotteryDrawList')}
            />
          </div>

          <div className="header">
            <h3>中奖图片</h3>
            <UploadFile
              key="hasDrawn"
              title={'只能上传zip格式文件'}
              fileType={['zip']}
              actionConfig={{ urlConfig: API_POST_SETTING_UPLOAD_ZIP_IMG, data: { ...uploadFileData,
                imgCategory: 1 // 素材类别,0:抽奖图片,1:中奖图片,2:背景图片(bg.jpg) 3:未中奖图片(null.jpg)
              } }}
              fileList={hasDrawnList}
              onOk={this.handleUploadSuccess.bind(this, 'hasDrawnList')}
            />
          </div>
        </div>
        <FileTable
          dataSource={dataSource}
          loading={tableLoading}
          fetchActivityImg={fetchActivityImg}
        />
      </YHModal>
    )
  }
}

export default UploadImageFileModal
