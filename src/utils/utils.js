import host from '@host'
import events from './event'
import { getVariant } from '@variant'
import { createBrowserHistory } from 'history'
import _ from 'lodash'

export const event = events

export function removeUndefined(obj) {
  let o = {}
  for (let key in obj) {
    if (obj.hasOwnProperty(key)) {
      if (obj[key] !== undefined) {
        o[key] = obj[key]
      }
    }
  }
  return o
}

export const history = createBrowserHistory()

export const tableCellFormatter = {
  number(val, suffix = '') {
    if (val === undefined) {
      return '--'
    } else if (Number.isNaN(val)) {
      return val
    } else {
      return `${Number(val).toFixed(2)}${suffix}`
    }
  },
  text(val, suffix = '') {
    if (val === undefined) {
      return '--'
    } else {
      return `${val}${suffix}`
    }
  }
}

export const exportBlobData = (blobData, configFileName) => {
  if (window.navigator.msSaveOrOpenBlob) {
    // IE 11
    window.navigator.msSaveOrOpenBlob(blobData, configFileName)
    return
  }
  let a = document.createElement('a')
  document.body.appendChild(a)
  let url = window.URL.createObjectURL(blobData)
  a.href = url
  a.download = configFileName
  a.click()
  // 异步导出，火狐专用
  setTimeout(() => window.URL.revokeObjectURL(url), 0)
}

export const generateUrl = (hostKey, api, searchParams, urlParams, replaceParams) => {
  const $host = host[hostKey]
  if (!$host) throw new Error(`Not hostKey in host => ${hostKey}`)
  let url = `${$host}${api}`
  if (replaceParams) {
    for (const key in replaceParams) {
      if (replaceParams.hasOwnProperty(key)) {
        const reg = new RegExp(`\\$${key}`, 'g')

        url = url.replace(reg, replaceParams[key])
      }
    }
  }
  if (urlParams) {
    url += `/${urlParams.join('/')}`
  }
  if (searchParams) {
    url += (() => {
      const str = []

      for (const key in searchParams) {
        if (
          searchParams.hasOwnProperty(key) &&
          searchParams[key] !== '' &&
          searchParams[key] !== undefined
        ) {
          str.push(`${key}=${searchParams[key]}`)
        }
      }

      return str.length ? `?${str.join('&')}` : ''
    })()
  }
  return url
}

/*
 获取url参数
 */
export function getQueryString(name) {
  if (name == undefined || name == null || name == '') {
    return false
  }
  let reg = new RegExp(`(^|&)${name}=([^&]*)(&|$)`)
  let r = window.location.search.substr(1).match(reg)

  if (r != null) {
    return decodeURI(r[2])
  } else {
    return null
  }
}

/**
 * [hasButtonAuth  判断用户时候有按钮 级别的权限]
 * @param  {[String]}  buttonCode [按钮编码]
 * @return {Boolean}
 */
export const hasButtonAuth = buttonCode => {
  const permissionsBans = getVariant('supplyButtonPermission') || []
  const hasBtnAuth = permissionsBans.some(
    item => item.permissionCode === buttonCode
  )
  return hasBtnAuth
}

export const dateReg = /^((((1[6-9]|[2-9]\d)\d{2})(0?[13578]|1[02])(0?[1-9]|[12]\d|3[01]))|(((1[6-9]|[2-9]\d)\d{2})(0?[13456789]|1[012])(0?[1-9]|[12]\d|30))|(((1[6-9]|[2-9]\d)\d{2})0?2(0?[1-9]|1\d|2[0-8]))|(((1[6-9]|[2-9]\d)(0[48]|[2468][048]|[13579][26])|((16|[2468][048]|[3579][26])00))0?229))$/ //  yyyymmdd
export const dateRegL = /^((((1[6-9]|[2-9]\d)\d{2})-(0?[13578]|1[02])-(0?[1-9]|[12]\d|3[01]))|(((1[6-9]|[2-9]\d)\d{2})-(0?[13456789]|1[012])-(0?[1-9]|[12]\d|30))|(((1[6-9]|[2-9]\d)\d{2})-0?2-(0?[1-9]|1\d|2[0-8]))|(((1[6-9]|[2-9]\d)(0[48]|[2468][048]|[13579][26])|((16|[2468][048]|[3579][26])00))-0?2-29-))$/ // yyyy-mm-dd
export function dateFormatConversion(dateStr) {
  // yyyy-mm-dd或yyyymmdd互相转换
  // dateReg
  if (dateRegL.test(dateStr)) {
    return dateStr
      .split('-')
      .toString()
      .replace(/,/g, '')
  } else if (dateReg.test(dateStr)) {
    const yyyy = dateStr.substring(0, 4)
    const mm = dateStr.substring(4, 6)
    const dd = dateStr.substring(6, 8)
    return `${yyyy}-${mm}-${dd}`
  } else {
    console.log('格式不正确！')
    return ''
  }
}

export const BKFrame = {
  breakEvent: e => {
    if (e.preventDefault) {
      e.preventDefault()
    } else {
      e.cancelBubble = true
      e.returnValue = false
    }
    e.stopPropagation()
  }
}

/*
 * 去重函数
 *
 * data : 数据来源
 * key : 重复字段, 应用数组值是对象形式
 * */
export function duplicate(data, key) {
  if (!data) return []
  try {
    if (_.isString(data[0])) {
      // 字符串
      return Array.from(new Set(data))
    } else if (_.isArray(data[0])) {
      // 数组
      return data.reduce((a, b) => a.concat(b), [])
    } else if (_.isObject(data[0])) {
      // 对象
      if (!_.isString(key)) throw 'key not to string'
      let obj = {}
      return data.reduce((cur, next) => {
        obj[next[key]] ? '' : (obj[next[key]] = true && cur.push(next))
        return cur
      }, [])
    } else {
      return []
    }
  } catch (e) {
    console.error(e)
    return []
  }
}

export function isIE() {
  if (!!window.ActiveXObject || 'ActiveXObject' in window) return true
  else return false
}

/**
 * 过滤Object 过滤值为-》undefined null  空字符串 （不过滤0, false）
 * @param obj
 * @returns {object}
 */
export function filterFormObject(obj) {
  if (!_.isObject(obj)) throw new Error('the value must be Object')
  return Object.entries(obj).reduce((o, [key, value]) => {
    if (value !== null && value !== undefined && value !== '') o[key] = value
    return o
  }, {})
}

export default { history, BKFrame, isIE, event, tableCellFormatter, removeUndefined, exportBlobData, generateUrl, getQueryString, dateReg, dateRegL, dateFormatConversion, hasButtonAuth, duplicate
}
