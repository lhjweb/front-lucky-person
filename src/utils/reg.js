/**
* Created by Jimmy on 2018-04-23
*/
const reg = {
  user: /[a-zA-Z0-9]{6,16}/,  // 用户名
  mobile: /^1\d{10}$/,        // 手机号
  number: /^[0-9]*$/,         // 数字
  decimal: /^[0-9]+(\.{1}[0-9]{1,2})?$/, // 小数点 1位 或者 2位
  firstZero: /^0[0-9]+/ // 匹配第一个零 例如：03 011 08.00
}

export default reg
export {
  reg
}
