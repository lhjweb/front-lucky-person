/*
* 公共部分
* */

// 页码
export const pageSizeOptions = ['10', '20', '50', '100']

export const getPagination = (settings = {}) => {
  let pageSettings = {
    showQuickJumper: true, // 是否可以快速跳转至某页
    showSizeChanger: true, // 是否可以改变 pageSize
    pageSizeOptions
  }
  if (!settings) return pageSettings

  if (settings.total) pageSettings.showTotal = (total, range) => `共 ${total} 条` // 显示总数
  if (settings.onChange && typeof settings.onChange === 'function') pageSettings.onShowSizeChange = settings.onChange // pageSize 变化的回调，可以和onChange事件重复使用

  return { ...pageSettings, ...settings }
}

export const commonSplitCode = (item, attrCode, attrName) => { // 单选根据空格拆分code和name
  if (item[attrCode] && (`${item[attrCode]}`).includes(' ')) {
    const num = item[attrCode].indexOf(' ')
    const newCode = item[attrCode].slice(0, num)
    const newName = item[attrCode].slice(num + 1)
    item[attrCode] = newCode
    item[attrName] = newName
  }
}

export const commonMoreSplitCode = (itemArr, attrCodes, attrNames) => { // 多选根据空格拆分code和name
  itemArr && itemArr.forEach(item => {
    if (item && item.includes(' ')) {
      const num = item.indexOf(' ')
      const newCode = item.slice(0, num)
      const newName = item.slice(num + 1)
      attrCodes.push(newCode)
      attrNames.push(newName)
    }
  })
}

export const getObj = (item) => { // 去除对象的空属性
  const it = {}
  for (let attr in item) {
    if (item[attr]) it[attr] = item[attr]
  }
  return it
}

export const disabledDateToStart = (current) => { // 只能选本月和以后的月份的第一天
  const date = new Date()
  const year = date.getFullYear()
  const month = date.getMonth()
  console.log(current.format('DD'))
  if (current.format('DD') === '01'
  && (current.format('YYYY') >= year - 1
  || (current.format('MM') > month) && current.format('YYYY') == year)) {
    return false
  } else {
    return true
  }
}

const saveOnce = memoize(getLastDay, {})// 缓存结果
export const disabledDateToEnd = (current) => { // 只能选本月和以后的月份的最后一天
  const lastday = saveOnce(current.format('YYYY'), current.format('MM'))
  // const lastday = getLastDay(current.format('YYYY'), current.format('MM'))
  const date = new Date()
  const year = date.getFullYear()
  const month = date.getMonth()
  if (current.format('DD') == lastday
  && (current.format('YYYY') >= year - 1
  || (current.format('MM') > month) && current.format('YYYY') == year)) {
    return false
  } else {
    return true
  }
}
// 缓存,每月的最大值只计算一次
function memoize(func, cache) {
  cache = cache || {}
  const shell = function(a, b) {
    if (!cache.hasOwnProperty(`${a}${b}`)) {
      cache[`${a}${b}`] = func(a, b)
    }
    return cache[`${a}${b}`]
  }
  // const clearCache = function() {
  //   cache = null
  // }
  return shell
}
// 获得某月的最后一天
function getLastDay(year, month) {
  let new_year = year    // 取当前的年份
  let new_month = month++// 取下一个月的第一天，方便计算（最后一天不固定）
  if (month > 12) {
    new_month -= 12        // 月份减
    new_year++            // 年份增
  }
  let new_date = new Date(new_year, new_month, 1)                // 取当年当月中的第一天
  return (new Date(new_date.getTime() - 1000 * 60 * 60 * 24)).getDate()// 获取当月最后一天日期
}

export const Timer = { // 计算执行时间
  _data: {},
  start(key) {
    Timer._data[key] = new Date()
  },
  stop(key) {
    const time = Timer._data[key]
    if (time) {
      Timer._data[key] = new Date() - time
    }
  },
  getTime(key) {
    return Timer._data[key]
  }
}

/* 前端 码表 */
export const clocks = {
  entries: (name) => {
    if (!name) throw 'name not undefined!'
    const obj = clocks[name] || {}
    let arr = []
    for (let key of Object.keys(obj)) {
      arr.push({ key, code: key, value: obj[key] })
    }
    return arr
  },
  specialUpdateFlag: { // 供应商信息修改与查询，特殊修改项
    1: '其他',
    2: '货款冻结解冻',
    3: '有纸化无纸化切换'
  }
}
