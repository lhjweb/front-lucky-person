export default {
  development: { // 本地
    usercenter: 'http://sit.usercenter.sitapis.yonghui.cn/', // 用户中心
    luckyday: 'http://luckyday-dev.yh-rfe-lucky-day.devgw.yonghui.cn/', // 抽奖
    prize: 'http://luckyday-dev.h5-lucky-person.devgw.yonghui.cn/' // H5活动地址
  },
  test: { // ITWork发布 test 地址
    usercenter: 'http://sit.usercenter.sitapis.yonghui.cn/',  // 用户中心
    luckyday: 'http://luckyday-dev.yh-rfe-lucky-day.devgw.yonghui.cn/', // 抽奖
    prize: 'http://luckyday-dev.h5-lucky-person.devgw.yonghui.cn/' // H5活动地址
  },
  production: { // 生产
    usercenter: 'http://pc-mid-p.usercenter.apis.yonghui.cn/', // 用户中心
    luckyday: 'http://luckyday-prod.yh-rfe-lucky-day.apis.yonghui.cn/', // 抽奖
    prize: 'http://luckyday-prod.h5-lucky-person.apis.yonghui.cn/' // H5活动地址
  }
}[process.env.NODE_ENV]
