/**
* Created by Jimmy on 2018-04-22
*/
import axios from 'axios'
import { message, Modal } from 'antd'
import _ from 'lodash'
import host from '@host'
import { exportBlobData, event, isIE } from '@utils'
import { getVariant, LOGIN_TOKEN } from '@variant'
let mdlShowing = false
const isLogin = () => location.pathname === '/login'

const handleAuthFailed = () => {
  if (mdlShowing) return

  mdlShowing = true

  let secondCount = 5
  const getContentStr = () => `您需要重新登录以验证身份，${secondCount} 秒后为您跳转到登录页`

  const mdl = Modal.warning({
    title: '身份信息校验失败',
    content: getContentStr(),
    centered: true,
    okText: '立即前往登录',
    onOk: () => {
      clearInterval(timer)
      mdlShowing = false
      window.location = '/login'
    }
  })

  const timer = setInterval(() => {
    secondCount -= 1
    if (secondCount === 0) {
      mdl.destroy()
      clearInterval(timer)
      window.location = '/login'
    } else {
      mdl.update({ content: getContentStr() })
    }
  }, 1000)
}

// console.log(event)

class Http {
  static _showErrMsg_(msg, duration = 2, onClose) {
    message.error(msg, duration, () => {
      onClose && onClose()
    })
  }

  async _request_(method, urlConfig, params = {}, data = {}, config = {}) {
    event.trigger('showLoading')
    try {
      const [ hostKey, path, option = {} ] = urlConfig
      const $host = host[hostKey]
      if (!$host) throw new Error(`Not hostKey in host => ${hostKey}`)
      let configFileName = config.configExportName || option.configExportName
      let hasExport = config.hasExport || option.hasExport
      let noWarn = config.noWarn || option.noWarn || false
      const loginToken = getVariant(LOGIN_TOKEN) || ''  // 登录接口返回值 缓存token
      const authHeader = {
        [LOGIN_TOKEN]: loginToken
      }
      const {
        disablePreProcessResp = false,
        urlParams = [],
        replaceParams = null,
        ...passedConfig

      } = config

      let url = this.getFinalUrl($host, path, urlParams, replaceParams)

      const requestConfig = _.merge({
        url,
        method,
        params,
        data,
        responseType: 'blob',
        timeout: 6000 * 1000,
        headers: {
          ...authHeader
        }
      }, passedConfig)

      if (isIE()) { // IE 去除缓存
        requestConfig.headers['Cache-Control'] = 'no-cache'
        requestConfig.headers.Pragma = 'no-cache'
      }

      let response
      try {
        response = await axios(requestConfig)
        event.trigger('hideLoading')
      } catch (err) {
        message.destroy()
        event.trigger('hideLoading')
        Http._showErrMsg_('请求出错，请检查您的网络链接')
        throw err
      }
      const { headers: responseHeaders } = response

      let resBlob = response.data // store the blob if it is
      let resData = null

      try {
        const resText = await this.transFormBolbData(resBlob)
        resData = JSON.parse(resText) // try to parse as json evantually
      } catch (err) {
        // console.error(err)
      }

      if (resData) {
        return this.dealWithJson(resData, disablePreProcessResp, noWarn)
      } else if (hasExport) {
        const fileName = this.getFileName(responseHeaders, configFileName)
        return exportBlobData(resBlob, fileName)
      }
    } catch (e) {
      throw e
    }
  }

  getFinalUrl = ($host, path, urlParams, replaceParams) => {
    let url = urlParams.length >= 1 ? `${$host}${path}/${urlParams.join('/')}` : `${$host}${path}`

    if (replaceParams) {
      for (let key in replaceParams) {
        if (replaceParams.hasOwnProperty(key)) {
          let reg = new RegExp(`\\$${key}`, 'g')

          url = url.replace(reg, replaceParams[key])
        }
      }
    }
    return url
  }

  getFileName(responseHeaders, configFileName) {
    let fileName = responseHeaders.filename || responseHeaders.fileName
    if (fileName) return decodeURI(fileName)
    const content = responseHeaders['Content-Disposition'] || responseHeaders['content-disposition'] // 从响应头中获取文件信息、截取文件名
    if (content && !configFileName) {
      fileName = decodeURI(content.match(/filename=(.+)/)[1])
    } else {
      fileName = (configFileName && configFileName.indexOf('.')) > -1 ? configFileName : `${configFileName || '导出信息'}.xlsx`
    }
    return fileName
  }

  async transFormBolbData(resBlob) {
    return await new Promise((resolve, reject) => {
      let reader = new FileReader()
      reader.addEventListener('abort', reject)
      reader.addEventListener('error', reject)
      reader.addEventListener('loadend', () => {
        resolve(reader.result)
      })
      reader.readAsText(resBlob)
    })
  }

  dealWithJson(resp, disablePreProcessResp, noWarn) {
    return new Promise((resolve, reject) => {
      let errMsg = ''
      const { code } = resp
      const sign = resp.hasOwnProperty('result') ? 'result' : 'page'
      switch (code) {
        case 200000:
          return disablePreProcessResp ? resolve(resp) : resolve(resp[sign])
        case 600207:
        case 8001001:
          !isLogin() && handleAuthFailed() // 非登录页面踢出来
          break
        default:
          errMsg = resp.message || resp.desc || '未知的错误'
          break
      }

      if (disablePreProcessResp) {  // 是否预处理返回的数据
        return reject(resp)
      }

      if (errMsg) {
        !noWarn && Http._showErrMsg_(errMsg)
        throw resp
      } else {
        return reject(resp)
      }
    })
  }

  static getInstance() {
    if (!this.instance) {
      this.instance = new Http()
    }
    return this.instance
  }

  /*
   * 1、理解 params 和 data
   * params (URL 形式) : ?page=1&size=10
   * data (body 形式): {page: 1, size: 10}
   *
   * 2、如果需要 url 形式的 API 接口，例如：/usercentent/id/1
   * api.js -> API_***：[base, /usercentent/id]
   * config 参数:
   *  {urlParams: [1]}
   * 这样形成 URL 拼接形式，当然 "数组" 还可形成这样，例如：/usercentent/id/1/del
   * {urlParams: [1, 'del']}
   * */
  post(url, data = {}, config = {}) {  // 如果 post 请求需要 params，放在 config 中
    return this._request_('POST', url, {}, data, config)
  }

  get(url, params = {}, config = {}) {
    return this._request_('GET', url, params, {}, config)
  }

  delete(url, params = {}, data = {}, config = {}) {
    return this._request_('DELETE', url, params, data, config)
  }

  put(url, params = {}, data = {}, config = {}) {
    return this._request_('PUT', url, params, data, config)
  }
}

const http = Http.getInstance()

export default http
