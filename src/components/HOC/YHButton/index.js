import React from 'react'
import { Button } from 'antd'
import PropTypes from 'prop-types'
import _ from 'lodash'

class YHButton extends React.Component {
  state = {
    loading: false
  }

  componentWillReceiveProps(nextProps, nextContext) {
    if (this.props.loading !== nextProps.loading) {
      this.setState({
        loading: nextProps.loading
      })
    }
  }

  handleClick = (event) => {
    if (this.state.loading) return
    const { onClick } = this.props
    if (!onClick) return false
    try {
      this.setState({ loading: true })
      clearTimeout(this.timeout)
      this.timeout = setTimeout(() => this.setState({ loading: false }), 30000) // 30s
      // onClick 必须返回 promise 才会出现按钮 loading 状态
      const prom = onClick(event)
      if (prom && prom.finally) {
        return prom.finally(() => {
          clearTimeout(this.timeout)
          this.setState({ loading: false })
        })
      } else {
        clearTimeout(this.timeout)
        this.setState({ loading: false })
      }
    } catch (e) {
      console.error(e)
      clearTimeout(this.timeout)
      this.setState({ loading: false })
    }
  }

  render() {
    const {
      onClick,
      ...props
    } = this.props

    if (!_.isFunction(onClick)) throw 'onClick it must be function'

    return <Button {...props} loading={this.state.loading} onClick={this.handleClick} />
  }
}

YHButton.propTypes = {
  onClick: PropTypes.func.isRequired
}

export default YHButton
