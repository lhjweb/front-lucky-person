# PageTabs - tabs 拓展组件

  居于 antd tabs 组件，所以基础 API 可以前往 <a href="https://ant.design/components/tabs-cn/" target="_blank">Tabs</a>。

## 自定义组件API
   - tabPanes: 按钮数据内容 | [{key, tab}, ...]
     - key 唯一性
     - tab 标题
     - badge 标记
     
## 新特性 eg
```
<PageTabs
  tabPanes={[{ key: '2', tab: '全部' }, { key: '4', tab: '我申请的' }, { key: '5', tab: '待我审的' }, { key: '6', tab: '我已审的' }]}
  disabled={tableLoading}
  activeKey={tabFlag}
  onChange={this.handleTabsChange}
>
    <div>1</div>
    <div>2</div>
    <div>3</div>
    <div>4</div>
</PageTabs>
```