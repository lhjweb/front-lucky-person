/**
 * Changed by Zero on 2019/05/17.
 */

import React from 'react'
import { Tabs, Badge } from 'antd'
import PropTypes from 'prop-types'
import './index.less'

class PageTabs extends React.PureComponent {
  render() {
    const {
      children,
      tabPanes = [], // 自定义：tabPanes 数据 ; Object[]
      type = 'card',
      activeKey = '',
      onChange,
      ...paops
    } = this.props

    const disabledAll = this.props.disabled // 全部
    const hasKey = children && children.every(chi => chi.key) // 判断是否所有子都有设置 key

    const TabsConfig = {
      type,
      activeKey,
      onChange,
      ...paops
    }

    const TabPanes = tabPanes.map((item, index) => {
      const {
        key = index,
        disabled,
        ...setting
      } = item
      let TabPanesConfig = {
        key,
        disabled: disabledAll || disabled,
        ...setting
      }

      if (item.badge) {
        TabPanesConfig.tab = (<div className="tabs-number">
          <span className="tabs-number-tab">{item.tab}</span>
          <Badge className="tabs-number-badge" count={item.badge || 0} />
        </div>)
      }

      if (hasKey) {
        let dom = children.find(chi => chi.key === item.key)
        if (dom) {
          return <Tabs.TabPane {...TabPanesConfig}
          >{dom}</Tabs.TabPane>
        }
      } else if (children && children[index]) {
        return <Tabs.TabPane {...TabPanesConfig}
        >{children[index]}</Tabs.TabPane>
      }
      return (
        <Tabs.TabPane {...TabPanesConfig}
        />
      )
    })

    return (
      <Tabs
        {...TabsConfig}
      >
        {TabPanes}
      </Tabs>
    )
  }
}

PageTabs.propTypes = {
  tabPanes: PropTypes.array.isRequired
}

export default PageTabs
