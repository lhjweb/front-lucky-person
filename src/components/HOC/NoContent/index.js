/*
* Created by Zero on 2019/06/24
*
* 暂无数据
* */

import React from 'react'
import './index.less'

class NoContent extends React.PureComponent {
  render() {
    return (
      <div className="no-content ant-empty ant-empty-normal">
        <div className="ant-empty-image">
          <img alt="暂无数据" src={require('@img/noContent-new.png')} />
        </div>
        <p className="ant-empty-description">暂无数据</p>
      </div>
    )
  }
}

export default NoContent
