/**
 * @Description:
 * @Company: YH
 * @Author: Ren YuLei
 * @LastEditors: Ren YuLei
 * @Date: 2019-06-20 11:47
 * @LastEditTime: 2019-06-20 11:47
 */

import React from 'react'
import { message, Upload, Button, Spin, Icon, Modal, Form, Tooltip } from 'antd'
import { getVariant, hasVariant, LOGIN_TOKEN } from '@variant'
import { generateUrl } from '@utils'
import _ from 'lodash'
import './index.less'
@Form.create()
export default class UploadFile extends React.Component {
  state = {
    loading: false,
    fileList: []
  }
  /**
   * 上传文件事件
   * @param info
   * @return {*}
   */
  handleChange = info => {
    const { onOk, type, code, fileInfoList } = this.props
    if (typeof info.file.status !== 'undefined' && info.file.status !== 'uploading' && info.file.status !== 'removed') {
      this.setState({ loading: false, fileList: info.fileList })
      let res = info.file.response
      let fileList = info.fileList
      if (res && res.code === 200000) {
        console.log(info)
        fileInfoList[type].push({
          fileTypeCode: code,
          fileTypeName: type,
          fileName: fileList[fileList.length - 1].name,
          fileUrl: fileList[fileList.length - 1].response.result.fileUrl
        })
        _.isFunction(onOk) ? onOk(fileInfoList) : message.success('上传成功')
      } else if (res.code === 8001001 || res.code === 600207) {  // 登陆超时处理
        window.location = '/login'
      } else {
        fileList.splice(fileList.findIndex(item => item.uid === info.file.uid), 1)
        this.setState({ fileList })
        message.error(res.message || '上传失败')
      }
    } else if (info.file.status === 'uploading') {
      this.setState({ loading: true, fileList: info.fileList })
    }
  }

  /**
   * 判断上传文件类型
   * @param file
   * @return {*}
   */
  beforeUpload = file => {
    const {
      beforeImport,
      fileType = ['jpg', 'png', 'doc', 'docx', 'pdf', 'excel'], // 优先判断文件类型
      maxSize = 5242880, // 判断文件体积最大值, 1k == 1024b  默认5M
      maxNmm = 5, // 上传文件的最大个数 默认5个
      overNumMessage,
      overSizeMessage, // 因业务需要可能会存在比较友善的提示、所以超出文件大小时增加自定义提示api
      type,
      fileInfoList
    } = this.props
    if (_.isFunction(beforeImport) && beforeImport()) return false
    let bool = false
    if (fileType) {
      if (_.isArray(fileType)) {
        let regexp = new RegExp(`\\.(${fileType.join('|')})`)
        bool = regexp.test(file.name)
      } else if (_.isString(fileType)) {
        let regexp = new RegExp(`\\.${fileType}`)
        bool = regexp.test(file.name)
      }
    }
    if (file.name.includes('xlsx') || file.name.includes('xls')) {
      bool = true
    }
    if (maxNmm && _.isNumber(maxNmm)) {
      if (fileInfoList[type] && fileInfoList[type].length >= maxNmm) {
        message.error(overNumMessage || `最多只能上传${maxNmm}个文件`)
        return false
      }
    }
    if (!bool) {
      message.error('上传文件格式不正确')
      return bool
    }
    if (maxSize && _.isNumber(maxSize)) {
      if (file.size > maxSize) {
        message.error(overSizeMessage || '上传文件太大')
        return false
      }
    }
    return bool
  }

  /**
   * 查看附件
   * @param res
   * @return {*}
   */
  handlePreview = (res) => {
    if (res.response.result) {
      let url = res.response.result.fileUrl
      window.open(url)
    }
  }
  handleOnClick = () => {
    const { onClick } = this.props
    onClick && onClick()
  }

  render() {
    const { title, label, actionConfig = {}, action, onChange, ...props } = this.props // 自定义
    const { urlConfig = [], params = {}, data = {}, config = {} } = actionConfig
    const { loading } = this.state
    const { urlParams } = config
    let url = _.isObject(actionConfig)
      ? generateUrl(urlConfig[0], urlConfig[1], params, urlParams)
      : action
    let settings = {
      action: url,
      data,
      name: 'file',
      beforeUpload: this.beforeUpload,
      onPreview: this.handlePreview,
      onChange: this.handleChange
    }
    let headers = { 'X-Requested-With': null }
    if (hasVariant(LOGIN_TOKEN)) {
      headers[LOGIN_TOKEN] = getVariant(LOGIN_TOKEN)
    }
    settings.headers = headers
    return (
      <div className='comp_upload-file'>
        <Upload {...settings}{...props}>
          <Tooltip title={title || '只能上传jpg/png/word/pdf/excel文件，且不超过5Mb'}>
            <Button onClick={this.handleOnClick}>
              <Icon type="upload" />
              {loading ? <Spin size="small" /> : null}
              {label || '上传附件'}
            </Button>
          </Tooltip>
        </Upload>
      </div>
    )
  }
}
