/**
 * @Description:
 * @Company: YH
 * @Author: Ren YuLei
 * @LastEditors: Ren YuLei
 * @Date: 2019-07-29 19:20
 * @LastEditTime: 2019-07-29 19:20
 */
import React from 'react'
import { Form, Button, Modal, message } from 'antd'
import UploadFile from './UploadFile'
import './index.less'
import http from '@http'
import _ from 'lodash'
@Form.create()
export default class UploadFileModal extends React.PureComponent {
  constructor(props) {
    super(props)
    this.state = {
      isShowPopup: false,
      fileInfoList: {},
      fileTypeList: []
    }
  }
  componentDidMount() {
    this.fetchGetFileTypeCode()
  }
  componentWillReceiveProps(nextProps) {
    if (this.props.fileInfoList !== nextProps.fileInfoList) {
      this.setState({ fileInfoList: nextProps.fileInfoList })
      this.fetchGetFileTypeCode()
    }
  }
  /**
   * 获取附件类型
   * @author Ren YuLei 2019/7/20
   */
  fetchGetFileTypeCode = () => {
    const { fileConfig = {} } = this.props
    const { method = 'GET', urlConfig = [], params = {}, config = {} } = fileConfig
    urlConfig && http._request_(method, urlConfig, params, {}, config).then(res => {
      let newArr = []
      res && res.map((item, i) => {
        newArr.push({
          code: item.valueCode,
          value: item.valueDesc
        })
      })
      this.setState({ fileTypeList: newArr }, () => {
        const { fileTypeList, fileInfoList } = this.state
        fileTypeList.map((item) => {
          Object.assign(fileInfoList, { [item.value]: [] })
        })
        this.setState({ fileInfoList })
      })
    })
  }
  /**
   * 上传成功
   * @return {*}
   */
  handleUploadSuccess = (res) => {
    const { handleUploadFileSuccess } = this.props
    this.setState({ fileInfoList: res }, () => {
      _.isFunction(handleUploadFileSuccess) ? handleUploadFileSuccess(res) : message.success('上传成功')
    })
  }
  render() {
    const {
      actionConfig
    } = this.props
    const { isShowPopup, fileTypeList, fileInfoList } = this.state
    return (
      <div>
        <Button onClick={() => this.setState({ isShowPopup: true })}>上传附件</Button>
        <Modal
          title="上传附件"
          visible={isShowPopup}
          onOk={() => this.setState({ isShowPopup: false })}
          onCancel={() => this.setState({ isShowPopup: false })}
          maskClosable={false}
          closable={false}
          className='file-modal'
        >
          {
            fileTypeList.map((item, i) =>
              <UploadFile
                actionConfig={actionConfig}
                onOk={(res) => this.handleUploadSuccess(res)}
                className="file"
                label={item.value}
                fileInfoList={fileInfoList}
                type={item.value}
                code={item.code}
                key={i}
                showUploadList={false}
              />)
          }
        </Modal>
      </div>
    )
  }
}
