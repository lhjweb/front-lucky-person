/**
 * @Description:
 * @Company: YH
 * @Author: Ren YuLei
 * @LastEditors: Ren YuLei
 * @Date: 2019-06-20 11:47
 * @LastEditTime: 2019-06-20 11:47
 */

import React from 'react'
import { message, Upload, Button, Spin, Icon, Modal, Form, Tooltip } from 'antd'
import { getVariant, hasVariant, LOGIN_TOKEN } from '@variant'
import { generateUrl } from '@utils'
import _ from 'lodash'
import './index.less'
@Form.create()
export default class UploadFile extends React.Component {
  state = {
    loading: false,
    fileList: []
  }

  componentWillReceiveProps(nextProps, nextContext) {
    if (this.props.defaultFileList !== nextProps.defaultFileList) {
      nextProps.defaultFileList && this.setState({ fileList: [...nextProps.defaultFileList] })
    }
  }

  /**
   * 上传文件事件
   * @param info
   * @return {*}
   */
  handleChange = info => {
    const { onOk } = this.props
    if (typeof info.file.status !== 'undefined' && info.file.status !== 'uploading' && info.file.status !== 'removed') {
      this.setState({ loading: false, fileList: info.fileList })
      let res = info.file.response
      let fileList = info.fileList
      if (res && res.code === 200000) {
        _.isFunction(onOk) ? onOk(this.state.fileList, res) : message.success('上传成功')
      } else if (res.code === 8001001 || res.code === 600207) {  // 登陆超时处理
        window.location = '/login'
      } else {
        fileList.splice(fileList.findIndex(item => item.uid === info.file.uid), 1)
        this.setState({ fileList })
        message.error(res.message || '上传失败')
      }
    } else if (info.file.status === 'uploading') {
      this.setState({ loading: true, fileList: info.fileList })
    }
  }

  /**
   * 判断上传文件类型
   * @param file
   * @return {*}
   */
  beforeUpload = file => {
    const {
      beforeImport,
      fileType = ['jpg', 'png', 'doc', 'docx', 'pdf', 'xlsx', 'xls'], // 优先判断文件类型 // defined default
      maxSize = 5242880, // 判断文件体积最大值, 1k == 1024b  默认5M
      maxNmm = 5, // 上传文件的最大个数 默认5个
      overNumMessage,
      overSizeMessage // 因业务需要可能会存在比较友善的提示、所以超出文件大小时增加自定义提示api
    } = this.props
    const { fileList } = this.state
    if (_.isFunction(beforeImport) && beforeImport()) return false
    let bool = false
    if (fileType) {
      if (_.isArray(fileType)) {
        let regexp = new RegExp(`\\.(${fileType.join('|')})`, 'i')
        bool = regexp.test(file.name)
      } else if (_.isString(fileType)) {
        let regexp = new RegExp(`\\.${fileType}`, 'i')
        bool = regexp.test(file.name)
      }
    }
    if (maxNmm && _.isNumber(maxNmm)) {
      if (fileList.length >= maxNmm) {
        message.error(overNumMessage || `最多只能上传${maxNmm}个文件`)
        return false
      }
    }
    if (!bool) {
      message.error('上传文件格式不正确')
      return bool
    }
    if (maxSize && _.isNumber(maxSize)) {
      if (file.size > maxSize) {
        message.error(overSizeMessage || '上传文件太大')
        return false
      }
    }
    return bool
  }

  /**
   * 查看附件
   * @param res
   * @return {*}
   */
  handlePreview = (res) => {
    let url = res.response ? res.response.result.fileUrl : res.url
    window.open(url)
  }
  handleOnClick = () => {
    const { onClick } = this.props
    onClick && onClick()
  }
  /**
   *  清空 fileList
   * @return {*}
   */
  handelEmpty = () => {
    this.setState({ fileList: [] })   // 清空 fileList
  }
  /**
   *  移除
   * @param file
   * @return {*}
   */
  handleRemove = (file) => {
    const { onOk } = this.props
    const { fileList } = this.state
    Modal.confirm({
      title: '提示',
      content: '您确认移除',
      onOk: () => {
        fileList.splice(fileList.findIndex(item => item.uid === file.uid), 1)
        this.setState({ fileList }, () => {
          _.isFunction(onOk) && onOk(this.state.fileList, 'del')
          message.success('移除成功')
        })
      }
    })
  }
  /**
   *  移除(弹窗式)
   * @param file
   * @return {*}
   */
  handleRemoveT = (file) => {
    const { fileList } = this.state
    fileList.splice(fileList.findIndex(item => item.uid === file.uid), 1)
    this.setState({ fileList }, () => {
      message.success('移除成功')
    })
  }
  render() {
    const { title, label, actionConfig = {}, action, onChange, ...props } = this.props // 自定义
    const { urlConfig = [], params = {}, data = {}, config = {} } = actionConfig
    const { loading, fileList } = this.state
    const { urlParams } = config
    let url = _.isObject(actionConfig)
      ? generateUrl(urlConfig[0], urlConfig[1], params, urlParams)
      : action
    let settings = {
      action: url,
      data,
      name: 'file',
      beforeUpload: this.beforeUpload,
      onPreview: this.handlePreview,
      onChange: this.handleChange,
      onRemove: this.handleRemove
    }
    let headers = { 'X-Requested-With': null }
    if (hasVariant(LOGIN_TOKEN)) {
      headers[LOGIN_TOKEN] = getVariant(LOGIN_TOKEN)
    }
    settings.headers = headers
    return (
      <div className='comp_upload-file'>
        <Upload {...settings}{...props} fileList={fileList}>
          <Tooltip title={title || '只能上传jpg/png/word/pdf/excel文件，且不超过5Mb'}>
            <Button onClick={this.handleOnClick}>
              <Icon type="upload" />
              {loading ? <Spin size="small" /> : null}
              {label || '上传附件'}
            </Button>
          </Tooltip>
        </Upload>
      </div>
    )
  }
}
