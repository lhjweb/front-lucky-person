#公共组件说明


## form公共组件
- 基础部分
    - CommonFormItem
    - CommonInput
    - CommonSelect
    - CommonTextArea

- 常用查询部分
    - PurchaseGroup         - 采购组
    - PurchaseOrg           - 采购组织
    - Supplier              - 供应商
    - Product               - 商品
    - StartAndEndDatePicker - 开始时间 & 结束时间 选择器
    - RangePicker - 开始时间 & 结束时间 选择器
    - DatePicker            - 时间选择器
    - Location              - 地点（门店）
    - ProcuctCategorySmall  - 商品类目
    - Business              - 商行
    - RootCategoryCodes     - 部类
    - RetailFormat          - 业态

## 高阶组件
- ConfirmLeave          - 页面离开组件
- FormSelect            - 表单-下拉框
- onCell                - 表格-超出信息样式设置
- PageTabs              - tabs 拓展组件

## 弹框部分
- LinkModal             - 常用链接 弹框
- RejectModal           - 审批驳回 使用审批流

## table
- MergedTable           - 合并单号 专用 表格
- HasArrowTable         - 表格左右fixed时  带操作箭头

## other 其他
- CollapsibleFormBox    - 查询模块 公用box
- CommonFlowLog         - 审批流 弹框
- DatetimePicker        - 日期时间选择器
- Import                - 导入公用组件
- LongText              - 超出隐藏
- ImageMagnifier        - 图像放大镜
