/**
 * Created by Jimmy on 2018/11/28
 */
import React from 'react'
import PropTypes from 'prop-types'
import { Button, Icon } from 'antd'
import './CollapsibleFormBox.less'

class CollapsibleFormBox extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      isZipped: false, // 普通查询
      isShow: props.showHighSearch || false, // 高级查询
      isCustom: props.isCustom || false // 是否自定义
    }
  }

  /* 普通 */
  toggleOrdinary = () => {
    this.setState({ isShow: false, isZipped: !this.state.isZipped })
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.showHighSearch !== this.props.showHighSearch) {
      this.setState({
        isShow: nextProps.showHighSearch
      })
    }
  }

  /* 高级 */
  toggleSenior = () => {
    !this.state.isZipped && this.setState({ isShow: !this.state.isShow })
  }

  render() {
    const { isZipped, isShow, isCustom } = this.state
    const {
      children,
      onSearch,
      onReset,
      hideBottom = false,
      searchBtnProps = {},
      resetBtnProps = {},
      onCustom
    } = this.props

    const text = isZipped ? '展开查询条件' : '收起查询条件'

    if (onSearch) searchBtnProps.onClick = onSearch
    if (onReset) resetBtnProps.onClick = onReset

    const onlyOne = React.Children.count(children) === 1 ? true : false

    let isSetting = isCustom
    if (isCustom) {
      if (onlyOne) {
        isSetting = !isZipped
      } else {
        isSetting = !isZipped && isShow
      }
    }

    return (
      <div className={`comp__collapsible-form-box ${isZipped ? 'zipped' : 'expanded'}`}>
        <div className="collapsible-form-box__content">
          {onlyOne ? children : children[0]}
          {
            !onlyOne &&
            <div className={`comp_advanced-box ${isShow ? 'expanded' : 'zipped'}`}>
              { children[1] }
            </div>
          }
        </div>
        <div className="collapsible-form-box__footer" style={{ display: hideBottom ? 'none' : 'block' }}>
          <div className="button-box">
            { onSearch && <Button {...{ type: 'primary', ...searchBtnProps }}>查询</Button>}
            { onReset && <Button {...resetBtnProps}>重置</Button>}
          </div>
          {
            onSearch &&
            <div className="collapse-controller" onClick={this.toggleOrdinary}>
              {text}
              <Icon className="collapse-icon" type="double-right" />
            </div>
          }
          {
            !onlyOne && !isZipped &&
            <div className="collapse-controller-advanced" onClick={this.toggleSenior}>
              高级查询
              <Icon className={`collapse-icon-advanced ${isShow ? 'an' : 'up'}`} type="double-right" />
            </div>
          }
          {
            isSetting &&
            <div className="abc">
              <Button className="mr10" onClick={onCustom} icon="setting" ></Button>
            </div>
          }
        </div>
      </div>
    )
  }
}

CollapsibleFormBox.propTypes = {
  onSearch: PropTypes.func,
  onReset: PropTypes.func,
  searchBtnProps: PropTypes.object,
  resetBtnProps: PropTypes.object,
  showHighSearch: PropTypes.bool
}

export default CollapsibleFormBox
