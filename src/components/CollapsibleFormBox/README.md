# CollapsibleFormBox 可折叠 form 框
| props | 说明 |
|-------|------|
| onSearch | 提交表单回调 |
| onReset | 重置表单回调 |
| searchBtnProps | 查询按钮 props |
| resetBtnProps | 重置按钮 props |
| showHighSearch | 显示高级查询  props|
| children | 一个children是一个普通查询，两个children分别是普通查询和高级查询 |
> 如果设置了 onSearch 会覆盖了 searchBtnProps 中的 onClick，reset btn 同理
