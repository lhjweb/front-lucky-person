/**
 * Created by Jimmy on 2018/11/28
 */
import React from 'react'
import PropTypes from 'prop-types'
import { Button, Icon, Form } from 'antd'
import './CollapsibleFormBox.less'

@Form.create()
export default class SearchForm extends React.PureComponent {
  constructor(props) {
    super(props)
    this.formRef = React.createRef()
    this.state = {
      update: {}, // 重置更新
      isZipped: false, // 普通查询
      isShow: props.showHighSearch || false, // 高级查询
      isCustom: props.isCustom || false // 是否自定义
    }
    const { onForm, form } = props
    onForm && onForm(form)
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.showHighSearch !== this.props.showHighSearch) {
      this.setState({
        isShow: nextProps.showHighSearch
      })
    }
  }

  /* 普通 */
  toggleOrdinary = () => {
    this.setState({ isShow: false, isZipped: !this.state.isZipped })
  }

  /* 高级 */
  toggleSenior = () => {
    !this.state.isZipped && this.setState({ isShow: !this.state.isShow })
  }

  handleClick = () => {
    const { onSearch, form } = this.props
    const { validateFields } = form
    validateFields((error, values) => {
      if (!error && onSearch) onSearch(values, form)
    })
  }

  handleReset = () => {
    const { onReset, form: { resetFields } } = this.props
    resetFields()
    this.setState({
      update: {}
    })
    onReset && onReset()
  }

  // 将form 的所有方法&属性  绑定到每个表单元素上
  renderChildren(children) {
    const { update } = this.state
    const { form } = this.props
    return React.Children.map(children,
      child => React.Children.map(
        child.props.children,
        subChild => React.cloneElement(subChild, {
          ...form,
          update
        })
      )
    )
  }

  render() {
    const { isZipped, isShow, isCustom } = this.state
    const {
      children,
      onSearch,
      onReset,
      hideBottom = false,
      searchBtnProps = {},
      resetBtnProps = {},
      onCustom
    } = this.props

    const text = isZipped ? '展开查询条件' : '收起查询条件'

    // if (onSearch) searchBtnProps.onClick = onSearch
    // if (onReset) resetBtnProps.onClick = onReset

    const onlyOne = React.Children.count(children) === 1 ? true : false

    let isSetting = isCustom
    if (isCustom) {
      if (onlyOne) {
        isSetting = !isZipped
      } else {
        isSetting = !isZipped && isShow
      }
    }

    return (
      <Form ref={this.formRef}>
        <div className={`comp__collapsible-form-box ${isZipped ? 'zipped' : 'expanded'}`}>
          <div className="collapsible-form-box__content">
            <div>{onlyOne ? this.renderChildren(children) : this.renderChildren(children[0])}</div>
            {
              !onlyOne &&
              <div className={`comp_advanced-box ${isShow ? 'expanded' : 'zipped'}`}>
                <div>{ this.renderChildren(children[1]) }</div>
              </div>
            }
          </div>
          <div className="collapsible-form-box__footer" style={{ display: hideBottom ? 'none' : 'block' }}>
            <div className="button-box">
              { onSearch && <Button onClick={this.handleClick} {...{ type: 'primary', ...searchBtnProps }}>查询</Button>}
              { <Button onClick={this.handleReset} {...resetBtnProps}>重置</Button>}
            </div>
            {
              onSearch &&
              <div className="collapse-controller" onClick={this.toggleOrdinary}>
                {text}
                <Icon className="collapse-icon" type="double-right" />
              </div>
            }
            {
              !onlyOne && !isZipped &&
              <div className="collapse-controller-advanced" onClick={this.toggleSenior}>
                高级查询
                <Icon className={`collapse-icon-advanced ${isShow ? 'an' : 'up'}`} type="double-right" />
              </div>
            }
            {
              isSetting &&
              <div>
                <Button className="mr10" onClick={onCustom} icon="setting" ></Button>
              </div>
            }
          </div>
        </div>
      </Form>
    )
  }
}

SearchForm.propTypes = {
  onSearch: PropTypes.func,
  onReset: PropTypes.func,
  searchBtnProps: PropTypes.object,
  resetBtnProps: PropTypes.object,
  showHighSearch: PropTypes.bool
}
