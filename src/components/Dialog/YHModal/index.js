import React, { PureComponent, Component } from 'react'
import { Modal, Button } from 'antd'
import './index.less'
const defaultStyle = {}

class Footer extends Component {
  render() {
    const { confirmLoading, isOk, isCancel, okText, cancelText, onCancel, onOk } = this.props
    return (
      <span>
        {isCancel ? <Button onClick={onCancel}>{cancelText || '取消'}</Button> : ''}
        {isOk ? <Button type="primary" loading={confirmLoading} onClick={onOk}>{okText || '确定'}</Button> : ''}
      </span>
    )
  }
}

class YHModal extends PureComponent {
  constructor(props) {
    super(props)
    // 唯一id用于获取dom结构
    this.id = `info-modal-${Math.random().toFixed(4) * 10000}-random`
    // 保存弹出框当前位置
    this.lastPosition
    // 是否已绑定事件
    this.eventFlag = false
    this._loadStyle = this._loadStyle().bind(this)
    this._renderPosition = this._renderPosition().bind(this)
    this.state = this._init(props)
  }
  componentDidMount() {
    this._addEvent()
  }
  componentDidUpdate() {
    this._addEvent()
  }
  componentWillReceiveProps(nextProps) {
    if (nextProps.style !== this.props.style) {
      this.state.style = this._loadStyle(nextProps.style)
    }
    if (nextProps.confirmLoading !== this.props.confirmLoading) {
      this.setState({ loading: nextProps.confirmLoading })
    }
  }
  componentWillUnmount() {
    clearTimeout(this.eventTimeout)
    this._afterClose()
    this.ele = null
  }

  _init = (props = {}) => ({
    style: this._loadStyle(this.props.style),
    isMouseDown: false,
    loading: props.confirmLoading || false
  })
  _afterClose = () => {
    this.setState(this._init())
    this._clearEvent()
  }
  _addEvent = () => {
    clearTimeout(this.eventTimeout)
    this.eventTimeout = setTimeout(() => {
      if (!this.eventFlag && this.props.visible) {
        this.ele = document.getElementsByClassName(this.id)[0]
        this.ele.getElementsByClassName('ant-modal-header')[0].addEventListener('mousedown', this._mousedown)
        this.ele.ownerDocument.addEventListener('mouseup', this._mouseup)
        this.ele.ownerDocument.addEventListener('mousemove', this._mousemove)
        this.eventFlag = true
      }
    }, 10)
  }
  _clearEvent = () => {
    if (this.ele) {
      this.ele.getElementsByClassName('ant-modal-header')[0].removeEventListener('mousedown', this._mousedown)
      this.ele.ownerDocument.removeEventListener('mouseup', this._mouseup)
      this.ele.ownerDocument.removeEventListener('mousemove', this._mousemove)
      this.eventFlag = false
    }
  }
  _mousedown = e => {
    this.computedStyle = window.getComputedStyle(this.ele)
    this.lastPosition = {
      left: this.state.left == undefined ? this._caculateLeft() : this.state.left,
      top: this.state.top == undefined ? this._caculateTop() : this.state.top
    }
    this.lastPositionEvent = {
      x: e.screenX,
      y: e.screenY
    }
    this.currentTarget = e.currentTarget
    this.setState({ isMouseDown: true })
  }
  _caculateLeft = () => {
    let left = parseInt(this.computedStyle.left)
    if (Number.isNaN(left)) {
      return (this.ele.parentNode.clientWidth - this.ele.clientWidth) / 2
    }
    return left
  }
  _caculateTop = () => {
    let top = parseInt(this.computedStyle.top)
    if (Number.isNaN(top)) {
      return (this.ele.parentNode.clientHeight - this.ele.clientHeight) / 2
    }
    return top
  }
  _mouseup = () => {
    this.setState({ isMouseDown: false })
  }
  _mousemove = e => {
    if (this.state.isMouseDown) {
      this._renderPosition(e)
    }
  }
  _middle = (input, min, max) => {
    if (min > max) {
      console.error(`${min} is large than ${max}`)
    } else {
      if (input < min) {
        return min
      } else if (input > max) {
        return max
      }
      return input
    }
  }

  _renderPosition = () => {
    let lastRender = false
    return e => {
      if (!lastRender) {
        lastRender = true
        let left,
          top,
          currentTarget = this.currentTarget

        setTimeout(() => {
          this.lastPosition.left += e.screenX - this.lastPositionEvent.x
          this.lastPosition.top += e.screenY - this.lastPositionEvent.y
          this.lastPositionEvent.x = e.screenX
          this.lastPositionEvent.y = e.screenY
          let marginTop = parseInt(this.computedStyle.marginTop) || 0
          let marginLeft = parseInt(this.computedStyle.marginLeft) || 0
          let clientHeight = currentTarget.clientHeight
          top = this._middle(this.lastPosition.top, -marginTop, this.ele.parentNode.clientHeight - marginTop - clientHeight)
          left = this._middle(this.lastPosition.left, -marginLeft - this.ele.clientWidth + 2 * clientHeight, this.ele.parentNode.clientWidth - marginLeft - clientHeight)
          // this.ele.style.transform = `translate(${lastTranslate.x}, ${lastTranslate.y})`
          this.setState({ style: Object.assign({}, this.state.style, { left: `${left}px`, top: `${top}px` }) })
          lastRender = false
        }, 30)
      }
    }
  }

  // 合并样式
  _loadStyle() {
    let props,
      result = defaultStyle

    return style => {
      if (style !== props) {
        props = style
        result = Object.assign({}, defaultStyle, style)
      }

      return result
    }
  }

  _handleOk = () => {
    if (this.state.loading) return
    const { onOk } = this.props
    if (!onOk) return false
    try {
      this.setState({ loading: true })
      clearTimeout(this.timeout)
      this.timeout = setTimeout(() => this.ele && this.setState({ loading: false }), 30000) // 30s
      // onOk 必须返回 promise 才会出现按钮 loading 状态
      const prom = onOk()
      if (prom && prom.finally) {
        return prom.finally(() => {
          if (this.ele) {
            clearTimeout(this.timeout)
            this.setState({ loading: false })
          }
        })
      } else {
        if (this.ele) {
          clearTimeout(this.timeout)
          this.setState({ loading: false })
        }
      }
    } catch (e) {
      console.error(e)
      if (this.ele) {
        clearTimeout(this.timeout)
        this.setState({ loading: false })
      }
    }
  }

  _handleCancel = () => {
    const { onCancel } = this.props
    onCancel && onCancel()
  }

  render() {
    const { style, isMouseDown, loading } = this.state

    let {
      className = '',
      okText,
      cancelText,
      footer,
      isOk,
      isCancel,
      ...props
    } = this.props
    let settings = {
      maskClosable: false,
      okText,
      cancelText,
      confirmLoading: loading,
      ...props,
      style,
      onCancel: this._handleCancel,
      onOk: this._handleOk,
      afterClose: this._afterClose,
      className: `info-modal ${this.id} ${isMouseDown ? ' no-select' : ''} ${className}`
    }

    if (footer !== undefined) {
      settings.footer = footer
    } else if (isOk || isCancel) {
      settings.footer = <Footer confirmLoading={loading} isOk={isOk} isCancel={isCancel} okText={okText} cancelText={cancelText} onCancel={this._handleCancel} onOk={this._handleOk} />
    }
    return (
      <Modal
        {...settings}
      />
    )
  }
}

export default YHModal
