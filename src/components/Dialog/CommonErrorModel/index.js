/**
 * Created by chencheng on 2018/4/17.
 *
 * Update by Zero on 2019/08/27.
 */
import React, { Component } from 'react'
import { Button } from 'antd'
import { YHModal } from '@comp'
import './style.less'

export default class App extends Component {
  render() {
    const { wrapClassName, visible, title, text, maskClosable = false, width = 500, hasntCancel, ...props } = this.props

    // hasntCancel 判断是否有取消按钮 [待优化]
    let settings = {}
    if (hasntCancel) {
      settings.footer = <Button key="submit" type="primary" onClick={this.props.onCancel}>确定</Button>
    }

    return (
      <YHModal
        visible={visible}
        className={'comp_common-error-model'}
        wrapClassName={wrapClassName ? `${wrapClassName} vertical-center-modal` : 'vertical-center-modal'}
        title={title}
        maskClosable={maskClosable}
        width={width}

        {...props}
        {...settings}
      >
        {text}
      </YHModal>
    )
  }
}
