/*
* Created by Zero to 2019/07/18
* */

import './index.less'
import React, { Component } from 'react'
import { Checkbox, Row, Col } from 'antd'
import { YHModal } from '@comp'
import PropTypes from 'prop-types'
import {
  API_GET_USER_TABLE_HEADER_LIST,
  API_POST_USER_TABLE_HEADER_SAVE
} from '@api'
import http from '@http'

const BUSINESS_TYPE = '2' // 业务类型 1:查询条件 2:table列头

let checkedValues = [] // 选中的值

class CustomTableHeader extends Component {
  state = {
    dataSource: []
  }

  componentDidMount() {
    this.props.visible && this.fetchSetting(this.props)
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.visible) {
      if (nextProps.columnList !== this.props.columnList) {
        this.fetchSetting(nextProps)
      }
    }
  }

  // 查询 设置
  fetchSetting = (props) => {
    const { columnList = {}, moduleType = '', businessType = BUSINESS_TYPE } = props
    const { urlConfig = API_GET_USER_TABLE_HEADER_LIST, params, data, config } = columnList

    const newParams = {
      businessType,
      moduleType,
      ...params,
      ...data
    }

    // 获取接口
    urlConfig && http.get(urlConfig, newParams, config).then(data => {
      this.setState({ dataSource: data.sort((a, b) => a.sort - b.sort).filter(item => item.keyName) })
    })
  }

  _onChange = values => {
    checkedValues = values
  }

  _handlerCancel = () => {
    const { handleCancel } = this.props

    handleCancel()
    checkedValues = []
  }

  // 保存按妞接口
  _handlerOk = () => new Promise((resolve, reject) => {
    try {
      let { columnSave = {}, moduleType = '', businessType = BUSINESS_TYPE, handleCancel, handleOK } = this.props
      const { urlConfig = API_POST_USER_TABLE_HEADER_SAVE, params, data, config } = columnSave
      let { dataSource } = this.state
      let list = JSON.parse(JSON.stringify(dataSource))

      list.map(item => {
        checkedValues.indexOf(item.enName) > -1 ? item.isChecked = 1 : item.isChecked = 0
      })

      let selectKeys = list.filter(item => item.isChecked).concat(list.filter(item => !item.isChecked)).map((item, index) => {
        item.sort = index

        return {
          isChecked: item.isChecked,
          keyName: item.keyName,
          sort: item.sort
        }
      })

      const newDate = {
        businessType,
        moduleType,
        selectKeys,
        ...params,
        ...data
      }

      urlConfig && http.post(urlConfig, newDate, config).then(() => {
        checkedValues = []
        handleCancel()
        handleOK()
      }).then(resolve).catch(reject)
    } catch (e) {
      console.error(e)
      reject()
    }
  })

  render() {
    const { visible } = this.props
    let { dataSource } = this.state

    // checkedValues = []
    dataSource.map(item => {
      item.isChecked ? checkedValues.push(item.enName) : null
    })
    dataSource.sort((a, b) => a.sort - b.sort)

    return (
      <YHModal
        title="自定义列表头"
        visible={visible}
        onOk={this._handlerOk}
        onCancel={this._handlerCancel}
        width="800px"
      >
        <Checkbox.Group defaultValue={checkedValues} onChange={this._onChange}>
          <Row>
            {dataSource.map(item =>
              <Col style={{ lineHeight: '30px' }} key={item.keyName} span={6}>
                <Checkbox
                  disabled={item.defaulted === 1 ? true : false}
                  value={item.enName}>
                  {item.chName}
                </Checkbox>
              </Col>
            )}
          </Row>
        </Checkbox.Group>
      </YHModal>
    )
  }
}

CustomTableHeader.propTypes = {
  moduleType: PropTypes.string.isRequired // 功能模块类型
  // businessType: PropTypes.string.isRequired // 业务类型 1:查询条件 2:table列头
}

export default CustomTableHeader
