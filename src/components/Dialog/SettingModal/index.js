/**
 * Created by wangjun on 2017/9/21.
 *
 * Update by Zero on 2019/09/04.
 */

import React, { Component } from 'react'
import { Checkbox, Row, Col, Spin } from 'antd'
import YHModal from '../YHModal'
import './index.less'
import http from '@http'
import _ from 'lodash'

export default class SettingModal extends Component {
  constructor(props) {
    super(props)
    this.state = {
      loading: false, // 表格loading
      columns: _.isArray(props.columns) ? this.init(props.columns) : []
    }
  }

  componentDidMount() {
    if (!_.isEmpty(this.props.columnConfig)) {
      this.fetchColumn(this.props)
    }
  }

  componentWillReceiveProps(props) {
    if (props.columns !== this.props.columns) {
      this.state.columns = this.init(props.columns)
    }
    if (props.columnConfig !== this.props.columnConfig) {
      this.fetchColumn(props)
    }
  }

  /* 初始化数据 */
  init = columns => {
    let columnList = [...columns].map(item => ({
      ...item,
      id: item.id || item.keyName,
      required: !!item.required,
      checked: !!(item.checked || item.isChecked)
    }))
    let keys = new Set()

    return columnList.filter(item => {
      if (keys.has(item.id)) {
        return false
      }
      keys.add(item.id)

      return true
    }).sort((a, b) => a.sort - b.sort)
  }

  /* 获取列表 */
  fetchColumn = props => {
    if (!props.columnConfig) return
    const { method = 'GET', urlConfig, params, data, config } = props.columnConfig

    this.setState({
      loading: true
    })
    // 获取接口
    urlConfig && http._request_(method, urlConfig, params, data, config).then(res => {
      const columns = _.isArray(res) ? this.init(res) : []
      if (columns.length) {
        this.setState({
          columns
        })
      }
    }).finally(() => this.setState({ loading: false }))
  }

  handleOk = () => {
    const { onOk } = this.props
    return onOk && onOk(this.state.columns)
  }

  handlerChange = e => {
    let pos = e.target.value,
      isChecked = e.target.checked

    this.state.columns[pos].checked = isChecked
    this.forceUpdate()
  }

  handleDragStart = e => {
    let { columns } = this.state

    let index = columns.findIndex(item => item.id == this.dragTarget)
    if (index >= 0) {
      index && (columns[index].isDrag = false)
    }
    this.dragTarget = e.currentTarget.getAttribute('data-id')
    columns[columns.findIndex(item => item.id == this.dragTarget)].isDrag = true
    this.setState({ columns })
  }

  handleDragOver = e => {
    let target = e.currentTarget.getAttribute('data-id')
    let draggable = e.currentTarget.getAttribute('draggable')

    if (draggable === 'false') {
      return
    }

    if (target !== this.dragTarget) {
      if (!this.isAnimation(target)) {
        this.exchangePosition(target, this.dragTarget)
      }
    }
  }

  isAnimation = target => {
    if (this.lastTarget) {
      if (this.lastTarget === target) {
        return this.isAnimationFlag
      }
    }
    this.lastTarget = target
    this.clearClassName(this.state.columns)
    this.setState({ columns: this.init(this.state.columns) })
    return false
  }

  clearClassName = columns => {
    columns.map(item => {
      item.className = undefined
    })
  }

  exchangePosition = (target, dragTarget) => {
    let { columns } = this.state
    this.clearClassName(columns)
    clearTimeout(this.animation)
    let dragIndex = columns.findIndex(item => item.id == dragTarget),
      toIndex = columns.findIndex(item => item.id == target),
      sort = columns[dragIndex].sort

    columns[dragIndex].className = this.caculateClass(sort, columns[toIndex].sort)
    columns[dragIndex].sort = columns[toIndex].sort
    columns[toIndex].className = this.caculateClass(columns[toIndex].sort, sort)
    columns[toIndex].sort = sort
    this.isAnimationFlag = true
    this.animation = setTimeout(() => {
      this.clearClassName(this.state.columns)
      this.isAnimationFlag = false
      this.setState({ columns: this.init(this.state.columns) })
    }, 400)
    this.setState({ columns: this.init(columns) })
  }

  caculateClass = (from, to) => {
    if (from < to) {
      if ((to - from) % 3 == 0) {
        return 'moveDown'
      } else if (to - from == 1) {
        return 'moveRight'
      } else if (to - from == 2) {
        return 'moveLeftDown'
      } else if (to - from == 4) {
        return 'moveRightDown'
      }
    } else {
      if ((from - to) % 3 == 0) {
        return 'moveUp'
      } else if (from - to == 1) {
        return 'moveLeft'
      } else if (from - to == 2) {
        return 'moveRightUp'
      } else if (from - to == 4) {
        return 'moveLeftUp'
      }
    }
  }

  handleDragEnd = e => {
    let { columns } = this.state
    columns[columns.findIndex(item => item.id == this.dragTarget)].isDrag = false
    this.setState({ columns })
  }

  render() {
    const { loading, columns } = this.state
    const { hasCheckBox = true } = this.props
    return (
      <YHModal
        {...this.props}
        onOk={this.handleOk}
        title="自定义列表项"
        width={500}
        className="custom-table-header"
      >
        <Spin spinning={loading} tip="Loading...">
          <Row>
            {
              columns &&
              columns.map(
                (item, index) =>
                  <Col
                    span={8}
                    key={item.id}
                    draggable={item.disableDraggable ? false : true}
                    data-id={item.id}
                    className={`${item.className || ''} noWrap ${item.isDrag ? 'draged' : ''} ${item.disableDraggable ? 'disabledraggalbe' : ''}`}
                    onDragStart={this.handleDragStart}
                    onDragOver={this.handleDragOver}
                    onDragEnd={this.handleDragEnd}
                  >
                    {
                      hasCheckBox ?
                        <Checkbox value={index} disabled={item.required} checked={item.checked} onChange={this.handlerChange}>
                          {item.chName}
                        </Checkbox> : <label className="dragItem">{item.chName}</label>
                    }

                  </Col>
              )
            }
          </Row>
        </Spin>
      </YHModal>
    )
  }
}
