### 超出隐藏
| props | 说明          |  默认值            |  type |
|-------|-------------- | ----------------- | ------ |
| title | 显示标题 必填      |  无              |  *  |
| width | 元素宽度 必填      | 无                     | Number | String  |
| children| 当显示与title内容不一致时， 分别传入children 和 title   | |
