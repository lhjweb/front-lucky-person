/**
* Created by Jimmy on 2019-06-19
*/
import React from 'react'
import PropTypes from 'prop-types'
import { Tooltip } from 'antd'
import './index.less'

class LongText extends React.Component {
  render() {
    const { children, title = '', placement = 'top', className, width } = this.props
    let w = width.toString().match(/^\d+/) || [110]
    w = w <= 10 ? 110 : w
    const hasChildren = Boolean(children)
    return (
      <Tooltip title={title} placement={placement} className={className}>
        <div className='comp_long-text-ellipsis_1' style={{ width: `${w - 10}px` }}>{hasChildren ? children : title}</div>
      </Tooltip>
    )
  }
}

LongText.propTypes = {
  width: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.number
  ]).isRequired
}

export default LongText
