/**
 * Created by 李华良 on 2018/11/29
 */
import React from 'react'
import PropTypes from 'prop-types'
import { DatePicker, TimePicker } from 'antd'
import moment from 'moment'
import './DatetimePicker.less'

// todo: disabled datetime
class DatetimePicker extends React.Component {
  static getDerivedStateFromProps(nextProps) {
    // Should be a controlled component.
    if ('value' in nextProps) {
      return {
        date: nextProps.value,
        time: nextProps.value
      }
    }
    return null
  }

  constructor(props) {
    super(props)

    const { value } = props
    this.state = {
      date: value,
      time: value
    }
  }

  handleDateChange = val => {
    let date
    let time

    if (!val) {
      date = val
      time = null
    } else {
      date = val
      time = this.state.time
    }

    if (!('value' in this.props)) {
      this.setState({ date, time })
    }

    this.triggerChange({ date, time })
  }

  handleTimeChange = val => {
    let date
    let time

    if (!val) {
      date = this.state.date
      time = date ? moment().startOf('day') : null
    } else {
      date = this.state.date || moment()
      time = val
    }

    if (!('value' in this.props)) {
      this.setState({ date, time })
    }

    this.triggerChange({ date, time })
  }

  triggerChange = changedVal => {
    const { onChange } = this.props
    if (onChange) {
      let { date, time } = changedVal

      if (date) {
        date = moment(date).startOf('day')
      }
      if (time) {
        time = moment(time).diff(moment(time).startOf('day'))
      }
      onChange(date ? date.add(time) : null)
    }
  }

  render() {
    const { date, time } = this.state

    return (
      <span className="comp__date-time-picker">
        <DatePicker
          value={date}
          onChange={this.handleDateChange}
        />
        <TimePicker
          value={time}
          onChange={this.handleTimeChange}
        />
      </span>
    )
  }
}

DatetimePicker.propTypes = {
}

export default DatetimePicker
