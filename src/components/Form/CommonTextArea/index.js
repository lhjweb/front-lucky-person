/**
 * Created by wangjun on 2017/8/30.
 */
import React, { Component } from 'react'
import { Form, Input } from 'antd'

const FormItem = Form.Item
const { TextArea } = Input
const defaultStyle = { width: '100%' }
import _ from 'lodash'

export default class CommonTextArea extends Component {
  constructor() {
    super()
    this._loadStyle = this._loadStyle().bind(this)
  }

  onChange = value => {
    const { onChange } = this.props
    onChange && onChange(value)
  }

  _loadStyle() {
    let props,
      result = defaultStyle

    return style => {
      if (style !== props) {
        props = style
        result = Object.assign({}, defaultStyle, style)
      }

      return result
    }
  }

  render() {
    const {
      getFieldDecorator,
      placeholder,
      style,
      label = 'label',
      decorator,
      required,
      className,
      initialValue,
      type,
      inputType,
      disabled,
      onBlur,
      colon = false,
      validateFirst,
      // max,
      labelCol,
      wrapperCol,
      message,
      validator,
      ...props
    } = this.props
    let settings = {}
    let textareaSettings = {
      ...props,
      style: this._loadStyle(style),
      onChange: this.onChange,
      onBlur,
      type: inputType,
      placeholder,
      disabled: disabled || false
    }

    // 表单配置
    if (validateFirst !== undefined) {
      settings.validateFirst = validateFirst
    }
    // 初始值
    if (initialValue !== undefined) {
      settings.initialValue = initialValue
    }

    // 校验规则
    if (!disabled) { // 支持rules，和单个项 两种格式 单项模式写的不全 如需 自行补充
      let rules = []
      let rulesFlag = false

      const propsRules = this.props.rules || []
      if (!_.isEmpty(propsRules)) {
        rules = [...propsRules]
        rulesFlag = true
      }
      // 是否为必选项
      if (required) {
        rules.push({ required: true, message })
        rulesFlag = true
      }
      // 检验类型
      if (type !== undefined) {
        rules.push({ type, message })
        rulesFlag = true
      }
      // //  报错信息
      // if (message !== undefined) {
      //   rules.message = message
      //   rulesFlag = true
      // }
      if (validator !== undefined) {
        rules.push({ validator, message })
        rulesFlag = true
      }
      rulesFlag && (settings.rules = [...rules])
    }

    return (
      <FormItem label={label} className={`${className || ''}`} labelCol={labelCol} wrapperCol={wrapperCol} colon={colon}>
        {
          getFieldDecorator(decorator || 'decorator', settings)(
            <TextArea {...textareaSettings} />
          )
        }
      </FormItem>
    )
  }
}
