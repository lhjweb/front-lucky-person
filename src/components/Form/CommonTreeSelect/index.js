/**
 * Created by wangjun on 2017/8/9.
 */
import React, { Component } from 'react'
import { Form, TreeSelect } from 'antd'
// import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import http from '@http'
const FormItem = Form.Item
const defaultStyle = { width: '100%', height: '30px' }
const defaultDropdownStyle = { maxHeight: '500px' }
const { SHOW_PARENT, SHOW_ALL, SHOW_CHILD } = TreeSelect

@connect(
  () => ({})
)

class CommonTreeSelect extends Component {
  constructor(props) {
    super(props)
    this._loadStyle = this._loadStyle().bind(this)
    this._fetchData = this._fetchData().bind(this)
    this.hasFetch = false
    // 保存的所有的key值
    this.treeAllKey
    // antd-onchange暴露出来的选中value值-复制品，用于onsearch还原
    this.copyValue = Array.isArray(props.initialValue) ? [...props.initialValue] : props.initialValue || []
    // antd-onchange暴露出来的选中值的树形结构-复制品，用于判断是否选中并还原
    this.allCheckedNodes = Array.isArray(props.initialValue) ? [...props.initialValue] : props.initialValue || []
    // onSearch时临时保存的树形结构
    this.onSearchAllCheckedNode = [...this.allCheckedNodes]
    // 保存的所有key值的树形结构
    this.treeKeysMap
    // 保存搜索的数组
    this.value = []
    this.state = { inputValue: '' }
  }
  componentWillMount() {
    this.state.treeData = this._initTreeData(this.props)
    this._initInitialValue(this.props)
    this._init(this.props)
  }
  componentWillReceiveProps(props) {
    this._init(props)
  }
  componentDidMount() {
    const { hasPaste } = this.props

    hasPaste && (this.refs.topDom.onpaste = this._handleOnPaste)
  }
  componentWillUnmount() {
    const { hasPaste } = this.props

    this.unMount = true
    hasPaste && (this.refs.topDom.onpaste = null)
  }

  // 下拉框显示隐藏切换回调
  handleOnDropdownVisibleChange = open => {
    const { onChange } = this.props
    if (!open && this.state.inputValue) {
      this.setState({ inputValue: '' })
      this.copyValue = this.onSearchSelect
      this.allCheckedNodes = this.onSearchAllCheckedNode
      onChange && onChange(this.copyValue, this.copyValue, { allCheckedNodes: this.allCheckedNodes })
    }
    this.props.setValue(this.copyValue)
    return true
  }
  handleOnSearch = value => {
    const { hasPaste } = this.props
    if (hasPaste) {
      if (this.pasteData !== undefined && this.pasteData == value.replace(/\s+/g, ' ').trim()) {
        this._onPaste(this.pasteData)

        return
      }
    }
    this._onSearch([value])
  }
  hanldeOnChange = (value, label, extra) => {
    const { onChange, autoSelect, hasPaste } = this.props

    // console.log('onchange', value, label, extra)
    if (autoSelect || hasPaste) {
      if (extra && value && extra.preValue && extra.preValue.length + 1 == value.length) {
        let valueItem = value[value.length - 1]
        switch (this._isSelected(valueItem)) {
          case 1:
            this.setState({ inputValue: '' })
            this.props.setValue(this.onSearchSelect)
            onChange && onChange(this.onSearchSelect, '', { allCheckedNodes: this.allCheckedNodes })
            return
          case 2: {
            this.setState({ inputValue: '' })
            let childKeys = this._getChildKeys(valueItem)
            let newValue = this.onSearchSelect.filter(item => !childKeys.includes(item))
            this.allCheckedNodes = this.allCheckedNodes.filter(item => !item.pos.match(`^${this.treeKeysMap[valueItem].pos}`))
            this.allCheckedNodes.push({ pos: this.treeKeysMap[valueItem].pos })
            this.copyValue = newValue.concat(this._getShouldSelect(valueItem))
            this.props.setValue(this.copyValue)
            onChange && onChange(this.copyValue, '', { allCheckedNodes: this.allCheckedNodes })
            return
          }
        }
      }
      if (extra && Array.isArray(extra.allCheckedNodes)) {
        this.allCheckedNodes = [...extra.allCheckedNodes]
      } else {
        this.allCheckedNodes = []
      }
      this.copyValue = value ? [...value] : null
      this.setState({ inputValue: '' })
      this.props.setValue(value)
      onChange && onChange(value, '', extra)
      return
    }
    onChange && onChange(value, label, extra)
    this.forceUpdate()
  }
  handleFilterTreeNode = (inputValue, treeNode) => {
    if (this.value.length > 1) {
      return this.value.includes(treeNode.props[this.props.treeNodeFilterProp || 'value'])
    } else {
      return this.props.filterTreeNode || treeNode.props[this.props.treeNodeFilterProp || 'value'].match(`^${inputValue}`)
    }
  }
  _onSearch = value => {
    const { onSearch, autoSelect, hasPaste } = this.props
    this.setState({ inputValue: value.join(' ') })
    if (autoSelect || hasPaste) {
      let initialValue = this.copyValue
      this.onSearchAllCheckedNode = [...this.allCheckedNodes]
      this.hasSearch = true
      /*
       * 输入了存在的key值
       * */
      value.forEach(valueItem => {
        if (this.treeAllKey.includes(valueItem)) {
          /*
           * 判断存在的key值是否未选中(0: 未选中, 2: 子节点部分选择, 1: 已选中)
           * */
          let isSelectFlag = this._isSelected(valueItem)
          switch (isSelectFlag) {
            case 0:
              this.onSearchAllCheckedNode.push({ pos: this.treeKeysMap[valueItem].pos })
              initialValue = initialValue.concat(this._getShouldSelect(valueItem))
              break
            case 1:
              break
            case 2: {
              let childKeys = this._getChildKeys(valueItem)
              /*
               * 获取应该选中的值
               * */
              let newValue = initialValue.filter(item => !childKeys.includes(item))
              this.onSearchAllCheckedNode = this.allCheckedNodes.filter(item => !item.pos.match(`^${this.treeKeysMap[valueItem].pos}`))
              this.onSearchAllCheckedNode.push({ pos: this.treeKeysMap[valueItem].pos })
              initialValue = newValue.concat(this._getShouldSelect(valueItem))
              break
            }
          }
        }
      })
      this.value = value || []
      this.props.setValue(initialValue)
      this.onSearchSelect = initialValue
    }
    onSearch && onSearch(value)
  }
  _initInitialValue = props => {
    this.state.initialValue = props.initialValue
    // antd-onchange暴露出来的选中value值-复制品，用于onsearch还原
    this.copyValue = Array.isArray(props.initialValue) ? [...props.initialValue] : props.initialValue || []
    // antd-onchange暴露出来的选中值的树形结构-复制品，用于判断是否选中
    this.allCheckedNodes = Array.isArray(props.initialValue) ? [...props.initialValue] : props.initialValue || []
    // onSearch时临时保存的树形结构
    this.onSearchAllCheckedNode = [...this.allCheckedNodes]
  }
  // 粘贴
  _onPaste = value => {
    let re = /\s+/
    let list = value.trim().split(re)
    if (list && list.length > 1) {
      if (!this.props.multiple) {
        this._onSearch(list[list.length - 1])
      } else {
        this._onSearch(list)
      }
    }
  }
  // 获取指定值的选择项
  _getShouldSelect = value => {
    const { showCheckedStrategy = 'child' } = this.props
    switch (showCheckedStrategy) {
      case 'child':
        return this._getChildren(value)
    }
  }
  // 获取指定值的所有叶子节点值
  _getChildren = value => {
    let result = []
    if (this.treeKeysMap[value].children) {
      this.treeKeysMap[value].children.forEach(item => {
        result = result.concat(this._getChildren(item))
      })
      return result
    }
    result.push(value)
    return result
  }
  // 判断是否选中
  // 0：未选中， 1：选中， 2：父项选中，导致重新便利该父项子数据
  _isSelected = value => {
    let result = 0

    this.onSearchAllCheckedNode.some(item => {
      let match
      if (!item || !item.pos) return false

      try {
        if (this.treeKeysMap[value].pos.length >= item.pos.length) {
          match = this.treeKeysMap[value].pos.match(`^${item.pos}`)
        } else {
          match = item.pos.match(`^${this.treeKeysMap[value].pos}`)
        }
        if (match) { // 包含情况
          if (this.treeKeysMap[value].pos.length == item.pos.length) { // 相同
            result = 1
            return true
          } else if (this.treeKeysMap[value].pos.length < item.pos.length) {
            /*
            * 场景：
            * this.treeKeysMap[value].pos： '0-1-1'
            * item.pos：'0-1-1-2'
            * */
            result = 2
            return true
          }
        } else {
          return false
        }

        /* if (match && this.treeKeysMap[value].pos.length < item.pos.length) {
          result = 2
          return true
        } else if (match) {
          result = 1
          return true
        } else {
          return false
        }*/
      } catch (err) {
        console.error(err)
        return false
      }
    })
    return result
  }
  // 获取所有子节点的值
  _getChildKeys = value => {
    let result = []
    if (this.treeKeysMap[value].children) {
      this.treeKeysMap[value].children.forEach(item => {
        result.push(item)
        result = result.concat(this._getChildKeys(item))
      })
    }
    return result
  }
  // 获取所有节点的层级关系
  _getTreeKeyMap = data => {
    const { treeNodeFilterProp = 'value' } = this.props
    this.treeAllKey = []
    let result = {}
    let getKey = (arr, parent, lastLevel = 0) => {
      if (Array.isArray(arr)) {
        arr.forEach((item, index) => {
          let pos = `${lastLevel}-${index}`
          if (item[treeNodeFilterProp]) {
            this.treeAllKey.push(item[treeNodeFilterProp])
            result[item[treeNodeFilterProp]] = {
              parent,
              pos
            }
          }
          if (item.children) {
            result[item[treeNodeFilterProp]].children = item.children.map(item => item[treeNodeFilterProp])
            getKey(item.children, result[item[treeNodeFilterProp]], pos)
          }
        })
      }
      return result
    }
    return getKey(data)
  }
  // 粘贴事件
  _handleOnPaste = e => {
    if (window.clipboardData && window.clipboardData.getData) { // IE
      this.pasteData = window.clipboardData.getData('Text').replace(/\s+/g, ' ').trim()
    } else {
      this.pasteData = e.clipboardData.getData('text/plain').replace(/\s+/g, ' ').trim()
    }
  }
  // 异步数据
  _fetchData = () => {
    let lastFetch

    return props => {
      const {
        listConfig
      } = props

      lastFetch = +new Date()
      let now = lastFetch
      const { method = 'GET', urlConfig, params, data, config } = listConfig
      http._request_(method, urlConfig, params, data, config).then(data => {
        if (!data || this.unMount || now !== lastFetch) {
          return
        }
        this.hasFetch = true
        this.list = data.result || data || []
        this.setState({ treeData: this._render(this.list) })
      })
    }
  }
  // 子节点渲染
  _render = list => {
    const { listRender } = this.props

    this.treeKeysMap = []
    if (listRender) {
      let treeData = listRender(list) || []
      this.treeKeysMap = this._getTreeKeyMap(treeData)
      // console.log(this.treeKeysMap)
      return treeData
    } else {
      return list
    }
  }
  // 初始化子节点
  _initTreeData = props => {
    const {
      treeData,
      children
    } = props
    if (treeData) {
      this.treeData = treeData

      return this._render(treeData)
    } else if (children) {
      return null
    } else {
      return []
    }
  }
  // 初始化
  _init = props => {
    const {
      listConfig,
      CommonTreeSelectUpdate,
      treeData
    } = props
    let isConstructor = false

    if (props === this.props) {
      isConstructor = true
    }
    if (listConfig && (isConstructor || listConfig !== this.props.listConfig)) {
      this._fetchData(props)
    } else if (!isConstructor && treeData !== this.props.treeData) {
      this.state.treeData = this._initTreeData(props)
    }
    if (CommonTreeSelectUpdate === this.props.CommonTreeSelectUpdate) {
      this._initInitialValue(props)
    }
  }
  // 合并样式
  _loadStyle() {
    let props,
      result

    return (source, style) => {
      if (style !== props) {
        props = style
        result = Object.assign({}, source, style)
      } else if (style === undefined && props === undefined) {
        result = source
      }

      return result
    }
  }
  _toShowCheckedStrategy = type => {
    if (typeof type == 'string') {
      switch (type.toLowerCase()) {
        case 'all':
          return SHOW_ALL
        case 'parent':
          return SHOW_PARENT
        case 'child':
          return SHOW_CHILD
        default :
          return SHOW_CHILD
      }
    }

    return SHOW_CHILD
  }
  _loadData = node => {
    const { loadData, loadDataConfig, fetchUrl } = this.props
    // const { method = 'GET', urlConfig, params, data, config } = listConfig
    // http._request_(method, urlConfig, params, data, config)
    let fetchData = (method = 'GET', urlConfig, params, data, config) => http._request_(method, urlConfig, params, data, config).then(data => {
      if (!data || this.unMount) {
        return
      }
      this.setState({ treeData: [...loadData(node, data.result || data || [], this.state.treeData)] })
    })
    // console.log(node)
    if (!loadData || !loadDataConfig) {
      return new Promise(resolve => resolve())
    } else {
      if (Array.isArray(loadDataConfig)) {
        for (let i = 0, j = loadDataConfig.length; i < j; i++) {
          if (loadDataConfig[i].isLoad(node)) {
            return fetchData(loadDataConfig[i].method, loadDataConfig[i].urlConfig, loadDataConfig[i].params.nextParams(node) || loadDataConfig[i].params, loadDataConfig[i].data, loadDataConfig[i].config)
          }
        }
        return new Promise(resolve => resolve())
      } else {
        return fetchData(loadDataConfig.method, loadDataConfig.urlConfig, loadDataConfig.params.nextParams(node) || loadDataConfig.params, loadDataConfig.data, loadDataConfig.config)
      }
    }
  }
  render() {
    const { treeData, inputValue } = this.state
    // console.log(treeData)
    let {
      getFieldDecorator,
      label,
      className = '',
      isHideRequireIcon,
      decorator,
      multiple,
      treeCheckable,
      children,
      initialValue,
      required,
      style,
      dropdownStyle,
      labelCol,
      wrapperCol,
      showCheckedStrategy,
      validateFirst,
      loadData,
      listConfig,
      disabled,
      message,
      validator,
      hasPaste,
      type,
      allowClear = true,
      colon = false,
      visiable = true,
      size = 'default',
      rules,
      ...props
    } = this.props
    let value = this.state.initialValue
    // 必选前的星星icon
    let formItemClass = `${visiable ? '' : 'none'} YH-form-item ${className} ${isHideRequireIcon ? 'no-required-icon' : ''}`
    let TreeSelectSettings = {
      getPopupContainer: () => window.document.querySelector('.layout') ? window.document.querySelector('.layout') : document.body,
      ...props,
      style: this._loadStyle(defaultStyle, style),
      dropdownStyle: this._loadStyle(defaultDropdownStyle, dropdownStyle),
      showCheckedStrategy: this._toShowCheckedStrategy(showCheckedStrategy),
      onChange: this.hanldeOnChange,
      onSearch: this.handleOnSearch,
      filterTreeNode: this.handleFilterTreeNode,
      onDropdownVisibleChange: this.handleOnDropdownVisibleChange,
      inputValue,
      multiple,
      allowClear,
      treeCheckable,
      size
    }
    if (treeData !== null) {
      TreeSelectSettings.treeData = treeData
    }
    if (loadData !== undefined) {
      TreeSelectSettings.loadData = this._loadData
    }

    let settings = {}
    // 表单配置
    if (validateFirst !== undefined) {
      settings.validateFirst = validateFirst
    }
    if (value === initialValue && !value) {
      settings.initialValue = value
    } else if (!listConfig) {
      settings.initialValue = value
    } else if (this.hasSearch) {
      settings.initialValue = value
      this.hasSearch = false
    } else if (this.hasFetch) {
      settings.initialValue = value
      this.hasFetch = false
    }
    // 校验规则
    settings.rules = rules || []
    if (!disabled) {
      let rules = {}
      let rulesFlag = false

      if (required) {
        rules.required = required
        rulesFlag = true
      }
      if (type !== undefined) {
        rules.type = type
        rulesFlag = true
      }
      if (message !== undefined) {
        rules.message = message
      }
      if (validator !== undefined) {
        rules.validator = validator
        rulesFlag = true
      }
      rulesFlag && (settings.rules = [rules])
    }
    let formItem = <FormItem label={label || ' '} className={formItemClass} labelCol={labelCol} wrapperCol={wrapperCol} colon={colon}>
      {
        getFieldDecorator(decorator || 'decorator', settings)(
          <TreeSelect
            {...TreeSelectSettings}
          >
            {TreeSelectSettings.treeData === null ? children : null}
          </TreeSelect>
        )
      }
    </FormItem>
    return hasPaste ? <span ref="topDom">{formItem}</span> : formItem
  }
}

export default class ComonTreeSelectWrap extends Component {
  constructor(props) {
    super(props)
    this.state = { CommonTreeSelectUpdate: {} }
    if ((props.hasPaste || props.autoSelect) && !props.setFieldsValue) {
      console.error('setFieldsValue is required')
    }
    if (props.setValue) {
      console.warn('_setValue has been used')
    }
    if (props.CommonTreeSelectUpdate) {
      console.warn('CommonTreeSelectUpdate has been used')
    }
  }
  setValue = value => {
    const { setFieldsValue, decorator = 'decorator' } = this.props
    if (setFieldsValue) {
      setFieldsValue({ [decorator]: value })
      this.setState({ CommonTreeSelectUpdate: {} })
    }
  }
  render() {
    return <CommonTreeSelect {...this.props} setValue={this.setValue} {...this.state}/>
  }
}
