/**
 * Created by wangjun on 2017/9/19.
 * copy by CC on 2018/11/27
 */

###---------------------------------------- props 内容类型 ---------------------------------------

| api | 类型 |initValue| 注释 |
| ----- | --- | ----- | --- |
|isAutoComplete                         | [Bool]                    | false                      | 是否使用 AutoComplete 组件
|list                         | [array]                    | [[]]                       |[数据（优先级高于children）]
|listRenderGetLabel            |[function]                 | [item=>item.value]         |[显示的在列表中的值，参数为每条数据，需要返回显示的值，默认取value]
|listRenderGetShow            | [function]                 | [listRenderGetLabel]       |[显示在输入框的值，默认同listRenderGetKey]
|listRenderGetValue            |[function]                 | [null]                     |[getFieldValue获取的值]
|beforeListRender              |[function(list)]           | []                         |[生成子节点前回调，参数为列表数据，需要返回显示的列表数据]
其余参数参照CommonFormItem与antd-select
