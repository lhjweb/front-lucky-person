/*
* Created by zero on 2019/07/31
* */

import React from 'react'
import { DatePicker } from 'antd'
import { CommonFormItem } from '@comp'
const { RangePicker } = DatePicker

class App extends React.Component {
  render() {
    const {
      formItemPrefixCls,
      formItemClass,
      formItemId,
      label,
      labelCol,
      wrapperCol,
      help,
      extra,
      validateStatus,
      hasFeedback,
      required,
      style,
      colon,
      valuePropName,
      initialValue,
      trigger,
      getValueFromEvent,
      validateTrigger,
      rules,
      exclusive,
      normalize,
      validateFirst,
      decorator,
      getFieldDecorator,
      ...props
    } = this.props

    const formItemSettings = {
      prefixCls: formItemPrefixCls,
      className: formItemClass,
      id: formItemId,
      label,
      labelCol,
      wrapperCol,
      help,
      extra,
      validateStatus,
      hasFeedback,
      required,
      style,
      colon,
      valuePropName,
      initialValue,
      trigger,
      getValueFromEvent,
      validateTrigger,
      rules,
      exclusive,
      normalize,
      validateFirst,
      decorator,
      getFieldDecorator
    }

    return (
      <CommonFormItem
        {...formItemSettings}
      >
        <RangePicker {...props} />
      </CommonFormItem>
    )
  }
}

export default App
