/**
* Created by chengcheng on 2019-09-19
*
* 活动名称模糊搜索
*/
import React from 'react'
import {
  API_GET_SETTING_CONTROL_QUERY_ACT_NAME_CODES
} from '@api'
import CommonSelect from '../CommonSelect'

class ActivityName extends React.Component {
  state = {
    listConfig: null
  }

  // 模糊查询
  handleSearch = value => this.setState({
    listConfig: {
      urlConfig: API_GET_SETTING_CONTROL_QUERY_ACT_NAME_CODES,
      params: { actName: value, size: 10 }
    }
  })

  render() {
    return (
      <CommonSelect
        showArrow={false}
        filterOption={false}
        allowClear={true}
        showSearch={true}
        onSearch={this.handleSearch}
        listRenderGetData= {item => item.right}
        listRenderGetValue={item => `${item.left} ${item.right}`}
        listRenderGetLabel={item => `${item.left} ${item.right}`}
        optionLabelProp="data"
        label="活动名称"
        placeholder="请输入活动名称"
        decorator="actName"
        {...this.state}
        {...this.props}
      />
    )
  }
}

export default ActivityName
