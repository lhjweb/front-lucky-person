import * as React from 'react'
import { Input } from 'antd'
import CommonFormItem from '../CommonFormItem/'

export default class CommonInput extends React.Component {
  constructor(props) {
    super(props)
    this.commonInput = React.createRef()
  }

  handleChange = value => {
    // console.log(value)
    const { hasPaste, onChange } = this.props

    this.searchValue = value
    if (hasPaste) {
      if (this.pasteData !== undefined && this.pasteData == value) {
        this._onPaste(this.pasteData)
        this.pasteData = undefined
      }
    }

    typeof onChange === 'function' && onChange(value)
  }

  componentDidMount() {
    const { hasPaste, getFieldDecorator, setFieldsValue, getFieldValue } = this.props
    if (hasPaste) {
      if (!setFieldsValue) {
        console.error('setFieldsValue must be required')
      }
      if (!getFieldValue) {
        console.error('getFieldValue must be required')
      }
      if (!getFieldDecorator) {
        console.error('getFieldDecorator must be required')
      }
    }
  }

  // 粘贴
  initValue = value => {
    const { decorator, setFieldsValue, getFieldValue } = this.props
    let re = /\s+/
    let list = value.trim().split(re)
    const data = getFieldValue(decorator) || ''
    let handledata = `${data},${list.join(',')}`.replace(/^,/, '').replace(/,$/, '')
    setTimeout(() => {
      setFieldsValue({
        [decorator]: handledata
      }, () => this.forceUpdate())
    }, 0)
  }

  // 粘贴事件
  _handleOnPaste = e => {
    if (window.clipboardData && window.clipboardData.getData) { // IE
      this.pasteData = window.clipboardData.getData('Text').replace(/\s+/g, ' ').trim()
    } else {
      this.pasteData = e.clipboardData.getData('text/plain').replace(/\s+/g, ' ').trim()
    }
    const { hasPaste } = this.props
    hasPaste && this.initValue(this.pasteData)
  }

  render() {
    const {
      prefixCls,
      className,
      formItemId,
      label,
      labelCol,
      wrapperCol,
      help,
      extra,
      validateStatus,
      hasFeedback,
      required,
      style,
      colon,
      message,
      valuePropName,
      initialValue,
      trigger,
      getValueFromEvent,
      validateTrigger,
      validator,
      rules,
      type,
      exclusive,
      normalize,
      validateFirst,
      decorator,
      disabled,
      getFieldDecorator,
      setFieldsValue,
      getFieldValue,
      hasPaste,
      ...props
    } = this.props
    let formItemSettings = {
      prefixCls,
      className,
      id: formItemId,
      label,
      labelCol,
      wrapperCol,
      help,
      extra,
      validateStatus,
      hasFeedback,
      required,
      style,
      colon,
      valuePropName,
      initialValue,
      trigger,
      getValueFromEvent,
      validateTrigger,
      validator,
      rules,
      exclusive,
      normalize,
      validateFirst,
      decorator,
      getFieldValue,
      setFieldsValue,
      getFieldDecorator
    }
    // 校验规则
    formItemSettings.rules = rules || []
    if (!disabled) {
      let rules = {}
      let rulesFlag = false

      if (required) {
        rules.required = required
        rulesFlag = true
      }
      // if (type !== undefined) {
      //   rules.type = type
      //   rulesFlag = true
      // }
      if (message !== undefined) {
        rules.message = message
      }
      if (validator !== undefined) {
        rules.validator = validator
        rulesFlag = true
      }
      rulesFlag && formItemSettings.rules.push(rules)
    }
    const inputSettings = {
      ...props
    }
    const formInput = <CommonFormItem
      hidden={type === 'hidden'}
      {...formItemSettings}
    >
      <Input {...inputSettings} type={type} disabled={disabled} onChange={this.handleChange} onPaste={this._handleOnPaste}/>
    </CommonFormItem>
    return hasPaste ? <span ref={this.commonInput}>{formInput}</span> : formInput
  }
}
