# StartAndEndDatePicker 
开始时间 ～ 结束时间
- 不支持必填 rules

| data | 说明          |  默认值            |  type |
|-------|--------------| ----------------- | ------ |
| label | 标签 |  | string |
| showToday | 是否展示“今天”按钮 | false | bool |
| format  | 一行显示几条数据  |  'YYYY-MM-DD' | string  | 
| showTime  | 增加时间选择功能  |  false   | Number  | 
| startDecorator  | 开始时间组件的 decorator  |  'startToDate'   | String  | 
| endToDate  | 结束时间组件的 decorator  |  'endToDate'   | String  | 

## 例子
### YYYY-MM-DD
```
<StartAndEndDatePicker
  getFieldDecorator={getFieldDecorator}
  getFieldValue = {getFieldValue}
  setFieldsValue={setFieldsValue}
  label="申请时间"
/>
```
### YYYY-MM-DD HH:mm:ss
showTime 是否显示时间选择器
```
<StartAndEndDatePicker
  getFieldDecorator={getFieldDecorator}
  getFieldValue = {getFieldValue}
  setFieldsValue={setFieldsValue}
  format="YYYY-MM-DD HH:mm:ss"
  showTime={true}
  label="申请时间"
/>
```