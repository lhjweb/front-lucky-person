/**
 * Created by wangjun on 2017/12/13.
 */
import React, { Component } from 'react'
import { DatePicker } from 'antd'
import './index.less'

const STYLE = { width: '48%' } // 设置组件样式

// 默认 decorator 值
const END_TO_DATE = 'endToDate' // 结束时间
const START_TO_DATE = 'startToDate' // 开始时间

class StartAndEndDatePicker extends Component {
  constructor(props) {
    super(props)
    this.startDecorator = props.startDecorator || START_TO_DATE
    this.endDecorator = props.endDecorator || END_TO_DATE
  }
  // 禁止选择部分日期
  disabledStartDate = startToDate => this._setDisabledDate(this.endDecorator, startToDate)

  // 禁止选择部分日期
  disabledEndDate = endToDate => this._setDisabledDate(this.startDecorator, endToDate)

  // 设置禁止选择部分日期
  _setDisabledDate = (type, date) => {
    const { getFieldValue } = this.props
    if (!getFieldValue) throw new Error('getFieldValue cannot be undefind ')
    const toDate = getFieldValue(type)
    if (!date || !toDate) return false
    if (type === this.startDecorator) {
      return date.valueOf() <= toDate.valueOf()
    } else if (type === this.endDecorator) {
      return date.valueOf() > toDate.valueOf()
    }
  }

  _onChange = (type, value) => {
    const { setFieldsValue } = this.props
    if (!setFieldsValue) throw new Error('setFieldsValue cannot be undefind ')
    setFieldsValue({ [type]: value })
  }

  handleStartChange = value => this._onChange(this.startDecorator, value)

  handleEndChange = value => this._onChange(this.endDecorator, value)

  render() {
    const {
      label,
      initialValue,
      getFieldDecorator,
      showToday = false,
      showTime = false, // 是否显示时间选择器
      format = 'YYYY-MM-DD',
      rules = {},
      ...props
    } = this.props

    let isDate = format === 'YYYY-MM-DD'

    // 公共配置
    const datePickerSettings = {
      showToday,
      format,
      showTime
    }
    // console.error(rules)
    return (
      <div className="cmp_start-and-end-date-picker ant-row ant-form-item ">
        <div className="ant-col ant-form-item-label">
          <label className="ant-form-item-no-colon" title={label}>{label}</label>
        </div>
        <div className="ant-col ant-form-item-control-wrapper">
          <div className={`ant-form-item-control ${isDate ? '' : 'type-date'}`}>
            <span className="date-flex">
              {
                getFieldDecorator(this.startDecorator, {
                  initialValue: initialValue || null,
                  rules
                })(
                  <DatePicker
                    style={isDate ? STYLE : undefined}
                    disabledDate={this.disabledStartDate}
                    placeholder="开始时间"
                    suffixIcon={<span></span>}
                    onChange={this.handleStartChange}
                    {...datePickerSettings}
                    {...props}
                  />
                )
              }
              <span>~</span>
              {
                getFieldDecorator(this.endDecorator, {
                  initialValue: initialValue || null,
                  rules
                })(
                  <DatePicker
                    style={isDate ? STYLE : undefined}
                    disabledDate={this.disabledEndDate}
                    placeholder="结束时间"
                    suffixIcon={<span></span>}
                    onChange={this.handleEndChange}
                    {...datePickerSettings}
                    {...props}
                  />
                )
              }
            </span>
          </div>
        </div>
      </div>
    )
  }
}

export default StartAndEndDatePicker
