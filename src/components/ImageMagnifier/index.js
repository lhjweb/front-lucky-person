/*
* 图像放大镜
* */

import React, { Component } from 'react'
import './index.less'

const getOffset = (Node, offset) => {
  if (!offset) {
    offset = {}
    offset.top = 0
    offset.left = 0
  }

  if (Node == document.body || !Node.offsetParent) {  //  当该节点为body节点时，结束递归
    return offset
  }

  offset.top += Node ? Node.offsetTop : 0
  offset.left += Node ? Node.offsetLeft : 0

  return getOffset(Node.offsetParent, offset) //  向上累加offset里的值
}
class ImageMagnifier extends Component {
  constructor() {
    super()
    this.state = {
      sliderStyle: {},
      bigPicStyle: {},
      bigPicShow: false
    }
  }
  _showMagnifier = e => {
    const minWrap = document.querySelector('#minWrap'),
      slider = document.querySelector('.slider'),
      bigPic = document.querySelector('.big_pic'),
      maxWrap = document.querySelector('#maxWrap')
    const scrollTop = document.body.scrollTop || document.documentElement.scrollTop
    let h = slider.offsetHeight || 0
    let x = e.clientX - getOffset(minWrap).left - h / 2
    let y = e.clientY + scrollTop - getOffset(minWrap).top - h / 2
    if (x <= 0) {
      x = 0
    }
    if (x >= minWrap.offsetWidth - slider.offsetWidth) {
      x = minWrap.offsetWidth - slider.offsetWidth
    }
    if (y <= 0) {
      y = 0
    }
    if (y > minWrap.offsetHeight - slider.offsetHeight) {
      y = minWrap.offsetHeight - slider.offsetHeight
    }

    this.setState({
      sliderStyle: {
        left: x,
        top: y
      }
    })
    const scaleX = x / (minWrap.offsetWidth - slider.offsetWidth)
    const scsleY = y / (y = minWrap.offsetHeight - slider.offsetWidth)
    const maxWidth = bigPic.offsetWidth - maxWrap.offsetWidth
    const maxHeight = bigPic.offsetHeight - maxWrap.offsetHeight
    // $(".big_pic").css({
    //   left: -scaleX * maxWidth,
    //   top: -scsleY * maxHeight
    // })

    this.setState({
      bigPicStyle: {
        left: -scaleX * maxWidth,
        top: -scsleY * maxHeight
      }
    })
  }
  render() {
    const { sliderStyle, bigPicStyle, bigPicShow } = this.state
    const { imgsrc, configWH } = this.props
    return (
      <div className="ImageMagnifier lunbo_pic_box">
        <div
          className="small_pic_box"
          id="minWrap"
          onMouseMove={this._showMagnifier}
          onMouseEnter={() => this.setState({ bigPicShow: true })}
          onMouseLeave={() => this.setState({ bigPicShow: false })}
          style={configWH ? { width: configWH, height: configWH } : {}}
        >
          <img className="small_pic" src={imgsrc} alt="" style={configWH ? { width: configWH, height: configWH } : {}}/>
          {
            bigPicShow &&
            <div className="slider" style={configWH ? { ...sliderStyle, width: configWH / 2, height: configWH / 2 } : sliderStyle}/>
          }
        </div>
        {
          bigPicShow &&
          <div className="big_pic_box" id="maxWrap" style={configWH ? { width: configWH, height: configWH } : {}}>
            <img className="big_pic" src={imgsrc} alt="" style={bigPicStyle}/>
          </div>
        }
      </div>
    )
  }
}

export default ImageMagnifier
