/*
 * author: zero
 * updated: yanzu lee
 * uodateTIme: 2018/12/5
 * */

import React from 'react'
import { Modal, message, Upload, Button } from 'antd'
import { getVariant, hasVariant, LOGIN_TOKEN } from '@variant'
import { generateUrl, isIE } from '@utils'
import http from '@http'
import _ from 'lodash'
let fileName = '导出信息'

export default class Import extends React.Component {
  state = {
    loading: false
  }

  /* 报错导出 - 返回是文件的二进制流数据 */
  handleStringExportError_Binary = data => {
    Modal.confirm({
      title: '温馨提示',
      content: '您的部分数据不符合规则，本次导入失败，请修改后再次导入，请重试！',
      okText: '报错导出',
      cancelText: '我知道了',
      onOk() {
        if (data.type && data.type.includes('excel')) {
          let a = document.createElement('a')
          document.body.appendChild(a)
          let url = window.URL.createObjectURL(data)
          a.href = url
          a.download = `${fileName}.xls`
          a.click()
          // 异步导出，火狐专用
          setTimeout(() => window.URL.revokeObjectURL(url), 0)
        }
      }
    })
  }
  /* 报错导出 */
  handleExportError = res => {
    const { onExprotError, onExprotErrorCancel } = this.props
    const content = () => {
      const { rightCount, errorCount, excludeCount } = res.result
      if (errorCount || excludeCount) {
        return (
          <div>
            <div>成功：{rightCount}</div>
            <div>失败：{errorCount}</div>
            { _.isNumber(excludeCount) && <div>重复：{excludeCount}</div> }
            <div>导入存在失败数据，请重试！</div>
          </div>
        )
      }
      return `${res.message || res.result}，请重试！`
    }

    Modal.confirm({
      title: '温馨提示',
      content: content(),
      okText: '报错导出',
      cancelText: '我知道了',
      onOk() {
        if (!_.isFunction(onExprotError)) throw 'The lack of "onExprotError"'
        onExprotError(res)
      },
      onCancel() {
        _.isFunction(onExprotErrorCancel) && onExprotErrorCancel(res)
      }
    })
  }

  /* 导入文件事件 */
  handleChange = info => {
    const { onOk, onError, onExprotError, onResponse, getErrorKey } = this.props

    if (typeof info.file.status !== 'undefined' && info.file.status !== 'uploading') {
      this.setState({ loading: false })
      let res = info.file.response

      if (res && res.constructor.name === 'Blob') { // 文件
        return this.handleStringExportError_Binary(res)
      } else if (typeof res !== 'object' || !res) {
        return message.error('系统异常')
      }

      if (res && res.code === 200000) {
        if (res.result && res.result.errorCount && res.result.errorCount > 0) {
          // 判断是否存在错误条数
          this.handleExportError(res)
          _.isFunction(getErrorKey) && getErrorKey(res)
        } else { // 严格意义上的成功
          _.isFunction(onOk) ? onOk(res) : message.success('导入成功')
        }
      } else if (res.code === 8001001 || res.code === 600207) {
        // 登陆超时处理
        history.replace('/')
      } else {
        _.isFunction(onResponse) && onResponse(res) // 导入失败，
        let ExportErrorCodes = []
        // 导入失败，错误处理
        if (_.isFunction(onError)) {
          onError(res)
        } else if (ExportErrorCodes.includes(res.code) && _.isFunction(onExprotError)) {
          this.handleExportError(res)
        } else {
          message.error(res.message || '导入失败')
        }
      }
    } else if (info.file.status === 'uploading') {
      this.setState({ loading: true })
    }
  }

  /* 判断上传文件类型 */
  beforeUpload = file => {
    const {
      beforeImport,
      fileType = ['xlsx', 'xls'], // 优先判断文件类型 // defined default
      maxSize, // 判断文件体积最大值, 1k == 1024b
      overSizeMessage // 因业务需要可能会存在比较友善的提示、所以超出文件大小时增加自定义提示api
    } = this.props
    if (this.state.loading) return false // 正常上传中阻止再次上传动作
    if (_.isFunction(beforeImport) && beforeImport(file) === false) return false

    let bool = false
    if (_.isArray(fileType)) {
      let regexp = new RegExp(`\\.(${fileType.join('|')})$`, 'i')
      bool = regexp.test(file.name)
    } else if (_.isString(fileType)) {
      let regexp = new RegExp(`\\.${fileType}$`, 'i')
      bool = regexp.test(file.name)
    }

    // 保证文件名中不能包含 逗号
    if (/[\,|\，]+/.test(file.name)) {
      message.error('导入文件名不能含有逗号')
      return false
    }

    if (!bool) {
      message.error('导入文件格式不正确')
      return bool
    }

    const size = Number(maxSize)
    if (size && _.isNumber(size)) {
      if (file.size > size) {
        message.error(overSizeMessage || '导入文件太大')
        return false
      }
    }

    return bool
  }

  /* 自定义上传 */
  handleCustomRequest = (file) => {
    const { onSuccess, onError } = file
    const { actionConfig = {} } = this.props
    const { method = 'GET', urlConfig = [], params = {}, config = {} } = actionConfig
    let formData = new FormData()
    formData.append('file', file.file)

    this.setState({ loading: true })
    urlConfig && http._request_(method, urlConfig, params, formData, config).then(res => {
      onSuccess(res)
    }).catch(res => {
      onError(res)
    }).finally(() => this.setState({ loading: false }))
  }

  handleOnClick = () => {
    const { onClick, getOrderId } = this.props
    getOrderId && getOrderId()
    onClick && onClick()
  }

  render() {
    const { loading } = this.state
    const { title, actionConfig = {}, isCustomRequest, customRequest } = this.props // 自定义
    const { openFileDialogOnClick, children, action, onChange, disabled, type, ghost, icon, ...props } = this.props // antd api接口
    const { urlConfig = [], params = {}, data = {}, config = {} } = actionConfig
    const { urlParams } = config
    let url = _.isObject(actionConfig)
      ? generateUrl(urlConfig[0], urlConfig[1], params, urlParams)
      : action
    // console.log(getVariant(LOGIN_TOKEN), LOGIN_TOKEN)
    // Upload 配置
    let settings = {
      action: url,
      data,
      name: 'file',
      showUploadList: false,
      beforeUpload: this.beforeUpload,
      onChange: onChange || this.handleChange,
      openFileDialogOnClick: openFileDialogOnClick === undefined ? !loading : openFileDialogOnClick
    }

    let headers = {
      'X-Requested-With': null
    }
    if (isIE()) {
      headers['Cache-Control'] = 'no-cache'
    }
    if (hasVariant(LOGIN_TOKEN)) {
      headers[LOGIN_TOKEN] = getVariant(LOGIN_TOKEN)
    }

    settings.headers = headers

    if (isCustomRequest) { // 判断是否自定义上传实现
      settings.customRequest = customRequest || this.handleCustomRequest
    }

    // Button 配置
    let btnSetting = {
      icon,
      ghost,
      disabled,
      type,
      loading,
      onClick: this.handleOnClick
    }

    return <Upload {...props} {...settings}>
      {children ? children : <Button {...btnSetting}>{`${title || '导入'}`}</Button>}
    </Upload>
  }
}
