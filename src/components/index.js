/*
 * @Description:
 * @Author: zhangchuangye
 * @Date: 2019-08-09 16:05:43
 */

export { default as Bread } from './Bread'

export { default as CompLoading } from './CompLoading'

// Form
export { default as DatetimePicker } from './Form/DatetimePicker'
export { default as CommonFormItem } from './Form/CommonFormItem'
export { default as CommonInput } from './Form/CommonInput'
export { default as CommonSelect } from './Form/CommonSelect'
export { default as CommonTreeSelect } from './Form/CommonTreeSelect'
export { default as CommonTextArea } from './Form/CommonTextArea'
export { default as StartAndEndDatePicker } from './Form/StartAndEndDatePicker' // 开始时间 - 结束时间
export { default as RangePicker } from './Form/RangePicker' // 开始时间 - 结束时间
export { default as DatePicker } from './Form/DatePicker'
export { default as TimePicker } from './Form/TimePicker'
export { default as ActivityName } from './Form/ActivityName' // 活动名称

// HOC
export { default as PageTabs } from './HOC/PageTabs'
export { default as YHButton } from './HOC/YHButton' // 提交按钮

// table
export { default as MergedTable } from './Table/MergedTable' // 合并单号的表格
export { default as TableMul } from './Table/TableMul' // handsontable
export { default as YHTable } from './Table/YHTable' // 封装antd table
export { default as EditableTable } from './Table/EditableTable' // 可编辑的表格

// Dialog
export { default as CommonErrorModel } from './Dialog/CommonErrorModel' // 证件 报错弹框
export { default as YHModal } from './Dialog/YHModal' // 拖拽表格
export { default as SettingModal } from './Dialog/SettingModal'
export { default as CustomTableHeader } from './Dialog/CustomTableHeader'// 自定义列表头

// other
export { default as LongText } from './LongText' // 超出 省略
export { default as CollapsibleFormBox } from './CollapsibleFormBox'
export { default as SearchForm } from './CollapsibleFormBox/SearchForm'
export { default as Import } from './Import' // 导入Excel
export { default as UploadFile } from './UploadFile' // 上传文件
export { default as ImageMagnifier } from './ImageMagnifier' // 图像放大镜
export { default as UploadFileModal } from './UploadFileModal' // 多个上传文件弹窗
