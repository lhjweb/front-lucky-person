/*
* Created by Zero on 2019/08/02.
*
* 可编辑单元格
* 带单元格编辑功能的表格。
* 参考：https://ant.design/components/table-cn/#components-table-demo-edit-row
*
* */

import React from 'react'
import { Table, Input, InputNumber, Popconfirm, Form, Button } from 'antd'
import { MergedTable } from '@comp'
import './index.less'
import PropTypes from 'prop-types'
import _ from 'lodash'
const EditableContext = React.createContext()

class EditableCell extends React.Component {
  // 获取输入框
  getInput = (inputType) => {
    if (inputType === 'number') {
      return <InputNumber />
    }
    return <Input />
  }
  // 获取子元素
  getChildren = (setting) => {
    try {
      const { form, dataIndex, title, inputType, record, index } = setting
      const { getFieldDecorator } = form
      if (_.isString(inputType)) { // string
        return (
          <Form.Item style={ { margin: 0 } }>
            { getFieldDecorator(dataIndex, {
              rules: [
                {
                  required: true,
                  message: `Please Input ${title}!`
                }
              ],
              initialValue: record[dataIndex]
            })(this.getInput(inputType)) }
          </Form.Item>
        )
      } else if (_.isFunction(inputType)) { // ReactNode
        return inputType(record[dataIndex], record, index, form)
      } else { // default
        return this.props.children
      }
    } catch (e) {
      console.error(e)
      return this.props.children
    }
  }
  // 渲染 col
  renderCell = (form) => {
    const {
      editing,
      dataIndex,
      title,
      inputType,
      record,
      index,
      children,
      ...restProps
    } = this.props

    const setting = {
      form,
      dataIndex,
      title,
      inputType,
      record,
      index
    }
    return (
      <td {...restProps}>
        {editing ? this.getChildren(setting) : (children)}
      </td>
    )
  };

  render() {
    return <EditableContext.Consumer>{this.renderCell}</EditableContext.Consumer>
  }
}

class EditableTable extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      data: props.dataSource || [],
      editingKey: ''
    }
    this.columns = [
      /* {
        title: 'name',
        dataIndex: 'name',
        width: '25%',
        editable: true,
        inputType: 'text'
      },
      {
        title: 'age',
        dataIndex: 'age',
        width: '15%',
        editable: true,
        inputType: 'number'
      },
      {
        title: 'address',
        dataIndex: 'address',
        width: '40%',
        editable: true,
        inputType: 'text'
      },*/
      {
        title: '操作',
        width: 90,
        fixed: 'right',
        dataIndex: 'operation',
        align: 'center',
        render: (text, record) => {
          const { editingKey } = this.state
          const editable = this.isEditing(record)
          return editable ? (
            <span>
              <EditableContext.Consumer>
                {form => (
                  <Button type="link" className="affirm" icon="check-circle" title="完成" onClick={() => this.save(form, record)} />
                )}
              </EditableContext.Consumer>
              <Popconfirm placement="topRight" title="确定取消吗?" onConfirm={() => this.cancel()}>
                <Button type="link" className="cancel" icon="close-circle" title="取消" />
              </Popconfirm>
            </span>
          ) : (
            <Button type="link" className="edit" icon="edit" title="编辑" disabled={editingKey !== ''} onClick={() => this.edit(record)} />
          )
        }
      }
    ]
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.dataSource !== this.props.dataSource) {
      this.setState({
        editingKey: '',
        data: nextProps.dataSource
      })
    }
  }
  // 获取行 key
  _getRowKey = record => {
    const { rowKey } = this.props
    return rowKey && _.isFunction(rowKey) ? rowKey(record) : record.key
  }
  // 判断是否编辑
  isEditing = record => this._getRowKey(record) === this.state.editingKey
  // 清除
  cancel = () => {
    this.setState({ editingKey: '' })
  }
  // 保存
  save = (form, record) => {
    if (this._save) return
    form.validateFieldsAndScroll((error, values) => {
      if (error) {
        return
      }
      try {
        // 高阶函数处理才行
        const { onEditSave } = this.props

        if (onEditSave && _.isFunction(onEditSave)) {
          this._save = true
          onEditSave(values, record).then((res) => {
            const newData = [...this.state.data]
            const index = newData.findIndex(item => this._getRowKey(record) === this._getRowKey(item))
            if (index > -1) {
              const item = newData[index]
              newData.splice(index, 1, {
                ...item,
                ...values,
                ...res
              })
              this.setState({ data: newData, editingKey: '' })
            } else {
              newData.push({ ...values, ...res })
              this.setState({ data: newData, editingKey: '' })
            }
          }).catch((e) => {
            console.error(e)
            // this.cancel()
          }).finally(() => this._save = false)
        }
      } catch (e) {
        console.error(e)
        this.cancel()
      }
    })
  }
  // 修改
  edit = (record) => {
    const { beforeClickEdit } = this.props
    let isEdit = true
    if (_.isFunction(beforeClickEdit)) {
      isEdit = beforeClickEdit(record)
    }
    isEdit && this.setState({ editingKey: this._getRowKey(record) })
  }
  // 分页处理
  handlPageChange = (page, pageSize) => {
    this.cancel()
    this.props.pagination.onChange(page, pageSize)
  }
  // 分页处理
  handleShowSizeChange = (current, size) => {
    this.cancel()
    this.props.pagination.onShowSizeChange(current, size)
  }

  render() {
    const { data } = this.state
    const { columns, pagination, onEditSave, dataSource, isMerged, ...props } = this.props
    if (!onEditSave || !_.isFunction(onEditSave)) throw 'onEditSave not to function'
    if (!dataSource) throw 'dataSource it must be array'

    const components = {
      body: {
        cell: EditableCell
      }
    }

    const newColumns = [...columns, ...this.columns].map(col => {
      if (!col.editable) {
        return col
      }
      return {
        ...col,
        onCell: record => ({
          record,
          inputType: col.inputType,
          dataIndex: col.dataIndex,
          title: col.title,
          editing: this.isEditing(record)
        })
      }
    })

    const Component = isMerged ? MergedTable : Table
    return (
      <EditableContext.Provider value={this.props.form}>
        <Component
          dataSource={data}
          components={components}
          columns={newColumns}
          rowClassName="editable-row"
          pagination={{ ...pagination, onChange: this.handlPageChange, onShowSizeChange: this.handleShowSizeChange }}
          {...props}
        />
      </EditableContext.Provider>
    )
  }
}

EditableTable.propTypes = {
  columns: PropTypes.array.isRequired,
  onEditSave: PropTypes.func.isRequired,
  dataSource: PropTypes.array.isRequired
}

export default Form.create()(EditableTable)
