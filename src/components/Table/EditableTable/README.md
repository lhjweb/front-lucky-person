# EditableTable - table 拓展组件

  居于 antd table 组件，所以基础 API 可以前往 https://ant.design/components/table-cn/#components-table-demo-edit-row

## 自定义组件API
   - onEditSave - 保存处理事件
   - columns
     - editable - 可编辑的
     - inputType - 输入类型 - ReactNode
     - isMerged  - 是否进行合并行
## 新特性 eg
```
    请前往：src/pages/ContractCenter/ContractClauseTemplateMaintenance/Components/PageTable.js
```
