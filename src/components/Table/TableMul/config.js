export default {
  baseConfig: {
    data: [], // 数据
    afterColumnsMoved: undefined, // 拖拽后回调
    hasPage: true, // 分页
    hasIndex: false, // 索引
    checkAllBox: false, // 选择框
    columnMoveConfig: undefined, // 拖拽接口配置
    disabledCheckAllBox: false, // 是否禁用选择项
    checkAllBoxWidth: 40, // 全选列宽度
    dataUrl: '', // 数据接口
    dataUrlConfig: undefined, // 数据接口配置
    columnList: undefined, // 异步表头配置
    columns: [], // 表头
    selectRows: undefined, // 选中行
    height: 500, // 高度
    afterChange: undefined, // 改变回调
    fixedColumnsLeft: 0, // 左固定
    manualColumnMove: false, // 拖拽表头
    stretchH: 'all',  // 自适应
    manualColumnResize: true, // 表头宽度自定义
    colHeaders: true, // 表头固定
    rowHeaders: true, // 左固定-序号
    visible: true, // 显示
    cells: undefined, // 单元格
    // currentRowClassName: 'currentRow',

    viewportRowRenderingOffset: 20, // 行40缓存
    viewportColumnRenderingOffset: 100, // 列100缓存
    scrollLoad: true, // 滚动加载

    autoRowsHeight: true, // 根据内容自适应高度
    autoRowsMaxHeight: 500, // 根据内容自适应最大高度

    columnSorting: true, // 默认排序
    sortConfig: false, // 异步排序配置
    noWarn: true,

    rowHeadersHeightAutoHandle: false // 左固定-序号，行高度自动处理 - 自定义
    // autoColumnSize: {syncLimit: 300}
    // fillHandle: true
  },
  defaultValues: {
    fetchPageSize: 200,
    pageSize: 40,
    firstPageSize: 40,
    loadSize: 20,
    cachePage: 5,
    columnWith: 60,
    maxHeight: 1000,
    minHeight: 500,
    scrollFetchHeight: 70
  }
}
