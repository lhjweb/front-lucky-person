/**
 * Created by wangjun on 2017/12/20.
 */
import {defaultValues} from './config'
import React from 'react'
import ReactDOM from 'react-dom'
import moment from 'moment'
export function __validArray(value) {
  if (!Array.isArray(value)) {
    return []
  }
  return value
}
export function __validObject(value) {
  if (typeof value == 'object') {
    return value
  }
  console.error(value, 'is not an object')
  return {}
}
export function __copyArray(array) {
  array = __validArray(array)
  return array.map(item => {
    item = __validObject(item)
    return Object.assign({}, item)
  }) || []
}
export function __isEqual(left, right) {
  for (let key in left) {
    if (left[key] !== right[key]) {
      return false
    }
  }
  return true
}
const enhancerRenderer = func => (...args) => ReactDOM.render(func(...args), args[1])
let root = 0 // 唯一表格id
export default class Settings {
  constructor(config) {
    config = __validObject(config)
    this.settings = config
    this.state = {
      key: Math.random(),
      root: config.root || `yh-table-${root++}`,
      height: defaultValues.height,
      pageSize: defaultValues.fetchPageSize,
      visible: config.visible
    }
    this._initColumns()
    this._initCache()
    this._setCache()
    this._setData(this._getCache())
    this._initSelectRows()
    this._initLightRows()
    this._initSort()
    this._initPage()
    this._initScroll()
  }

  _updateState(state) {
    Object.assign(this.state, state)
  }

  _isInnerProp(prop) {
    let arr = []
    const {checkAllBox, hasIndex} = this.settings

    checkAllBox && arr.push('checkbox')
    hasIndex && arr.push('index')
    return arr.indexOf(prop) > -1
  }

  _toggleSelectAll() {
    if (this.selectRows == 'all') {
      this._initSelectRows(new Set())
      this.state.data.forEach(item => item.checkbox = false)
    } else {
      this._initSelectRows('all')
      this.state.data.forEach(item => item.checkbox = true)
    }
  }

  _batchModifySelect(selected, isSelect) {
    if (isSelect) {
      for (let k = selected[0], t = selected[2]; k <= t; k++) {
        let data = this.state.data[k]
        if (data.checkbox != isSelect) {
          data.checkbox = true
          this.selectRows.add(data.key)
          this.selectRowsData.push(data)
          this.selectRowsDataSource.push(this.settings.data[k])
        }
      }
    } else {
      if (this.selectRows == 'all') {
        this._initSelectRows(new Set())
        this.state.data.forEach((item, index) => {
          if (index > selected[2] || index < selected[0]) {
            item.checkbox = true
            this.selectRows.add(item.key)
            this.selectRowsData.push(item)
            this.selectRowsDataSource.push(this.settings.data[index])
          } else {
            item.checkbox = false
          }
        })
      } else {
        for (let k = selected[0], t = selected[2]; k <= t; k++) {
          this.state.data[k].checkbox = false
          this.selectRows.delete(this.state.data[k].key)
          this._initSelectRows(this.selectRows)
        }
      }
    }
  }

  _shouldColumnMove(columns, target) {
    if (columns) {
      if (!this.state.data.length || this.state.columns.every(item => item + 1 - this._getInnerColumns()) || target == columns[columns.length - 1] + 1 || target == columns[columns.length - 1]) {
        this.shouldMovedFlag = false
      } else {
        this.shouldMovedFlag = true
      }
    }
    return this.shouldMovedFlag
  }

  _shouldScroll() {
    const {page, totalPage} = this.state
    return !this.isScroll && (totalPage > page || page == totalPage && this.loadSizePage == 0) && totalPage !== 0
  }

  _updateScroll(isScroll = true) {
    this.isScroll = isScroll
  }

  _initScroll() {
    this.isScroll = false
  }

  _updateSettings(settings) {
    Object.assign(this.settings, settings)
  }

  _modifyColumnsMoved(columns, start, selectColumns, target) {
    let movedColumns = columns.splice(selectColumns[0] - start, selectColumns.length)

    if (target - 1 > selectColumns[0]) {
      columns.splice(target - start - selectColumns.length, 0, ...movedColumns)
    } else if (target < selectColumns[0]) {
      columns.splice(target - start, 0, ...movedColumns)
    }
  }

  _columnsSourceMoved(selectColumns, target) {
    this._modifyColumnsMoved(this._getSourceColumns(), this._getInnerColumns(), selectColumns, target)
  }

  _columnsMoved(selectColumns, target) {
    this._modifyColumnsMoved(this.state.columns, 0, selectColumns, target)
  }

  _clearData() {
    this._initSelectRows(new Set())
    this._initLightRows(new Set())
    this._initSort(null)
    this._initPage({page: 0, totalPage: 0})
    this._initCache()
    this._setData([])
    this._setTotalNum(0)
    this._initScroll()
  }

  _setHeight(height) {
    height = height > defaultValues.minHeight ? height : defaultValues.minHeight
    height = height > defaultValues.maxHeight ? defaultValues.maxHeight : height
    if (height !== this.state.height) {
      this.state.height = height
      return true
    }
    return false
  }

  _setFecthResult(params) {
    this.fecthResult = params
  }

  _setAutoHeight(height) {
    let rangeMinHeight = height + 50,
      rangeMaxHeight = height + 100
    if (this.settings.height > rangeMaxHeight || this.settings.height < rangeMinHeight && rangeMinHeight < this.settings.autoRowsMaxHeight) {
      this.state.height = rangeMinHeight
      return true
    }
    return false
  }

  _getInnerColumns() {
    const {checkAllBox, hasIndex} = this.settings
    let innerColumns = 0

    checkAllBox && innerColumns++
    hasIndex && innerColumns++
    return innerColumns
  }

  _setSortColumn(header) {
    if (this.lastSortColumn !== header) {
      this._initSort()
    }
    this.sortSetting[header] = !this.sortSetting[header]
    this.lastSortColumn = header
  }

  _cacheEnough() {
    return this.state.page < this.cachePage
  }

  _needCache() {
    return this.state.page >= this.cachePage - defaultValues.cachePage && this.cachePage < this.state.totalPage
  }

  _setAsyncColumns(columns) {
    this.asyncColumns = __copyArray(columns).sort((left, right) => left.sort - right.sort)
    // 合并接口表头与自定义表头
    this._initColumns(this.asyncColumns.map(item => Object.assign({}, item, this._findColumn(item.enName))))
  }

  _getSourceColumns() {
    const {columnList, columnMoveConfig} = this.settings

    if (this.asyncColumns && (columnList || columnMoveConfig)) {
      return this.asyncColumns
    } else {
      return this.settings.columns || []
    }
  }

  _toColumns(value) {
    let columns = []
    // 排序
    value = value.sort((left, right) => left.sort - right.sort)
    value.forEach(item => {
      // 控制表头字段是否显示
      if (item.checked || item.checked === undefined) {
        let editor = false
        let readOnly = true
        // 控制单元格是否可编辑
        if (item.editor == true) {
          editor = 'text'
          readOnly = false
        } else if (item.editor !== undefined) {
          editor = item.editor
          readOnly = false
        }
        if (item.type !== undefined) {
          readOnly = false
        }
        let defaultSettings = {
          ...item,
          title: item.chName,
          data: item.enName,
          readOnly,
          editor
        }
        if (item.render) {
          defaultSettings.renderer = item.render
        } else if (item.type === undefined) {
          // 显示文字的增加title属性
          defaultSettings.renderer = (instance, TD, row, col, prop, value, cellProperties) => <span title={value}>{value}</span>
        }
        columns.push(defaultSettings)
      }
    })
    return columns
  }

  _getAddColumns() {
    const {checkAllBox, hasIndex} = this.settings
    let columns = []

    if (checkAllBox) {
      columns.push({
        data: 'checkbox',
        editor: false,
        type: 'checkbox',
        className: 'checkbox'
      })
    }
    if (hasIndex) {
      columns.push({
        title: '序号',
        data: 'index',
        editor: false,
        renderer: (instance, TD, row, col, prop, value, cellProperties) => <span>{value || row + 1}</span>
      })
    }
    return columns
  }

  _addLightRender(columns) {
    for (let i = 0, j = columns.length; i < j; i++) {
      let column = columns[i]

      if (column && column.type !== 'checkbox') {
        let render = column.renderer

        column.renderer = (instance, TD, row, col, prop, value, cellProperties) => {
          if (this.lightRows.has(instance.getSourceDataAtRow(row).id)) {
            TD.parentNode && TD.parentNode.setAttribute('class', 'lighter')
          }
          if (render) {
            return render(instance, TD, row, col, prop, value, cellProperties)
          }

          return <span>{value}</span>
        }

        return columns
      }
    }

    return columns
  }

  _enhancerRender(columns) {
    return columns.map(col => {
      if (col.renderer) {
        col.renderer = enhancerRenderer(col.renderer)
      }
      return col
    })
  }

  _findColumn(name) {
    let {columns, columnWith} = this.settings
    for (let i = 0, j = columns.length; i < j; i++) {
      let item = columns[i]

      if (item.enName === name || item.data === name) {
        item.width = item.width || columnWith || defaultValues.columnWith

        return item
      }
    }
  }

  _getColumns() {
    // 合并接口表头与自定义表头
    this.asyncColumns.map(item => {
      Object.assign(item, this._findColumn(item.enName))
    })
  }

  _initColumns(columns) {
    columns = __copyArray(columns)
    this._gatherRenderColumns(columns)
    this.state.columns = this._enhancerRender(this._addLightRender(this._getAddColumns().concat(this._toColumns(columns))))
  }

  _initSelectRows(selectRows = this.settings.selectRows) {
    this.selectRows = new Set()
    if (typeof selectRows == 'object' && selectRows) {
      selectRows.forEach(item => {
        this.selectRows.add(item)
      })
      this.selectRowsDataSource = this.settings.data.filter(item => this.selectRows.has(item.key))
      this.selectRowsData = this.state.data.filter(item => this.selectRows.has(item.key))
    } else if (String(selectRows).toLowerCase() == 'all') {
      this.selectRows = 'all'
      this.selectRowsDataSource = this.settings.data
      this.selectRowsData = this.state.data
    } else {
      console.error(selectRows, 'is not allowed')
    }
  }

  _initSort(sortBy = this.settings.sortBy) {
    this.sortSetting = {}
    if (typeof sortBy == 'object' && sortBy) {
      this.sortSetting = {...sortBy} // 排序记录
    }
    this.lastSortColumn = undefined // 上次排序
  }

  _initPage(hasPage = this.settings.hasPage) {
    this.lastPage = undefined
    this.lastLoadSizePage = undefined
    if (typeof hasPage == 'object' && hasPage) {
      this.state.page = hasPage.page || 0
      this.state.totalPage = hasPage.totalPage || 0
    } else if (String(hasPage).toLowerCase() == 'true') {
      this.state.page = 0
      this.state.totalPage = 0
    } else {
      this.state.page = 0
      this.state.totalPage = 0
    }
    this.loadSizePage = 0
  }

  _setCache(value = this.settings.data) {
    value = __validArray(value)
    this.cacheData = value
    this.cachePage = Math.ceil(value.length / defaultValues.pageSize)
  }

  _addCache(data, page) {
    this.cacheData.length = (page - 1) * defaultValues.fetchPageSize
    this.cacheData = this.cacheData.concat(data)
    this.cachePage = Math.ceil(this.cacheData.length / defaultValues.pageSize)
  }

  _setTotalNum(num) {
    if (Number.isNaN(Number(num))) {
      console.error(num, 'is not a number')
    }
    this.totalNum = num << 0
    this.state.totalPage = Math.ceil(this.totalNum / defaultValues.pageSize)
  }

  _setDataAtCell(row, col, value, source) {
    this.state.data[row][this.state.columns[col]] = value
    this.settings.data[row][this.state.columns[col]] = source
  }

  _getherLightRows(data) {
    data.map(item => {
      if (item && item[defaultValues.highLight]) {
        if (item.id) {
          this.lightRows.add(item.id)
        } else {
          console.error(item, 'needs id')
        }
      }
    })
  }

  _addData(data) {
    let modifyKey = function(item, index) {
      item.key = item.id
      if (item.key === undefined) {
        console.error(item, 'needs id')
        item.key = index
      }
    }
    let _addData = function(data) {
      let modifySelect = (item, index) => {
        if (this.selectRows == 'all') {
          item.checkbox = true
          this.selectRowsData.push(item)
          this.selectRowsDataSource.push(this.settings.data[index])
        } else {
          item.checkbox = this.selectRows.has(item.key)
          if (item.checkbox) {
            this.selectRowsData.push(item)
            this.selectRowsDataSource.push(this.settings.data[index])
          }
        }
      }
      const {hasIndex, checkAllBox} = this.settings
      if (checkAllBox && hasIndex) {
        data.map((item, index) => {
          modifySelect(item, index)
          modifyKey(item, index)
          item.index = item.index || index + 1
        })
      } else if (checkAllBox) {
        data.map((item, index) => {
          modifySelect(item, index)
          modifyKey(item, index)
        })
      } else if (hasIndex) {
        data.map((item, index) => {
          item.index = item.index || index + 1
          modifyKey(item, index)
        })
      } else {
        data.map((item, index) => {
          modifyKey(item, index)
        })
      }
      return data
    }
    Settings.prototype._addData = _addData
    return this._addData(data)
  }

  _renderData(data) {
    if (this.renderTimeColumns.length) {
      this.renderTimeColumns.map(item => {
        data.map(dataItem => {
          dataItem[item.enName] = moment(dataItem[item.enName]).format(item.format)
        })
      })
    }
    if (this.renderDataColumns.length) {
      this.renderDataColumns.map(item => {
        data.map(dataItem => {
          dataItem[item.enName] = item.render(dataItem[item.enName])
        })
      })
    }
    return data
  }

  _initData(data) {
    this._getherLightRows(data)
    data = this._renderData(this._addData(data))

    return data
  }

  _setData(data) {
    this.settings.data = data
    this.state.data = this._initData(data)
  }

  _getNextCachePage(isConcat) {
    let data = this._initData(this._getCache())
    this.loadSizePage = (this.loadSizePage + 1) % (defaultValues.pageSize / defaultValues.loadSize)
    this.state.page++
    return isConcat ? this.state.data.concat(data) : data
  }

  _getCache(page = this.state.page) {
    let start
    if (page == 0) {
      start = page * defaultValues.pageSize + defaultValues.loadSize * this.loadSizePage
      this.loadSizePage = defaultValues.firstPageSize / defaultValues.loadSize % (defaultValues.pageSize / defaultValues.loadSize)
      return this.cacheData.slice(start, start + defaultValues.firstPageSize)
    } else {
      if (this.loadSizePage >= 1) {
        start = (page - 1) * defaultValues.pageSize + defaultValues.loadSize * this.loadSizePage
      } else {
        start = page * defaultValues.pageSize + defaultValues.loadSize * this.loadSizePage
      }
      return this.cacheData.slice(start, start + defaultValues.loadSize)
    }
  }

  _gatherRenderColumns(columns) {
    this._initRenderColumns()
    columns.map(item => {
      if (item.yhType == 'time' && item.yhTimeFormat) {
        this.renderTimeColumns.push({enName: item.enName, format: item.yhTimeFormat})
      } else if (item.yhRender && item.yhRender) {
        this.renderDataColumns.push({enName: item.enName, render: item.yhRender})
      }
    })
  }

  _initRenderColumns() {
    // 需要渲染时间的列
    this.renderTimeColumns = []
    // 需要渲染数据的列
    this.renderDataColumns = []
  }

  _initLightRows(lightRows = this.settings.lightRows) {
    this.lightRows = new Set()
    if (typeof lightRows == 'object' && lightRows) {
      lightRows.forEach(item => {
        this.lightRows.add(item)
      })
    } else {
      console.error(lightRows, 'is not allowed')
    }
  }

  _initCache() {
    this.cacheData = [] // 缓存的数据
    this.cachePage = 0 // 缓存的数据页数
    // this.cachePageStart = 0 // 缓存开始的数据页数
  }
}
