/**
 * Created by wangjun on 2017/9/30.
 *
 * 生命周期
 * constructor初始化阶段
 * 配置this.settings => 初始化配置_init() => 初始化缓存_setCache() => 初始化数据_getCacheData() => 初始化表头_initColumns()
 *
 * 异步加载数据
 * fetchData() = 置空lightRows，cacheData，cachePage => 配置请求参数params => _fetchData() = 设置分页参数_nextPageParams()
 * => 请求数据 => 设置缓存初始化缓存_setCache() => 获取缓存数据_getCacheData() => afterFetch()
 *
 * 异步加载表头
 * fetchColumn() = 配置请求参数params => 请求数据 => 数据合并 => 初始化_initColumns() = 深二级拷贝 =>
 * 增加配置项列宽_addColWidths() => 转换至handsontable列_toColumns() => 增加配置列_addColumns() => 增加高亮行addLightRender()
 * => 渲染自定义单元格_enhancerRender() => afterFetch()
 *
 * cells                        function(row, col, props)                                               单元格配置
 *
 * columns            array<                                null          表头
 *                      enName             string                         表头对应数据字段
 *                      cnName             string                         表头显示名称
 *                      width              number           100           列宽
 *                      isChecked            boolean          true          是否显示
 *                      editor             string           false         是否可编辑
 *                      data               string                         异步表头对应字段
 *                      render             function(
 *                                           instance,
 *                                           TD,
 *                                           row,
 *                                           col,
 *                                           prop,
 *                                           value,
 *                                           cellProperties
 *                                         )
 *                    >
 * api
 *
 * setTableRegister,            function(decorator)(react.nodeElement)                                  注册
 *
 * deleteData                   function(array[id], decorator)                                          删除
 * disableSelect                function(decorator)                                                     禁用选择项
 * unDisableSelect              function(decorator)                                                     启用选择项
 * fetchColumn                  function(decorator)                                                     异步加载表头
 * fetchData                    function(decorator)                                                     异步加载数据
 * reInit                       function(hasIndex, checkAllBox, selectRows, hasPage, sortBy, decorator) 初始化
 * renderData                   function(data, decorator)                                               渲染数据
 * renderColumns                function(columns, decorator)                                            渲染表头
 * reRender                     function(decorator)                                                     重新渲染表格
 * setData                      function(sourceData, data, decorator)                                   修改数据
 * toggleTable                  function(decorator)                                                     显示隐藏
 * showTable                    function(decorator)                                                     显示
 * hideTable                    function(decorator)                                                     隐藏
 *
 * getDataSource                function(decorator)                                                     获取原始数据
 * getData                      function(decorator)                                                     获取数据
 * getSelectRowsData            function(decorator)                                                     获取选中数据
 * getSelectRowsDataSource      function(decorator)                                                     获取选中原始数据
 * getSourceColumns             function(decorator)                                                     获取表头原始数据
 * isSelectAll                  function(decorator)                                                     是否全选
 *
 * 原生api
 * setDataAtCell(row, col, value, source, decorator)
 * setDataAtRowProp(row, props, value, source, decorator)
 */
import React, { PureComponent } from 'react'
import { HotTable } from '@handsontable/react'
import ReactDOM from 'react-dom'
import config from './config'
import moment from 'moment'
import http from '@http'
import _ from 'lodash'
import './style.less'

const { baseConfig, defaultValues } = config
const enhancerRenderer = func => (...args) => ReactDOM.render(func(...args), args[1])
let root = 0

class YHTable extends PureComponent {
  constructor(props) {
    super(props)
    // this._getColWidths = this._getColWidths().bind(this)
    this._toColumns = this._toColumns().bind(this)
    this._getAddColumns = this._getAddColumns().bind(this)
    this._addData = this._addData().bind(this)
    this._handleScroll = this._handleScroll().bind(this)
    this.__getNextPage = this.__getNextPage().bind(this)
    this._fetchData = this._fetchData().bind(this)
    this._copyArray = this._copyArray().bind(this)
    this.state = { key: Math.random(), root: props.root || `yh-table-${root++}`, pageSize: defaultValues.fetchPageSize }
    // 滚动加载条数分页
    this.loadSizePage = 0
    // 选中数据
    this.selectRowsData = []
    // 需要渲染时间的列
    this.renderTimeColumns = []
    // 需要渲染数据的列
    this.renderDataColumns = []
    // 选中初始数据
    this.selectRowsDataSource = []
    // 记录上次分页加载的页码和分片
    this.lastPage = undefined
    this.lastLoadSizePage = undefined
    // 保存配置
    this.settings = Object.assign({}, baseConfig, props)
    const { columns, data, hasIndex, checkAllBox, selectRows = new Set(), hasPage, sortBy } = this.settings
    // 初始化内部资源-全选
    this._initSelectRows(selectRows)
    // 初始化内部资源-排序
    this._initSort(sortBy)
    // 清空缓存
    this._initCache()
    // 初始化分页
    this._initPage(hasPage)
    // 初始化封装配置
    this._init(hasIndex, checkAllBox)
    // 设置缓存内容
    this._setCache(data, this.cachePageStart)
    // 设置data
    this._setData(this._getCacheData())
    // 设置内部表头
    Object.assign(this.state, this._initColumns(this._copyArray(columns)))
  }
  componentWillMount() {
    const {
      dataConfig,
      columnList,
      data
    } = this.settings

    if (columnList) {
      this.fetchColumn()
    }
    if (dataConfig) {
      this.fetchData()
    }
    // 当传 data 有数据时，分页加一
    if (data && data.length) {
      this.setState(() => ({ page: 1 }))
    }
  }
  componentDidMount() {
    this.reSize()
  }
  componentWillReceiveProps(props) {
    let updateFlag = false
    let renderFlag = false
    let dataFlag = false
    let columnsFlag = false

    Object.assign(this.settings, props)
    const { columns, deleteRows, hasIndex, checkAllBox, hasPage } = this.settings

    for (let key in props) {
      if (props.hasOwnProperty(key)) {
        if (props[key] !== this.props[key]) {
          if (props[key] && key == 'dataConfig') {
            this.fetchData()
          } else if (props[key] && key == 'columnList') {
            this.fetchColumn()
          } else if (key == 'deleteRows') {
            this.deleteData(deleteRows)
          } else if (key == 'visible') {
            renderFlag = true
          } else if (key == 'data') {
            dataFlag = true
          } else if (key == 'columns') {
            columnsFlag = true
          } else if (key == 'fetchUrl') {
            continue
          } else if (key == 'hasPage') {
            this._initPage(hasPage)
          } else {
            updateFlag = true
          }
        }
      }
    }
    if (updateFlag) {
      this.reInit(hasIndex, checkAllBox)
    } else if (renderFlag) {
      return true
    }
    if (dataFlag) {
      this.renderData(this.settings.data)
    }
    if (columnsFlag) {
      this.asyncColumns = null
      if (this.nextKey) {
        this.state.key = Math.random()
        this.nextKey = false
      }
      this.setState(() => this._initColumns(this._copyArray(columns)))
    }

    return false
  }
  componentDidUpdate() {
    if (this.nextUpdate) {
      this.nextUpdate = false
      this.reRender()
      window.setTimeout(() => {
        this.settings.update = {}
        this.forceUpdate()
        this.apis.render()
      }, 0)
    }
    this._caculateHeight()
    // this._caculatePage()
  }
  componentWillUnmount() {
    this.unMount = true
  }

  __validArray = value => {
    if (!Array.isArray(value)) {
      return []
    }
    return value
  }
  __validObject = value => {
    if (typeof value == 'object') {
      return value
    }
    return {}
  }
  /*
   * 全选--表头
   * */
  __colHeaders = col => {
    switch (col) {
      case 0:
        return `<input class="colHeader-checkbox" type="checkbox" ${this.selectRows == 'all' ? 'checked=checked' : ''} ${this.settings.disabledCheckAllBox ? 'disabled' : ''}/>`
    }
  }
  /*
   * 排序前回调
   * */
  __handleBeforeColumnSort = (currentSortConfig, destinationSortConfigs) => {
    if (!destinationSortConfigs[0]) return false

    const index = destinationSortConfigs[0].column
    const { sortConfig, checkAllBox, hasIndex } = this.settings
    // 自定义列的数量
    let innerColumns = 0

    checkAllBox && innerColumns++
    hasIndex && innerColumns++
    // 如果开启了排序
    if (sortConfig) {
      try {
        // 获取需要排序的表头字段
        let header = this._getSourceColumns()[index - innerColumns].enName

        // 如果是自定义列中止排序
        if (header == 'checkbox' || header == 'index') {
          return false
        }
        // 如果和上次排序字段不相同--初始化排序配置
        if (this.lastSortColumn && this.lastSortColumn !== header) {
          this._initSort()
        }
        // 设置更新后的排序配置
        this._setSort(header)
        // 请求排序后的数据
        this._fetchSortData()
      } catch (error) {
        console.error('__handleBeforeColumnSort', error)
      }
    }

    return false
  }
  /*
   * 修改前回调
   * */
  __handleBeforeChange = changes => {
    let { data } = this.state
    let dataSource = this.settings.data
    const { afterChange } = this.props

    // 是否点击了表头全选
    if (changes && changes[0] && Number.isNaN(changes[0][0])) {
      // 切换表格全选状态
      if (this.selectRows == 'all') {
        this.unSelectAll()
      } else {
        this.selectAll()
      }
      changes[0] = null
      if (typeof afterChange == 'function') {
        afterChange(
          changes,
          '',
          this.selectRowsDataSource,
          this.selectRowsData,
          this.settings.data,
          this.state.data,
          this.selectRows == 'all',
          this.setData,
          this.totalNum
        )
      }
    } else if (changes) {
      for (let i = 0, j = changes.length; i < j; i++) {
        let item = changes[i]

        // 是否改变了行的选择项
        if (item && item[1] === 'checkbox') {
          if (String(item[3]) == 'false' || String(item[3]) == 'true') {
            try {
              // 获取表格选中区域
              let selected = this.apis.getSelected()
              if (_.isArray(selected[0])) selected = selected[0]
              // 对行数据选中项进行修改
              if (item[3]) {
                if (this.selectRows !== 'all') {
                  for (let k = selected[0], t = selected[2]; k <= t; k++) {
                    data[k].checkbox = true
                    dataSource[k].checkbox = true
                    this.selectRows.add(data[k].key)
                    this._modifySelectData(data[k])
                    this._modifySelectDataSource(this.settings.data[k])
                  }
                  if (this.selectRows.size == data.length) {
                    this.selectRows = 'all'
                  }
                }
              } else {
                if (this.selectRows === 'all') {
                  this._initSelectRows(new Set())
                  data.forEach((dataItem, index) => {
                    if (index <= selected[2] && index >= selected[0]) {
                      return
                    }
                    dataItem.checkbox = true
                    dataSource[index].checkbox = true
                    this.selectRows.add(dataItem.key)
                    this._modifySelectData(dataItem)
                    this._modifySelectDataSource(this.settings.data[index])
                  })
                } else {
                  for (let k = selected[0], t = selected[2]; k <= t; k++) {
                    data[k].checkbox = false
                    dataSource[k].checkbox = false
                    this.selectRows.delete(data[k].key)
                  }
                  this.selectRowsData = data.filter(dataItem => this.selectRows.has(dataItem.key))
                  this.selectRowsDataSource = this.settings.data.filter((dataItem, index) => this.selectRows.has(data[index].key))
                }
              }
            } catch (error) {
              console.error('__handleBeforeChange', error)
            }
          }
        }
      }
    }
  }
  /*
   * 修改后回调
   * */
  __afterChange = (changes, source) => {
    if (changes && !changes.length) {
      return true
    }
    if (source != 'edit') {
      return false
    }
    const { afterChange } = this.settings

    if (typeof afterChange == 'function') {
      let changesData = afterChange(
        changes,
        source,
        this.selectRowsDataSource,
        this.selectRowsData,
        this.settings.data,
        this.state.data,
        this.selectRows == 'all',
        this.setData,
        this.totalNum
      )

      // 修改后回调
      if (typeof changesData == 'object') {
        for (let key in changesData) {
          if (changesData.hasOwnProperty(key)) {
            if (typeof this[key] == 'function') {
              this[key](...changesData[key])
            }
          }
        }
      }
    }
  }
  /*
   * 拖拽后回调
   * */
  __afterColumnsMove = () => {
    // this.shouldMovedFlag && this.apis.render() // 虚拟columns同时切换位置
    this.shouldMovedFlag && this.setState(() => ({ columns: [...this.state.columns] }))
    // 如果拖拽了表头，下次需要一个新的表格
    this.shouldMovedFlag && (this.nextKey = true)

    return false
  }
  /*
   * 拖拽前回调
   * */
  __beforeColumnMove = (columns, target) => {
    const { hasIndex, checkAllBox, columnMoveConfig, columnList, afterColumnsMoved } = this.settings
    let innerColumnsNumber = 0

    checkAllBox && innerColumnsNumber++
    hasIndex && innerColumnsNumber++
    columns = this.__validArray(columns)
    columns = [...columns]
    try {
      // 如果没数据并且拖拽对象涉及到自定义列，中断拖拽
      if (!this.getData().length || !columns.every(item => item + 1 - innerColumnsNumber) || target < innerColumnsNumber || target == columns[columns.length - 1] + 1 || target == columns[columns.length - 1]) {
        this.shouldMovedFlag = false

        return false
      }
      const { page, pageSize } = this.state
      // 获取表头的副本
      let innerColumns = [...this._getSourceColumns()]
      let setting = {}

      // 生成表头修改的参数
      /* if (columnList) {
        setting = columnList
      } else */
      if (columnMoveConfig) {
        setting = columnMoveConfig
      } else {
        return
      }
      const { method = 'GET', urlConfig, params, config } = setting
      const { nextParams } = columnMoveConfig
      let newParams = { ...params }
      this._modifyColumnsMoved(innerColumns, innerColumnsNumber, columns, target)
      newParams.selectKeys = innerColumns.filter(item => item.isChecked).concat(innerColumns.filter(item => !item.isChecked)).map((item, index) => {
        item.sort = index

        return item
      })
      if (typeof nextParams == 'function') {
        Object.assign(newParams, nextParams(page, pageSize, ...params))
        if (columnMoveConfig && typeof columnMoveConfig === 'object') {
          Object.assign(columnMoveConfig.params, newParams)
        }
      }
      this.shouldMovedFlag = true

      urlConfig && http[method.toLocaleLowerCase()](urlConfig, newParams, config).then(data => {
        if (!data || this.unMount) {
          return
        }
        // 修改成功后改变初始表头
        this._modifyColumnsMoved(this._getSourceColumns(), innerColumnsNumber, columns, target)
      })
      afterColumnsMoved && this.settings.afterColumnsMoved(innerColumns)
      this._modifyColumnsMoved(this.state.columns, 0, columns, target)
    } catch (error) {
      console.error('__beforeColumnMove', error)
    }
    return false
  }
  // 获取下一页
  __getNextPage = () => {
    let isFetch = false,
      lastFetchTime = 0

    return () => {
      if (this.lastPage == this.state.page && this.lastLoadSizePage == this.loadSizePage) {
        return
      }
      this.lastPage = this.state.page
      this.lastLoadSizePage = this.loadSizePage
      // let {scrollLoad, rowHeadersHeightAutoHandle} = this.settings
      let { scrollLoad } = this.settings
      let nowTime = +new Date()

      if (!isFetch && nowTime - lastFetchTime > 100) {
        isFetch = true
        lastFetchTime = nowTime
        this._fetchScrollData(values => {
          isFetch = false
          scrollLoad = this.__validObject(scrollLoad)
          if (values !== null && typeof scrollLoad.afterFetch == 'function') {
            scrollLoad.afterFetch(values)
          }

          // if (rowHeadersHeightAutoHandle) this.nextUpdate = true
        })
      }
    }
  }
  // 获取handsontable原生API
  __getApi = apis => {
    const { _yh_getApi } = this.props

    this.apis = apis && apis.hotInstance || this.apis
    // console.log(this.apis)
    _yh_getApi && _yh_getApi({
      state: this.getState,
      setDataAtCell: this.setDataAtCell,
      setDataAtRowProp: this.setDataAtRowProp,
      settings: this.getSettings,
      isSelectAll: this.isSelectAll,
      getSelectRowsData: this.getSelectRowsData,
      getSelectRowsDataSource: this.getSelectRowsDataSource,
      getSortSetting: this.getSortSetting,
      fetchData: this.fetchData,
      fetchColumn: this.fetchColumn,
      deleteData: this.deleteData,
      toggleTable: this.toggleTable,
      getData: this.getData,
      renderData: this.renderData,
      renderColumns: this.renderColumns,
      reInit: this.reInit,
      hideTable: this.hideTable,
      showTable: this.showTable,
      setData: this.setData,
      unSelectAll: this.unSelectAll,
      disableSelect: this.disableSelect,
      unDisableSelect: this.unDisableSelect,
      reRender: this.reRender,
      reSize: this.reSize,
      setDataUrl: this.setDataUrl,
      setColumnList: this.setColumnList,
      getSourceColumns: this._getSourceColumns,
      updateSettings: this.updateSettings,
      clear: this.clear,
      selectAll: this.selectAll
    })
  }

  // 获取序号和选择框数据
  _addData = () => {
    let pre, result, preHasIndex, preCheckAllBox, preSelectRows
    let modifySelect = (item, index) => {
      if (this.selectRows == 'all') {
        item.checkbox = true
        this._modifySelectData(item)
        this._modifySelectDataSource(this.settings.data[index])
      } else {
        item.checkbox = this.selectRows.has(item.key)
        if (item.checkbox) {
          this._modifySelectData(item)
          this._modifySelectDataSource(this.settings.data[index])
        }
      }
    }
    let modifyKey = (item, index) => {
      item.key = item.id
      if (item.key === undefined) {
        item.key = index
      }
    }

    return data => {
      const {
        hasIndex,
        checkAllBox
      } = this.settings

      if (data !== pre || preHasIndex !== hasIndex || preCheckAllBox !== checkAllBox ||
        (preSelectRows !== this.selectRows ||
        preSelectRows && this.selectRows && preSelectRows.size && this.selectRows.size && preSelectRows.size !== this.selectRows.size)
      ) {
        pre = data
        data = this.__validArray(data)
        preHasIndex = hasIndex
        preCheckAllBox = checkAllBox
        preSelectRows = this.selectRows
        if (checkAllBox && hasIndex) {
          data.map((item, index) => {
            modifySelect(item, index)
            modifyKey(item, index)
            item.index = item.index || index + 1
          })
        } else if (checkAllBox) {
          data.map((item, index) => {
            modifySelect(item, index)
            modifyKey(item, index)
          })
        } else if (hasIndex) {
          data.map((item, index) => {
            item.index = item.index || index + 1
            modifyKey(item, index)
          })
        } else {
          data.map((item, index) => {
            modifyKey(item, index)
          })
        }
        result = data
      }

      return result
    }
  }
  // 设置高亮
  _addLightRender = columns => {
    columns = this.__validArray(columns)
    for (let i = 0, j = columns.length; i < j; i++) {
      let column = columns[i]

      if (column && column.type !== 'checkbox') {
        let render = column.renderer

        column.renderer = (instance, TD, row, col, prop, value, cellProperties) => {
          if (this.lightRows && this.lightRows.has(instance.getSourceDataAtRow(row).id)) {
            TD.parentNode && TD.parentNode.setAttribute('class', 'lighter')
          }
          if (render) {
            return render(instance, TD, row, col, prop, value, cellProperties)
          }

          return <span>{value}</span>
        }

        return columns
      }
    }

    return columns
  }
  // 复制数组（深二层）
  _copyArray = () => {
    let pre, result

    return arr => {
      if (arr !== pre) {
        pre = arr
        arr = this.__validArray(arr)
        result = [...arr].map(item => Object.assign({}, item)) || []
      }

      return result
    }
  }
  // 计算高度
  _caculateHeight = () => {
    if (this.apis && this.settings.autoRowsHeight) {
      let data = this.getData()
      if (!data.length || data.length > 10) {
        this.reSize()
      } else {
        try {
          let height = this.apis.container.childNodes[0].childNodes[0].clientHeight,
            rangeMinHeight = height + 50,
            rangeMaxHeight = height + 100
          if (this.settings.height > rangeMaxHeight || this.settings.height < rangeMinHeight && rangeMinHeight < this.settings.autoRowsMaxHeight) {
            this.settings.height = rangeMinHeight
            this.forceUpdate()
          }
        } catch (error) {
          console.error('_caculateHeight', error)
          this.settings.height = this.settings.autoRowsMaxHeight
          this.forceUpdate()
        }
      }
    }
  }
  // 计算是否加载下一页
  _caculatePage = () => {
    if (this.scrollTarget && this.scrollTarget.scrollHeight - (this.scrollTarget.offsetHeight + this.scrollTarget.scrollTop) < defaultValues.scrollFetchHeight) {
      this.__getNextPage()
    }
  }
  // 增强渲染
  _enhancerRender(columns) {
    columns = this.__validArray(columns)
    columns.length && columns.map(col => {
      if (col.renderer) {
        col.renderer = enhancerRenderer(col.renderer)
      }
    })

    return columns
  }
  // 获取数据
  _fetchData = () => {
    let lastFetchTime = {},
      hasFetch = {},
      otherParams
    let _fetchData = (setting, params, next) => {
      const { method = 'GET', urlConfig, config } = setting
      if (!hasFetch[params.page] || lastFetchTime[params.page] && new Date() - lastFetchTime[params.page] > 10000) {
        let now = +new Date()

        lastFetchTime[params.page] = now
        hasFetch[params.page] = true

        return urlConfig && http[method.toLocaleLowerCase()](urlConfig, params, config).then(value => {
          hasFetch[params.page] = false
          if (this.unMount) {
            return null
          }
          if (lastFetchTime[params.page] !== now) {
            return null
          } else {
            return value
          }
        }).then(next)
      } else {
        return null
      }
    }

    return (setting, callback, isConcat = false, afterLoad, isClear) => {
      const { params } = setting
      // const { fetchUrl } = this.props
      let { page, totalPage } = this.state
      let cachePages = defaultValues.cachePage
      let _setData = () => {
        let data = this._getCacheData(isConcat, callback, otherParams)

        this.settings.data = data.dataSource
        this._loadData(data.data)
        if (this.loadSizePage == 1 || page == 0 && data.data.length) {
          this.setState(() => ({ page: page + 1 }))
        }
        if (typeof afterLoad == 'function') {
          afterLoad(data.data)
        }
      }

      if (page + 1 > this.cachePage - cachePages) {
        let concatFlag = this._nextPageParams(params)

        if (page + 1 > this.cachePage) {
          if (this.cachePage < totalPage || totalPage == 0) {
            let next = () => {
              let promiseDate = _fetchData(setting, params, value => {
                if (!value || !value.result) {
                  if (typeof callback == 'function') {
                    callback()
                  }

                  return
                }
                let { result, ...props } = value

                otherParams = props
                if (concatFlag) {
                  this._setCache(this.cacheData.concat(result), this.cachePageStart)
                } else {
                  this.cachePageStart = (params.page - 1) * cachePages
                  this._setCache(result, this.cachePageStart)
                }
                this.totalNum = props.totalNum
                this.state.totalPage = Math.ceil(props.totalNum / defaultValues.pageSize)
                _setData()

                return value
              })

              if (promiseDate === null) {
                if (typeof callback == 'function') {
                  callback()
                }
              }
            }
            if (isClear) {
              this.clear(next)
            } else {
              next()
            }
          } else {
            _setData()
          }
        } else {
          if (this.cachePage < totalPage) {
            _fetchData(setting, params, value => {
              let { result, ...props } = value

              result = this.__validArray(result)
              otherParams = props
              if (concatFlag) {
                this._setCache(this.cacheData.concat(result), this.cachePageStart)
              } else {
                this.cachePageStart = (params.page - 1) * cachePages
                this._setCache(result, this.cachePageStart)
              }
              this.totalNum = props.totalNum << 0
              this.state.totalPage = Math.ceil(this.totalNum / defaultValues.pageSize)
            })
          }
          _setData()
        }
      } else {
        _setData()
      }
    }
  }
  // 滚动加载
  _fetchScrollData = callback => {
    const { page, pageSize } = this.state
    let setting = {}

    if (this.settings.scrollLoad && typeof this.settings.scrollLoad === 'object') {
      setting = this.settings.scrollLoad
    } else if (this.settings.dataConfig) {
      setting = this.settings.dataConfig
    } else {
      /* if (typeof callback == 'function') {
        callback(null)
      }*/

      // 分页加载
      let data = this._getCacheData(true, callback)
      this.settings.data = data.dataSource
      this._loadData(data.data)
      if (this.loadSizePage == 1 || page == 0 && data.data.length) {
        this.setState(() => ({ page: page + 1 }))
      }

      return
    }
    const { method = 'GET', urlConfig, params, config } = setting
    const { nextParams } = setting
    let newParams = { ...params }

    if (this.lastSortColumn) {
      newParams.orderBy = this.lastSortColumn
      newParams.sort = this.sortSetting[this.lastSortColumn] ? 'ASC' : 'DESC'
    }
    if (typeof nextParams == 'function') {
      Object.assign(newParams, nextParams(page, pageSize, ...params))
      if (this.settings.scrollLoad && typeof this.settings.scrollLoad === 'object') {
        Object.assign(this.settings.scrollLoad.params, newParams)
      } else {
        Object.assign(this.settings.dataConfig.params, newParams)
      }
    }
    this._fetchData({ method, urlConfig, params: newParams, config }, callback, true)
  }
  // 排序加载
  _fetchSortData = () => {
    const { sortConfig } = this.settings
    let isClear = false

    if (this.getData().length) {
      isClear = true
    }
    if (!sortConfig) {
      return
    }
    const { page, pageSize } = this.state
    let setting = {}

    if (this.settings.sortConfig && typeof this.settings.sortConfig === 'object') {
      setting = this.settings.sortConfig
    } else {
      setting = this.settings.dataConfig
    }
    const { method = 'GET', urlConfig, params, config } = setting
    const { nextParams } = setting
    let newParams = { ...params }

    if (this.lastSortColumn) {
      newParams.orderBy = this.lastSortColumn
      newParams.sort = this.sortSetting[this.lastSortColumn] ? 'ASC' : 'DESC'
    }
    if (typeof nextParams == 'function') {
      Object.assign(newParams, nextParams(page, pageSize, ...params))
      if (this.settings.sortConfig && typeof this.settings.sortConfig === 'object') {
        Object.assign(this.settings.sortConfig.params, newParams)
      } else {
        Object.assign(this.settings.dataConfig.params, newParams)
      }
    }
    this._initCache()
    this._initSelectRows()
    this.lightRows = new Set()
    this._initPage()
    this.totalNum = 0

    this._fetchData({ method, urlConfig, params: newParams, config }, sortConfig.afterFetch, false, sortConfig.afterLoad, isClear)
  }
  // 查找相同name的行
  _findColumn = name => {
    let {
      columns,
      columnWith
    } = this.settings

    columns = this.__validArray(columns)
    for (let i = 0, j = columns.length; i < j; i++) {
      let item = columns[i]

      if (item.enName === name || item.data === name) {
        item.width = item.width || columnWith || defaultValues.columnWith

        return item
      }
    }
  }
  // 获取需要渲染的数据列
  _gatherRenderColumns = columns => {
    columns = this.__validArray(columns)
    this.renderTimeColumns = []
    this.renderDataColumns = []
    columns.map(item => {
      if (item.yhType == 'time') {
        this.renderTimeColumns.push({ enName: item.enName, format: item.yhTimeFormat })
      } else if (item.yhRender) {
        this.renderDataColumns.push({ enName: item.enName, render: item.yhRender })
      }
    })
  }
  // 获取配置列
  _getAddColumns = () => {
    let result, preHasIndex, preCheckAllBox

    return () => {
      const {
        hasIndex,
        indexWidth,
        checkAllBox
      } = this.settings

      if (preHasIndex !== hasIndex || preCheckAllBox !== checkAllBox) {
        preHasIndex = hasIndex
        preCheckAllBox = checkAllBox
        let columns = []

        if (checkAllBox) {
          columns.push({
            data: 'checkbox',
            editor: false,
            type: 'checkbox',
            className: 'checkbox'
          })
        }
        if (hasIndex) {
          columns.push({
            title: '序号',
            data: 'index',
            editor: false,
            width: indexWidth || defaultValues.columnWith,
            renderer: (instance, TD, row, col, prop, value, cellProperties) => {
              TD.innerHTML = `<span>${row + 1}</span>`
            }
          })
        }
        result = columns
      }

      return result
    }
  }
  // 获取缓存数据
  _getCacheData = (isConcat, callback, otherParams) => {
    let value = this._getCacheDataSource()
    let dataSource, data
    if (isConcat) {
      this.settings.data.splice(this.settings.data.length, 0, ...value)
      this.state.data.splice(this.state.data.length, 0, ...this._initData(value))
      dataSource = this.settings.data
      data = this.state.data
    } else {
      dataSource = value || []
      data = this._initData(value)
    }

    callback && callback({ result: value, ...otherParams })
    return { data, dataSource }
  }
  // 获取原始缓存数据
  _getCacheDataSource = () => {
    const { page } = this.state
    let start
    if (page == 0) {
      start = page * defaultValues.pageSize + defaultValues.loadSize * this.loadSizePage
      this.loadSizePage = defaultValues.firstPageSize / defaultValues.loadSize % (defaultValues.pageSize / defaultValues.loadSize)
      return this.cacheData.slice(start, start + defaultValues.firstPageSize)
    } else {
      if (this.loadSizePage >= 1) {
        start = (page - 1) * defaultValues.pageSize + defaultValues.loadSize * this.loadSizePage
      } else {
        start = page * defaultValues.pageSize + defaultValues.loadSize * this.loadSizePage
      }
      this.loadSizePage = (this.loadSizePage + 1) % (defaultValues.pageSize / defaultValues.loadSize)
      return this.cacheData.slice(start, start + defaultValues.loadSize)
    }
  }
  // 获取原始表头
  _getSourceColumns = () => {
    const { columnList, columnMoveConfig } = this.settings

    if (this.asyncColumns && (columnList || columnMoveConfig)) {
      return this.asyncColumns.sort((left, right) => left.sort - right.sort)
    } else {
      return this.settings.columns || []
    }
  }
  // 获取列宽
  /* _getColWidths = () => {
   let pre, result, preHasIndex, preCheckAllBox, preIndexWidth, preCheckAllBoxWidth, preColumnWwidth

   return columns => {
   const {
   hasIndex,
   checkAllBox,
   indexWidth,
   checkAllBoxWidth,
   columnWith
   } = this.settings

   if (columns !== pre || preHasIndex !== hasIndex || preCheckAllBox !== checkAllBox || preIndexWidth !== indexWidth ||
   preCheckAllBoxWidth !== checkAllBoxWidth || preColumnWwidth !== columnWith) {
   pre = columns
   preHasIndex = hasIndex
   preCheckAllBox = checkAllBox
   preIndexWidth = indexWidth
   preCheckAllBoxWidth = checkAllBoxWidth
   preColumnWwidth = columnWith
   let colWidths = []

   if (checkAllBox) {
   colWidths.push(checkAllBoxWidth || columnWith || defaultValues.columnWith)
   }
   if (hasIndex) {
   colWidths.push(indexWidth || columnWith || defaultValues.columnWith)
   }
   columns.forEach(item => {
   if (item.isChecked || item.isChecked === undefined) {
   colWidths.push(item.width || columnWith || defaultValues.columnWith)
   }
   })
   result = colWidths

   return colWidths
   } else {
   return result
   }
   }
   }*/
  // 滚动事件
  _handleScroll = () => {
    let lastScroll = {}

    return e => {
      const { scrollLoad } = this.settings
      let target = e.target

      if (scrollLoad) {
        if (!target.key) {
          target.key = Math.random()
        }
        let key = target.key

        lastScroll[key] = this.__validObject(lastScroll[key])
        if (lastScroll[key].scrollLeft != target.scrollLeft) {
          lastScroll[key].scrollLeft = target.scrollLeft
        } else if (lastScroll[key].scrollTop != target.scrollTop) {
          lastScroll[key].scrollTop = target.scrollTop
          const { page, totalPage } = this.state

          if ((page > totalPage || page == totalPage && this.loadSizePage == 0) && totalPage !== 0) {
            return
          }
          if (target.scrollHeight > defaultValues.scrollFetchHeight &&
            target.scrollHeight - (target.offsetHeight + target.scrollTop) < defaultValues.scrollFetchHeight) {
            this.scrollTarget = target
            this.__getNextPage()
          }
        }
      }
    }
  }
  // 初始化配置
  _init = (newHasIndex = this.settings.hasIndex, newCheckAllBox = this.settings.checkAllBox) => {
    Object.assign(this.state, {
      hasIndex: newHasIndex,
      checkAllBox: newCheckAllBox
    })

    return this.state
  }
  // 清空缓存
  _initCache = () => {
    this.cacheData = [] // 缓存的数据
    this.cachePage = 0 // 缓存的数据页数
    this.cachePageStart = 0 // 缓存开始的数据页数
  }
  // 初始化列
  _initColumns = columns => {
    if (columns) {
      let state = { columns }

      this._gatherRenderColumns(state.columns)
      state.columns = this._toColumns(state.columns)
      // state.colWidths = this._getColWidths(state.columns)
      state.columns = this._getAddColumns().concat(state.columns)
      state.columns = this._enhancerRender(this._addLightRender(state.columns))

      return state
    } else {
      return {}
    }
  }
  // 初始化数据
  _initData = data => {
    let newData = this._copyArray(data)

    this._setLightRows(newData)
    newData = this._renderData(this._addData(newData))
    this.selectRowsData = this.selectRowsData.concat(newData.filter(item => this.selectRows == 'all' || this.selectRows.has(item.key)))
    this.selectRowsDataSource = this.selectRowsDataSource.concat(data.filter((item, index) => this.selectRows == 'all' || this.selectRows.has(newData[index].key)))

    return newData
  }
  // 清空分页
  _initPage = (hasPage = this.settings.hasPage) => {
    this.lastPage = undefined
    this.lastLoadSizePage = undefined
    if (typeof hasPage == 'object' && hasPage) {
      this.state.page = hasPage.page || 0
      this.state.totalPage = hasPage.totalPage || 0
    } else if (String(hasPage).toLowerCase() == 'true') {
      this.state.page = 0
      this.state.totalPage = 0
    } else {
      this.state.page = 0
      this.state.totalPage = 0
    }
    this.loadSizePage = 0
  }
  // 清空全选 -（重置selectRows）
  _initSelectRows = (selectRows = this.settings.selectRows) => {
    this.selectRows = new Set()
    if (typeof selectRows == 'object' && selectRows && selectRows.size == 0 || !selectRows) {
      this.selectRowsDataSource = []
      this.selectRowsData = []
    } else {
      if (typeof selectRows == 'object' && selectRows) {
        selectRows.forEach(item => {
          this.selectRows.add(item)
        })
      } else if (String(selectRows).toLowerCase() == 'all') {
        this.selectRows = 'all'
      }
    }
  }
  // 清空配置
  _initSettings = () => {
    // 全选
    this._initSelectRows()
    // 高亮
    this.lightRows = new Set()
    // 缓存
    this._initCache()
    // 排序
    this._initSort()
    // 总条数
    this.totalNum = 0
  }
  // 清空排序
  _initSort = (sortBy = this.settings.sortBy) => {
    this.sortSetting = {}
    if (typeof sortBy == 'object' && sortBy) {
      this.sortSetting = { ...sortBy } // 排序记录
    }
    this.lastSortColumn = undefined // 上次排序
  }
  // 渲染数据
  _loadData = (data, callback) => {
    /* if (this.apis && this.apis.loadData) {
     this.state.data = data
     this.apis.loadData(data)
     } else {
     this.setState({data: [...data]})
     }*/
    data = this.__validArray(data)
    this.state.data = data
    this.settings.update = {}
    this.forceUpdate(callback)
  }
  // 编辑拖拽表头
  _modifyColumnsMoved = (columns, start, selectColumns, target) => {
    columns = this.__validArray(columns)
    selectColumns = this.__validArray(selectColumns)
    start <<= 0
    target <<= 0
    let movedColumns = columns.splice(selectColumns[0] - start, selectColumns.length)

    if (target - 1 > selectColumns[0]) {
      columns.splice(target - start - selectColumns.length, 0, ...movedColumns)
    } else if (target < selectColumns[0]) {
      columns.splice(target - start, 0, ...movedColumns)
    }
  }
  // 添加已选择数据
  _modifySelectData = item => {
    this.selectRowsData.push(item)
  }
  // 添加已选择原始数据
  _modifySelectDataSource = item => {
    this.selectRowsDataSource.push(item)
  }
  // 生成下一页请求参数配置
  _nextPageParams = params => {
    const { hasPage } = this.settings

    if (!hasPage) {
      return false
    }
    const { page } = this.state
    let cachePages = defaultValues.fetchPageSize / defaultValues.pageSize
    let concatFlag = false

    params = this.__validObject(params)
    params.page = Math.ceil((page + 1) / cachePages)
    if (params.page * cachePages <= this.cachePage) {
      params.page++
      concatFlag = true
    }
    params.size = defaultValues.fetchPageSize

    return concatFlag
  }
  // 渲染前修改数据
  _renderData = data => {
    if (this.renderTimeColumns.length) {
      this.renderTimeColumns.map(item => {
        data.map(dataItem => {
          dataItem[item.enName] = moment(dataItem[item.enName]).format(item.format)
        })
      })
    }
    if (this.renderDataColumns.length) {
      this.renderDataColumns.map(item => {
        data.map(dataItem => {
          dataItem[item.enName] = item.render(dataItem[item.enName])
        })
      })
    }
    return data
  }
  // 设置缓存
  _setCache = (value, pageStart) => {
    value = this.__validArray(value)
    this.cacheData = value
    this.cachePage = pageStart + Math.ceil(value.length / defaultValues.pageSize)
  }
  // 设置data
  _setData = data => {
    data.data = this.__validArray(data.data)
    data.dataSource = this.__validArray(data.dataSource)
    this.state.data = data.data
    this.settings.data = data.dataSource
  }
  // 设置排序
  _setSort = header => {
    this.sortSetting[header] = !this.sortSetting[header]
    this.lastSortColumn = header
  }
  // 查找高亮行
  _setLightRows = data => {
    data = this.__validArray(data)
    data.map(item => {
      if (item && item.highLight) {
        this.lightRows && this.lightRows.add(item.id)
      }
    })
  }
  // 转换列
  _toColumns = () => {
    let pre, result, preColumnList

    return value => {
      const { columnList } = this.settings

      if (pre !== value || preColumnList !== columnList) {
        pre = value
        value = this.__validArray(value)
        preColumnList = columnList
        let columns = []

        if (columnList) {
          value = value.sort((left, right) => left.sort - right.sort)
        }
        value.forEach(item => {
          if (item.isChecked || item.isChecked === undefined) {
            let editor = false
            let readOnly = true

            if (item.editor == true) {
              editor = 'text'
              readOnly = false
            } else if (item.editor != undefined) {
              editor = item.editor
              readOnly = false
            }
            if (item.type !== undefined) {
              readOnly = false
            }
            let defaultSettings = {
              ...item,
              title: item.chName,
              data: item.enName,
              readOnly,
              editor
            }

            if (item.render) {
              defaultSettings.renderer = item.render
            } else if (item.type === undefined) {
              defaultSettings.renderer = (instance, TD, row, col, prop, value, cellProperties) => <span title={value}>{value}</span>
            }
            columns.push(defaultSettings)
          }
        })
        result = columns

        return result
      } else {
        return result
      }
    }
  }

  // 清空表格
  clear = callback => {
    if (this.getData().length) {
      this._initPage()
      this.totalNum = 0

      this.settings.data = []
      this._loadData([], callback)
    } else {
      callback && callback()
    }
    /* if (this.apis && this.apis.clear) {
     this.apis.clear()
     } else {
     this.renderData([])
     }*/
  }
  // 删除行--接口
  deleteData = (deleteRows = []) => {
    deleteRows = this.__validArray(deleteRows)
    this.settings.data = this.settings.data.filter(item => !deleteRows.includes(item.id))
    this.setState(() => ({ data: this.getData().filter(item => !deleteRows.includes(item.id)) }))
  }
  // 禁用选择项--接口
  disableSelect = () => {
    if (this.settings.disabledCheckAllBox) {
      return
    }
    this.settings.disabledCheckAllBox = true
    this.state.columns[0].readOnly = true
    this.setState(() => ({ columns: [...this.state.columns] }))
  }
  // 获取表头--接口
  fetchColumn = () => {
    const setting = this.settings.columnList
    const { method = 'GET', urlConfig, params, config, afterFetch } = setting
    const { nextParams } = setting
    const { page, pageSize } = this.state
    let newParams = { ...params }

    if (this.nextKey) {
      this.state.key = Math.random()
      this.nextKey = false
    }

    if (typeof nextParams == 'function') {
      Object.assign(newParams, nextParams({ page, pageSize, ...params }))
      Object.assign(this.settings.columnList.params, newParams)
    }

    return urlConfig && http[method.toLocaleLowerCase()](urlConfig, newParams, config).then(data => {
      if (this.unMount) {
        return
      }
      if (!data) {
        afterFetch && afterFetch()

        return
      }
      this.renderColumns(data)
      afterFetch && afterFetch(data)

      return data
    })
  }
  // 初次获取数据接口数据--接口
  fetchData = () => {
    let isClear = false
    if (this.getData().length) {
      isClear = true
    }
    this._initPage()
    this._initSettings()
    const { dataConfig } = this.settings
    const { page, pageSize } = this.state

    if (typeof dataConfig == 'object') {
      const { method = 'GET', urlConfig, params, config } = dataConfig
      const { nextParams, afterFetch, afterLoad } = dataConfig
      let newParams = { ...params }

      if (typeof nextParams == 'function') {
        Object.assign(newParams, nextParams(page, pageSize, ...params))
        Object.assign(this.settings.dataConfig.params, newParams)
      }

      return this._fetchData({ method, urlConfig, params: newParams, config }, afterFetch, false, afterLoad, isClear)
    } else {
      return this._fetchData(dataConfig, null, false, null, isClear)
    }
  }
  // 获取数据--接口
  getData = () => {
    if (this.apis && this.apis.getSourceData) {
      let data = this.apis.getSourceData()
      data = this.__validArray(data)
      return data
    } else {
      return this.state.data || []
    }
  }
  // 获取选择行原始数据
  getSelectRowsDataSource = () => this.selectRowsDataSource
  // 获取选择行数据
  getSelectRowsData = () => this.selectRowsData
  // 获取排序状态
  getSortSetting = () => this.sortSetting
  // 获取配置
  getSettings = () => this.settings
  // 获取配置
  getState = () => this.state
  // 隐藏表格--接口
  hideTable = () => {
    if (this.settings.visible !== false) {
      this.settings.visible = false
      this.forceUpdate()
    }
  }
  // 判断是否全选
  isSelectAll = () => this.selectRows == 'all'
  // 初始化配置--接口
  reInit = (hasIndex, checkAllBox) => {
    this.setState(() => this._init(hasIndex, checkAllBox))
  }
  // 渲染列--接口
  renderColumns = columns => {
    if (this.nextKey) {
      this.state.key = Math.random()
      this.nextKey = false
    }
    this.asyncColumns = this._copyArray(columns).sort((left, right) => left.sort - right.sort)
    columns = this._copyArray(columns)
    columns.map(item => {
      Object.assign(item, this._findColumn(item.enName))
    })
    // this.nextUpdate = true
    this.setState(() => this._initColumns(columns))
  }
  // 渲染数据--接口
  renderData = (data, hasPage = this.settings.hasPage) => {
    let dataList = this.getData()
    dataList = this.__validArray(dataList)
    let render = () => {
      this._initPage(hasPage)
      this._initSettings()
      this.totalNum = data.length || 0
      this._setCache(data, this.cachePageStart)
      let dataSource = this._getCacheData()

      this._loadData(dataSource.data)
      this.settings.data = dataSource.dataSource
      this.setState(() => ({ page: 1 }))

      // 补充：显示分页条件
      // this.state.totalPage = Math.ceil(this.totalNum / defaultValues.pageSize)
    }
    if (dataList.length) {
      this.clear(render)
    } else {
      render()
    }
  }
  // 重新渲染表格--接口
  reRender = () => {
    if (this.apis && this.apis.render) {
      this.apis.render()
    } else {
      this.settings.update = {}
      this.forceUpdate()
    }
  }
  // 自适应--接口
  reSize = () => {
    let dom = this.refs.tableDom,
      height = dom.clientHeight - 50 // 50顶部导航

    if (this.settings.hasPage) {
      height -= 48 // 48分页
    }
    /* let height = document.getElementsByTagName('body')[0].clientHeight - document.getElementsByClassName('container')[0].clientHeight
     - document.getElementById('root').clientHeight + dom.clientHeight - 50*/
    while (dom && dom.getAttribute && (dom.parentNode && (dom.getAttribute('class') && !dom.getAttribute('class').split(' ').includes('layout--content')) || !dom.getAttribute('class'))) {
      dom = dom.parentNode
    }
    if (dom && !dom.getAttribute) {
      return
    }
    // height += document.getElementById('root').clientHeight
    height += document.getElementsByTagName('body')[0].clientHeight
    Array.prototype.map.call(dom.children, item => {
      height -= item.clientHeight
    })

    height = height > defaultValues.minHeight ? height : defaultValues.minHeight
    height = height > defaultValues.maxHeight ? defaultValues.maxHeight : height
    if (height !== this.settings.height) {
      this.settings.height = height
      this.forceUpdate()
    }
  }
  // 选择所有--接口
  selectAll = () => {
    if (this.selectRows == 'all') {
      return
    }
    let data = this.getData().map(item => {
      item.checkbox = true

      return item
    })

    this.selectRows = 'all'
    this.selectRowsData = data
    this.selectRowsDataSource = this.settings.data
    if (this.apis && this.apis.selectCell) {
      this.apis.selectCell(0, 'checkbox', 0)
    } else {
      this._loadData(data)
    }
  }
  // 编辑数据--接口
  setData = (sourceData, data) => {
    if (sourceData) {
      this.settings.data = sourceData
    }
    if (data) {
      this._loadData(data)
    }
  }
  // 设置单元格数据--接口
  setDataAtCell = (row, col, value, source) => {
    try {
      this.state.data[row][this.state.columns[col]] = value
      this.settings.data[row][this._getSourceColumns()[col]] = source
      if (this.apis && this.apis.setDataAtCell) {
        this.apis.setDataAtCell(row, col, value, source)
      } else {
        this.setState(() => ({ data: [...this.state.data] }))
      }
    } catch (error) {
      console.error('setDataAtCell', error)
    }
  }
  // 设置单元格数据--接口
  setDataAtRowProp = (row, prop, value, source) => {
    try {
      this.state.data[row][prop] = value
      this.settings.data[row][prop] = source
      if (this.apis && this.apis.setDataAtRowProp) {
        this.apis.setDataAtRowProp(row, prop, value, source)
      } else {
        this.setState(() => ({ data: [...this.state.data] }))
      }
    } catch (error) {
      console.error('setDataAtRowProp', error)
    }
  }
  // 显示表格--接口
  showTable = () => {
    if (this.settings.visible !== true) {
      this.settings.visible = true
      this.forceUpdate()
    }
  }
  // 显示隐藏切换--接口
  toggleTable = () => {
    this.settings.visible = !this.settings.visible
    this.forceUpdate()
  }
  // 启用选择项--接口
  unDisableSelect = () => {
    if (!this.settings.disabledCheckAllBox) {
      return
    }
    this.settings.disabledCheckAllBox = false
    this.state.columns[0] && (this.state.columns[0].readOnly = false)
    this.setState(() => ({ columns: [...this.state.columns] }))
  }
  // 取消选择所有--接口
  unSelectAll = () => {
    if (this.selectRows !== 'all' && !this.selectRows.size) {
      return
    }
    this._initSelectRows(new Set())
    this._loadData(this.getData().map(item => {
      item.checkbox = false

      return item
    }))
  }
  // 设置异步数据接口并请求数据--接口
  setDataUrl = (dataConfig) => {
    this.settings.dataConfig = dataConfig

    return this.fetchData()
  }
  // 设置异步接口表头并请求数据--接口
  setColumnList = columnList => {
    this.settings.columnList = columnList

    return this.fetchColumn()
  }
  // 设置异步接口表头并请求数据--接口
  updateSettings = (settings, init) => {
    this.apis.updateSettings(settings, init)
  }
  /* document 键盘点击之前事件 */
  __handleAfterDocumentKeyDown = (e) => {
    if (e.target.nodeName.toLocaleLowerCase() === 'textarea' && e.target.className === 'handsontableInput') {
      if (window.keydownCount == undefined) {
        window.keydownCount = 1
      } else {
        window.keydownCount += 1
      }
      // window.clearTimeout(window.afterDocumentKeyDownTimeout)

      let val = e.target.value
      if (val && val.charAt) {
        let key = val.charAt(val.length - 1)
        if (e.keyCode === 229 && /\d/.test(key)) { // 输入数字情况
          window.afterDocumentKeyDownTimeout = window.setTimeout(() => {
            if (!e.target.value) {
              e.target.value = val
            } else if (e.target.value.length >= 1 && window.keydownCount > 1) {
              e.target.value = val + e.target.value
            }
            window.keydownCount = 0
          }, 0)

          let event = {}
          // zero 字符串转码
          for (let idx in e) { // 拷贝
            event[idx] = e[idx]
          }
          let key = event.target.value.charAt(event.target.value.length - 1)
          event.keyCode = key.charCodeAt()
          return event
        } else if (e.keyCode === 229 && val === ' ') { // 输入无反应情况
          window.afterDocumentKeyDownTimeout = window.setTimeout(() => {
            let val = e.target.value
            let key = val.charAt(val.length - 1)
            if (/\d/.test(key)) {
              e.target.value = val
            }
            window.keydownCount = 0
          })

          let event = {}
          // zero 字符串转码
          for (let idx in e) { // 拷贝
            event[idx] = e[idx]
          }
          event.keyCode = event.target.value.charCodeAt()
          return event
        } else if (e.keyCode === 229 && /\'/.test(val)) { // 输入 x'm
          let _left = this
          window.clearTimeout(window.keyDownTimeout)
          window.keyDownTimeout = window.setTimeout(() => {
            window.keydownCount = 0
            _left.fireKeyEvent(e.target, 'keydown', 229)
          })

          let event = {}
          // zero 字符串转码
          for (let idx in e) { // 拷贝
            event[idx] = e[idx]
          }
          event.keyCode = 24 // 返回无效code码
          return event
        } else {
          window.keydownCount = 0
        }
      }
    }
  }
  // JavaScript 模拟键盘事件和鼠标事件（比如模拟按下回车等）
  fireKeyEvent = (el, evtType, keyCode) => {
    let doc = el.ownerDocument,
      win = doc.defaultView || doc.parentWindow,
      evtObj
    if (doc.createEvent) {
      if (win.KeyEvent) {
        evtObj = doc.createEvent('KeyEvents')
        evtObj.initKeyEvent(evtType, true, true, win, false, false, false, false, keyCode, 0)
      } else {
        evtObj = doc.createEvent('UIEvents')
        Object.defineProperty(evtObj, 'keyCode', {
          get() {
            return this.keyCodeVal
          }
        })
        Object.defineProperty(evtObj, 'which', {
          get() {
            return this.keyCodeVal
          }
        })
        evtObj.initUIEvent(evtType, true, true, win, 1)
        evtObj.keyCodeVal = keyCode
        if (evtObj.keyCode !== keyCode) {
          console.log(`keyCode ${evtObj.keyCode} 和 (${evtObj.which}) 不匹配`)
        }
      }
      el.dispatchEvent(evtObj)
    } else if (doc.createEventObject) {
      evtObj = doc.createEventObject()
      evtObj.keyCode = keyCode
      el.fireEvent(`on${evtType}`, evtObj)
    }
  }

  render() {
    let {
      hasPage,
      visible,
      ..._settings
    } = this.settings
    let state = this.state
    let {
      page,
      totalPage,
      data,
      key,
      columns,
      // colWidths,
      root
    } = state
    const settings = Object.assign(_settings, {
      colHeaders: this.__colHeaders,
      afterDocumentKeyDown: this.__handleAfterDocumentKeyDown,
      // beforeKeyDown: () => {console.log('beforeKeyDown')},
      beforeColumnSort: this.__handleBeforeColumnSort,
      // beforeAutofill: () => {console.log('beforeAutofill');return false},
      // beforeAutofillInsidePopulate: () => {console.log('beforeAutofillInsidePopulate');return false},
      // beforeCellAlignment: () => {console.log('beforeCellAlignment');return false},
      beforeChange: this.__handleBeforeChange,
      // beforeChangeRender: () => {console.log('beforeChangeRender');return false},
      // beforeColumnMove: () => {console.log('beforeColumnMove');return false},
      // beforeColumnResize: () => {console.log('beforeColumnResize');return false},
      // beforeContextMenuSetItems: () => {console.log('beforeContextMenuSetItems');return false},
      // beforeCopy: () => {console.log('beforeCopy');return false},
      // beforeCreateCol: () => {console.log('beforeCreateCol');return false},
      // beforeCreateRow: () => {console.log('beforeCreateRow');return false},
      // beforeCut: () => {console.log('beforeCut');return false},
      // beforeDrawBorders: () => {console.log('beforeDrawBorders');return false},
      // beforeGetCellMeta: () => {console.log('beforeGetCellMeta');return false},
      // beforeInit: () => {console.log('beforeInit');return false},
      // beforeInitWalkontable: () => {console.log('beforeInitWalkontable');return false},
      // beforeOnCellMouseDown: () => {console.log('beforeOnCellMouseDown');return false},
      // beforeOnCellMouseOut: () => {console.log('beforeOnCellMouseOut');return false},
      // beforeOnCellMouseOver: () => {console.log('beforeOnCellMouseOver');return false},
      // beforePaste: () => {console.log('beforePaste');return false},
      // beforeRedo: () => {console.log('beforeRedo');return false},
      // beforeRemoveCellMeta: () => {console.log('beforeRemoveCellMeta');return false},
      // beforeRemoveCol: () => {console.log('beforeRemoveCol');return false},
      // beforeRemoveRow: () => {console.log('beforeRemoveRow');return false},
      // beforeRender: () => {console.log('beforeRender');return false},
      // beforeRenderer: () => {console.log('beforeRenderer');return false},
      // beforeRowMove: () => {console.log('beforeRowMove');return false},
      // beforeRowResize: () => {console.log('beforeRowResize');return false},
      // beforeSetRangeEnd: () => {console.log('beforeSetRangeEnd');return false},
      // beforeSetRangeStart: () => {console.log('beforeSetRangeStart');return false},
      // beforeStretchingColumnWidth: () => {console.log('beforeStretchingColumnWidth');return false},
      // beforeTouchScroll: () => {console.log('beforeTouchScroll');return false},
      // beforeUndo: () => {console.log('beforeUndo');return false},
      // beforeValidate: () => {console.log('beforeValidate');return false},
      // beforeValueRender: () => {console.log('beforeValueRender');return false},
      // afterScrollVertically: (...scrollProps) => {console.log('afterScrollVertically', scrollProps)},
      // afterScrollHorizontally: (...scrollProps) => {console.log('afterScrollHorizontally', scrollProps)},
      // beforeTouchScroll: (...scrollProps) => {console.log('beforeTouchScroll', scrollProps)},
      data,
      columns,
      root,
      // colWidths,
      afterChange: this.__afterChange,
      afterColumnMove: this.__afterColumnsMove,
      beforeColumnMove: this.__beforeColumnMove
    })

    this.__getApi()

    return (
      <div className={`handsontable-wrapper ${visible ? '' : 'visible'}`} onScroll={this._handleScroll} ref="tableDom" key={key}>
        <PureHotTable
          getApi={this.__getApi}
          {...settings}
        />
        {
          hasPage && totalPage > 1 &&
          <LoadMore
            page={page}
            totalPage={totalPage}
            onClick={this.__getNextPage}
          />
        }
      </div>
    )
  }
}

YHTable.create = (Comp, key) => {
  let tableState = null
  let clonedElement
  let lastProps = {}
  let isEqual = (left, right) => {
    for (let key in left) {
      if (left[key] !== right[key]) {
        return false
      }
    }
    return true
  }
  let setTableRegister = (decorator, props) => {
    let _yh_getApi = (() => {
      let localDecorator = decorator

      return state => {
        if (localDecorator) {
          tableState[localDecorator] = state
        } else {
          tableState = state
        }
      }
    })()

    if (decorator && tableState && !tableState[decorator]) {
      tableState[decorator] = {}
      lastProps[decorator] = props
      clonedElement = {}
    } else if (decorator && !tableState) {
      tableState = {}
      tableState[decorator] = {}
      lastProps[decorator] = props
      clonedElement = {}
    } else if (!tableState) {
      tableState = {}
      lastProps = props
    }

    return TableItem => {
      if (decorator) {
        if (isEqual(props, lastProps[decorator])) {
          if (clonedElement[decorator]) {
            return clonedElement[decorator]
          }
        }
      } else {
        if (isEqual(props, lastProps)) {
          if (clonedElement) {
            return clonedElement
          }
        }
      }
      if (React.isValidElement(TableItem)) {
        clonedElement = React.cloneElement(TableItem, { _yh_getApi, key: decorator || 'tableMul', ...props })
        return clonedElement
      } else {
        console.error(`setTableRegister ${TableItem} is not a React element`)
      }
    }
  }
  let fetchData = decorator => {
    if (decorator) {
      if (tableState[decorator] == undefined) {
        console.error('表格还未初始化')
      } else {
        tableState[decorator].fetchData()
      }
    } else {
      tableState.fetchData()
    }
  }
  let fetchColumn = decorator => {
    if (decorator) {
      if (tableState[decorator] == undefined) {
        console.error('表格还未初始化')
      } else {
        tableState[decorator].fetchColumn()
      }
    } else {
      tableState.fetchColumn()
    }
  }
  let deleteData = (data, decorator) => {
    if (decorator) {
      if (tableState[decorator] == undefined) {
        console.error('表格还未初始化')
      } else {
        tableState[decorator].deleteData(data)
      }
    } else {
      tableState.deleteData(data)
    }
  }
  let toggleTable = decorator => {
    if (decorator) {
      if (tableState[decorator] == undefined) {
        console.error('表格还未初始化')
      } else {
        tableState[decorator].toggleTable()
      }
    } else {
      tableState.toggleTable()
    }
  }
  let hideTable = decorator => {
    if (decorator) {
      if (tableState[decorator] == undefined) {
        console.error('表格还未初始化')
      } else {
        tableState[decorator].hideTable()
      }
    } else {
      tableState.hideTable()
    }
  }
  let showTable = decorator => {
    if (decorator) {
      if (tableState[decorator] == undefined) {
        console.error('表格还未初始化')
      } else {
        tableState[decorator].showTable()
      }
    } else {
      tableState.showTable()
    }
  }
  let renderData = (data, hasPage, decorator) => {
    if (decorator) {
      if (tableState[decorator] == undefined) {
        console.error('表格还未初始化')
      } else {
        tableState[decorator].renderData(data, hasPage)
      }
    } else {
      tableState.renderData(data, hasPage)
    }
  }
  let renderColumns = (data, decorator) => {
    if (decorator) {
      if (tableState[decorator] == undefined) {
        console.error('表格还未初始化')
      } else {
        tableState[decorator].renderColumns(data)
      }
    } else {
      tableState.renderColumns(data)
    }
  }
  let reInit = (hasIndex, checkAllBox, selectRows, hasPage, sortBy, decorator) => {
    if (decorator) {
      if (tableState[decorator] == undefined) {
        console.error('表格还未初始化')
      } else {
        tableState[decorator].reInit(hasIndex, checkAllBox, selectRows, hasPage, sortBy)
      }
    } else {
      tableState.reInit(hasIndex, checkAllBox, selectRows, hasPage, sortBy)
    }
  }
  let getDataSource = decorator => {
    if (decorator) {
      if (tableState[decorator] == undefined) {
        console.error('表格还未初始化')
      } else {
        return tableState[decorator].settings().data
      }
    }

    return tableState.settings().data
  }
  let getData = decorator => {
    if (decorator) {
      if (tableState[decorator] == undefined) {
        console.error('表格还未初始化')
      } else {
        return tableState[decorator].state().data
      }
    }

    return tableState.state().data
  }
  let getSelectRowsData = decorator => {
    if (decorator) {
      if (tableState[decorator] == undefined) {
        console.error('表格还未初始化')
      } else {
        return tableState[decorator].getSelectRowsData()
      }
    }

    return tableState.getSelectRowsData()
  }
  let getSortSetting = decorator => {
    if (decorator) {
      if (tableState[decorator] == undefined) {
        console.error('表格还未初始化')
      } else {
        return tableState[decorator].getSortSetting()
      }
    }

    return tableState.getSortSetting()
  }
  let getSelectRowsDataSource = decorator => {
    if (decorator) {
      if (tableState[decorator] == undefined) {
        console.error('表格还未初始化')
      } else {
        return tableState[decorator].getSelectRowsDataSource()
      }
    }

    return tableState.getSelectRowsDataSource()
  }
  let getSourceColumns = decorator => {
    if (decorator) {
      if (tableState[decorator] == undefined) {
        console.error('表格还未初始化')
      } else {
        return tableState[decorator].getSourceColumns()
      }
    }

    return tableState.getSourceColumns()
  }
  let setData = (dataSource, data, decorator) => {
    if (decorator) {
      if (tableState[decorator] == undefined) {
        console.error('表格还未初始化')
      } else {
        return tableState[decorator].setData(dataSource, data)
      }
    }

    return tableState.setData(dataSource, data)
  }
  let isSelectAll = decorator => {
    if (decorator) {
      if (tableState[decorator] == undefined) {
        console.error('表格还未初始化')
      } else {
        return tableState[decorator].isSelectAll()
      }
    }

    return tableState.isSelectAll()
  }
  let unSelectAll = decorator => {
    if (decorator) {
      if (tableState[decorator] == undefined) {
        console.error('表格还未初始化')
      } else {
        return tableState[decorator].unSelectAll()
      }
    }

    return tableState.unSelectAll()
  }
  let selectAll = decorator => {
    if (decorator) {
      if (tableState[decorator] == undefined) {
        console.error('表格还未初始化')
      } else {
        return tableState[decorator].selectAll()
      }
    }

    return tableState.selectAll()
  }
  let disableSelect = decorator => {
    if (decorator) {
      if (tableState[decorator] == undefined) {
        console.error('表格还未初始化')
      } else {
        return tableState[decorator].disableSelect()
      }
    }

    return tableState.disableSelect()
  }
  let unDisableSelect = decorator => {
    if (decorator) {
      if (tableState[decorator] == undefined) {
        console.error('表格还未初始化')
      } else {
        return tableState[decorator].unDisableSelect()
      }
    }

    return tableState.unDisableSelect()
  }
  let reRender = decorator => {
    if (decorator) {
      if (tableState[decorator] == undefined) {
        console.error('表格还未初始化')
      } else {
        return tableState[decorator].reRender()
      }
    }

    return tableState.reRender()
  }
  let reSize = decorator => {
    if (decorator) {
      if (tableState[decorator] == undefined) {
        console.error('表格还未初始化')
      } else {
        return tableState[decorator].reSize()
      }
    }

    return tableState.reSize()
  }
  let setDataUrl = (url, urlConfig, decorator) => {
    if (decorator) {
      if (tableState[decorator] == undefined) {
        console.error('表格还未初始化')
      } else {
        return tableState[decorator].setDataUrl(url, urlConfig)
      }
    }

    return tableState.setDataUrl(url, urlConfig)
  }
  let setColumnList = (columns, decorator) => {
    if (decorator) {
      if (tableState[decorator] == undefined) {
        console.error('表格还未初始化')
      } else {
        return tableState[decorator].setColumnList(columns)
      }
    }

    return tableState.setColumnList(columns)
  }
  let setDataAtCell = (row, col, value, source, decorator) => {
    if (decorator) {
      if (tableState[decorator] == undefined) {
        console.error('表格还未初始化')
      } else {
        return tableState[decorator].setDataAtCell(row, col, value, source)
      }
    }

    return tableState.setDataAtCell(row, col, value, source)
  }
  let setDataAtRowProp = (row, props, value, source, decorator) => {
    if (decorator) {
      if (tableState[decorator] == undefined) {
        console.error('表格还未初始化')
      } else {
        return tableState[decorator].setDataAtRowProp(row, props, value, source)
      }
    }

    return tableState.setDataAtRowProp(row, props, value, source)
  }
  let updateSettings = (settings, init, decorator) => {
    if (decorator) {
      if (tableState[decorator] == undefined) {
        console.error('表格还未初始化')
      } else {
        return tableState[decorator].updateSettings(settings, init)
      }
    }

    return tableState.updateSettings(settings, init)
  }
  let clear = decorator => {
    if (decorator) {
      if (tableState[decorator] == undefined) {
        console.error('表格还未初始化')
      } else {
        return tableState[decorator].clear()
      }
    }

    return tableState.clear()
  }
  const table = {
    setTableRegister,
    fetchData,
    fetchColumn,
    deleteData,
    toggleTable,
    hideTable,
    showTable,
    renderData,
    renderColumns,
    reInit,
    getDataSource,
    getData,
    getSelectRowsData,
    getSelectRowsDataSource,
    setData,
    isSelectAll,
    unSelectAll,
    disableSelect,
    unDisableSelect,
    reRender,
    reSize,
    setDataAtCell,
    setDataAtRowProp,
    setDataUrl,
    setColumnList,
    getSourceColumns,
    getSortSetting,
    updateSettings,
    clear,
    selectAll
  }

  return class HotTable extends PureComponent {
    render() {
      return <Comp {...this.props} table={table} key={key || 'TableMul'}/>
    }
  }
}

export default YHTable

class LoadMore extends PureComponent {
  constructor() {
    super()
  }

  handleClick = () => {
    const {
      page,
      totalPage,
      onClick
    } = this.props

    if (page < totalPage) {
      onClick && onClick()
    }
  }

  render() {
    const {
      page,
      totalPage
    } = this.props

    let html = []

    if (page < totalPage) {
      html.push(<span key="1">{page} / {totalPage}</span>)
      html.push(<span key="2" className="load-txt">加载更多</span>)
    } else if (page >= totalPage) {
      html.push(<span key="1">{page} / {totalPage}</span>)
      html.push(<span key="2" className="load-txt">加载到底了</span>)
    }

    return (
      <a className="load-more" onClick={this.handleClick}>
        {html}
      </a>
    )
  }
}

class PureHotTable extends PureComponent {
  getApi = apis => {
    const { getApi } = this.props

    getApi(apis)
  }
  render() {
    // console.error(this.props)
    return (
      <HotTable
        ref={this.getApi}
        {...this.props}
      />
    )
  }
}
