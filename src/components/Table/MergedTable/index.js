/**
* Created by Jimmy on 2019-05-16
*/
import React from 'react'
import { Table, Checkbox } from 'antd'
import PropTypes from 'prop-types'

const checkBoxIndex = 'CHECK_BOX_INDEX' // 设置checkbox 在columns中的索引
const SINGLE_CHECKED = 'SINGLE_CHECKED' // record中记录checkbox checked的字段

function getDefaultRowSpanList(data) {
  let rowSpanList = []
  rowSpanList.length = data.length
  rowSpanList.fill(1)
  return rowSpanList
}
/**
 * 根据mergedKey, 获取colmuns的每行合并的情况
 * @param  {[Array]}  arr      [数据源]
 * @param  {[String]} mergedKey [需要合并的字段]
 * @param  {[Array]}  columns
 * @return {[Array]}
 */
function getRowSpanList(data, mergedKey, columns) {
  let rowSpanList = []
  if (columns.every(col => col.dataIndex !== mergedKey)) { // 如果columns中没有 mergeKey的属性
    return getDefaultRowSpanList(data)
  }
  const countedMerKey = data.reduce((res, cur) => {
    const mergedKeyValue = cur[mergedKey] || Math.random()
    res[mergedKeyValue] ? res[mergedKeyValue] += 1 : res[mergedKeyValue] = 1
    return res
  }, {})

  if (Object.keys(countedMerKey).length === 0) { // 如果data中没有 mergeKey的属性
    return getDefaultRowSpanList(data)
  }

  for (let key in countedMerKey) {
    for (let i = 0; i < countedMerKey[key]; i++) {
      rowSpanList.push(i === 0 ? countedMerKey[key] : 0)
    }
  }
  return rowSpanList
}

class MergedTable extends React.Component {
  constructor(props) {
    super(props)
    this.rowSpanList = this.getDefaultRowSpanList(props)
    this.state = {
      checkedAll: false,
      dataSource: this.initDataSource(props),
      columns: props.columns
    }
  }

  componentDidMount() {
    this.updateColumns(this.props)
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.dataSource !== this.props.dataSource) {
      this.setState({
        checkedAll: false,
        dataSource: this.initDataSource(nextProps)
      }, () => {
        this.rowSpanList = this.getDefaultRowSpanList(nextProps)
        this.updateColumns(nextProps)
      })
    }
  }

  getDefaultRowSpanList = ({ dataSource, mergedKey, columns }) => mergedKey ? getRowSpanList(dataSource, mergedKey, columns) : []

  /**
   * 每行数据添加 checkBox索引 & isChecked索引
   * @param  {Array}   [data=[]]          [description]
   * @param  {Boolean} [isSelected=false] [description]
   * @return {[type]}
   */
  initDataSource = (props) => {
    const { dataSource, rowSelection } = props
    if (!rowSelection) {
      return dataSource
    }
    const { selectedRowKeys = [] } = rowSelection
    return dataSource.map((item, index) => ({
      [SINGLE_CHECKED]: selectedRowKeys.indexOf(item.key) > -1 ? true : false,
      checkBoxIndex: index,
      // fixed: 'left', // TODO: fixed
      // width: 80,
      ...item
    }))
  }

  /**
   * 行 表格变动
   * @param  {[Object]} record [description]
   * @param  {[type]} index  [description]
   * @param  {[type]} e      [description]
   */
  onRowChange = (record, index, e) => {
    const selected = e.target.checked
    const { dataSource } = this.state
    const { rowSelection: { onChange, onSelect, onSelectAll }, mergedKey } = this.props
    if (mergedKey && record[mergedKey]) {
      const mergedKeyValue = record[mergedKey]
      dataSource.forEach(item => {
        if (item[mergedKey] === mergedKeyValue) {
          item[SINGLE_CHECKED] = selected
        }
      })
    } else {
      record[SINGLE_CHECKED] = selected
      dataSource[index] = record
    }
    let [selectedRowKeys, selectedRows] = this.getChangeList(dataSource, selected, record.key)
    onChange && onChange(selectedRowKeys, selectedRows)
    onSelect && onSelect(record, selected, selectedRows, e)
    const isCheckedAll = dataSource.every(item => item[SINGLE_CHECKED] === true)
    isCheckedAll && onSelectAll && onSelectAll(isCheckedAll, selectedRowKeys)
    this.setState({
      checkedAll: isCheckedAll,
      dataSource
    }, () => this.updateColumns(this.props))
  }

  /**
   * [表头变动时]
   * @param  {[type]} data [description]
   * @return {[type]}
   */
  getChangeList = (data, selected, key) => {
    let { rowSelection: { selectedRowKeys = [] } } = this.props
    // 去除 取消选中的key
    selectedRowKeys = selected ? selectedRowKeys : selectedRowKeys.filter(item => item !== key)
    let selectedRows = []
    // data.filter(item => selectedRowKeys.find(subItem => subItem === item.key))
    for (let i = 0; i < data.length; i++) {
      if (data[i][SINGLE_CHECKED] === true) {
        selectedRowKeys.push(data[i].key)
        selectedRows.push(data[i])
      }
    }
    let newSelectedRowKeys = [...new Set(selectedRowKeys)]
    return [newSelectedRowKeys, selectedRows]
  }

  /**
   * [表头全部选择]
   */
  onSelectAll = e => {
    const selected = e.target.checked
    const { dataSource } = this.state
    const { rowSelection: { onSelectAll, onChange } } = this.props
    dataSource.forEach(item => item[SINGLE_CHECKED] = selected)
    const [selectedRowKeys, selectedRows] = selected ? this.getChangeList(dataSource, selected) : [[], []]
    onSelectAll && onSelectAll(selected, selectedRows)
    onChange && onChange(selectedRowKeys, selectedRows)

    this.setState({
      checkedAll: selected,
      dataSource
    }, () => this.updateColumns(this.props))
  }

  updateColumns = (props) => {
    this.setState({
      columns: [].concat(this.renderColumns(props))
    })
  }

  /**
   * 合并 mergekey 这个字段
   * @param  {[Array]}  columns
   * @param  {[String]} mergedKey
   * @return {[Array]}
   */
  getMergedColumns = (columns, mergedKey) => {
    for (let i = 0; i < columns.length; i++) {
      if (columns[i].dataIndex === mergedKey) {
        columns[i].render = (text, record, index) => ({
          children: text,
          props: {
            rowSpan: this.rowSpanList[index]
          }
        })
        break
      }
    }
    return columns
  }

  renderColumns = (props) => {
    let { columns, mergedKey, rowSelection = {}, dataSource } = props
    if (mergedKey) {
      columns = this.getMergedColumns(columns, mergedKey)
    }

    if (!rowSelection) {
      return columns
    }

    const columnTitle = rowSelection.columnTitle

    const { checkedAll } = this.state
    const disabled = dataSource.length >= 1 ? false : true
    return [{
      className: 'ant-table-selection-column',
      title: <React.Fragment>
        <Checkbox disabled={disabled} checked={checkedAll} onChange={this.onSelectAll}/>{columnTitle && <span>{columnTitle}</span>}
      </React.Fragment>,
      width: columnTitle ? '102px' : '50px',
      dataIndex: checkBoxIndex, key: checkBoxIndex,
      render: (text, record, index) => {
        let obj = {
          children: <React.Fragment><Checkbox checked={record[SINGLE_CHECKED]} onChange={this.onRowChange.bind(this, record, index)}/>{columnTitle && <span>{columnTitle}</span>}</React.Fragment>,
          props: {
            rowSpan: this.rowSpanList[index] // TODO:  mergedKey 判断
          }
        }
        return obj
      } }].concat(columns)
  }

  render() {
    const { rowSelection, ...rest } = this.props
    const { columns, dataSource } = this.state
    // console.log(this)
    // rowSelection 设置为失效
    return (
      <Table
        rowSelection={rowSelection ? null : undefined}
        { ...rest}
        columns={columns}
        dataSource={dataSource}
      />
    )
  }
}

MergedTable.propTypes = {
  mergedKey: PropTypes.string
}

export default MergedTable
