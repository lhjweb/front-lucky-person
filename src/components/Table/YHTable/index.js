import React, { Component, PureComponent } from 'react'
import { Table } from 'antd'
import _ from 'lodash'
import http from '@http'
import { pageSizeOptions } from '@yhUtil'

export default class App extends PureComponent {
  state = {
    loading: false,
    page: 1,
    size: 10,
    dataSource: [],
    total: 0,
    selectedRowKeys: [],
    params: null // 请求参数
  }

  fetchData = (porps) => {
    const { page, size } = this.state
    const { method = 'post', url, params, config } = porps || this.props
    const finalParams = { ...params, page, size }
    this.setState({ loading: true })
    url && http[method](url, finalParams, config).then(res => {
      this.dealWithData(res)
    }, this.dealWithError)
  }

  dealWithData = res => {
    res && this.setState({
      dataSource: res.result || res,
      total: res.totalNum,
      loading: false
    })
  }

  dealWithError = err => {
    this.setState({ loading: false })
  }

  componentWillReceiveProps(nextProps) {
    if (this.props.params !== nextProps.params) {
      console.log(nextProps.params)
      this.setState({
        page: 1
      }, this.fetchData(nextProps))
    }
  }

  handleShowSizeChange = (current, size) => {
    this.setState({
      TableLoading: true,
      size,
      page: 1
    }, () => this.fetchData())
  }

  onPageChange = page => {
    console.log(page)
    this.setState({
      page
    }, () => this.fetchData())
  }

  render() {
    const { dataSource, loading, total, page } = this.state
    let { columns, rowSelection = null } = this.props
    columns.forEach(col => {
      col.dataIndex = col.dataIndex || col.key
    })
    const paginationConfig = {
      total,
      current: page,
      onChange: this.onPageChange, // 页码改变的回调，参数是改变后的页码及每页条数
      showQuickJumper: true, // 快速跳转
      showTotal: (total, range) => `共 ${total} 条`, // 显示总数
      pageSizeOptions,
      size: 'small',
      showSizeChanger: true, // 是否可以改变 pageSize
      onShowSizeChange: this.handleShowSizeChange // pageSize 变化的回调
    }
    const x = _.sumBy(columns, 'width')
    return (
      <div className="page-table">
        <Table
          columns={columns}
          dataSource={dataSource}
          rowKey={record => record.id || record.key}
          scroll={{ x }}
          rowSelection={rowSelection}
          loading={loading}
          pagination={paginationConfig}
        />
      </div>
    )
  }
}
