/*
 * Created by Zero on 2019/05/10
 */
import React from 'react'
import { Breadcrumb } from 'antd'
import './index.less'
import _ from 'lodash'
import { getVariant } from '@variant'

const initRoute = pathname => {
  try {
    let routeName = []
    // const path = pathname.slice(1, pathname.length)
    const menus = getVariant('luckyMenu')
    if (!menus) return routeName

    let routeNames = []
    findPathName(pathname, routeNames)(menus)
    // console.error(routeNames)
    return routeNames
    // loop: for (let i = 0; i < menus.length; i++) {
    //   if (menus[i].children) {
    //     const subItem = menus[i].children
    //     for (let j = 0; j < subItem.length; j++) {
    //       if (subItem[j].path === path) {
    //         routeName = [menus[i].name, subItem[j].name]
    //         break loop
    //       } else {
    //         if (subItem[j].children) {
    //           const thirdItem = subItem[j].children
    //           for (let k = 0; k < thirdItem.length; k++) {
    //             if (thirdItem[k].path === path) {
    //               routeName = [menus[i].name, subItem[j].name, thirdItem[k].name]
    //               break loop
    //             }
    //           }
    //         }
    //       }
    //     }
    //   }
    // }
    //
    // return routeName
  } catch (e) {
    console.error(e)
    return []
  }
}

/**
 * 遍历路径名称
 * @param path
 * @param routeNames
 * @returns {traverse|Array}
 */
const findPathName = (path, routeNames = []) => {
  if (!path) return routeNames
  const traverse = (menus = [], parent) => {
    try {
      for (let i in menus) {
        let item = menus[i]
        if (item.path === path) {
          return item
        } else if (item.children.length) {
          let subItem = traverse(item.children, item)
          if (subItem && subItem.name) {
            // 插入数组前
            routeNames.unshift(subItem.name)
            // 判断是否到一级树
            if (!parent) {
              routeNames.unshift(item.name)
              return routeNames
            }
            return item
          }
        }
      }
    } catch (e) {
      console.error(e)
      return routeNames
    }
  }
  return traverse
}

const CommonBread = (props) => {
  const { location, list, right } = props
  let realRoute = []
  if (_.isArray(list)) {
    realRoute = list
  } else {
    const routeName = initRoute(location.pathname)
    if (_.isArray(routeName)) {
      realRoute = routeName
    }
  }
  return (
    <div className={right ? 'comp_breadcrumb-box gap' : 'comp_breadcrumb-box'}>
      <Breadcrumb className="breadcrumb-box">
        { realRoute.map((item, index) => (<Breadcrumb.Item key={index}>{item}</Breadcrumb.Item>)) }
      </Breadcrumb>
      <div className="right-box">
        { right && right.map((item, i) => <div className="fr" key={`right-${i}`}>{item}</div>) }
      </div>
    </div>
  )
}
export default CommonBread
