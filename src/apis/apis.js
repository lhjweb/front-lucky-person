/**
 * Created by Jimmy on 2018/04/24
 *  商品中心
 * api config，格式为 [ host, path, option ]，host 取值为 [ 'fresh', 'pm', option ]
 *  option:  { hasExport: boolean——是否需要导出, configExportName: string——导出文件名、如果未定义并且无法获取响应头中的文件名、则默认为'导出文件' }
 *
 */

// 活动管理活动配置
const activityResets = {
  API_POST_GET_SETTING_QUERY_ACTIVITY: ['luckyday', '/setting/queryActivity'], // 活动配置查询列表
  API_POST_CONFIG_SUBMIT: ['luckyday', '/setting/confirmSubmit'], // 活动配置提交提交
  API_POST_SETTING_SUBMIT: ['luckyday', '/setting/cancelSubmit', { noWarn: true }], // 活动配置废弃
  API_POST_SETTING_EXCEL: ['luckyday', '/setting/excel'], // 活动配置导入
  API_GET_AUDIT_ACTIVITY: ['luckyday', '/setting/auditActivity'], // 活动配置-审核
  API_GET_DOWN_LOAD_TEMPLATE: ['luckyday', '/setting/downloadTemplate', { hasExport: true, configExportName: '活动配置下载导入模板' }], // 下载模板
  API_GET_QUERY_ACTNAME_CODES: ['luckyday', '/setting/control/queryActNameCodes'], // 活动参与统计

  API_POST_QUERY_ACTIVITY_INFO: ['luckyday', '/setting/queryActivityInfo'],  // 活动信息-查询
  API_POST_ADD_ACTIVITY_INFO: ['luckyday', '/setting/addActivityInfo'], // 活动信息-新增
  API_POST_UPDATE_ACTIVITY_INFO: ['luckyday', '/setting/updateActivityInfo'], // 活动信息-修改
  API_GET_DELETE_ACTIVITY_INFO: ['luckyday', '/setting/deleteActivityInfo'],  // 活动信息-删除

  API_POST_QUERY_SHOP: ['luckyday', '/setting/queryShop'], // 活动门店-查询
  API_POST_ADD_SHOP: ['luckyday', '/setting/addShop'], // 活动门店-新增
  API_POST_UPDATE_SHOP: ['luckyday', '/setting/updateShop'], // 活动门店-修改
  API_GET_DELETE_SHOP: ['luckyday', '/setting/deleteShop'], // 活动门店-删除

  AP_POST_QUERY_UN_SUBMIT_PRIZE: ['luckyday', '/setting/queryUnSubmitPrize'], // 奖项设置-查询
  API_POST_ADD_SUBMIT_PRIZE: ['luckyday', '/setting/addUnSubmitPrize'], // 奖项设置-新增
  API_POST_UPDATE_SUBMIT_PRIZE: ['luckyday', '/setting/updateUnSubmitPrize'], // 奖项设置-修改
  API_GET_DELETE_PRIZE: ['luckyday', '/setting/deletePrize'],  // 奖项设置-删除

  API_POST_QUERY_PRIZE_TIME: ['luckyday', '/setting/queryPrizeTime'], // 开奖时间-查询
  API_POST_ADD_PRIZE_TIME: ['luckyday', '/setting/addPrizeTime'], // 开奖时间-新增
  API_POST_UPDATE_PRIZE_TIME: ['luckyday', '/setting/updatePrizeTime'], // 开奖时间-修改
  API_GET_DELETE_PRIZE_TIME: ['luckyday', '/setting/deletePrizeTime'], // 开奖时间-删除

  API_POST_QUERY_SHOP_RULE: ['luckyday', '/setting/queryShopRule' ], // 门店奖项-查询
  API_POST_ADD_SHOP_RULE: ['luckyday', '/setting/addShopRule'], // 门店奖项-新增
  API_POST_UPDATE_SHOP_RULE: ['luckyday', '/setting/updateShopRule'], // 门店奖项-修改
  API_GET_DELETE_SHOP_RULE: ['luckyday', '/setting/deleteShopRule']// 门店奖项-删除
}
// 活动成本-活动成本统计
const activityCostStatistics = {
  API_POST_REPORT_QUERY_PRIZE_COST: ['luckyday', '/report/queryPrizeCost'], // 活动成本统计查询
  API_GET_SETTING_GET_PRIZE_SHOP_COST_DETAIL: ['luckyday', '/setting/getPrizeShopCostDetail'], // 成本统计 - 详情
  API_POST_QUERY_PRIZE: ['luckyday', '/setting/queryPrize'], // 奖品单价设置查询
  API_POST_UPDATE_PRIZE_COST: ['luckyday', '/setting/updatePrizeCost'], // 奖品单价设置编辑
  API_GET_QUERY_PRIZE_PRICE_HISTORY: ['luckyday', '/setting/queryPrizePriceHistory'] // 活动成本单价编辑历史记录
}
// 活动统计分析
const activityStatistics = {
  API_POST_QUERY_JOIN_REPORT: ['luckyday', '/report/queryJoinReport'], // 参与活动统计
  API_POST_QUERY_PRIZE_DETAIL: ['luckyday', '/report/queryPrizeDetail'], // 出奖明细查询
  API_POST_QUERY_ACCSS_ANALYSIS: ['luckyday', '/report/queryAccessAnalysis'] // 活动数据分析
}

// 活动素材
const activityMaterial = {
  API_POST_SETTING_QUERY_PRIZE_BY_CODE: ['luckyday', '/setting/queryPrizeByCode'], // 查询奖项
  API_POST_SETTING_UPLOAD_ZIP_IMG: ['luckyday', '/setting/uploadZipImg'], // 图片上传
  API_POST_SETTING_UPLOAD_IMG: ['luckyday', '/setting/uploadImg'], // 重新上传
  API_POST_SETTING_IMAGE_CANCEL_SUBMIT: ['luckyday', '/setting/image/cancelSubmit'], // 全部作废
  API_GET_SETTING_QUERY_ACTIVITY_IMG: ['luckyday', '/setting/queryActivityImg'], // 查询活动图片
  API_POST_SETTING_IMAGE_CONFIRM_SUBMIT: ['luckyday', '/setting/image/confirmSubmit'] // 全部应用
}

// 活动核销
const activitiesWriteOff = {
  API_POST_QUERY_REPORT_QUERY_GOD_RECORD: ['luckyday', '/report/queryGodRecord'], // 活动核销 - 查询
  API_GET_QUERY_REPORT_GET_GOD_RECORD_DETAIL: ['luckyday', '/report/getGodRecordDetail'], // 活动核销 - 详情
  API_POST_QUERY_REPORT_SETTLEMENT: ['luckyday', '/report/settlement'], // 活动核销 - 核销
  API_POST_EXPORT_LIST_GOD_RECORD: ['luckyday', '/report/exportListGodRecord', { hasExport: true, configExportName: '活动核销数据' }] // 活动核销-导出
}
// 奖项调整
const activityAwardAdjustment = {
  API_POST_QUERY_SHOP_RULE_DETAIL: ['luckyday', '/setting/queryShopRuleDetail'], // 查询
  API_POST_SHOP_RULE_APPLY: ['luckyday', '/setting/shopRuleApply'] // 应用
}
export default {
  ...activityResets,
  ...activityCostStatistics,
  ...activityStatistics,
  ...activityMaterial,
  ...activitiesWriteOff,
  ...activityAwardAdjustment
}
