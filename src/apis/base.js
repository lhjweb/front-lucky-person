/**
 * Created by Jimmy on 2018/04/24
 *
 * api config，格式为 [ host, path, option ]，host 取值为 [ 'fresh', 'pm', option ]
 *  option:  { hasExport: boolean——是否需要导出, configExportName: string——导出文件名、如果未定义并且无法获取响应头中的文件名、则默认为'导出文件' }
 *
 */
export default {
  API_GET_USERINFO_BYTOKEN: ['usercenter', '/userInfo/byToken'], // 根据token 拿用户信息
  API_GET_ACC: ['usercenter', '/acc'], // 单点登录
  API_LOGOUT: ['usercenter', '/logout'], // 登出
  API_GET_ROLES_ALL: ['usercenter', '/admin/roles/all'], // 用户角色下拉
  API_GET_USER_ADDRESS_ALL: ['usercenter', '/user/address/all'], // 行政区域
  API_GET_USER_EXTEND_CONFIG: ['usercenter', '/admin/userPropertyExtend/config/list'], // 自定义配置
  API_GET_LOGIN: ['usercenter', '/login', { noWarn: true }], // 登录
  API_POST_USER_CENTER_SMS_CODE: ['usercenter', '/sms/smsCode'], // 找回密码获取验证码
  API_POST_USER_CENTER_CHECK_CODE: ['usercenter', '/sms/smsCode'], // 找回密码获取验证码
  API_POST_USER_CENTER_GET_BACK: ['usercenter', '/usersInfo/password/getBack '], // 找回密重置密码
  API_GET_OPERATION_OBJECTIVES: [ 'usercenter', '/operationObjectives' ], // 菜单栏
  API_GET_MENU_BUTTON_PERMISSSION_INFO: ['usercenter', '/user/menuPermissionInfo'], // 按钮级别权限
  API_GET_SETTING_CONTROL_QUERY_ACT_NAME_CODES: ['luckyday', '/setting/control/queryActNameCodes'], // 活动名称模糊搜索
  API_POST_SETTING_QUERY_SHOP: ['luckyday', '/setting/queryShop'] // 门店编码模糊搜索
}
