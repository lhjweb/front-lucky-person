/**
 * Created by Jimmy on 2018/04/24
 *
 * api config，格式为 [ host, path, option ]，host 取值为 [ 'fresh', 'pm', option ]
 *  option:  { hasExport: boolean——是否需要导出, configExportName: string——导出文件名、如果未定义并且无法获取响应头中的文件名、则默认为'导出文件' }
 *
 */
import base from './base'
import apis from './apis'
module.exports = {
  ...base, // 基础
  ...apis, // 接口地址
}
